#!/usr/bin/env python
from setuptools import setup, find_packages, Extension  # This setup relies on setuptools since distutils is insufficient and badly hacked code
import numpy as np
from distutils.command.build_ext import build_ext
from Cython.Build import cythonize
import os

version = '0.1'


author = 'Nikolaus Owtscharenko'
author_email = 'owtscharenko@physik.uni-bonn.de'

# requirements for core functionality from requirements.txt
with open('./info/py_requirements.txt') as f:
    install_requires = f.read().splitlines()

setup(
    name='ship_pixel_tracking',
    version=version,
    description='Analysis of SHiP Charm Testbeam',
    url='https://gitlab.cern.ch/nowtscha/ship-pixel-tracking',
    license='BSD 3-Clause ("BSD New" or "BSD Simplified") License',
    long_description='Tools for analysis and processing of pixel data in SHiP Charm testbeam, converts from root to h5 and back.',
    author=author,
    maintainer=author,
    author_email=author_email,
    maintainer_email=author_email,
    install_requires=install_requires,
    packages=find_packages(),  # exclude=['*.tests', '*.test']),
    include_package_data=True,  # accept all data files and directories matched by MANIFEST.in or found in source control
    package_data={'': ['README.*', 'VERSION'], 'docs': ['*'], 'examples': ['*']},
    include_dirs=[np.get_include()],
    keywords=['testbeam', 'particle', 'reconstruction', 'pixel', 'detector'],
    platforms='any',
)
