#!/bin/bash

Spill=$1
Run=$2
Events=1000000

source /etc/profile
cd /jwd
mkdir unter
cd unter
tar -xvf /cephfs/user/$USER/tar/tbfp.tar -C /jwd/unter

module load root/6.14.06
module load rave/0.6.25
rootcling -f aDict.cxx  -c  a.h LinkDef.h
g++ -o libMylib.so aDict.cxx `root-config --cflags --libs` -shared -fPIC
root -b -l -q "runTestBeamFullPix.C($Run, $Events, $Spill, 0, 111)"

mv pix_tracks.root $BUDDY/data/run_$Run/pix_tracks_$Spill.root
