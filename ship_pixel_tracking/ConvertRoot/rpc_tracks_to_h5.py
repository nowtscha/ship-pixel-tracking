import numpy as np
import uproot as up
import re
import os
import tables as tb
import logging

from numba import njit
from tqdm import tqdm

from ship_pixel_tracking.tools.analysis_utils import save_table, get_files

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def algin_hits_to_track(tracks_array, hits_array):
    '''
        input array has only one coordinate (x OR y) per row, for easier analysis create an array with all fitted track positions in one row
    '''

    logging.info("align hits for RPCTracks array")
    pbar = tqdm(total=hits_array.shape[0])
    i = 0
    j = 0
    while j < hits_array.shape[0] and (i < tracks_array.shape[0]):
        tracks_array[i]["event_number"] = hits_array[j]["event_number"]
        tracks_array[i]["timestamp"] = hits_array[j]["timestamp"]
        tracks_array[i]["nclusters"] = hits_array[j]["nclusters"]
        tracks_array[i]["slopexz"] = hits_array[j]["slopexz"]
        tracks_array[i]["slopeyz"] = hits_array[j]["slopeyz"]
        tracks_array[i]["trackID"] = hits_array[j]["trackID"]
        for plane in range(1, 6):
            if j >= hits_array.shape[0]-1:
                return
            if (hits_array[j]["rpc_plane"] == plane) and (hits_array[j]["cluster_direction"] == 0):
                tracks_array[i]["y%s" % plane] = hits_array[j]["y"]
                tracks_array[i]["z%sy" % plane] = hits_array[j]["z"]
                j += 1
                pbar.update(1)
            if (hits_array[j]["rpc_plane"] == plane) and (hits_array[j]["cluster_direction"] == 1):
                tracks_array[i]["x%s" % plane] = hits_array[j]["x"]
                tracks_array[i]["z%sx" % plane] = hits_array[j]["z"]
                j += 1
                pbar.update(1)
            if j >= hits_array.shape[0]-1:
                return
        if (hits_array["event_number"][j] != hits_array["event_number"][j-1]) or (hits_array[j]["trackID"] != hits_array[j-1]["trackID"]):
            i += 1


class RPC_converter():

    def __init__(self, in_file):
        self.in_file = in_file
        self.hits_out_file = in_file.split(".")[0] + "_hits.h5"
        self.tracks_out_file = in_file.split(".")[0] + "_tracks.h5"

    def load_rpc_track_file(self):
        '''
            Loads root n-tuple file with RPC tracks and creates pytables file with one hit per row
        '''

        logging.info("loading root file %s" % self.in_file)
        file = up.open(self.in_file)
        tree = file["RPC_RecoTracks"]
        self.arrays = tree.arrays(['trigger', 'nclusters', 'timestamp', 'id_track', 'cl_x', 'cl_y', 'cl_z', 'cl_xfit',
                                   'cl_yfit', 'cl_zfit', 'trk_slopexz', 'trk_slopeyz', 'cl_rpc', 'cl_dir', ], namedecode="utf-8")

        total_size = tree.array("cl_x", flatten=True).shape[0]
        self.n_events = tree.array("trigger", flatten=True).shape[0]
        self.hits_array = np.full(fill_value=np.nan,
                                  dtype=[("event_number", np.int64),
                                         ("timestamp", np.int64),
                                         ("nclusters", np.int32),
                                         ("trackID", np.int32),
                                         ("cluster_x", np.float32),
                                         ("cluster_y", np.float32),
                                         ("cluster_z", np.float32),
                                         ("x", np.float32),
                                         ("y", np.float32),
                                         ("z", np.float32),
                                         ("slopexz", np.float32),
                                         ("slopeyz", np.float32),
                                         ("rpc_plane", np.int32),
                                         ("cluster_direction", np.bool)],
                                  shape=(total_size,))

    def create_rpc_tracks_array(self, save_raw_data=False):

        logging.info('create Hits array')
        # loop over nested uproot array to create numpy row based array
        nclusters, timestamp, slopexz, slopeyz, hitx, hity, hitz, fitx, fity, fitz, trackID, rpc_plane, cluster_dir, events = [], [], [], [], [], [], [], [], [], [], [], [], [], []

        for event in tqdm(range(self.n_events)):
            eventsize = self.arrays["cl_x"][event].shape[0]
            hitx.extend(self.arrays["cl_x"][event])
            hity.extend(self.arrays["cl_y"][event])
            hitz.extend(self.arrays["cl_z"][event])
            fitx.extend(self.arrays["cl_xfit"][event])
            fity.extend(self.arrays["cl_yfit"][event])
            fitz.extend(self.arrays["cl_zfit"][event])
            trackID.extend([self.arrays["id_track"][event]]*eventsize)
            timestamp.extend([self.arrays["timestamp"][event]]*eventsize)
            nclusters.extend([self.arrays["nclusters"][event]]*eventsize)
            rpc_plane.extend(self.arrays["cl_rpc"][event])
            cluster_dir.extend(self.arrays["cl_dir"][event])
            events.extend([self.arrays["trigger"][event]]*eventsize)
            slopexz.extend([self.arrays["trk_slopexz"][event]]*eventsize)
            slopeyz.extend([self.arrays["trk_slopeyz"][event]]*eventsize)

        logging.info("write RPCHits array")
        self.hits_array["cluster_x"] = hitx
        self.hits_array["cluster_y"] = hity
        self.hits_array["cluster_z"] = hitz
        self.hits_array["x"] = fitx
        self.hits_array["y"] = fity
        self.hits_array["z"] = fitz
        self.hits_array["trackID"] = trackID
        self.hits_array["slopexz"] = slopexz
        self.hits_array["slopeyz"] = slopeyz
        self.hits_array["nclusters"] = nclusters
        self.hits_array["rpc_plane"] = rpc_plane
        self.hits_array["cluster_direction"] = cluster_dir
        self.hits_array["event_number"] = events
        self.hits_array["timestamp"] = timestamp

        if save_raw_data:
            save_table(array_in = self.hits_array, path = self.hits_out_file, node_name = "RPCHits")
            logging.info("saved hits raw data to %s" % self.hits_out_file)

        self.tracks_array = np.full(fill_value=np.nan,
                                    dtype=[("event_number", np.int64), ("timestamp", np.int64), ("nclusters", np.int32), ("trackID", np.int32),
                                           ("x1", np.float32), ("y1", np.float32), ("z1x", np.float32), ("z1y", np.float32),
                                           ("x2", np.float32), ("y2", np.float32), ("z2x", np.float32), ("z2y", np.float32),
                                           ("x3", np.float32), ("y3", np.float32), ("z3x", np.float32), ("z3y", np.float32),
                                           ("x4", np.float32), ("y4", np.float32), ("z4x", np.float32), ("z4y", np.float32),
                                           ("x5", np.float32), ("y5", np.float32), ("z5x", np.float32), ("z5y", np.float32),
                                           ("slopexz", np.float32),
                                           ("slopeyz", np.float32), ],
                                    shape=(self.n_events,))

        # fill new array with all hits in one row
        algin_hits_to_track(tracks_array=self.tracks_array, hits_array=self.hits_array)

        save_table(array_in = self.tracks_array, path = self.tracks_out_file, node_name = "RPCTracks")
        logging.info("saved h5 file at %s" % self.tracks_out_file)


if __name__ == '__main__':
    # spills = np.loadtxt("/media/niko/big_data/charm_testbeam_july18/rpc_data/run_2836/run_2836/spill_ids.txt", skiprows=1, usecols=0, dtype="str")
    rpc_files = get_files("/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/run_2815/run_2815", ".root")
    for file in rpc_files:
        rpc_converter = RPC_converter(in_file=file)
        rpc_converter.load_rpc_track_file()
        rpc_converter.create_rpc_tracks_array(save_raw_data=False)
