import numpy as np
import uproot as up
import re
import os
import sys
import tables as tb
import logging

from tqdm import tqdm, trange
from numba import jit, njit

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


@njit
def read_hits(read_index, chunk_size, nevents, arrays):
    hitsx, hitsy, hitsz, cluster_charge, event_number, spill, timestamp, trackID, hitplane, hitused = [], [], [], [], [], [], [], [], [], []
    x, y, z, track_timestamp, track_eventnumber, track_trackID, xerr, yerr, tx, ty, txerr, tyerr, cov, trackspill = [], [], [], [], [], [], [], [], [], [], [], [], [], []
    for index in range(read_index, read_index + chunk_size):
        # for last chunk make sure only valid indices are used
        if index >= nevents:
            break
        eventsize = arrays["hitx"][index].shape[0]
        hitsx.extend(arrays["hitx"][index])
        hitsy.extend(arrays["hity"][index])
        hitsz.extend(arrays["hitz"][index])
        cluster_charge.extend(arrays["clusterCharge"][index])
        trackID.extend(arrays["trackID"][index])
        hitplane.extend(arrays["hitplane"][index])
        hitused.extend(arrays["hitused"][index])

        n_hit_shares += arrays["hitused"][index][arrays["hitused"][index] > 1].shape[0]

        event_number.extend([arrays["evnum"][index]]*eventsize)
        spill.extend([arrays["pspill"][index]]*eventsize)
        timestamp.extend([arrays["timestamp"][index]]*eventsize)
        # check for tracks in event
        ntracks = arrays["x"][index].shape[0]
        if ntracks:
            x.extend(arrays["x"][index])
            y.extend(arrays["y"][index])
            z.extend(arrays["z"][index])
            tx.extend(arrays["tx"][index])
            ty.extend(arrays["ty"][index])
            txerr.extend(arrays["etx"][index])
            tyerr.extend(arrays["ety"][index])
            xerr.extend(arrays["ex"][index])
            yerr.extend(arrays["ey"][index])
            cov.extend(arrays["chi2"][index])

            trackspill.extend([arrays["pspill"][index]]*ntracks)
            track_eventnumber.extend([arrays["evnum"][index]]*ntracks)
            track_timestamp.extend([arrays["timestamp"][index]]*ntracks)
            for track in range(ntracks):
                track_trackID.append(track)


def open_pixel_tracks_root(in_file, chunk_size=100000, read_events=None):

    tracks_dtype = np.dtype([("event_number", np.int64),
                             ("timestamp", np.int64),
                             ("spill", np.int32),
                             ("trackID", np.int16),
                             ("globalTrackID", np.int64),
                             ("x", np.float64),
                             ("y", np.float64),
                             ("z", np.float64),
                             ("x_error", np.float64),
                             ("y_error", np.float64),
                             ("x_slope", np.float64),
                             ("y_slope", np.float64),
                             ("x_slope_error", np.float64),
                             ("y_slope_error", np.float64),
                             ("n_hits", np.int32),
                             ("n_tracks_evt", np.int32),
                             ("red_chi2", np.float64),
                             ("x_y_error", np.float64),
                             ("x_tx_error", np.float64),
                             ("x_ty_error", np.float64),
                             ("y_tx_error", np.float64),
                             ("y_ty_error", np.float64),
                             ("tx_ty_error", np.float64),
                             ("x_update", np.float64),
                             ("y_update", np.float64),
                             ("z_update", np.float64),
                             ("x_slope_update", np.float64),
                             ("y_slope_update", np.float64),
                             ("x_update_error", np.float64),
                             ("y_update_error", np.float64),
                             ("z_update_error", np.float64),
                             ("x_slope_update_error", np.float64),
                             ("y_slope_update_error", np.float64)]
                             )

    hits_dtype = np.dtype([("event_number", np.int64),
                           ("timestamp", np.int64),
                           ("spill", np.int32),
                           ("trackID", np.int16),
                           ("globalTrackID", np.int64),
                           ("hitx", np.float64),
                           ("hity", np.float64),
                           ("hitz", np.float64),
                           ("hit_ex", np.float64),
                           ("hit_ey", np.float64),
                           ("cluster_size", np.int32),
                           ("cluster_charge", np.float32),
                           ("hitplane", np.uint8),
                           ("hitused", np.uint8)])

    logging.info("loading root file %s" % in_file)
    file = up.open(in_file)

    tree = file["p"]

    arrays = tree.arrays(['evnum', 'timestamp', 'pspill', 'hitx', 'hity', 'hit_ex', 'hit_ey', 'hitz', 'clusterCharge', 'hitplane', 'hitused', 'clusterSize',
                          'x', 'trackID', 'globalTrackID', 'hitGlobalTrackID', 'y', 'z', 'ex', 'ey', 'tx', 'ty', 'etx', 'ety', 'ex_y','ey_tx','ey_ty','ex_tx','ex_ty','etx_ty', 'chi2', 'nHits', 'xUpdate', 'yUpdate', 'zUpdate', 'txUpdate', 'tyUpdate', 'exUpdate', 'eyUpdate', 'etxUpdate', 'etyUpdate'], library="np")
    nevents = arrays["evnum"].shape[0]
    logging.info("found %s events" % nevents)
    if read_events:
        nevents = read_events
        logging.info("reading first %s events" % nevents)

    # tracks_file_name = in_file.split(".")[0] + "_tracks.h5"
    tracks_file_name = in_file.replace(".root", ".h5")
    hits_file_name = in_file.replace("tracks", "hits").replace(".root", ".h5")
    # hits_file_name = in_file.split(".")[0] + "_hits.h5"

    with tb.open_file(tracks_file_name, "w") as out_file_tracks_h5:
        track_table_out = out_file_tracks_h5.create_table(where=out_file_tracks_h5.root,
                                                          name='Tracks',
                                                          description=tracks_dtype, title='SHiP Charm Tracks',
                                                          filters=tb.Filters(
                                                              complib='blosc',
                                                              complevel=5,
                                                              fletcher32=False))

        with tb.open_file(hits_file_name, "w") as out_file_hits_h5:
            hit_table_out = out_file_hits_h5.create_table(where=out_file_hits_h5.root,
                                                          name='Hits',
                                                          description=hits_dtype, title='Hits to Charm Tracks',
                                                          filters=tb.Filters(
                                                              complib='blosc',
                                                              complevel=5,
                                                              fletcher32=False))

            n_hit_shares = 0
            for read_index in range(0, nevents, chunk_size):

                hitsx, hitsy,hits_ex, hits_ey, hitsz, cluster_charge, event_number, spill, timestamp, trackID, globTrID, hitplane, hitused, cluster_size = [], [], [], [], [], [], [], [], [], [], [], [], [], []
                x, y, z, track_timestamp, track_eventnumber, track_trackID, track_glob_trackID, xerr, yerr, tx, ty, txerr, tyerr, ex_y, ey_tx, ey_ty, ex_tx, ex_ty, etx_ty ,cov, n_hits, n_tracks, trackspill = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []
                xupdate, yupdate, zupdate, txupdate, tyupdate, exupdate, eyupdate, etxupdate, etyupdate = [],[],[],[],[],[],[],[],[]
                for index in trange(read_index, read_index + chunk_size):
                    # for last chunk make sure only valid indices are used
                    if index >= nevents:
                        break
                    eventsize = arrays["hitx"][index].shape[0]
                    hitsx.extend(arrays["hitx"][index])
                    hitsy.extend(arrays["hity"][index])
                    hits_ex.extend(arrays["hit_ex"][index])
                    hits_ey.extend(arrays["hit_ey"][index])
                    hitsz.extend(arrays["hitz"][index])
                    cluster_charge.extend(arrays["clusterCharge"][index])
                    cluster_size.extend(arrays["clusterSize"][index])
                    trackID.extend(arrays["trackID"][index])
                    globTrID.extend(arrays["hitGlobalTrackID"][index])
                    hitplane.extend(arrays["hitplane"][index])
                    hitused.extend(arrays["hitused"][index])

                    n_hit_shares += arrays["hitused"][index][arrays["hitused"][index] > 1].shape[0]

                    event_number.extend([arrays["evnum"][index]]*eventsize)
                    spill.extend([arrays["pspill"][index]]*eventsize)
                    timestamp.extend([arrays["timestamp"][index]]*eventsize)
                    # check for tracks in event
                    ntracks = arrays["x"][index].shape[0]
                    if ntracks:
                        x.extend(arrays["x"][index])
                        y.extend(arrays["y"][index])
                        z.extend(arrays["z"][index])
                        tx.extend(arrays["tx"][index])
                        ty.extend(arrays["ty"][index])
                        txerr.extend(arrays["etx"][index])
                        tyerr.extend(arrays["ety"][index])
                        xerr.extend(arrays["ex"][index])
                        yerr.extend(arrays["ey"][index])

                        xupdate.extend(arrays["xUpdate"][index])
                        yupdate.extend(arrays["yUpdate"][index])
                        zupdate.extend(arrays["zUpdate"][index])
                        txupdate.extend(arrays["txUpdate"][index])
                        tyupdate.extend(arrays["tyUpdate"][index])
                        exupdate.extend(arrays["exUpdate"][index])
                        eyupdate.extend(arrays["eyUpdate"][index])
                        etxupdate.extend(arrays["etxUpdate"][index])
                        etyupdate.extend(arrays["etyUpdate"][index])

                        ex_y.extend(arrays["ex_y"][index])
                        ey_tx.extend(arrays["ey_tx"][index])
                        ey_ty.extend(arrays["ey_ty"][index])
                        ex_tx.extend(arrays["ex_tx"][index])
                        ex_ty.extend(arrays["ex_ty"][index])
                        etx_ty.extend(arrays["etx_ty"][index])
                        cov.extend(arrays["chi2"][index])
                        n_hits.extend(arrays["nHits"][index])
                        track_glob_trackID.extend(arrays["globalTrackID"][index])
                        
                        n_tracks.extend([ntracks]*ntracks)
                        trackspill.extend([arrays["pspill"][index]]*ntracks)
                        track_eventnumber.extend([arrays["evnum"][index]]*ntracks)
                        track_timestamp.extend([arrays["timestamp"][index]]*ntracks)
                        for track in range(ntracks):
                            track_trackID.append(track)
                        

                tracks_array = np.full(fill_value=np.nan,
                                       dtype=tracks_dtype,
                                       shape=(len(track_eventnumber),))

                hits_array = np.full(fill_value=np.nan,
                                     dtype=hits_dtype,
                                     shape=(len(event_number),))

                logging.info("write numpy hit array")
                hits_array["event_number"] = event_number
                hits_array["hitx"] = hitsx
                hits_array["hity"] = hitsy
                hits_array["hitz"] = hitsz
                hits_array["hit_ex"] = hits_ex
                hits_array["hit_ey"] = hits_ey
                hits_array["cluster_charge"] = cluster_charge
                hits_array["trackID"] = trackID
                hits_array["globalTrackID"] = globTrID
                hits_array["hitplane"] = hitplane
                hits_array["hitused"] = hitused
                hits_array["timestamp"] = timestamp
                hits_array["cluster_size"] = cluster_size
                hits_array["spill"] = spill
                hits_array = np.sort(hits_array, order=["event_number", "trackID"])

                max_hit_shares = hits_array["hitused"].max()-1
                if max_hit_shares > 0:
                    logging.info("highest number of hit shares = %s" % max_hit_shares)
                logging.info("number of hits which were shared: %s" % n_hit_shares)
                logging.info("write numpy track array")

                tracks_array["event_number"] = track_eventnumber
                tracks_array["spill"] = trackspill
                tracks_array["timestamp"] = track_timestamp
                tracks_array["x"] = x
                tracks_array["y"] = y
                tracks_array["z"] = z
                tracks_array["x_error"] = xerr
                tracks_array["y_error"] = yerr
                tracks_array["x_slope"] = tx
                tracks_array["y_slope"] = ty
                tracks_array["x_slope_error"] = txerr
                tracks_array["y_slope_error"] = tyerr
                tracks_array["x_y_error"] = ex_y
                tracks_array["x_tx_error"] = ex_tx
                tracks_array["x_ty_error"] = ex_ty
                tracks_array["y_tx_error"] = ey_tx
                tracks_array["y_ty_error"] = ey_ty
                tracks_array["tx_ty_error"] = etx_ty

                tracks_array["x_update"] = xupdate
                tracks_array["y_update"] = yupdate
                tracks_array["z_update"] = zupdate
                tracks_array["x_slope_update"] = txupdate
                tracks_array["y_slope_update"] = tyupdate
                tracks_array["x_update_error"] = exupdate
                tracks_array["y_update_error"] = eyupdate
                tracks_array["x_slope_update_error"] = etxupdate
                tracks_array["y_slope_update_error"] = etyupdate
                
                tracks_array["red_chi2"] = cov
                tracks_array["n_hits"] = n_hits
                tracks_array["n_tracks_evt"] = n_tracks
                tracks_array["trackID"] = track_trackID
                tracks_array["globalTrackID"] = track_glob_trackID

                track_table_out.append(tracks_array)
                track_table_out.flush()

                hit_table_out.append(hits_array)
                hit_table_out.flush()

    logging.info('saved tracks file at %s' % tracks_file_name)
    logging.info("saved hits file at %s" % hits_file_name)

    return tracks_file_name, hits_file_name


def open_pixel_2TrackVertices_root(in_file, out_file_name = None, chunk_size=100000, read_events=None):

    vertex_dtype = np.dtype([("event_number", np.int64),
                             ("timestamp", np.int64),
                             ("spill", np.int32),
                             ("x", np.float64),
                             ("y", np.float64),
                             ("z", np.float64),
                             ("x_error", np.float64),
                             ("y_error", np.float64),
                             ("z_error", np.float64),
                             ("distance",np.float64),
                             ("trackID", np.int16),
                             ("vertexID", np.int16),
                             ("vertex_chi2", np.float64)])

    logging.info("loading root file %s" % in_file)
    file = up.open(in_file)

    tree = file["p"]

    arrays = tree.arrays(['evnum', 'timestamp', 'pspill', 'vx', 'vy', 'vz', 'evx', 'evy', 'evz','vdist', 'vitrk', 'vchi2'], library="np")
    nevents = arrays["evnum"].shape[0]
    logging.info("found %s events" % nevents)
    if read_events:
        nevents = read_events
        logging.info("reading first %s events" % nevents)
    if not out_file_name:
        out_file_name = in_file.replace("tracks", "2track_vertices").replace(".root", ".h5")

    with tb.open_file(out_file_name, "w") as out_file_vtx_h5:
        vtx_table_out = out_file_vtx_h5.create_table(where=out_file_vtx_h5.root,
                                                          name='Vertices',
                                                          description=vertex_dtype, title='SHiP Charm vertices',
                                                          filters=tb.Filters(
                                                              complib='blosc',
                                                              complevel=5,
                                                              fletcher32=False))
        
        if nevents< chunk_size:
            chunk_size = nevents
        vertexID = 0
        for read_index in range(0, nevents, chunk_size):
            
            vtx_event_number, vtx_spill, vtx_timestamp = [], [], []
            vx, vy, vz, evx, evy, evz = [], [], [], [], [], []
            vtx_trackIDs, vdist, vID, vchi2 = [], [], [], []
            
            for index in trange(read_index, read_index + chunk_size):
                # for last chunk make sure only valid indices are used
                if index >= nevents:
                    break
                # check for tracks in event
                n_vtx = arrays["vx"][index].shape[0]
                for vtx in range(n_vtx):
                    vertexID +=1
                    vtx_trackIDs.extend(arrays["vitrk"][index][vtx])
                    n_tracks_in_vtx = len(arrays["vitrk"][index][vtx])
                    vx.extend(np.repeat(arrays["vx"][index][vtx],n_tracks_in_vtx))
                    vy.extend(np.repeat(arrays["vy"][index][vtx],n_tracks_in_vtx))
                    vz.extend(np.repeat(arrays["vz"][index][vtx],n_tracks_in_vtx))
                    evx.extend(np.repeat(arrays["evx"][index][vtx],n_tracks_in_vtx))
                    evy.extend(np.repeat(arrays["evy"][index][vtx],n_tracks_in_vtx))
                    evz.extend(np.repeat(arrays["evz"][index][vtx],n_tracks_in_vtx))
                    vchi2.extend(np.repeat(arrays["vchi2"][index][vtx],n_tracks_in_vtx))

                    vtx_spill.extend([arrays["pspill"][index]]*n_tracks_in_vtx)
                    vtx_event_number.extend([arrays["evnum"][index]]*n_tracks_in_vtx)
                    vtx_timestamp.extend([arrays["timestamp"][index]]*n_tracks_in_vtx)

                    vdist.extend(np.repeat(arrays["vdist"][index][vtx],n_tracks_in_vtx))
                    vID.extend([vertexID] * n_tracks_in_vtx)
                    
                
                # for track in range(ntracks):
                #     track_trackID.append(track)

            vtx_array = np.full(fill_value=np.nan,
                                    dtype=vertex_dtype,
                                    shape=(len(vtx_event_number),))


            logging.info("write numpy vertex array")

            vtx_array["event_number"] = vtx_event_number
            vtx_array["spill"] = vtx_spill
            vtx_array["timestamp"] = vtx_timestamp
            vtx_array["x"] = vx
            vtx_array["y"] = vy
            vtx_array["z"] = vz
            vtx_array["x_error"] = evx
            vtx_array["y_error"] = evy
            vtx_array["z_error"] = evz

            vtx_array["distance"] = vdist
            vtx_array["vertexID"] = vID
            vtx_array["trackID"] = vtx_trackIDs
            vtx_array["vertex_chi2"] = vchi2

            vtx_table_out.append(vtx_array)
            vtx_table_out.flush()

    logging.info('saved vertex file at %s' % out_file_name)

    return out_file_name


def open_pixel_vertices_root(in_file, out_file_name = None, chunk_size=100000, read_events=None):
    vertex_dtype = np.dtype([("event_number", np.int64),
                             ("timestamp", np.int64),
                             ("spill", np.int32),
                             ("x", np.float64),
                             ("y", np.float64),
                             ("z", np.float64),
                             ("distance",np.float64),
                             ("trackID", np.int16),
                             ("vertexID", np.int64),
                             ("vertex_chi2", np.float64),
                             ("vertex_p", np.float64),
                             ("vertex_chi2_red", np.float64),
                             ("vertex_cov_XX", np.float64),
                             ("vertex_cov_YY", np.float64),
                             ("vertex_cov_ZZ", np.float64),
                             ("vertex_cov_XY", np.float64),
                             ("vertex_cov_YZ", np.float64),
                             ("vertex_cov_ZX", np.float64),
                             ("ndf", np.int16),
                             ("nvtx_in_evt", np.int16),
                             ("refined", bool),
                             ("nTracks", np.int16)])

    logging.info("loading root file %s" % in_file)
    file = up.open(in_file)

    tree = file["p"]

    arrays = tree.arrays(['evnum', 'timestamp', 'pspill', 'multivx', 'multivy', 'multivz', 'multiVdist', 'multiVtrackID', 'multiVchi2', "redMultiVchi2", "multiVndf", "multiVcovXX", "multiVcovYY", "multiVcovZZ", "multiVcovXY", "multiVcovYZ", "multiVcovZX", "nVtxinEvent", "refined", "multiVpval"], library="np")
    nevents = arrays["evnum"].shape[0]
    logging.info("found %s events" % nevents)
    if read_events:
        nevents = read_events
        logging.info("reading first %s events" % nevents)
    if not out_file_name:
        out_file_name = in_file.replace("tracks", "vertices").replace(".root", ".h5")

    with tb.open_file(out_file_name, "w") as out_file_vtx_h5:
        vtx_table_out = out_file_vtx_h5.create_table(where=out_file_vtx_h5.root,
                                                          name='Vertices',
                                                          description=vertex_dtype, title='SHiP Charm vertices',
                                                          filters=tb.Filters(
                                                              complib='blosc',
                                                              complevel=5,
                                                              fletcher32=False))
        
        if nevents< chunk_size:
            chunk_size = nevents
        vertexID = 0
        for read_index in range(0, nevents, chunk_size):
            
            vtx_event_number, vtx_spill, vtx_timestamp = [], [], []
            vx, vy, vz = [], [], []
            vtx_trackIDs, vdist, vID, vchi2, vhchi2red, ndf, nvtx, nTracks , refined, pval = [], [], [], [], [], [], [], [],[], []
            vtx_cov_XX, vtx_cov_YY, vtx_cov_ZZ, vtx_cov_XY, vtx_cov_YZ, vtx_cov_ZX = [],[],[],[],[],[]
            
            for index in trange(read_index, read_index + chunk_size):
                # for last chunk make sure only valid indices are used
                if index >= nevents:
                    break
                
                # check for tracks in event
                n_vtx = arrays["multivx"][index].shape[0]
                for vtx in range(n_vtx):
                    vertexID +=1
                    vtx_trackIDs.extend(arrays["multiVtrackID"][index][vtx])
                    n_tracks_in_vtx = len(arrays["multiVtrackID"][index][vtx])
                    vx.extend(np.repeat(arrays["multivx"][index][vtx],n_tracks_in_vtx))
                    vy.extend(np.repeat(arrays["multivy"][index][vtx],n_tracks_in_vtx))
                    vz.extend(np.repeat(arrays["multivz"][index][vtx],n_tracks_in_vtx))
                    vchi2.extend(np.repeat(arrays["multiVchi2"][index][vtx],n_tracks_in_vtx))
                    pval.extend(np.repeat(arrays["multiVpval"][index][vtx],n_tracks_in_vtx))
                    vhchi2red.extend(np.repeat(arrays["redMultiVchi2"][index][vtx],n_tracks_in_vtx))
                    ndf.extend(np.repeat(arrays["multiVndf"][index][vtx], n_tracks_in_vtx))
                    nTracks.extend(np.repeat(n_tracks_in_vtx,n_tracks_in_vtx))
                    refined.extend(np.repeat(arrays["refined"][index][vtx], n_tracks_in_vtx))
                    nvtx.extend([arrays["nVtxinEvent"][index]]*n_tracks_in_vtx)

                    vtx_cov_XX.extend(np.repeat(arrays["multiVcovXX"][index][vtx],n_tracks_in_vtx))
                    vtx_cov_YY.extend(np.repeat(arrays["multiVcovYY"][index][vtx],n_tracks_in_vtx))
                    vtx_cov_ZZ.extend(np.repeat(arrays["multiVcovZZ"][index][vtx],n_tracks_in_vtx))
                    vtx_cov_XY.extend(np.repeat(arrays["multiVcovXY"][index][vtx],n_tracks_in_vtx))
                    vtx_cov_YZ.extend(np.repeat(arrays["multiVcovYZ"][index][vtx],n_tracks_in_vtx))
                    vtx_cov_ZX.extend(np.repeat(arrays["multiVcovZX"][index][vtx],n_tracks_in_vtx))

                    vtx_spill.extend([arrays["pspill"][index]]*n_tracks_in_vtx)
                    vtx_event_number.extend([arrays["evnum"][index]]*n_tracks_in_vtx)
                    vtx_timestamp.extend([arrays["timestamp"][index]]*n_tracks_in_vtx)

                    vdist.extend(np.repeat(arrays["multiVdist"][index][vtx],n_tracks_in_vtx))
                    vID.extend([vertexID] * n_tracks_in_vtx)
                    
                
                # for track in range(ntracks):
                #     track_trackID.append(track)

            vtx_array = np.full(fill_value=np.nan,
                                    dtype=vertex_dtype,
                                    shape=(len(vtx_event_number),))


            logging.info("write numpy vertex array")

            vtx_array["event_number"] = vtx_event_number
            vtx_array["spill"] = vtx_spill
            vtx_array["timestamp"] = vtx_timestamp
            vtx_array["x"] = vx
            vtx_array["y"] = vy
            vtx_array["z"] = vz

            vtx_array["distance"] = vdist
            vtx_array["vertexID"] = vID
            vtx_array["trackID"] = vtx_trackIDs
            vtx_array["vertex_chi2"] = vchi2
            vtx_array["vertex_chi2_red"] = vhchi2red
            vtx_array["nTracks"] = nTracks
            vtx_array["ndf"] = ndf
            vtx_array["refined"] = refined
            vtx_array["vertex_p"] = pval

            vtx_array["vertex_cov_XX"] = vtx_cov_XX
            vtx_array["vertex_cov_YY"] = vtx_cov_YY
            vtx_array["vertex_cov_ZZ"] = vtx_cov_ZZ
            vtx_array["vertex_cov_XY"] = vtx_cov_XY
            vtx_array["vertex_cov_YZ"] = vtx_cov_YZ
            vtx_array["vertex_cov_ZX"] = vtx_cov_ZX
            vtx_array["nvtx_in_evt"] = nvtx

            vtx_table_out.append(vtx_array)
            vtx_table_out.flush()

    logging.info('saved vertex file at %s' % out_file_name)

    return out_file_name