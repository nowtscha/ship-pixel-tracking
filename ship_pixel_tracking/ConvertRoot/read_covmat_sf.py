'''
script to fully read content of linked_tracks.root
Works only on lxplus, need to configure FairShip and need to have access to additional libraries:

$ source /cvmfs/ship.cern.ch/SHiP-2021/latest/setUp.sh

$ alienv enter FairShip/latest

$ source /afs/cern.ch/work/a/aiuliano/public/fedra/setup_new.sh

'''

import ROOT as r
import numpy as np
import fedrarootlogon
c = r.TCanvas()

# opening the file
f = r.TFile.Open("/eos/experiment/ship/data/charmxsec/Emulsion/CHARM1_RUN6/linked_tracks.root")
tree = f.Get('tracks')
tree.SetAlias("t", "t.")  # setting the alias for the track branch

nentries = tree.GetEntries()

#covs = np.full(fill_value = 0, shape = (tree.GetEntries()*2,4,4), dtype = np.float)
covs = np.full(fill_value=0, shape=(nentries,), dtype=[("trk", np.int32), ("x_x", np.float16), ("x_y", np.float16), ("x_tx", np.float16), ("x_ty", np.float16), ("y_y", np.float16), ("y_tx", np.float16), ("y_ty", np.float16), ("tx_tx", np.float16), ("tx_ty", np.float16), (
    "ty_ty", np.float16), ("x_x_2", np.float16), ("x_y_2", np.float16), ("x_tx_2", np.float16), ("x_ty_2", np.float16), ("y_y_2", np.float16), ("y_tx_2", np.float16), ("y_ty_2", np.float16), ("tx_tx_2", np.float16), ("tx_ty_2", np.float16), ("ty_ty_2", np.float16)])

for n in range(nentries):
    if n % 10000 == 0:
        print("loading track %i" % n)
    tree.GetEntry(n)
    track = tree.t
    segments = tree.s
    fittedsegments = tree.sf

    #print ('Track TX angle:', track.TX(), "Segment 0 position", segments[0].X(), segments[0].Y(), segments[0].Z())

    # we need to access Covariance matrix of first and last segment
    # first entry, segment where track starts
    mycov = fittedsegments[0].COV()

    covs[n]["trk"] = track.ID()
    covs[n]["x_x"] = mycov[0][0]
    covs[n]["x_y"] = mycov[0][1]
    covs[n]["x_tx"] = mycov[0][2]
    covs[n]["x_ty"] = mycov[0][3]
    covs[n]["y_y"] = mycov[1][1]
    covs[n]["y_tx"] = mycov[1][2]
    covs[n]["y_ty"] = mycov[1][3]
    covs[n]["tx_tx"] = mycov[2][2]
    covs[n]["tx_ty"] = mycov[2][3]
    covs[n]["ty_ty"] = mycov[3][3]

    # last entry, where track ends
    mycov2 = fittedsegments[fittedsegments.GetEntries()-1].COV()

    covs[n]["x_x_2"] = mycov[0][0]
    covs[n]["x_y_2"] = mycov[0][1]
    covs[n]["x_tx_2"] = mycov[0][2]
    covs[n]["x_ty_2"] = mycov[0][3]
    covs[n]["y_y_2"] = mycov[1][1]
    covs[n]["y_tx_2"] = mycov[1][2]
    covs[n]["y_ty_2"] = mycov[1][3]
    covs[n]["tx_tx_2"] = mycov[2][2]
    covs[n]["tx_ty_2"] = mycov[2][3]
    covs[n]["ty_ty_2"] = mycov[3][3]

np.savetxt("./covs.csv", covs, delimiter=",")
