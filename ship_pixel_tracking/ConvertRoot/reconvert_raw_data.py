import tables as tb
import numpy as np
import os
import time
from beam_telescope_analysis.converter import pybar_ship_converter
from beam_telescope_analysis.tools.merge_data import open_files, merge_hits, save_new_tables
from ctypes import Structure, c_ushort, c_int, c_uint32
from numba import njit
from tqdm import tqdm

import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(message)s')
logger = logging.getLogger('TBConverter')


class FrHeader(Structure):
    _fields_ = [("size",   c_ushort),
                ("partID", c_ushort),
                ("cycleID", c_uint32),
                ("frameTime", c_uint32),
                ("timeExtent", c_ushort),
                ("flags",  c_ushort)]


class Hit(Structure):
    _fields_ = [("channelID", c_ushort),
                ("hit_data", c_ushort)]


class Header_table(tb.IsDescription):
    event_number = tb.Int64Col(pos=0)
    size = tb.UInt16Col(pos=1)
    partID = tb.UInt16Col(pos=2)
    cycleID = tb.UInt32Col(pos=3)
    frameTime = tb.UInt32Col(pos=4)
    timeExtent = tb.UInt16Col(pos=5)
    flags = tb.UInt16Col(pos=6)


class SHiP_data_table(tb.IsDescription):
    event_number = tb.Int64Col(pos=0)
    channelID = tb.UInt16Col(pos=1)
    hit_data = tb.UInt16Col(pos=2)


@njit
def find_new_spills(table):
    indices = []
    spills = 0
    for row in range(0, table.shape[0]-1):
        if (table[row+1]["trigger_time_stamp"] + 1) < table[row]["trigger_time_stamp"]:
            spills += 1
            indices.append(row+1)
    return indices, spills


def save_table(file_name, array_in):
    '''
    Save numpy struct array as pytables file.
    ----------------
    INPUT:
        file_name, str: location where the file should be saved
        array_in, np.array: structured array with data to be saved
    '''

    out_file = tb.open_file(file_name, mode='w')  # as out_file:
    hit_table_out = out_file.create_table(
        where=out_file.root,
        name='Hits',
        description=array_in.dtype,
        title='Hits for raw data conversion aligned at trigger time stamp',
        filters=tb.Filters(
            complib='blosc',
            complevel=5,
            fletcher32=False))
    # remove emtpy events for testbeam analysis
    hit_table_out.append(array_in)
    hit_table_out.flush()
    out_file.close()


def count_spills(in_file_name, table_name="Hits"):
    ''' 
    Check data in input file for timestamp resets and count the number of resetes i.e. number of spills.
    Also get moduleID from input file name.
    Create a new file with additional columns "spill" and "moduleID"
    --------------------
    INPUT:
        in_file_name, str: location of the input hits file.
        table_name, str: name of the table inside the file to check for timestampss
    OUTPUT:
        out_file_path_h5, str: location of the new file
        npsills, int: number of spills found in data
    '''

    with tb.open_file(in_file_name) as in_file:
        moduleID = in_file_name[in_file_name.find("module_")+7]
        for table in in_file.walk_nodes('/', classname='Table'):
            if table.name == table_name:
                table = table[:]
                break
    indices, nspills = find_new_spills(table)
    logger.info("found %s spills" % nspills)
    logger.info("adding spill counter and moduleID")
    # add NSpills to Hit Table
    dtypes = []
    for j, name in enumerate(table.dtype.names):
        dtypes.append((table.dtype.names[j], table[name].dtype))
    dtypes.extend([('spill', np.uint32), ('moduleID', np.uint8)])
    new_array = np.zeros(shape=table.shape, dtype=dtypes)
    for column in table.dtype.names:
        new_array[column] = table[column]
    for i in range(nspills+1):
        if i == 0:
            new_array[:indices[i+1]]['spill'] = i
        elif i == nspills:
            new_array[indices[i-1]:]['spill'] = i
        else:
            new_array[indices[i-1]:indices[i]]['spill'] = i
    new_array["moduleID"] = np.uint8(moduleID)

    out_file_path_h5 = in_file_name.replace('.h5', '_spills.h5')
    save_table(out_file_path_h5, new_array)
    logger.info('saved ClusterTable with NSpills and moduleID columns at %s' % out_file_path_h5)
    return out_file_path_h5, nspills


def get_files(folder, string):
    '''
    crawl folder for files with name matching given string recursively
    -----------
    INPUT:
        folder, str: partent directory where to start searching
        string, str: phrase which is looked for in the file name
    OUTPUT:
        raw_data_file_list, list: list with absolute paths of files which match given string
    '''

    raw_data_file_list = []
    for dirpath, _, filenames in os.walk(folder):
        for f in filenames:
            if string in f:
                raw_data_file_list.append(os.path.abspath(os.path.join(dirpath, f)))
    raw_data_file_list.sort()
    return raw_data_file_list


def get_cycleIDs(input_file):
    '''
    crawl file with record of data sent to the eventbuilder for the spillcounters ( == cycleIDs).
    If there are no headers for the respective spill there is also no data to be read, and the spill is ignored.
    If a subpartition received a trigger, there must be a record of at least 7 empty hits, 
    if this is not the case the spill data is corrupted and ignored.
    --------------
    INPUT:
        input_file, str: location of the pytables file to crawl for spill nodes.
    OUTPUT:
        list of found spillcounters / cycleIDs
    '''

    spill_numbers = []
    partID = input_file.split('_')[-3][2:]
    run_number = input_file.split('_')[-1].split('.')[0]
    logger.info('checking run %s partition %s' % (run_number, partID))
    with tb.open_file(input_file, mode='r') as in_file:
        for group in in_file.walk_groups():
            if group._v_name == '/':
                continue
            if group._v_name == "Spill_522404110":
                continue
            if (group.Headers.shape[0] > 0) and (group.Hits.shape[0] > 1):  # make sure only events with more than 1 hit per partiotion are considered TODO: valid cut?
                spill_numbers.append(np.int32(group._v_name.split('_')[-1]))
            if (group.Headers.shape[0] > 0) and (group.Hits.shape[0] <= 1):
                logger.warning("%s has only 1 hit" % group._v_name)
                logger.warning("%s has only 1 hit" % group._v_name)
                logger.warning("%s has only 1 hit" % group._v_name)
    spill_numbers.sort()
    logger.info("found %s spills" % len(spill_numbers))
    return spill_numbers


def _compare_cycleIDs(cycleIDs_1, cycleIDs_2):
    ''' 
    Compare all spillnumbers in both lists, if they are different stop the script.
    -----------
    INPUT:
        cycleIDs_1, list: list with cycleIDs
        cycleIDs_2, list: list with cycleIDs
    '''

    for i, cycle in enumerate(cycleIDs_1):
        if (cycle != cycleIDs_2[i]):
            logger.error("different cycleIDs in partitions - Abort!")
            raise ValueError


def check_cycleIDs(cycleIDs):
    '''
    Compare number of cycleIDs in each partition. If they are different, check if at least 2 partitions have the same number of cycleIDs.
    If this is the case ignore the not matching partition.
    Also check for same spill identifiers in each partition.
    -----------------
    INPUT:
        cycleIDS: dict with keys 0,2,3, values are a list each with the found cycleIDs for the respective subpartition.
    '''

    try:
        lengths = [len(cycleIDs[0]), len(cycleIDs[1]), len(cycleIDs[2])]
        # if ((lengths[0] != lengths[1]) or (lengths[0] != lengths[2]) or (lengths[1] != lengths[2])):
        if not lengths[0] == lengths[1] == lengths[2]:
            logger.error("different number of spills in partitions!")
            raise ValueError
    except ValueError:
        for i, cycle in enumerate(cycleIDs[0]):
            print(cycle, cycleIDs[1][i], cycleIDs[2][i])
        if lengths[0] == lengths[1]:
            _compare_cycleIDs(cycleIDs[0], cycleIDs[1])
            cycleIDs[2] = cycleIDs[0]

        elif lengths[1] == lengths[2]:
            _compare_cycleIDs(cycleIDs[1], cycleIDs[2])
            cycleIDs[0] = cycleIDs[1]

        elif lengths[0] == lengths[2]:
            _compare_cycleIDs(cycleIDs[0], cycleIDs[2])
            cycleIDs[1] = cycleIDs[0]
        else:
            logger.error("all 3 partitions have different number of spills")
            raise ValueError
    for i, cycle in enumerate(cycleIDs[0]):
        if (cycle != cycleIDs[1][i]) or (cycle != cycleIDs[2][i]):
            logger.error("different cycleIDs in partitions - Abort!")
            raise ValueError


def spillcount_to_cycleID(input_file, cycleIDs):
    '''
    Replace simple iterated spill number in hits file by found cycleIDs and save as new file.
    The input file needs to already have the "spills" column where a new spill is identified.
    -----------
    INPUT:
        input_file, str: path to the pytables file with hit data, and columns "spill" and "moduleID" (see function count_spills )
        cycleIDs, list: list with cycleIDs for given partition.
    OUTPUT:
        output_file, str: location of the new file
    '''

    with tb.open_file(input_file, mode="r") as in_file:
        hits = in_file.root.Hits[:]
        spills, indices = np.unique(hits["spill"], return_index=True)

        if len(spills) != len(cycleIDs):
            print(input_file)
            print(len(spills), len(cycleIDs))
            logger.error("number of cycleIDs does not match number of spills - Aborting!")
            raise ValueError
        for i, spill in enumerate(spills):
            if i == len(spills)-1:  # last iteration,
                hits[indices[i]:]["spill"] = cycleIDs[i]
            else:
                hits[indices[i]:indices[i+1]]["spill"] = cycleIDs[i]
        output_file = input_file[:-9]+"cycleIDs.h5"
        save_table(file_name=output_file, array_in=hits)
    return output_file


def write_spill_files(merged_array, partitionID, filename):
    '''
    Write rawdata files for event builder.
    Create a pytables file for each run and each subpartition, inside the file create a node for every spill. 
    Write headers and hits tables for each spill according to charm cross section data format.
    This is necessary due to transmission issues during live datataking.
    -----------
    INPUT:
        merged_array, np.array: structured array with hits from all modules of one partition. 
                                The table has to have the cycleIDs in the "spill" column. (see function spillcount_to_cycleID)

        partitionID, str:       string with number of partition e.g. "801" this is converted to a hex string and written to the headers.

        filename, str:          location to write the created file.
    '''

    with tb.open_file(filename, mode='w', title="SHiP_raw_data") as run_file:
        logger.info('opening run file %s' % run_file.filename)
        spills, spill_indices = np.unique(merged_array["spill"], return_index=True)
        for i, spill in enumerate(spills):
            logger.info("converting spill %s" % spill)
            if i == len(spills)-1:
                spill_array = merged_array[spill_indices[i]:]
                event_numbers, indices = np.unique(spill_array['event_number'], return_index=True)
            else:
                spill_array = merged_array[spill_indices[i]:spill_indices[i+1]]
                event_numbers, indices = np.unique(spill_array['event_number'], return_index=True)
            n_events = indices.shape[0]
            nhits = spill_array.shape[0]
            headers = np.zeros(shape=(n_events), dtype=[('event_number', np.int64), ('size', np.uint16), ('partID', np.uint16),
                                                        ('cycleID', np.uint32), ('frameTime', np.int32), ('timeExtent', np.uint16), ('flags', np.uint16)])
            hits = np.zeros(shape=(nhits,), dtype=[('event_number', np.int64), ('channelID', np.uint16), ('hit_data', np.uint16)])
            ch_event_numbers = np.arange(0, n_events + 1)

            spill_group = run_file.create_group(where="/", name='Spill_%s' % spill, title='Spill_%s' %
                                                spill, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            header_table = run_file.create_table(where=spill_group, name='Headers', description=Header_table, title='Headers to event data')
            hit_table = run_file.create_table(where=spill_group, name='Hits', description=SHiP_data_table, title='Hits')

            for i, index in tqdm(enumerate(indices)):
                if i == n_events-1:
                    event = spill_array[index:]
                else:
                    event = spill_array[index:indices[i+1]]
                # build SHiP data format
                channelID = np.bitwise_or(event['row'] << 7, event['column'], order='C', dtype=np.uint16)
                if channelID.nbytes == 2:
                    print(event)
                    print(channelID.dtype)
                first_word = np.bitwise_or(event['charge'] << 4, event['moduleID'], dtype=np.uint16)
                second_word = np.bitwise_or(0 << 4, event['frame'], dtype=np.uint16)
                hit_data = np.bitwise_or(first_word << 8, second_word, order='C', dtype=np.uint16)
                # fill numpy array with SHiP data
                ch_hit_data = np.zeros(channelID.shape[0], dtype=Hit)
                ch_hit_data["channelID"] = channelID
                ch_hit_data["hit_data"] = hit_data
                # each event needs a frame header, extra array for header
                data_header = np.zeros(shape=(1,), dtype=FrHeader)
                data_header['size'] = channelID.nbytes + hit_data.nbytes + 16
                data_header['partID'] = int("0x0"+partitionID, 16)
                data_header['cycleID'] = spill
                data_header['frameTime'] = event[0]['trigger_time_stamp']
                data_header['timeExtent'] = ch_event_numbers[i] + 1
                data_header['flags'] = 0
                # write hits to numpy array for local storage
                if i == n_events-1:
                    hits[index:]['event_number'] = event['event_number']
                    hits[index:]['channelID'] = ch_hit_data['channelID']
                    hits[index:]['hit_data'] = ch_hit_data['hit_data']
                else:
                    hits[index:indices[i+1]]['event_number'] = event['event_number']
                    hits[index:indices[i+1]]['channelID'] = ch_hit_data['channelID']
                    hits[index:indices[i+1]]['hit_data'] = ch_hit_data['hit_data']

                # write header to numpy array for local storage
                headers[i]['event_number'] = event[0]["event_number"]
                headers[i]['size'] = data_header['size']
                headers[i]['partID'] = data_header['partID']
                headers[i]['cycleID'] = data_header['cycleID']
                headers[i]['frameTime'] = data_header['frameTime']
                headers[i]['timeExtent'] = data_header['timeExtent']
                headers[i]['flags'] = data_header['flags']

            # save numpy arrays in .h5 file
            hit_table.append(hits)
            header_table.append(headers)
            header_table.flush()
            hit_table.flush()


if __name__ == "__main__":

    runs = {
        # CHARM1
        # 2786 : ("369", "263", "197"),
        # 2787 : ("370", "264", "198"),
        # 2788 : ("371", "265", "199"),
        # 2789 : ("372", "266", "200"),
        # 2790 : ("373", "267", "201"),
        # # 2791 : ("374", "268", "202"),
        # # 2792 : ("375", "269", "203"),
        # 2793 : ("376", "270", "204"),
        # # CHARM2
        # 2781 : ("364", "258", "192"),
        # 2794 : ("377", "271", "205"),
        # 2795 : ("378", "272", "206"),
        # 2796 : ("379", "273", "207"),
        # 2782 : ("365", "259", "193"),
        # 2783 : ("366", "260", "194"),
        # # CHARM3
        # 2797 : ("380", "274", "208"),
        # 2798 : ("381", "275", "209"),
        # 2799 : ("381", "275", "209"),
        # # CHARM4
        # 2800 : ("383", "277", "211"),
        # 2805 : ("386", "280", "214"),
        # 2806 : ("387", "281", "215"),
        # # CHARM5
        # 2807 : ("388","282","216"),
        # 2811 : ("391","285","219"),
        # 2784 : ("367","261","195"),
        # # CHARM6
        # 2812 : ("392","286","220"),
        # 2814 : ("393","287","221"),
        # 2785 : ("368","262","196"),

        2863: ("439", "333", "267")
    }

    dirs = ['/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/pybar/part_0x0800',
            '/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/pybar/part_0x0801',
            '/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/pybar/part_0x0802'
            ]

    partitions = ("800", "801", "802")
    old_files_dir = "/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/original_run_data/"
    output_dir = "/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/output_folder/"

    for key in runs.keys():
        run_number = key
        pybar_runs = runs[key]
        data_files_folder = os.path.join(output_dir, 'run_%s' % run_number)
        if not os.path.exists(data_files_folder):
            os.makedirs(data_files_folder)
        hit_files = []
        cycleIDs = {0: [], 1: [], 2: []}
        for i, directory in enumerate(dirs):
            first_plane, second_plane = pybar_ship_converter.get_plane_files(pyBARrun=pybar_runs[i], subpartition_dir=directory)
            for data_file in first_plane:
                hit_files.append(pybar_ship_converter.process_dut(data_file, trigger_data_format=1, do_corrections=False, empty_events=True))
            for data_file in second_plane:
                hit_files.append(pybar_ship_converter.process_dut(data_file, trigger_data_format=1, do_corrections=False, empty_events=True))
            # pybar_ship_converter.merge_dc_module_local(output_dir = data_files_folder, plane_files = first_plane, pyBARrun = pybar_runs[i], plane_number = i*2, output_file_list = hit_files)
            # pybar_ship_converter.merge_dc_module_local(output_dir = data_files_folder, plane_files = second_plane, pyBARrun = pybar_runs[i], plane_number = (i*2)+1 , output_file_list = hit_files)
            for old_file in get_files(old_files_dir, "%s_run_%s.h5" % (partitions[i], run_number)):
                cycleIDs[i] = np.array(get_cycleIDs(old_file))
        check_cycleIDs(cycleIDs)

        # hit_files = get_files("/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/", "scan_s_hi_p_interpreted_formatted_fei4.h5")
        hit_tables_in = open_files(hit_files)
        new_tables = merge_hits(hit_tables_in=hit_tables_in)
        hit_files = save_new_tables(hit_files, new_tables, remove_empty_events=False)
        cycle_files = []

        for table in hit_files:
            spill_file, nspills = count_spills(table)
            cycle_files.append(spillcount_to_cycleID(table[:-3] + "_spills.h5", cycleIDs[0]))
        # cycle_files = [hit_file.replace(".h5","_cycleIDs.h5") for hit_file in hit_files]

        for i, directory in enumerate(dirs):
            logger.info("converting partition %s" % partitions[i])
            if i == 0:
                hit_tables_in = open_files(cycle_files[0:7])
            if i == 1:
                hit_tables_in = open_files(cycle_files[7:15])
            if i == 2:
                hit_tables_in = open_files(cycle_files[15:])
            merged_array = np.concatenate(hit_tables_in)
            logger.info("sorting merged array")
            merged_array.sort(order=["event_number", "moduleID"])
            save_table(file_name=output_dir + "merged_file_0x%s_run_%s.h5" % (partitions[i], run_number), array_in=merged_array)
            write_spill_files(merged_array, partitions[i], filename=output_dir + "partition_0x%s_run_%s.h5" % (partitions[i], run_number))
