import os
import numpy as np
import tables as tb
import datetime
import warnings
from numba import njit, jit

from tqdm import tqdm
from ctypes import Structure, c_ushort, c_int, c_uint32

import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - [%(levelname)-8s] (%(threadName)-10s) %(message)s')
logger = logging.getLogger('Converter')


class FrHeader(Structure):
    _fields_ = [("size",   c_ushort),
                ("partID", c_ushort),
                ("cycleID", c_uint32),
                ("frameTime", c_uint32),
                ("timeExtent", c_ushort),
                ("flags",  c_ushort)]


class Hit(Structure):
    _fields_ = [("channelID", c_ushort),
                ("hit_data", c_ushort)]


def get_file_date(cycleID):
    ''' 
    convert spillID / cycleID to human readable date for filename.
    ---------
    INPUT:
        cycleID, int: integer which counts in 5s steps from 08.04.2015 13.00 pm the date of forming SHiP
    OUTPUT:
        string with date of the cycleID, 5s stepping

    '''

    start_date = datetime.datetime(2015, 4, 8, 13, 0)  # careful: python datetime is 1 hour behind c time.h module. TODO: check why
    return (start_date + datetime.timedelta(seconds=cycleID / 5.)).strftime("%Y%m%d_%H%M%S")


def unpack_run_file(input_file):
    '''
    Takes pytables file with rawdata saved as numpy struct arrays. The input_file has to have a node for every spill, the node name is the cycleID.
    Inside every node there have to be two arrays: headers, and hits. These are extracted and saved as .txt files.
    ----------------
    INPUT:
        input_file, str: location of the input file.
    OUTPUT:
        Nothing
    '''

    empty_header_exception = Exception('Empty header inbetween frames')

    partID = "0x" + input_file.split('_')[-3]
    run_number = input_file.split('_')[-1].split('.')[0]
    directory = os.path.dirname(input_file)
    spillpath = "%s/part_%s/RUN_%s/" % (directory, partID, run_number)
    logger.info('Unpacking partition %s run %s' % (partID, run_number))
    if not os.path.exists(spillpath):
        os.makedirs(spillpath)

    with tb.open_file(input_file, mode='r') as in_file:
        for group in in_file.walk_groups():
            spill_number = group._v_name

            if spill_number == '/':
                continue

            else:
                spill_number = spill_number.split('_')[-1]
                file_name = get_file_date(np.uint32(spill_number))
#                 logger.info('spill %s corresponds to date %s' %(spill_number,file_name))
                headers_in = group.Headers[:]
                hits_in = group.Hits[:]
                logger.info('found %s hits in %s frames for spill %s / %s' % (hits_in.shape[0], headers_in.shape[0], spill_number, file_name))
                i = 0
                with open(spillpath + file_name + '.txt', 'w') as spillfile:
                    for row in range(0, headers_in.shape[0]):
                        try:
                            nhits = int((headers_in[row]['size']-16)/4)
                            if nhits <= 0:
                                logger.warning('header %s has size %s' % (row, nhits))
                                if row > 0:
                                    ''' also breaks if empty header at the end of file. this empty header usually is followed by nhits empty headers.
                                        no occurence of an empty header in between normal frames was found but only empty headers at the end.
                                    '''
                                    logger.warning('empty header in between spills, stopping spill readout')
                                    raise empty_header_exception
                                continue
                            else:
                                header_out = np.zeros(shape=(1,), dtype=FrHeader)
                                hits_out = np.zeros(shape=nhits, dtype=Hit)

                                header_out['size'] = headers_in[row]['size']
                                header_out['partID'] = headers_in[row]['partID']
                                header_out['cycleID'] = headers_in[row]['cycleID']
                                header_out['frameTime'] = headers_in[row]['frameTime']
                                header_out['timeExtent'] = headers_in[row]['timeExtent']
                                header_out['flags'] = headers_in[row]['flags']

                                hits_out['channelID'] = hits_in[i:(i+nhits)]['channelID']
                                hits_out['hit_data'] = hits_in[i:(i+nhits)]['hit_data']
    #                             np.savetxt(spillfile, headers[row].reshape((1,)))
    #                             np.savetxt(spillfile, hits[i:(i+nhits)])
#                                 headers[row].tofile(spillfile)
#                                 hits[i:(i+nhits)].tofile(spillfile)
                                header_out.tofile(spillfile)
                                hits_out.tofile(spillfile)
                                i += nhits
                        except empty_header_exception:
                            logger.info('ended loop')
                            break


class EmptyHeaderException(ValueError):
    pass


def unpack_run_file_charm_naming(input_file):
    '''
    Takes pytables file with rawdata saved as numpy struct arrays. The input_file has to have a node for every spill, the node name is the cycleID.
    Inside every node there have to be two arrays: headers, and hits. These are extracted and saved as uncoded .rawdata files.
    This function creates the file structure and file naming as it was in charm cross section measurement to rerun the eventbuilder.
    e.g. : ./RUN_0800_2863/SPILLDATA_0800_0522707110_20180731_121702.rawdata
    ----------------
    INPUT:
        input_file, str: location of the input file.
    OUTPUT:
        Nothing
    '''

    partID = "0x" + input_file.split('_')[-3][2:]
    run_number = input_file.split('_')[-1].split('.')[0]
    directory = os.path.dirname(input_file) + "/rawdata"
    runpath = "%s/RUN_0%s_%s/" % (directory, partID[2:], run_number)
    logger.info('Unpacking run %s to %s' % (run_number, runpath))
    if not os.path.exists(runpath):
        os.makedirs(runpath)

    with tb.open_file(input_file, mode='r') as in_file:
        for group in in_file.walk_groups():
            node_name = group._v_name
            if node_name == '/':
                continue
            else:
                spill_number = node_name.split('_')[-1]
                file_name = "SPILLDATA_0%s_0%s_%s.rawdata" % (partID[2:], spill_number, get_file_date(np.uint32(spill_number)))
                # logger.info('spill %s saved at %s' %(spill_number,file_name))
                headers_in = group.Headers[:]
                hits_in = group.Hits[:]
                logger.info('found %s hits in %s frames for spill %s' % (hits_in.shape[0], headers_in.shape[0], spill_number))
                i = 0
                with open(runpath + file_name, 'w') as spillfile:
                    SoS_header = np.zeros(shape=(1,), dtype=FrHeader)
                    SoS_header['size'] = 16
                    SoS_header['partID'] = int(partID, 0)
                    SoS_header['timeExtent'] = 0
                    SoS_header['flags'] = 0
                    SoS_header['frameTime'] = 0xFF005C03
                    SoS_header['cycleID'] = spill_number
                    SoS_header.tofile(spillfile)
                    for row in range(0, headers_in.shape[0]):
                        try:
                            nhits = int((headers_in[row]['size']-16)/4)
                            if nhits <= 0:
                                logger.warning('header %s has size %s' % (row, nhits))
                                if row > 0:
                                    '''
                                    also breaks if empty header at the end of file. this empty header usually is followed by nhits empty headers.
                                    no occurence of an empty header in between normal frames was found but only empty headers at the end.
                                    '''
                                    raise EmptyHeaderException
                                continue
                            else:
                                header_out = np.zeros(shape=(1,), dtype=FrHeader)
                                hits_out = np.zeros(shape=nhits, dtype=Hit)

                                header_out['size'] = headers_in[row]['size']
                                header_out['partID'] = headers_in[row]['partID']
                                header_out['cycleID'] = headers_in[row]['cycleID']
                                header_out['frameTime'] = headers_in[row]['frameTime']
                                header_out['timeExtent'] = headers_in[row]['timeExtent']
                                header_out['flags'] = headers_in[row]['flags']

                                hits_out['channelID'] = hits_in[i:(i+nhits)]['channelID']
                                hits_out['hit_data'] = hits_in[i:(i+nhits)]['hit_data']
    #                             np.savetxt(spillfile, headers[row].reshape((1,)))
    #                             np.savetxt(spillfile, hits[i:(i+nhits)])
#                                 headers[row].tofile(spillfile)
#                                 hits[i:(i+nhits)].tofile(spillfile)
                                header_out.tofile(spillfile)
                                hits_out.tofile(spillfile)
                                i += nhits
                        except EmptyHeaderException:
                            logger.info('ended loop')
                            break
                    EoS_header = np.zeros(shape=(1,), dtype=FrHeader)
                    EoS_header['size'] = 16
                    EoS_header['partID'] = int(partID, 0)
                    EoS_header['flags'] = 0
                    EoS_header['frameTime'] = 0xFF005C04
                    EoS_header['cycleID'] = spill_number
                    EoS_header['timeExtent'] = i + 1  # also count EoS header
                    EoS_header.tofile(spillfile)


def check_bytefiles(input_bytefile):
    '''
    simple hex dump of input bytefile to check content.
    ----------
    INPUT:
        input_bytefile, str: location of the file to be dumped
    '''
    with open(input_bytefile, 'r') as in_file:
        data = in_file.read().encode('hex')
        print(data)


if __name__ == '__main__':

    raw_data_file_list = ["/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/output_folder/partition_0x800_run_2863.h5",
                          "/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/output_folder/partition_0x801_run_2863.h5",
                          "/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/output_folder/partition_0x802_run_2863.h5"]

    # for data_file in os.listdir('/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/output_folder'):
    #     if ("partition_0x" in data_file) and ("_run_" in data_file):
    #         raw_data_file_list.append(os.path.abspath(os.path.join('/media/niko/big_data/charm_testbeam_july18/reconvert_raw_data/output_folder', data_file)))
    # raw_data_file_list.sort()

    for scan_file in tqdm(raw_data_file_list):
        unpack_run_file_charm_naming(scan_file)
#         check_bytefiles('/media/data/ship_charm_xsec_Jul18/run_data/part_0x800/RUN_2467/2018_07_26_16_44_10.txt')
