'''
Take cbmsim file and extract EventTime from header. Only works inside FairSHiP since special header class is needed.
'''
from ROOT import TFile, TTree, TChain
from array import array

# f=TFile("SPILLDATA_8000_0522366760_20180730_172232.root","READ")


sTree = TChain("cbmsim")
sTree.Add("/media/niko/big_data/charm_testbeam_july18/combined_data/run2793/SPILLDATA_8000_0522366760_20180730_172232.root")

m = sTree.GetEntries()

fE = array('d', m*[0])
t1 = TTree("t1", "fEventTime")
t1.Branch("fEventTime", fE, "fEventTime/D")

sTree.Print()

for i in range(0, sTree.GetEntries()):
    sTree.GetEntry(i)
    fE[0] = sTree.EventHeader.GetEventTime()
    t1.Fill()

t1.Print()
f1 = TFile("fEventTime.root", "RECREATE")
t1.Write()
f1.Close()
