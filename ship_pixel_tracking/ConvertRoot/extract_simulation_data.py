from ROOT import TChain
import numpy as np
import tables as tb
import os
import argparse


def extract_sim_data(start_dir, file_name="pythia8_Geant4_1000_0.5.root"):
    ''' take all FairShip simulation files inside start_dir (also subdirectories), saves pixel data in single .h5 file per root file.
            Due to ROOT header classes needed, only works when called from FairShip environment.

            INPUT:
                    start_dir : string
                            parent folder to start looking for files.
                    file_name : string
                            name of the simulation file, FairShip output always has the same name. It seems to be expected to create a new folder for every simulation.

            RETURN:
                    nothing

    '''
    sim_files = []
    for root, _, files in os.walk(start_dir):
        for file in files:
            if file == file_name:
                sim_files.append(os.path.join(root, file))

    for path in sim_files:
        out_file_name = os.path.dirname(path) + "/simextract.h5"

        print("reading %s" % path)
        sTree = TChain("cbmsim")
        sTree.Add(path)
        # px, py and pz are the momentum vector
        event, x, y, z, px, py, pz, eloss, plane, pdgcode = [], [], [], [], [], [], [], [], [], []

        for i in range(0, sTree.GetEntries()):
            sTree.GetEntry(i)
            for coin in sTree.PixelModulesPoint:
                event.append(i)
                x.append(coin.GetX())
                y.append(coin.GetY())
                z.append(coin.GetZ())
                px.append(coin.GetPx())
                py.append(coin.GetPy())
                pz.append(coin.GetPz())
                eloss.append(coin.GetEnergyLoss())
                plane.append(coin.GetDetectorID() - 1110)
                pdgcode.append(coin.PdgCode())

        sim_data = np.full(fill_value=-1, shape=(len(x),),
                           dtype=[("event_number", np.int64), ("x", np.float64), ("y", np.float64), ("z", np.float64),
                                  ("px", np.float64), ("py", np.float64), ("pz", np.float64),
                                  ("EnergyLoss", np.float32), ("plane", np.uint32), ("PdgCode", np.int32), ("spill", np.int32)
                                  ]
                           )

        sim_data["event_number"] = event
        sim_data["x"] = x
        sim_data["y"] = y
        sim_data["z"] = z
        sim_data["px"] = px
        sim_data["py"] = py
        sim_data["pz"] = pz
        sim_data["EnergyLoss"] = eloss
        sim_data["plane"] = plane
        sim_data["PdgCode"] = pdgcode
        sim_data["spill"] = 1*len(x)

        with tb.open_file(out_file_name, "w") as out_file_h5:
            hit_table_description = sim_data.dtype
            hit_table_out = out_file_h5.create_table(where=out_file_h5.root,
                                                     name='Hits',
                                                     description=hit_table_description, title='Simulated hits in pixel detector',
                                                     filters=tb.Filters(
                                                         complib='blosc',
                                                         complevel=5,
                                                         fletcher32=False)
                                                     )
            hit_table_out.append(sim_data)

        print()
        print("pytables file written to %s" % out_file_name)
        print()

    return out_file_name


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('start_folder', type=str, help='parent folder to start looking for simulation files')
    args = parser.parse_args()
    extract_sim_data(args.start_folder)
