import uproot as up
import logging
import tables as tb
import numpy as np
import awkward as ak
from tqdm import tqdm
#from ROOT import TFile, TChain, TNtuple


def load_SND_cbmsim_scifi(in_file_path):
    file = up.open(in_file_path)
    tree = file["cbmsim"]
    arrays = tree.arrays([
                        #   "ScifiPoint",
                        #   "ScifiPoint.fUniqueID",
                        #   "ScifiPoint.fTrackID",
                        #   "ScifiPoint.fEventId",
                          "ScifiPoint.fPx",
                          "ScifiPoint.fPy",
                          "ScifiPoint.fPz",
                          "ScifiPoint.fTime",
                          "ScifiPoint.fLength",
                          "ScifiPoint.fELoss",
                          "ScifiPoint.fDetectorID",
                          "ScifiPoint.fX",
                          "ScifiPoint.fY",
                          "ScifiPoint.fZ",
                          "ScifiPoint.fPdgCode",
                          "ScifiPoint.fLpos",
                          "ScifiPoint.fLmom"])

    event_size = []
    for row in arrays["ScifiPoint.fX"]:
        event_size.append(len(row))
    
    total_size = sum(event_size)
    n_events= len(event_size)

    new_arr = np.full(fill_value=-1, dtype=[("event", np.uint32),
                                            ("event_time", np.float64), 
                                            ("scifi_detID", np.int32),
                                            ("trackID", np.int32), 
                                            ("energy_loss", np.float64), 
                                            ("p_x", np.float64), 
                                            ("p_y", np.float64), 
                                            ("p_z", np.float64), 
                                            ("x", np.float64), 
                                            ("y", np.float64), 
                                            ("z", np.float64), 
                                            ("pdg_code", np.int32), 
                                            ("Lpos_x", np.float64), 
                                            ("Lpos_y", np.float64), 
                                            ("Lpos_z", np.float64), 
                                            ("Lmom_x", np.float64), 
                                            ("Lmom_y", np.float64), 
                                            ("Lmom_z", np.float64)
                                            ], 
                                    shape=total_size)

    events, detID, event_time, p_x, p_y, p_z, x, y, z, pdg_code, lpos_x, lpos_y, lpos_z, lmom_x, lmom_y, lmom_z, = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

    for event in tqdm(range(n_events)):

        events.extend([event] * event_size[event])
        event_time.extend(ak.to_numpy(arrays["ScifiPoint.fTime"][event]))
        detID.extend(ak.to_numpy(arrays["ScifiPoint.fDetectorID"][event]))
        p_x.extend(ak.to_numpy(arrays["ScifiPoint.fPx"][event]))
        p_y.extend(ak.to_numpy(arrays["ScifiPoint.fPy"][event]))
        p_z.extend(ak.to_numpy(arrays["ScifiPoint.fPz"][event]))
        x.extend(ak.to_numpy(arrays["ScifiPoint.fX"][event]))
        y.extend(ak.to_numpy(arrays["ScifiPoint.fY"][event]))
        z.extend(ak.to_numpy(arrays["ScifiPoint.fZ"][event]))
        pdg_code.extend(ak.to_numpy(arrays["ScifiPoint.fPdgCode"][event]))

        lpos_x.extend(ak.to_numpy(arrays["ScifiPoint.fLpos"][event]["fX"]))
        lpos_y.extend(ak.to_numpy(arrays["ScifiPoint.fLpos"][event]["fY"]))
        lpos_z.extend(ak.to_numpy(arrays["ScifiPoint.fLpos"][event]["fZ"]))
        lmom_x.extend(ak.to_numpy(arrays["ScifiPoint.fLpos"][event]["fX"]))
        lmom_y.extend(ak.to_numpy(arrays["ScifiPoint.fLpos"][event]["fY"]))
        lmom_z.extend(ak.to_numpy(arrays["ScifiPoint.fLpos"][event]["fZ"]))


    logging.info("write numpy arrays")
    new_arr["event"] = events
    new_arr["scifi_detID"] = detID
    new_arr["event_time"] = event_time
    new_arr["x"] = x
    new_arr["y"] = y
    new_arr["z"] = z
    new_arr["p_x"] = p_x
    new_arr["p_y"] = p_y
    new_arr["p_z"] = p_z
    new_arr["pdg_code"] = pdg_code

    new_arr["Lpos_x"] = lpos_x
    new_arr["Lpos_y"] = lpos_y
    new_arr["Lpos_z"] = lpos_z
    new_arr["Lmom_x"] = lmom_x
    new_arr["Lmom_y"] = lmom_y
    new_arr["Lmom_z"] = lmom_z

    return new_arr


def load_SND_cbmsim_montecarlo(in_file_path):
    file = up.open(in_file_path)
    tree = file["cbmsim"]
    arrays = tree.arrays([
                        #   "ScifiPoint",
                        #   "ScifiPoint.fUniqueID",
                        #   "ScifiPoint.fTrackID",
                        #   "ScifiPoint.fEventId",
                          "MCTrack.fPx",
                          "MCTrack.fPy",
                          "MCTrack.fPz",
                          "MCTrack.fStartX",
                          "MCTrack.fStartY",
                          "MCTrack.fStartZ",
                          "MCTrack.fPdgCode",
                          "MCTrack.fMotherId",
                          "MCTrack.fM",
                          "MCTrack.fW",
                          "MCTrack.fProcID",
                          "MCTrack.fNPoints",
                          "MCTrack.fStartT"])

    event_size = []
    for row in arrays["MCTrack.fStartX"]:
        event_size.append(len(row))
    
    total_size = sum(event_size)
    n_events= len(event_size)

    new_arr = np.full(fill_value=np.nan, dtype=[("event", np.uint32),
                                            ("event_time", np.float64), 
                                            ("mass", np.float64), 
                                            ("motherID", np.int32),
                                            ("procID", np.int32),
                                            ("nPoints", np.int32),
                                            ("fW", np.float64),
                                            ("p_x", np.float64), 
                                            ("p_y", np.float64), 
                                            ("p_z", np.float64), 
                                            ("x", np.float64), 
                                            ("y", np.float64), 
                                            ("z", np.float64), 
                                            ("pdg_code", np.int32), 
                                            ], 
                                    shape=total_size)

    events, procID, motherID, fW, mass, nPoints, event_time, p_x, p_y, p_z, x, y, z, pdg_code, = [], [], [], [], [], [], [], [], [], [], [], [], [], []

    for event in tqdm(range(n_events)):

        events.extend([event] * event_size[event])
        event_time.extend(ak.to_numpy(arrays["MCTrack.fStartT"][event]))
        procID.extend(ak.to_numpy(arrays["MCTrack.fProcID"][event]))
        motherID.extend(ak.to_numpy(arrays["MCTrack.fMotherId"][event]))
        nPoints.extend(ak.to_numpy(arrays["MCTrack.fNPoints"][event]))
        mass.extend(ak.to_numpy(arrays["MCTrack.fM"][event]))
        fW.extend(ak.to_numpy(arrays["MCTrack.fW"][event]))
        p_x.extend(ak.to_numpy(arrays["MCTrack.fPx"][event]))
        p_y.extend(ak.to_numpy(arrays["MCTrack.fPy"][event]))
        p_z.extend(ak.to_numpy(arrays["MCTrack.fPz"][event]))
        x.extend(ak.to_numpy(arrays["MCTrack.fStartX"][event]))
        y.extend(ak.to_numpy(arrays["MCTrack.fStartY"][event]))
        z.extend(ak.to_numpy(arrays["MCTrack.fStartZ"][event]))
        pdg_code.extend(ak.to_numpy(arrays["MCTrack.fPdgCode"][event]))


    logging.info("write numpy arrays")
    new_arr["event"] = events
    new_arr["motherID"] = motherID
    new_arr["event_time"] = event_time
    new_arr["x"] = x
    new_arr["y"] = y
    new_arr["z"] = z
    new_arr["p_x"] = p_x
    new_arr["p_y"] = p_y
    new_arr["p_z"] = p_z
    new_arr["pdg_code"] = pdg_code
    new_arr["mass"] = mass
    new_arr["nPoints"] = nPoints
    new_arr["fW"] = fW
    new_arr["procID"] = procID

    return new_arr


def load_cbmsim_scifi_file(in_file_path, timestamp_path):
    file = up.open(in_file_path)
    tree = file["cbmsim"]
    arrays = tree.arrays(["Digi_SciFiHits", "Digi_SciFiHits.fDetectorID", "Digi_SciFiHits.hitTime", "Digi_SciFiHits.fineTime"])
    total_size = tree.array("Digi_SciFiHits.fDetectorID", flatten=True).shape[0]
    ts_file = up.open(timestamp_path)
    ts_tree = ts_file["t1"]
    timestamps = ts_tree.array("fEventTime")
    n_events = ts_tree.array("fEventTime").shape[0]

    new_arr = np.full(fill_value=-1, dtype=[("event", np.int32), ("event_time", np.int64), ("scifi_detID", np.int32),
                                            ("scifi_hit_time", np.uint32), ("scifi_fine_time", np.uint16)], shape=total_size)

    events, detID, event_time, scifi_hit_time, scifi_fine_time = [], [], [], [], []

    for event in range(n_events):
        eventsize = arrays[b"Digi_SciFiHits"][event]
        events.extend([event]*eventsize)
        event_time.extend([timestamps[event]]*eventsize)
        detID.extend(arrays[b"Digi_SciFiHits.fineTime"][event])
        scifi_hit_time.extend(arrays[b"Digi_SciFiHits.hitTime"][event])
        scifi_fine_time.extend(arrays[b"Digi_SciFiHits.fineTime"][event])

    logging.info("write numpy arrays")
    new_arr["event"] = events
    new_arr["scifi_detID"] = detID
    new_arr["event_time"] = event_time
    new_arr["scifi_hit_time"] = scifi_hit_time
    new_arr["scifi_fine_time"] = scifi_fine_time

    return new_arr


def load_cbmsim_pixel_file(in_file_path, timestamp_path):
    file = up.open(in_file_path)
    tree = file["cbmsim"]
    arrays = tree.arrays(["Digi_PixelHits_1", "Digi_PixelHits_1.fDetectorID", "Digi_PixelHits_1.fdigi"])
    total_size = tree.array("Digi_PixelHits_1.fDetectorID", flatten=True).shape[0]
    ts_file = up.open(timestamp_path)
    ts_tree = ts_file["t1"]
    timestamps = ts_tree.array("fEventTime")
    n_events = ts_tree.array("fEventTime").shape[0]

    new_arr = np.full(fill_value=-1, dtype=[("event", np.int32), ("event_time", np.int64), ("pixel_detID", np.int32), ("tot", np.int32)], shape=total_size)

    events, detID, event_time, tot = [], [], [], []

    for event in range(n_events):
        eventsize = arrays[b"Digi_PixelHits_1"][event]
        events.extend([event]*eventsize)
        event_time.extend([timestamps[event]]*eventsize)
        tot.extend(arrays[b"Digi_PixelHits_1.fdigi"][event])
        detID.extend(arrays[b"Digi_PixelHits_1.fDetectorID"][event])

    logging.info("write numpy arrays")
    new_arr["event"] = events
    new_arr["pixel_detID"] = detID
    new_arr["event_time"] = event_time
    new_arr["tot"] = tot

    return new_arr


def save_cbmsim_array(new_arr, path):
    with tb.open_file(path, "w") as out_file_h5:
        hit_table_description = new_arr.dtype
        hit_table_out = out_file_h5.create_table(where=out_file_h5.root,
                                                 name='Hits',
                                                 description=hit_table_description, title='Hits to Charm Tracks',
                                                 filters=tb.Filters(
                                                     complib='blosc',
                                                     complevel=5,
                                                     fletcher32=False)
                                                 )
        hit_table_out.append(new_arr)


if __name__ == '__main__':
    pix_root_path = "/media/niko/big_data/charm_testbeam_july18/combined_data/run2793/SPILLDATA_8000_0522366760_20180730_172232.root"
    pix_ts_path = "/media/niko/big_data/charm_testbeam_july18/combined_data/s522366760_fEventTime.root"
    scifi_root_path = "/media/niko/big_data/charm_testbeam_july18/combined_data/scifi_data/RUN_0900_2793/SPILLDATA_0900_0522366760.rawdata.root"
    scifi_ts_path = "/media/niko/big_data/charm_testbeam_july18/combined_data/scifi_data/RUN_0900_2793/SPILLDATA_0900_0522366760_fEventTime.root"

    scifi_data = load_cbmsim_scifi_file(scifi_root_path, scifi_ts_path)
    save_cbmsim_array(scifi_data, scifi_root_path.split(".")[0] + "_scifi_hits.h5")

    # pix_data = load_cbmsim_pixel_file(pix_root_path, pix_ts_path)
    # save_cbmsim_array(pix_data, pix_root_path.split(".")[0] + "_pix_hits.h5")
