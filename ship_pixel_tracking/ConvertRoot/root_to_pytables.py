import logging
import os
import numpy as np
import tables as tb
import uproot as up
from tqdm import tqdm
from ship_pixel_tracking.tools.analysis_utils import save_table, sort_table

logging.basicConfig(level=logging.INFO, format="%(asctime)s - [%(levelname)s] %(message)s")

def load_root_file(in_file_root, tree_name = None):
    '''
    use uproot to read root file and get all available trees. If tree_name not specified return first found tree.
    If tree_name is given return only tree matching the name.
    ---------------
    INPUT:
        in_file_root : string
            path to root file
        tree_name : string, optional
            name of the tree to read, if not given, first found tree returned
    OUTPUT:
        dict : pointer ? to all arrays found in the tree
    '''
    trees = []
    root_file = up.open(in_file_root)
    for key in root_file.keys():
        tree_id = key.split(";")[0]# key.decode("utf-8").split(";")[0]
        if tree_name == None:
            trees.append(root_file[tree_id])
        elif tree_name == tree_id:
            trees.append(root_file[tree_id])
    if len(trees)==0:
        logging.warning("no tree with name %s found" % tree_name)
        return
    else:
        return [trees[array].arrays( library="np") for array in range(len(trees))]


def tree_to_numpy_array(arrays, n_events = None, total_size = None):
    '''
    Read data from given root tree and build numpy array. A bit stupid right now but works for simple arrays.
    For nTuples events and total size still need to be deterimend. WIP
    ----------------
    INPUT:
        arrays : dict
            dict with names of the arrays as keys in byte string and numpy array containing the data as values
        n_events : int
            number of events in the tree (important if tree branches have different size)
        total_size : int
            length of longest branch in tree
    OUTPUT :
        out_array : numpy structured array
            contains one column for every key in arrays
    '''
    out_arrays = []
    for array in arrays:
        dtype = []
        # get dtype from array
        for i, key in enumerate(array.keys()):
            # dtype.append((key.decode("utf-8"), array[key].dtype))
            dtype.append((key, array[key].dtype))
            if i == 0:
                total_size = array[key].flatten().shape[0]

        out_array = np.full(fill_value = np.nan, dtype = dtype, shape = (total_size,))
        
        for entry in tqdm(range(total_size)):
            for key in array.keys():
                # out_array[key.decode("utf-8")][entry] = array[key][entry]
                out_array[key][entry] = array[key][entry]
        out_arrays.append(out_array)

    return out_arrays


def save_tables(arrays_in, node_names, path):

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    
    with tb.open_file(path, "w") as out_file_h5:
        for i, array in enumerate(arrays_in):
            table_out = out_file_h5.create_table(where=out_file_h5.root, name=node_names[i], description=array.dtype,
                                                    filters=tb.Filters(complib='blosc', 
                                                    complevel=5, fletcher32=False)
                                                    )
            table_out.append(array)
            table_out.flush()
            logging.info("saved %s at %s" % (node_names[i], path))


if __name__ == "__main__":
    for spill in [8,9,10,11,12]:
        arrays = load_root_file('/Users/Niko/Desktop/charm_exp_2018/combined_data/emu_pix_matching/CH1_R6/CH1R6_Spill_%i.root' %spill, tree_name=None)
        out_arrays = tree_to_numpy_array(arrays)
        sort_table(out_arrays[0], columns = ["emu_vid","match_time", ])
        save_tables(out_arrays, path = '/Users/Niko/Desktop/charm_exp_2018/combined_data/emu_pix_matching/CH1_R6/CH1R6_Spill_%i.h5' %spill, node_names = ["MatchedTracks", "unmatched_emulsion", "unmatched_pixel"])