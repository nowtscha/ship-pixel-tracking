import subprocess
from ship_pixel_tracking.ConvertRoot.h5_to_root import ConvertH5Root


if __name__ == "__main__":

    in_file = "/home/niko/Desktop/simulation/simextract.h5"
    n_tuple_path = "/home/niko/Desktop/simulation/pix_tracks.root"
    outfile = in_file[:-2] + "root"
    call_string = "~/git/ship_tracking/ship_pixel_tracking/root_scripts/fairship_sim_to_tree/fairship_sim_to_tree.exe %s %s " %(outfile, n_tuple_path)

    converter = ConvertH5Root()
    converter.write_simple_root_file(in_file_h5=in_file, table_name="Hits")
    process1 = subprocess.Popen(call_string , shell=True)
    process1.wait()
    print("done")