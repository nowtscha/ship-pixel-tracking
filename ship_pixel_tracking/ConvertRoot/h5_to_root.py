from numba import njit
import os
import numpy as np
import tables as tb
import uproot
import subprocess

import logging
import warnings

from tqdm import tqdm
from beam_telescope_analysis.tools import geometry_utils, analysis_utils
from ship_pixel_tracking.tools.analysis_utils import load_config


@njit
def event_indices(table, event_numbers):
    indices = [0]
    index = 0
    for event_number in event_numbers:
        while table["event_number"][index] == event_number:
            index += 1
        indices.append(index)
        # event = table[table['event_number'] == event_number]
        # events.append(event)
    return indices


@njit
def _find_spills(array):
    # TODO: faster loop

    indices = []
    spills = 0
    for row in range(0, array.shape[0]-1):
        if (array[row+1]["trigger_time_stamp"] + 1) < array[row]["trigger_time_stamp"]:
            spills += 1
            indices.append(row + 1)
    return indices, spills


def find_new_spills(table, chunk_size):
    spill_indices = []
    indices = [0]
    spills = 0
    for data_chunk, index in analysis_utils.data_aligned_at_events(table, try_speedup=True, chunk_size=chunk_size, first_event_aligned=True, fail_on_missing_events=False):
        indices.append(index)
        chunk_indices, chunk_spills = _find_spills(data_chunk,)
        spills += chunk_spills
        chunk_indices_out = [(chunk_index + indices[-2]) for chunk_index in chunk_indices]
        spill_indices.extend(chunk_indices_out)
    print(spill_indices)
    return spill_indices, spills


def local_to_global_position(x, y, z = None, translation_x=None, translation_y=None, translation_z=None, rotation_alpha=None, rotation_beta=None, rotation_gamma=None):
    if isinstance(x, (list, tuple)) or isinstance(y, (list, tuple)):
        x = np.array(x, dtype=np.float64)
        y = np.array(y, dtype=np.float64)
    if z is None:
            z = np.zeros_like(x)
    elif isinstance(z, (list, tuple)):
        z = np.array(z, dtype=np.float64)
    # check for valid z coordinates
    if translation_x is None and translation_y is None and translation_z is None and rotation_alpha is None and rotation_beta is None and rotation_gamma is None and not np.allclose(np.nan_to_num(z), 0.0):
        raise RuntimeError('The local z positions contain values z!=0.')
    # apply DUT alignment
    transformation_matrix = geometry_utils.local_to_global_transformation_matrix(
        x=0. if translation_x is None else float(translation_x),
        y=0. if translation_y is None else float(translation_y),
        z=0. if translation_z is None else float(translation_z),
        alpha=0. if rotation_alpha is None else float(rotation_alpha),
        beta=0. if rotation_beta is None else float(rotation_beta),
        gamma=0. if rotation_gamma is None else float(rotation_gamma))
    return geometry_utils.apply_transformation_matrix(
        x=x,
        y=y,
        z=z,
        transformation_matrix=transformation_matrix)


class ConvertH5Root():

    def __init__(self):
        self.events = []
        self.table = None
        self.root_file = None
        self.root_tree = None

    logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - [%(levelname)-8s] (%(threadName)-10s) %(message)s")

    def transform_table_global(self, array, align_dict_in):

        align_dict = align_dict_in["TELESCOPE"]["DUT"]
        
        for plane in range(12):
            x_global, y_global, z_global = local_to_global_position(
            x=array["x_dut_%i" %plane], y=array["y_dut_%i" %plane], translation_x=align_dict[plane]["translation_x"], translation_y=align_dict[plane]["translation_y"], translation_z=align_dict[plane]["translation_z"], rotation_alpha=align_dict[plane]["rotation_alpha"], rotation_beta=align_dict[plane]["rotation_beta"], rotation_gamma=align_dict[plane]["rotation_gamma"])
            array["x_dut_%i" %plane] = x_global
            array["y_dut_%i" %plane] = y_global
            array["z_dut_%i" %plane] = z_global

        
    def read_table(self, in_file_name, table_name, no_tuples=True, spill = None, transform_global=False, align_yaml=None, chunk_size=100000):
        '''
        reads table and creates root file with one row per table rows.
        For fast and generic conversion to root.
        '''
        logging.info("reading %s for table \"%s\"" % (in_file_name, table_name))
        with tb.open_file(in_file_name, "r") as in_file:
            for table in in_file.walk_nodes('/', classname='Table'):
                if table.name == table_name:
                    cluster_table = table #[:]
                    logging.info("counting spills")
                    indices, nspills = find_new_spills(cluster_table, chunk_size)
                    logging.info("found %s spills" % (nspills+1))  # first spill is not counted in loop

            dtypes = []
            for dtype in cluster_table.description._v_types.items():
                dtypes.append(dtype)
            dtypes.extend([('spill', np.int32), ])
            spill_array = np.full(fill_value=np.nan, shape=cluster_table.shape, dtype=np.int32)

            # filling new table with spill values
            for i in range(nspills+1):
                if i == 0:
                    spill_array[0:indices[i+1]] = i
                elif i == nspills:
                    spill_array[indices[i-1]:]= i
                else:
                    spill_array[indices[i-1]:indices[i]] = i

            outfile_name = '_spills'
            logging.info('write ClusterTable with NSpills column')

            if transform_global:
                outfile_name = '_spills_global'
                logging.info("transforming to global coordinates")

            out_file_path_h5 = in_file_name.replace('.h5', outfile_name + '.h5')
            out_file_path_root = in_file_name.replace('.h5', outfile_name + '.root')

            with tb.open_file(in_file_name, "r") as in_file:
                for table in in_file.walk_nodes('/', classname='Table'):
                    if table.name == table_name:
                        cluster_table = table
                with tb.open_file(out_file_path_h5, 'w') as out_file,  uproot.recreate(out_file_path_root) as root_out_file:
                    hit_table_out = out_file.create_table(
                        where=out_file.root,
                        name='MergedClusters',
                        description=np.dtype(dtypes), title='Selected hits for test beam analysis, event number aligned at trigger_time_stamp',
                        filters=tb.Filters(
                            complib='blosc',
                            complevel=5,
                            fletcher32=False))
                    if transform_global:
                        align_dict = load_config(align_yaml)
                    for read_index in tqdm(range(0, cluster_table.nrows, chunk_size)):
                        cluster_chunk = cluster_table.read(read_index, read_index + chunk_size)
                        new_array_chunk = np.full(fill_value=np.nan, shape=cluster_chunk.shape, dtype=dtypes)
                        for column in new_array_chunk.dtype.names:
                            if column == "spill":
                                new_array_chunk[column] = spill_array[read_index: (read_index + chunk_size)]
                            else:   
                                new_array_chunk[column] = cluster_chunk[column]
                        if transform_global:
                            self.transform_table_global(new_array_chunk, align_dict)
                        hit_table_out.append(new_array_chunk)
                        hit_table_out.flush()
                        if read_index == 0:
                            root_out_file["Clusters"] = new_array_chunk
                        else:
                            root_out_file["Clusters"].extend(new_array_chunk)

                    logging.info("file saved at %s" % out_file_path_h5)

            # logging.info("creating generic root file")
           
            
            # with tb.open_file(out_file_path_h5, "r") as convert_file:
            #     convert_array = convert_file.root.MergedClusters[:]
            #     rnp.array2root(arr=convert_array, filename=out_file_path_root, mode="recreate")
            logging.info('saved 1D root file at %s' % out_file_path_root)
            return out_file_path_root

    def write_simple_root_file(self, in_file_h5, table_name, spill = None, chunk_size=100000):
        logging.info("reading %s" % in_file_h5)
        first_chunk = True
        with tb.open_file(in_file_h5, "r") as in_file:
            for table in in_file.walk_nodes('/', classname='Table'):
                if table.name == table_name:
                    cluster_table = table
            if spill:
                cluster_table = cluster_table[cluster_table["spill"]==spill]
            logging.info("creating generic root file")
            out_file_path_root = in_file_h5.replace('.h5', '.root')
            root_out_file = uproot.recreate(out_file_path_root)
            for read_index in tqdm(range(0, cluster_table.nrows, chunk_size)):
                cluster_chunk = cluster_table.read(read_index, read_index + chunk_size)
                if first_chunk == True:
                    root_out_file["table"] = cluster_chunk
                    first_chunk = False
                else:
                    root_out_file["table"].extend(cluster_chunk)
            logging.info('saved 1D root file at %s' % out_file_path_root)
        return out_file_path_root

    def merge_runs(self, merged_cluster_files, max_event_size=None, output_name=None, chunk_size=100000):
        '''
        Takes merged cluster files from several runs and merges them to one large file for analysis.

        Input:
        ------------
        merged_cluster_files : list
                files with Merged_spills.h5
        max_event_size : Int
                only events with nrows <= max_event_size will be written to output file
        output_name : str
                if given result file is saved there
        chunk_size : Int
                number of lines to read at once, to reduce memory usage for large files

        Output:
        ------------
        merged_runs_clustered.h5 :
                file of clusters of different runs. First 4 digits of event number identify original run.

        '''
        if output_name:
            out_file_name = output_name
        else:
            out_file_name = os.path.dirname(os.path.dirname(merged_cluster_files[0])) + "/Merged_runs.h5"

        with tb.open_file(merged_cluster_files[0], "r") as cluster_file_in:
            run_data = cluster_file_in.root.MergedClusters.read(0, 2)
            merged_array_dtype = run_data.dtype
        with tb.open_file(out_file_name, "w") as merged_out_file:
            hit_table_description = merged_array_dtype
            hit_table_out = merged_out_file.create_table(
                where=merged_out_file.root,
                name='MergedClusters',
                description=hit_table_description, title='Clusters of Merged Runs',
                filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            for run, merged_cluster_file in enumerate(merged_cluster_files):
                run_number = int(merged_cluster_file[merged_cluster_file.rfind("run_")+4:merged_cluster_file.rfind("run_")+8])
                evt_offset = int(run_number * 10e8)  # max number of events in a single run is 99 Million
                logging.info("loading run %s" % run_number)
                with tb.open_file(merged_cluster_file, "r") as cluster_file_in:
                    run_data = cluster_file_in.root.MergedClusters
                    for data_chunk, _ in tqdm(analysis_utils.data_aligned_at_events(run_data, chunk_size=chunk_size)):
                        if data_chunk["event_number"].max() > (99*10e6):
                            warnings.warn("========== too many events in run, new event number would be ambiguous ==========")
                            return
                        data_chunk["event_number"] += evt_offset
                        if max_event_size:
                            events, indices, inverse, counts = np.unique(data_chunk["event_number"], return_index=True, return_counts=True, return_inverse=True)
                            cut_events = events[counts > max_event_size]
                            new_events = indices[inverse[inverse <= max_event_size]]

                            # build new index array with only indices of event occurence < max_event_size
                            new_indices = []
                            new_counts = counts[counts <= max_event_size].tolist()
                            for i, index in enumerate(indices[counts <= max_event_size].tolist()):
                                j = 0
                                while j < new_counts[i]:
                                    new_indices.append(index+j)
                                    j += 1

                            data_chunk = data_chunk[new_indices]
                        hit_table_out.append(data_chunk)
                        hit_table_out.flush()

        logging.info("merged runs and saved table at %s" % out_file_name)
        return out_file_name


if __name__ == '__main__':

    runs = [2793, 2836, 2853] #2823, 2838, 2842, 2844, 2845, 2846, 2847, 2848, 2858, 2859, 2860, 2861, 2862]

    converter = ConvertH5Root()
    # converter.merge_runs(files, max_event_size = 6, output_name = '/media/niko/big_data/charm_testbeam_july18/analysis/1_brick_merged_runs.h5')

    matched_tracks = False

    for run in runs:
        in_file = "/mount/niko/big_data/charm_testbeam_july18/analysis_2022/output_folder_run_%s/Merged.h5" %run
        align_file = "/media/niko/big_data/charm_testbeam_july18/analysis/output_folder_local_run_2818/run2818_telescope_aligned_kalman.yaml" #%(run,run)
        outfile = converter.read_table(in_file_name=in_file, table_name='MergedClusters', no_tuples=True, transform_global=False, align_yaml=align_file ,chunk_size=100000)
        # outfile = converter.write_simple_root_file(in_file_h5 = in_file, table_name="MergedClusters", spill = 21)
        # call compiled c function in terminal to build n-tuple files make sure .exe is there
        n_tuple_path = outfile[:-len(outfile.split("/")[-1])] + "run" + in_file[in_file.find("run_") + 4:in_file.find("run_") + 8] + ".root"
        tuple_function = r'../root_scripts/vectors_to_tree/VectorsToTreeCharmx.exe %s %s' % (outfile, n_tuple_path)
        if matched_tracks:
            n_tuple_path = "/media/niko/big_data/charm_testbeam_july18/rpc_data/match_timestamps/matched_tracks_tuple.root"
            tuple_function = r'../root_scripts/matched_ntuple/BuildMatchedntuple.exe %s %s' % (outfile, n_tuple_path)
        # n_tuple_path = in_file.replace(".h5",".root")
        logging.info("creating N-tuple file at %s" % n_tuple_path)
        process1 = subprocess.Popen(tuple_function, shell=True)
        process1.wait()
        logging.info("======== finished ========")
    #     # outfile_global = converter.read_table(in_file_name = in_file, table_name= 'MergedClusters',no_tuples=True, transform_global = True)
    #     # n_tuple_global_path = outfile_global[:-len(outfile_global.split("/")[-1])] + "run" + in_file[in_file.find("run_") +4:in_file.find("run_") +8]  + "_global.root"
    #     # logging.info("creating N-tuple file with global coordinates at %s" % n_tuple_global_path)
    #     # process2 = subprocess.Popen(r'./root_scripts/vectors_to_tree/VectorsToTreeCharmx.exe %s %s' %(outfile_global, n_tuple_global_path ),shell=True)
    #     # process2.wait()
    #     # logging.info("finished with run %s" % in_file[in_file.find("run_")+4:in_file.find("run_")+8])
    # logging.info("finished processing merged run file")
