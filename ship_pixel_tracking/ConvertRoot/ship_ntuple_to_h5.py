import numpy as np
import uproot as up
import re
import os
import tables as tb
import logging

from tqdm import tqdm

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def convert_ship_ntuple(in_file):
    '''
        open combined ship ntuple file and create pytables file with one hit per row
    '''
    logging.info("loading root file %s" %in_file)
    file = up.open(in_file)

    tree = file["shippositions"]

    arrays = tree.arrays(['nhits','detID','posx','posy','posz','trackID','subdetector'], namedecode="utf-8")
    total_size = tree.array("posx",flatten=True).shape[0]
    n_events = tree.array("nhits",flatten=True).shape[0]

    new_arr = np.full(fill_value = np.nan,
                        dtype = [("event", np.int32),
                                ("detID", np.int32),
                                ("hitx", np.float32),
                                ("hity", np.float32),
                                ("hitz", np.float32),
                                ("trackID", np.int32),
                                ("subdetector", np.int32)],
                        shape = (total_size,))

    nhits, detID, hitx, hity, hitz, trackID, subdetector , events = [], [], [], [], [], [], [], []

    logging.info('create numpy array')

    for event in tqdm(range(0,n_events)):
        eventsize = arrays["posx"][event].shape[0]
        hitx.extend(arrays["posx"][event])
        hity.extend(arrays["posy"][event])
        hitz.extend(arrays["posz"][event])
        trackID.extend(arrays["trackID"][event])
        detID.extend(arrays["detID"][event])
        subdetector.extend(arrays["subdetector"][event])
        events.extend([event]*eventsize)

    logging.info("write numpy arrays")
    new_arr["hitx"] = hitx
    new_arr["hity"] = hity
    new_arr["hitz"] = hitz
    new_arr["trackID"] = trackID
    new_arr["detID"] = detID
    new_arr["subdetector"] = subdetector
    new_arr["event"] = events

    logging.info("save hits file")
    with tb.open_file(in_file.split(".")[0] + "_hits.h5","w") as out_file_h5:
        hit_table_description = new_arr.dtype
        hit_table_out = out_file_h5.create_table(where=out_file_h5.root,
                                                name='Hits',
                                                description=hit_table_description, title='Hits to Charm Tracks',
                                                filters=tb.Filters(
                                                    complib='blosc',
                                                    complevel=5,
                                                    fletcher32=False))
        hit_table_out.append(new_arr)

if __name__ == '__main__':
    # open_ship_ntuple("/Users/Niko/Desktop/charm_exp_2018/CH2R1_spill_10_fairship_positions.root")
    open_ship_ntuple("/media/niko/big_data/charm_testbeam_july18/combined_data/run2793/ntuples_522366830.root")
