import numpy as np
import uproot as up
import re
import os
import tables as tb
import logging
# import ROOT
# from ctypes import UShort_t
from tqdm import tqdm
import matplotlib.pyplot as plt
from ship_pixel_tracking.tools.analysis_utils import save_table, get_files

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(message)s")

"""
Script for converting Scifi root data to pytables file. The root nutple data is tree like organized, 
meaning one event has vectors of different length. Also for conversion it is necessary to know the tree name and the branch names.
The result will be a pytables .h5 file with a table where one row is one hit. Opening this will give a numpy structured array
"""


def open_scifi_clusters_root(in_file):
    '''
    Read in scifi clusters. Different number of hits per plane in one event.
    TriggerT is in 1 ns
    SciFi positions are saved in mm, while pixel is saved in cm. Therefore SciFi positions are converted to cm here.
    '''

    logging.info("loading file %s" % in_file)
    file = up.open(in_file)
    tree = file["clusters"]

    arrays = tree.arrays(["Eventnum", "TriggerT", "x", "y", "z", "plane", "ToT", "Spill", "TriggerID", "n_plane_hits", "cluster_size"], namedecode="utf-8")
    total_size = tree.array("ToT", flatten=True).shape[0]
    n_events = tree.array("Eventnum", flatten=True).shape[0]
    logging.info("found %s events with %s hits" % (n_events, total_size))

    new_arr = np.full(fill_value=np.nan,
                      dtype=[("event_number", np.int32),
                             ("trigger_shift", np.int64),
                             ("delta_trigger_shift", np.int64),
                             ("trigger_id", np.int32),
                             ("x", np.float64),
                             ("y", np.float64),
                             ("z", np.float64),
                             ("x1", np.float64), ("y1", np.float64), ("z1x", np.float64), ("z1y", np.float64),
                             ("x2", np.float64), ("y2", np.float64), ("z2x", np.float64), ("z2y", np.float64),
                             ("x3", np.float64), ("y3", np.float64), ("z3x", np.float64), ("z3y", np.float64),
                             ("x4", np.float64), ("y4", np.float64), ("z4x", np.float64), ("z4y", np.float64),
                             ("x5", np.float64), ("y5", np.float64), ("z5x", np.float64), ("z5y", np.float64),
                             ("x6", np.float64), ("y6", np.float64), ("z6x", np.float64), ("z6y", np.float64),
                             ("x7", np.float64), ("y7", np.float64), ("z7x", np.float64), ("z7y", np.float64),
                             ("x8", np.float64), ("y8", np.float64), ("z8x", np.float64), ("z8y", np.float64),
                             ("plane", np.int32),
                             ("tot", np.int32),
                             ("cluster_size", np.int32),
                             ("hitused", np.int16),
                             ("n_plane_hits", np.int32),
                             ("spill", np.int32)],
                      shape=(total_size,))

    x, y, z, plane, tot, spill, Eventnum, TriggerT, TriggerID, n_plane_hits, cluster_size = [], [], [], [], [], [], [], [], [], [], []

    for event in tqdm(range(0, n_events)):
        eventsize = arrays["ToT"][event].shape[0]
        x.extend(arrays["x"][event])
        y.extend(arrays["y"][event])
        z.extend(arrays["z"][event])
        n_plane_hits.extend(arrays["n_plane_hits"][event])
        plane.extend(arrays["plane"][event])
        tot.extend(arrays["ToT"][event])
        cluster_size.extend(arrays["cluster_size"][event])
        Eventnum.extend([arrays["Eventnum"][event]]*eventsize)
        TriggerID.extend([arrays["TriggerID"][event]]*eventsize)
        TriggerT.extend([arrays["TriggerT"][event]]*eventsize)
        spill.extend([arrays["Spill"][event]]*eventsize)

    new_arr["plane"] = plane
    new_arr["n_plane_hits"] = n_plane_hits
    new_arr["x"] = x
    new_arr["y"] = y
    new_arr["z"] = z
    new_arr["tot"] = tot
    new_arr["cluster_size"] = cluster_size
    new_arr["event_number"] = Eventnum
    new_arr["trigger_shift"] = TriggerT
    new_arr["spill"] = spill
    new_arr["trigger_id"] = TriggerID

    # remove empty values ( x, y = -10000.)
    new_arr["x"][new_arr["x"] == -10000.] = np.nan
    new_arr["y"][new_arr["y"] == -10000.] = np.nan

    # convert from mm to cm
    new_arr["x"] *= 0.1
    new_arr["y"] *= 0.1

    # fill x,y,z coordinate per plane for easier track fitting seems a bit unnecessary
    for plane in [1,2,7,8]:
        new_arr["x%s" % plane][new_arr["plane"]==plane] = new_arr["x"][new_arr["plane"]==plane]
        new_arr["z%sx" % plane][new_arr["plane"]==plane] = new_arr["z"][new_arr["plane"]==plane]      
    for plane in [3,4,5] :
        new_arr["y%s" % plane][new_arr["plane"]==plane] = new_arr["y"][new_arr["plane"]==plane]
        new_arr["z%sy" % plane][new_arr["plane"]==plane] = new_arr["z"][new_arr["plane"]==plane]

    # catch empty spill files
    if total_size > 0:
        # calculate difference to next timestamp
        new_arr["delta_trigger_shift"][1:] = np.diff(new_arr["trigger_shift"])
        # first entry of delta_trigger_shift is not useful since calculated from last timestamp of preceeding spill ... or empty in case of single spill file
        new_arr["delta_trigger_shift"][0] = new_arr["trigger_shift"][0]

    save_table(array_in=new_arr, path=in_file.split(".")[0] + ".h5", node_name="Clusters")
    logging.info("saved cluster file at %s" % in_file.split(".")[0] + ".h5")


def open_scifi_channel_root(in_file):
    '''
    Read in basic scifi root file with channel and conversion to x,y,z
    Trigger shift is the corrected timestamp, hit time is some sub timestamp timing for single hits.
    '''

    logging.info("loading file %s" % in_file)
    file = up.open(in_file)
    tree = file["scifi_out"]

    arrays = tree.arrays(["board_ID", "STiC_ID", "ch", "tot", "hit_time", "event_number",
                          "trigger_ID", "fine_time", "trigger_shift", "spill", "x", "y", "z"], namedecode="utf-8")
    total_size = tree.array("ch", flatten=True).shape[0]
    n_events = tree.array("event_number", flatten=True).shape[0]
    logging.info("found %s events with %s hits" % (n_events, total_size))

    new_arr = np.full(fill_value=np.nan,
                      dtype=[("event_number", np.int64),
                             ("trigger_shift", np.uint64),
                             ("delta_trigger_shift", np.int64),
                             ("x", np.float32),
                             ("y", np.float32),
                             ("z", np.float32),
                             ("BoardID", np.uint32),
                             ("plane", np.uint16),
                             ("STiCID", np.uint32),
                             ("Ch", np.uint32),
                             ("TOT", np.uint32),
                             ("HitTime", np.int64),
                             ("TriggerID", np.int64),
                             ("FineTime", np.uint32),
                             ("spill", np.int32)],
                      shape=(total_size,))

    BoardID, STiCID, Ch, TOT, HitTime, Eventnum, TriggerID, FineTime, TriggerT, Spillnum, x, y, z = [], [], [], [], [], [], [], [], [], [], [], [], []

    for event in tqdm(range(0, n_events)):
        eventsize = arrays["ch"][event].shape[0]
        BoardID.extend(arrays["board_ID"][event])
        STiCID.extend(arrays["STiC_ID"][event])
        Ch.extend(arrays["ch"][event])
        x.extend(arrays["x"][event])
        y.extend(arrays["y"][event])
        z.extend(arrays["z"][event])
        TOT.extend(arrays["tot"][event])
        HitTime.extend(arrays["hit_time"][event])
        Eventnum.extend([arrays["event_number"][event]]*eventsize)
        TriggerID.extend([arrays["trigger_ID"][event]]*eventsize)
        FineTime.extend(arrays["fine_time"][event])
        TriggerT.extend([arrays["trigger_shift"][event]]*eventsize)
        Spillnum.extend([arrays["spill"][event]]*eventsize)

    logging.info("write numpy array")

    new_arr["BoardID"] = BoardID
    new_arr["plane"] = (new_arr["BoardID"]-1)//3
    new_arr["STiCID"] = STiCID
    new_arr["Ch"] = Ch
    new_arr["x"] = x
    new_arr["y"] = y
    new_arr["z"] = z
    new_arr["TOT"] = TOT
    new_arr["HitTime"] = HitTime
    new_arr["event_number"] = Eventnum
    new_arr["TriggerID"] = TriggerID
    new_arr["FineTime"] = FineTime
    new_arr["trigger_shift"] = TriggerT*25
    new_arr["spill"] = Spillnum

    # remove empty values ( x, y = 10000.)
    new_arr["x"][new_arr["x"] == -10000.] = np.nan
    new_arr["y"][new_arr["y"] == -10000.] = np.nan
    # calculate difference to next timestamp
    new_arr["delta_trigger_shift"][1:] = np.diff(new_arr["trigger_shift"])
    # first entry of delta_trigger_shift is not useful since calculated from last timestamp of preceeding spill ... or empty in case of single spill file
    new_arr["delta_trigger_shift"][0] = new_arr["trigger_shift"][0]

    save_table(array_in=new_arr, path=in_file.split(".")[0] + "_hits.h5", node_name="Hits")
    logging.info("saved hits file at %s" % in_file.split(".")[0] + "_hits.h5")


if __name__ == "__main__":

    # scifi_files = ["/Users/Niko/git/scifi-offline/results/522521060_clusters.root",
    #                "/Users/Niko/git/scifi-offline/results/522521130_clusters.root",
    #                "/Users/Niko/git/scifi-offline/results/522521205_clusters.root",
    #                "/Users/Niko/git/scifi-offline/results/522521275_clusters.root", 
    #                "/Users/Niko/git/scifi-offline/results/522521350_clusters.root"]
    # for scifi_file in scifi_files:
    #     spill = scifi_file[scifi_file.find("_1F")+1:scifi_file.find("_1F")+9]
    #     logging.info("spill %s = %i" %(spill, int(spill,16)))
    #     # open_scifi_channel_root("/media/niko/big_data/charm_testbeam_july18/combined_data/scifi_offline/2863_spill_1F27EDDA.root")
    #     open_scifi_channel_root(scifi_file)
    
    cluster_files = get_files(folder = "/Users/Niko/git/scifi-offline/results/run_2828", string = "clusters.root")
    
    for scifi_file in cluster_files:
        open_scifi_clusters_root(scifi_file)
