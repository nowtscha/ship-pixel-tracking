import uproot as up
import numpy as np
import os
from ship_pixel_tracking.tools.analysis_utils import save_table, load_table
from tqdm import tqdm


emu_tracks_dtype = [("trackID", np.int32), ("x", np.float32), ("y", np.float32), ("z", np.float32), ("tx", np.float32), ("ty", np.float32), ("x2", np.float32), ("y2", np.float32), ("z2", np.float32), ("vID", np.int32), ("vx", np.float32), ("vy", np.float32), ("vz", np.float32), ("nTracks", np.int32), ("quarter", np.int32), ("nseg", np.int32), ("track_chi2", np.float32)]

emu_vtx_dtype = [("trackID", np.int32), ("x", np.float32), ("y", np.float32), ("z", np.float32), ("tx", np.float32), ("ty", np.float32), ("x2", np.float32), ("y2", np.float32), ("z2", np.float32), ("vID", np.int32), ("vx", np.float32), ("vy", np.float32), ("vz", np.float32), ("nTracks", np.int32), ("quarter", np.int32), ("nseg", np.int32), ("track_chi2", np.float32), ("vtx_prob", np.float32)]


def load_emu_tracks(in_file_path):
    in_file = up.open(in_file_path)
    emu_tracks = in_file["tracks"]
    arrays = emu_tracks.arrays(["t.eX", "t.eY", "t.eZ", "t.eTX", "t.eTY", "trid"], library="np")

    out_array = np.full(fill_value=np.nan, shape=arrays["trid"].shape, dtype=emu_tracks_dtype)
    for i, track in enumerate(tqdm(arrays["trid"])):
        out_array[i]["trackID"] = track
        out_array[i]["x"] = arrays["t.eX"][i]*1e-4
        out_array[i]["y"] = arrays["t.eY"][i]*1e-4
        out_array[i]["z"] = arrays["t.eZ"][i]*1e-4
        out_array[i]["tx"] = arrays["t.eTX"][i]*1e3
        out_array[i]["ty"] = arrays["t.eTY"][i]*1e3

    save_table(out_array, in_file_path[:-4] + "h5", node_name="EmuTracks")
    

def load_emu_tracks_sf(in_file_path):
    in_file = up.open(in_file_path)
    emu_tracks = in_file["tracks"]
    arrays = emu_tracks.arrays(["sf.eX", "sf.eY", "sf.eZ", "sf.eTX", "sf.eTY", "trid", "nseg", "sf.eChi2"], library="np")


    out_array = np.full(fill_value=np.nan, shape=arrays["trid"].shape, dtype=emu_tracks_dtype)
    for i, track in enumerate(tqdm(arrays["trid"])):
        out_array[i]["trackID"] = track
        out_array[i]["nseg"] = arrays["nseg"][i]
        out_array[i]["track_chi2"] = arrays["sf.eChi2"][i][0] # seems to be chi2/ndf
        out_array[i]["x"] = arrays["sf.eX"][i][-1]*1e-4
        out_array[i]["y"] = arrays["sf.eY"][i][-1]*1e-4
        out_array[i]["z"] = arrays["sf.eZ"][i][-1]*1e-4
        out_array[i]["x2"] = arrays["sf.eX"][i][0]*1e-4
        out_array[i]["y2"] = arrays["sf.eY"][i][0]*1e-4
        out_array[i]["z2"] = arrays["sf.eZ"][i][0]*1e-4
        out_array[i]["tx"] = arrays["sf.eTX"][i][-1] #*1e3
        out_array[i]["ty"] = arrays["sf.eTY"][i][-1] #*1e3

    save_table(out_array, in_file_path[:-5] + "_sf.h5", node_name="EmuTracks")


def load_emu_vertex_info(in_file_path):
    """
    Get vertices and all tracks associated to the vertices. Uses uproot to loop through the TrackID information.
    The table created contains also columns for the end position of every track, but these are not yet filled here.
    While the information is in principle available in this file, it is not logically connected to the vertex and not easily accessible.
    But using the TrackID one can merge the vertex information with the already existing track segment positions in a seperate function.
    """
    in_file = up.open(in_file_path)
    emu_tracks = in_file["vtx"]
    arrays = emu_tracks.arrays(["vx", "vy", "vz", "sf.eX", "sf.eY", "sf.eZ", "sf.eTX", "sf.eTY", "TrackID", "vID", "n", "nseg", "probability"], library="np")
    n_vertices = arrays["vID"].shape[0]
    
    quarter = 0
    if "firstquarter" in in_file_path:
        quarter = 1
    elif "secondquarter" in in_file_path:
        quarter = 2
    elif "thirdquarter" in in_file_path:
        quarter = 3
    elif "fourthquarter" in in_file_path:
        quarter = 4

    trackIDs = emu_tracks["TrackID"].array(library="np")
    total_size =0
    for vtx in trackIDs:
        total_size +=vtx.size

    vx, vy, vz, TrackID, vID, nTracks, prob = [],[],[],[],[],[],[]

    out_array = np.full(fill_value=np.nan, shape=(total_size,), dtype=emu_vtx_dtype)
    for vertex in tqdm(range(0,n_vertices)):
        vtx_len = arrays["n"][vertex]
        TrackID.extend(arrays["TrackID"][vertex])
        
        vID.extend([arrays["vID"][vertex]]*vtx_len)

        vx.extend([arrays["vx"][vertex]*1e-4]*vtx_len)
        vy.extend([arrays["vy"][vertex]*1e-4]*vtx_len)
        vz.extend([arrays["vz"][vertex]*1e-4]*vtx_len)
        nTracks.extend([vtx_len]*vtx_len)
        prob.extend([arrays["probability"][vertex]]*vtx_len)

    out_array["vID"] = vID
    out_array["trackID"] = TrackID
    out_array["vx"] = vx
    out_array["vy"] = vy
    out_array["vz"] = vz
    out_array["quarter"] = quarter
    out_array["nTracks"] = nTracks
    out_array["vtx_prob"] = prob
    out_array = out_array[out_array["vz"]<0.]

    save_table(out_array, in_file_path[:-5] + "_trackID.h5", node_name="EmuVtx")


def merge_vtx_sf(segment_file_in, vertex_file_in):
    """
    Due to datastructure of vertex file it is easier and faster to combine the information of the already existing segments file with the vertex position and ID by looping over the trackID
    """
    segments = load_table(in_file = segment_file_in, table_name="EmuTracks")
    vertices = load_table(in_file=vertex_file_in, table_name="EmuVtx")
    # sort both tables by common info and loop over the shorter one.
    segments.sort(order = "trackID")
    vertices.sort(order = "trackID")
    i = 0
    for vertex in tqdm(vertices):
        while (segments[i]["trackID"] != vertex["trackID"]) and (i < (segments.shape[0]-1)):
            i += 1
        if segments[i]["trackID"] == vertex["trackID"]:
            for column in ["vx","vy","vz","nTracks","vID", "quarter"]:
                segments[column][i] = vertex[column]
    out_file_path = segment_file_in[:-3] + "_trackID_vtx_merged.h5"
    save_table(segments, out_file_path, node_name="EmuTracks")
    return out_file_path


def merge_quarters(in_files = [], out_file_name = "CH1_R6_vertices_merged.h5"):
    out_path = os.path.dirname(os.path.realpath(in_files[0])) + "/" + out_file_name
    for i, q_in in enumerate(in_files):
        quarter = load_table(q_in, table_name="EmuVtx")
        append = True
        if i == 0:
            append = False
        save_table(quarter, path = out_path, node_name="EmuVtx", append= append)


if __name__ == "__main__":
    load_emu_tracks_sf("/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks.root" )
    quarter_files = ["/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/vertextree_firstquarter.root", "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/vertextree_secondquarter.root",
                    "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/vertextree_thirdquarter.root", "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/vertextree_fourthquarter.root"]
    for in_file in quarter_files:
        load_emu_vertex_info(in_file)
    merge_quarters([quarter_file[:-5] + "_trackID.h5" for quarter_file in quarter_files])
    merge_vtx_sf("/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf.h5", "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6_vertices_merged.h5" )



