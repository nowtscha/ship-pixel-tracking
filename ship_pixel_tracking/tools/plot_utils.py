import numpy as np
import tables as tb
import os.path
import logging

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.backends.backend_agg import FigureCanvas
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.artist import setp
from matplotlib.figure import Figure
import matplotlib.patches
from matplotlib.collections import LineCollection, PolyCollection
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from matplotlib import colors, cm

from beam_telescope_analysis.tools import analysis_utils
from ship_pixel_tracking.tools.analysis_utils import circle, red_chisquare, fit_gauss, _mask_empty_bins, fit_boxfc, boxfc

def plot_1d_bars_ordererd_stacking(hists_in, output_pdf, bar_edges, legends, xlim=None, ylim = None, xlabel=None, 
                                    ylabel = None, ax=None, fig=None, title=None, log = False,
                                    fontsize = 12,output_png = None):

    # from https://stackoverflow.com/questions/44309507/stacked-bar-plot-using-matplotlib
    # changed to remove summing up of all entries for one bar

    hists_in = np.array(hists_in)
    cmap_rgb = matplotlib.cm.get_cmap('viridis')(np.linspace(0,1,hists_in.shape[1]+1))
    cmap = [matplotlib.colors.rgb2hex(cmap_rgb[i]) for i in range(hists_in.shape[1])]
    if ax is None:
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True
    
    indexes = np.argsort(hists_in).T
    
    heights = np.sort(hists_in).T
    order = -1
    # bottoms = heights[::order].cumsum(axis=0)
    # bottoms = np.insert(bottoms, 0, np.zeros(len(bottoms[0])), axis=0)
    mpp_colors = dict(zip(legends, cmap))
    # for btms, (idxs, vals) in enumerate(list(zip(indexes, heights))[::order]):
    for (idxs, vals) in list(zip(indexes, heights))[::order]:
        mps = np.take(np.array(legends), idxs)
        colors = [mpp_colors[m] for m in mps]
        ax.bar(bar_edges[:-1], height=vals, color=colors, width = np.diff(bar_edges), align="edge")
    if xlabel:
        ax.set_xlabel(xlabel, fontsize = fontsize)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize = fontsize)
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    if title:
        ax.set_title(title)
    if log:
        ax.set_yscale('log')
    ax.legend((np.take(np.array(legends), np.argsort(hists_in)[0]))[::order], prop={'size': fontsize-4})
    if save is True:
        output_pdf.savefig(fig)
        if output_png:  
            fig.savefig(output_png, dpi=300)


def plot_1d_hist(array_in, output_pdf, bins=None, range=None, legend=None, title=None, ax=None, fig=None,
                xlabel=None, log=False, output_png = None):
    save = False
    if not range:
        range = [array_in.min(), array_in.max()]
    if ax is None:
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True
    hist, edges, _ = ax.hist(array_in, bins = bins, range = range, log = log, label = legend)
    ax.grid()
    if xlabel:
        ax.set_xlabel(xlabel)
    if legend:
        ax.legend(prop={'size': 8})
    if title:
        ax.set_title(title)
    if save is True:
        output_pdf.savefig(fig)
        if output_png:
            fig.savefig(output_png, dpi=1000)
    return hist, edges


def plot_1d_bar(hist_in, output_pdf, bar_edges, xlim=None, ylim = None, xlabel=None, ylabel = None, ax=None, fig=None,
                title=None, legend = None, yerr = None, error_kw= None, capsize = None, log = False, mask_empty_bins = True, linewidth = 0.005, fontsize = 12, edge_color = "k",
                output_png = None, vlines=None, gauss_fit=False, box_fit = False, fit_lim = None, gauss_unit = "$\mu m$"):
    save = False
    
    cmap_pick = cm.get_cmap('viridis')
    # colors David in RGB HEX:
    # 1: #51A686
    # 2: #41768C
    # 3: #414383
    # 4: #3D0851
    # color_1 = "#41768C" #cmap_pick(0.2)
    # color_2 = "#1e9b88" #cmap_pick(0.55)
    # color_3 = "#414383" #cmap_pick(0.85)
    # color_4 = "#3D0851"
    color_1 = cmap_pick(0.2)
    color_5 = cmap_pick(0.375)
    color_2 = cmap_pick(0.55)
    color_3 = cmap_pick(0.75)
    color_4 = cmap_pick(0.9)


    if ax is None:
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True
    if error_kw:
        ax.bar(bar_edges[:-1], hist_in, width = np.diff(bar_edges), align = "edge", 
            label = legend, yerr =yerr, error_kw = error_kw, capsize= capsize, color = color_2)
    else:
        ax.bar(bar_edges[:-1], hist_in, width = np.diff(bar_edges), align = "edge", 
            label = legend, yerr =yerr , edgecolor = edge_color, linewidth = linewidth, color = color_2)
    ax.grid()
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize = fontsize)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize = fontsize)
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    if title:
        ax.set_title(title, fontsize = fontsize)
    if log:
        ax.set_yscale('log')
    # else:
    #     ax.ticklabel_format(style = "plain")
    if vlines:
        legend=True
        for i,line in enumerate(vlines):
            if i==0:
                ax.axvline(x=line, color = color_3, linestyle = "-", label = r"cut limit = %.1f " %line)
            else:
                ax.axvline(x=line, color = color_3, linestyle = "-")
    if gauss_fit:
        bin_center = (bar_edges[1:] + bar_edges[:-1]) / 2.0
        if fit_lim:
            hist_in = hist_in[np.logical_and(bin_center >= fit_lim[0], bin_center <= fit_lim[1])]
            bin_center = bin_center[np.logical_and(bin_center >= fit_lim[0], bin_center <= fit_lim[1])]
        hist_in = hist_in[1:-1]
        bin_center = bin_center[1:-1]
        if mask_empty_bins:
            hist_in, bin_center = _mask_empty_bins(hist_in, bin_center)
        # bin_center = (bar_edges[1:] + bar_edges[:-1]) / 2.0
        # bin_center = bin_center[2:-2]
        # mean = analysis_utils.get_mean_from_histogram(hist_in[2:-2], bin_center)
        # rms = analysis_utils.get_rms_from_histogram(hist_in[2:-2], bin_center)
        if hist_in.shape[0] > 0:
            x_gauss = np.linspace(np.min(bin_center), np.max(bin_center), num=1000)
            # popt, pcov = curve_fit(analysis_utils.gauss, bin_center, hist_in[2:-2], p0=[np.amax(hist_in[2:-2]), mean, rms] )
            # perr = np.sqrt(np.diag(pcov))
            # red_chi2 = red_chisquare(hist_in[2:-2], analysis_utils.gauss(bin_center, *popt), np.sqrt(hist_in[2:-2]), popt, empty_hist_bins = True)
            popt, pcov, reduced_chi2 = fit_gauss(hist_in, bin_center, empty_bins_in_chi2 = False)
            gauss_fit_legend_entry = 'Gauss fit: \n$\mathrm{A}=%.1f\pm %.1f$\n$\mathrm{\mu}=%.2f\pm %.2f$ %s \n$\mathrm{\sigma}=%.2f\pm %.2f$ %s \n$\mathrm{\chi^2/ndf} = %.3f$' % (popt[0], np.sqrt(pcov[0][0]), popt[1], np.sqrt(pcov[1][1]), 
                                        gauss_unit, np.absolute(popt[2]),  np.sqrt(pcov[2][2]), gauss_unit, reduced_chi2)
            ax.plot(x_gauss, analysis_utils.gauss(x_gauss, *popt), label = gauss_fit_legend_entry, 
                    color = color_1, linewidth = linewidth*300 , solid_capstyle='round', alpha = 1)
            legend=True
        else:
            popt = [float("inf"), float("inf"), float("inf")]
            pcov = np.full(fill_value = float("inf"), shape = (3,3))
            reduced_chi2 = float("inf")
            logging.warning("empty histogram, no fit performed")
        if log:
            # hack to cope with ax.bar issue for empty bars in log scale
            ax.set_ylim([1, hist_in.max()*1.1])
    if box_fit:
        bin_center = (bar_edges[1:] + bar_edges[:-1]) / 2.0
        if fit_lim:
            hist_in = hist_in[np.logical_and(bin_center >= fit_lim[0], bin_center <= fit_lim[1])]
            bin_center = bin_center[np.logical_and(bin_center >= fit_lim[0], bin_center <= fit_lim[1])]
        hist_in = hist_in[1:-1]
        bin_center = bin_center[1:-1]
        if mask_empty_bins:
            hist_in, bin_center = _mask_empty_bins(hist_in, bin_center)
        # bin_center = (bar_edges[1:] + bar_edges[:-1]) / 2.0
        # bin_center = bin_center[2:-2]
        # mean = analysis_utils.get_mean_from_histogram(hist_in[2:-2], bin_center)
        # rms = analysis_utils.get_rms_from_histogram(hist_in[2:-2], bin_center)
        if hist_in.shape[0] > 0:
            x_gauss = np.linspace(np.min(bin_center), np.max(bin_center), num=1000)
        popt, pcov, reduced_chi2 = fit_boxfc(bin_center, hist_in, empty_bins_in_chi2 = False)
        # TODO: correct sigmas for histogram ?
        # reduced_chi2 = red_chisquare(hist_in, bta_ana_utils.gauss(bin_center, *popt), np.sqrt(hist_in), popt, use_empty_bins = empty_bins_in_chi2)
        box_fit_legend_entry = 'Box fit: \n$\mathrm{A}=%.1f\pm %.1f$\n$\mathrm{center}=%.2f\pm %.2f$ %s \n$\mathrm{width}=%.2f\pm %.2f$ %s \n$\mathrm{\sigma}=%.2f\pm %.2f$ %s \n$\mathrm{\chi^2/ndf} = %.3f$' % (popt[0], np.sqrt(pcov[0][0]), popt[1], np.sqrt(pcov[1][1]), 
                                        gauss_unit, np.absolute(popt[2]),  np.sqrt(pcov[2][2]), gauss_unit, np.absolute(popt[3]),  np.sqrt(pcov[3][3]), gauss_unit, reduced_chi2)
        ax.plot(x_gauss, boxfc(x_gauss, *popt), label = box_fit_legend_entry, 
                    color = color_1, linewidth = linewidth*300 , solid_capstyle='round', alpha = 1)
        if log:
            # hack to cope with ax.bar issue for empty bars in log scale
            ax.set_ylim([1, hist_in.max()*1.1])
    
    if legend:
        if ax is None:
            ax.legend(prop={'size': fontsize-4})
        else:
            ax.legend(prop={'size': fontsize})
    if save is True:
        output_pdf.savefig(fig)
        if output_png:
            fig.savefig(output_png, dpi=1000)
    if gauss_fit:
        return popt, pcov, reduced_chi2


def plot_2d_hist(hist2d, output_pdf, bins, xlabel=None, ylabel=None, zlabel = None, title=None, grid = False, box = None, mean_eff = None, ax=None, fig=None, print_bin_values = False, force_int_z_ticks = True,
                output_png=None, zlog=False, min_hits=None, limits=None, scale_large_pixels = False, invert_x_axis = False, cmap_upper_lim = 1., tight_layout = True, fontsize = 12):
    '''
    plot a 2d histogram with ax.imshow().
    ------------------
    INPUT:
        hist2d: np.array with histogram data
            histogram with x and y values (in this order)
        output_pdf: pdf_pages object
        bins: list or other iterable with 2 entries
            first entry are xbins of histogram, second entry are ybins
        xlabel: string
            text for x axis
        ylabel: string
            text for y axis
        zlabel: string
            text for colorbar
        title: string
            plot title
        box: bool
            if true make extra box with text in plot 
        mean_eff: list or other iterable with 2 entries
            calculated mean efficiency and error
        ax: matplotlib axes
            if given use the given axis for plotting in this case also fig needs to be given, otherwise make new one
        fig: matplotlib figure
            see above
        output_png: string
            if given create an png under given location
        zlog: bool
            if true use log scale for z values
        min_hits: int
            if given mask histogram where entries are below min_hits
        limits: list or other iterable with 2 entries
            if given use first entry as minimum z and second entry as maximum
        scale_large_pixels: bool
            if True scale occupancy of larger pixel (center and edeg) according to geometrical ratio 250/450 
        invert_x_axis: bool
            if True positive x is on left side and negative on right side, in other words x is increasing from right to left
    '''

    save = False
    full_cmap = cm.get_cmap("viridis")
    cmap = colors.LinearSegmentedColormap.from_list("trunc({n},{a:.2f},{b:.2f})".format(
        n=full_cmap.name, a=0, b=cmap_upper_lim), full_cmap(np.linspace(0, cmap_upper_lim, 256)))
    cmap.set_bad('w')
    
    if min_hits:
        hist2d = np.ma.array(hist2d, mask=hist2d<min_hits)
    if limits:
        zmin = limits[0]
        zmax = limits[1]
    else:
        zmin = np.min(hist2d)
        zmax = np.max(hist2d)
        if zmin==zmax:
            # zmin = zmax-1
            zmax = zmin + 1
    levels = MaxNLocator(nbins=256).tick_values(zmin, zmax)
    norm = BoundaryNorm(levels, ncolors=256, clip=True)

    bounds = np.linspace(start=zmin, stop=zmax, num=256, endpoint=True)
    ticks = np.linspace(start=zmin, stop=zmax, num=11, endpoint=True)
    if force_int_z_ticks:
        # ticks = np.around(ticks)
        colorbar_format = "%d"
    else : 
        colorbar_format = None
    if zlog:
        if zmin==0:
            zmin = 0.1
        norm = colors.LogNorm(vmin=zmin, vmax=zmax)
        bounds = np.logspace(start=zmin, stop=zmax, num=256, endpoint=True)
        ticks = None # np.logspace(start=zmin, stop=zmax, num=11, endpoint=True)
        colorbar_format = None
    if scale_large_pixels:
        hist2d[:,79:81] = hist2d[:,79:81] * 250/450.
        hist2d[:,0] = hist2d[:,0] * 250/500.
        hist2d[:,-1] = hist2d[:,-1] * 250/500.
    if ax is None:
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True

    im = ax.pcolormesh(bins[0], bins[1], hist2d.T, edgecolors='None', snap=True, cmap=cmap, norm=norm, rasterized=True)
    
    if title:
        ax.set_title(title, fontsize = fontsize)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize = fontsize)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize = fontsize)
    if invert_x_axis:
        ax.set_xlim(bins[0].max(),bins[0].min())
    if grid:
        ax.grid()
    # fig.colorbar(im, boundaries=bounds, ticks=ticks, fraction=0.04, pad=0.05)
    # ax.grid()
    if box :
        rect = matplotlib.patches.Rectangle(xy=(min(box[0]), min(box[1])), width=np.abs(np.diff(box[0])), height=np.abs(np.diff(box[1])), linewidth=1.5, edgecolor="crimson", facecolor='none', alpha=0.75)
        ax.add_patch(rect)
        ax.text(np.sum(box[0]) / 2.0, np.sum(box[1]) / 2.0, 'Region \nefficiency:\n%.2f%%\n%i pixel' % (mean_eff[0] * 100.0, mean_eff[1]), horizontalalignment='center', verticalalignment='center', fontsize=int(fontsize*0.75))
        # rect = matplotlib.patches.Rectangle(xy=(min(dut_extent[:2]), min(dut_extent[2:])), width=np.abs(np.diff(dut_extent[:2])), height=np.abs(np.diff(dut_extent[2:])), linewidth=mesh_line_width, edgecolor=mesh_color, facecolor='none', alpha=mesh_alpha)
        # ax.add_patch(rect)
    cbar = fig.colorbar(im, cmap=cmap, ticks = ticks, norm=norm, format = colorbar_format, fraction=0.04, pad=0.05)
    cbar.ax.tick_params(labelsize = fontsize)
    cbar.set_label(zlabel, fontsize = fontsize)
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    if tight_layout:
        fig.tight_layout()
    # done like in https://stackoverflow.com/questions/43538581/printing-value-in-each-bin-in-hist2d-matplotlib
    if print_bin_values:
        for i in range(len(bins[1])-1):
            for j in range(len(bins[0])-1):
                    ax.text(bins[0][j]+0.5,bins[1][i]+0.5, hist2d.T[i,j].astype(np.int32), 
                            color="w", ha="center", va="center", fontweight="bold", fontsize=int(fontsize*0.75))
    if save is True:
        output_pdf.savefig(fig, dpi=1000)
        if output_png:
            fig.savefig(output_png, dpi=1000)


def plot_xy(x, y,output_pdf, x2 = None, y2=None, xlim=None, xlabel=None, ylim=None, ylabel=None, title=None, ax=None, fig=None,
            legend=None, legend2 = None, output_png=None, markersize = 1, linfit = False, fit_data = None, fontsize = 12, color1=None, color2=None, alpha2=0.7, linestyle = "None", linewidth = 2):
    save = False
    if ax is None:
        fig = Figure()
        fig.tight_layout()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True
    ax.grid()

    if color1 is None or color2 is None:
        cmap_pick = cm.get_cmap('viridis')
        color_1 = cmap_pick(0.2)
        color_5 = cmap_pick(0.375)
        color_2 = cmap_pick(0.55)
        color_3 = cmap_pick(0.75)
        color_4 = cmap_pick(0.9)

        color1=color_2
        color2= color_4

    ax.plot(x, y, marker = "o" , markersize = markersize, linestyle = linestyle, rasterized = True, label = legend, color = color1, linewidth = linewidth)
    if title:
        ax.set_title(title)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize = fontsize)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize = fontsize)
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    if y2 is not None:
        ax.bar(x2[:-1], y2, width = np.diff(x2), align = "edge", label = legend2, alpha = alpha2, color = color2)
    if linfit:
        try : 
            popt, pcov = curve_fit(analysis_utils.linear, fit_data[0], fit_data[1], p0 = (0., -2000.))
            perr = np.sqrt(np.diag(pcov))
            ax.plot(fit_data[0], analysis_utils.linear(fit_data[0], *popt), label = "fit: (%.f $\pm$ %.f) * x + (%.2f $\pm$ %.2f)" %(popt[1], perr[1], popt[0], perr[0]), 
                    color = "crimson", linewidth = 0.95 , solid_capstyle='round')
        except :
            logging.warning("could not fit line")
            popt = [float("inf"),float("inf")]
            pcov = [float("inf"),float("inf")]
    if legend:
        if ax is None:
            ax.legend(prop={'size': 8},loc = "upper right")
        else:
            ax.legend(prop={'size': fontsize})
    if ax is None:
        fig.tight_layout()

   
    if save is True:
        output_pdf.savefig(fig, dpi=1000)
        if output_png:
            fig.savefig(output_png, dpi=1000)
    if linfit:
        return popt, pcov


def plot_xy_2axis(x, y, hist_in, bar_edges, output_pdf, xlim=None, xlabel=None, ylim=None, y1log = False, ax=None, fig=None,
                    ylabel=None, y2label = None, y2lim = None, y2log = False, title=None, legend=None, color_ax1 = None, 
                    color_ax2 = None, edge_color = "darkorange", linestyle = "None", linewidth = 2, color_labels = True,
                    yerr = None,
                    legend2 = None, alpha1 = 0.85, alpha2 = 0.7, output_png=None, markersize = 3, linfit = False, fit_data = None, fontsize = 8):
    
    cmap_pick = cm.get_cmap('viridis')
    color_1 = cmap_pick(0.2)
    color_5 = cmap_pick(0.375)
    color_2 = cmap_pick(0.55)
    color_3 = cmap_pick(0.75)
    color_4 = cmap_pick(0.9)

    if color_ax1 is None:
        color_ax1 = color_2
    if color_ax2 is None:
        color_ax2 = color_4
        edge_color = color_4
    
    save = False
    if ax is None:
        fig = Figure()
        fig.tight_layout()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True
    ax.grid()
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    if yerr is not None:
        lns1 = ax.errorbar(x,y,yerr = yerr, marker = "o" , ecolor = color_ax1, capsize = linewidth, capthick=linewidth, 
                            elinewidth = linewidth, markersize = markersize, linestyle = linestyle, linewidth = linewidth, 
                            label = legend, color = color_ax1, alpha = alpha1
                            )
    else:
        lns1 = ax.plot(x,y, marker = "o" , markersize = markersize, linestyle = linestyle, linewidth = linewidth, label = legend, 
                        color = color_ax1, markeredgewidth = 0, alpha = alpha1
                        )
        lns1 = lns1[0]
    if title:
        ax.set_title(title, fontsize=fontsize)
    if xlabel:
        ax.set_xlabel(xlabel, fontsize = fontsize)
    if ylabel:
        if color_labels:
            axcolor = color_ax1
        else:
            axcolor = "k"
        ax.set_ylabel(ylabel, color = axcolor, fontsize = fontsize )
    ax.tick_params(axis='both', which='major', labelsize=fontsize)
    if linfit:
        popt, pcov = curve_fit(analysis_utils.linear, fit_data[0], fit_data[1], p0 = (0., -2000.))
        perr = np.sqrt(np.diag(pcov))
        lns2 = ax.plot(fit_data[0], analysis_utils.linear(fit_data[0], *popt), label = "fit: %.f $\pm$ %.f * x + %.2f $\pm$ %.2f" %(popt[1], perr[1], popt[0], perr[0]), 
                color = "crimson", linewidth = 0.95 , solid_capstyle='round')
    if color_labels:
        ax.tick_params(axis='y', labelcolor=color_ax1, labelsize=fontsize)
    ax2 = ax.twinx()
    lns3 = ax2.bar(bar_edges[:-1], hist_in, width = np.diff(bar_edges), color = color_ax2, alpha = alpha2, zorder= 10, label = legend2, linewidth = 0.1, edgecolor = edge_color)
    if y2label:
        if color_labels:
            axcolor = color_ax2
        else:
            axcolor = "k"
        ax2.set_ylabel(y2label, color = axcolor)
    if y2lim:
        ax2.set_ylim(y2lim)
    if color_labels:
        ax2.tick_params(axis='y', labelcolor=color_ax2, labelsize=fontsize)
    ax2.tick_params(axis='both', which='major', labelsize=fontsize)
    ax.set_zorder(2)
    ax2.set_zorder(1)
    ax.patch.set_visible(False)
    if y1log:
        ax.set_yscale("log")
    if y2log:
        ax2.set_yscale("log")
    if legend:
        if linfit:
            lns = lns1+lns2+[lns3]
        else:
            lns = [lns1]+[lns3]
        labs = [l.get_label() for l in lns]
        # ax.legend(lns, labs, loc=0)
        ax.legend(lns, labs, prop={'size': fontsize})
    if ax is None:
        fig.tight_layout()
    if save is True:
        output_pdf.savefig(fig, dpi=1000)
        if output_png:
            fig.savefig(output_png, dpi=1000)
    if linfit:
        return popt, pcov


def plot_xy_confidence_band(x, y, x2, y2_lower, y2_upper, output_pdf, xlim=None, xlabel=None, ax=None, fig=None,
                        ylim=None, ylabel=None, y2label = None, y2lim = None, title=None, 
                        legend=None, legend2 = None, output_png=None, markersize = 1, linfit = False, fit_data = None):
    save = False
    if ax is None:
        fig = Figure()
        fig.tight_layout()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True
    ax.grid()
    if xlim:
        ax.set_xlim(xlim)
    if ylim:
        ax.set_ylim(ylim)
    ax.plot(x,y, marker = "o" , markersize = markersize, linestyle = "None", rasterized = True, label = legend)
    if title:
        ax.set_title(title)
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    if linfit:
        popt, pcov = curve_fit(analysis_utils.linear, fit_data[0], fit_data[1], p0 = (0., -2000.))
        perr = np.sqrt(np.diag(pcov))
        ax.plot(fit_data[0], analysis_utils.linear(fit_data[0], *popt), label = "fit: %.f $\pm$ %.f * x + %.2f $\pm$ %.2f" %(popt[1], perr[1], popt[0], perr[0]), 
                color = "crimson", linewidth = 0.95 , solid_capstyle='round')
    ax.tick_params(axis='y', labelcolor="black")
    color = "orange"
    ax2 = ax.twinx()
    ax2.plot(x,y,linestyle = "None")
    ax2.fill_between(x, y2_lower, y2_upper, color = color, alpha = 0.5, label = legend2)
    if y2label:
        ax2.set_ylabel(y2label, color = color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax.set_zorder(0)
    ax2.set_zorder(1.)
    if y2lim:
        ax2.set_ylim(y2lim)
    ax.patch.set_visible(False)
    if legend:
        ax.legend(prop={'size': 8},loc = "upper right")
    fig.tight_layout()
    if save is True:
        output_pdf.savefig(fig, dpi=1000)
        if output_png:
            fig.savefig(output_png, dpi=1000)
    if linfit:
        return popt, pcov


def plot_residuals_vs_position(hist2d, x, y, output_pdf, bins, counts, ax=None, fig=None, xlabel=None, ylabel=None, title=None,
                        output_png=None, zlog=False, min_hits=None, limits=None, invert_x_axis = False, xlimits = None, ylimits = None,
                        linfit = False, legend = True, markersize = 1):
    '''
    plot a 2d histogram with ax.imshow() and plot a linear fit on top.
    -----------------
    INPUT:
        hist2d: np.array with histogram data
            histogram with x and y values (in this order)
        x: np.array or list
            x values for lin fit
        y: np.array or list
            y values for lin fit
        output_pdf: pdfpages object to save plots
        bins: list or iterable with 2 entries
            first entry: x bins (np.array) second entry y bins (np.array)
        counts: np.array or list
            input for linfit, used to mask certain entries
        ax : matplotlib axes
            if given use this for plotting, fig then also needs to be given, otherwise create new axis
        fig: matplotlib figure
            see above
        xlabel: string
            text for x axis
        ylabel: string
            text for y axis
        title: string
            text for plot title
        output_png: string
            if given save a copy of the plot as png under given location
        zlog: bool
            if True use logarithmic scaling for z colorbar
        min_hits : int
            if given mask bins in histogram with count lower than min_hits
        limits: list or other iterable with 2 entries
            if given use first entry as lower limit and second entry as upper limit for z scaling
        invert_x_axis: bool
            if True positive x is on left side and negative on right side, in other words x is increasing from right to left
        xlimits: list or other iterable of 2 entries
            if given use first entry as lower limit and second entry as upper limit for x axis
        ylimits: list or other iterable of 2 entries
            if given use first entry as lower limit and second entry as upper limit for y axis
        linfit: bool
            if True perform linear fit of 2dhist x values and plot resulting function
        legend: bool
            if given plot fit results in legend
        markersize: float
            size of markers for x,y, plot
    '''
    
    save = False
    cmap = cm.get_cmap("viridis")
    cmap.set_bad('w')
    if min_hits:
        hist2d = np.ma.array(hist2d, mask=hist2d<min_hits)
    if limits:
        zmin = limits[0]
        zmax = limits[1]
    else:
        zmin = np.min(hist2d)
        zmax = np.max(hist2d)
        if zmin==zmax:
            zmin = zmax-1
    levels = MaxNLocator(nbins=255).tick_values(zmin, zmax)
    norm = BoundaryNorm(levels, ncolors=256, clip=True)
    bounds = np.linspace(start=zmin, stop=zmax, num=256, endpoint=True)
    ticks = np.linspace(start=zmin, stop=zmax, num=11, endpoint=True)
    if zlog:
        if zmin==0:
            zmin = 0.1
        norm = colors.LogNorm(vmin=zmin, vmax=zmax)
        bounds = np.logspace(start=zmin, stop=zmax, num=256, endpoint=True)
        ticks = np.logspace(start=zmin, stop=zmax, num=11, endpoint=True)
    if ax is None:
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        save = True
    im = ax.pcolormesh(bins[0], bins[1], hist2d.T, edgecolors='None', snap=True, cmap=cmap, norm=norm, rasterized=True)
    
    if linfit:
        fit, cov, select, mean_fit = analysis_utils.fit_residuals_vs_position(hist=hist2d, xedges = bins[0], yedges = bins[1], mean = None, count = counts, limit = xlimits)
        res_pos = (bins[0][1:] + bins[0][:-1]) / 2.0
        ax.scatter(res_pos[select], mean_fit[select], marker = "o", color = "orange", label = "Mean residual", s = markersize)
        ax.scatter(res_pos[~select], mean_fit[~select], marker = "o", color = "red", s = markersize)
        x_lim = np.array(ax.get_xlim(), dtype=np.float64)
        ax.plot(x_lim, analysis_utils.linear(x_lim, *fit), linestyle='-', linewidth=2, label='Mean residual fit\n%.2e + %.2e x' % (fit[0], fit[1]))
    if title:
        ax.set_title(title)
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    if ylimits:
        ax.set_ylim(ylimits)
    if xlimits:
        ax.set_xlim(xlimits)
    if invert_x_axis:
        ax.set_xlim(bins[0].max(),bins[0].min())
    # fig.colorbar(im, boundaries=bounds, ticks=ticks, fraction=0.04, pad=0.05)
    ax.grid()
    if legend is True:
        ax.legend(prop={'size': 8},loc = "upper right")
    fig.colorbar(im, ax=ax, cmap=cmap, ticks = ticks, boundaries = bounds, norm=norm, fraction=0.04, pad=0.05)
    # fig.tight_layout()
    if save is True:
        output_pdf.savefig(fig, dpi=1000)
        if output_png:
            fig.savefig(output_png, dpi=1000)


def create_polyplot_fig(cols, rows, figsize =[29.7,21.], super_title= None, sharex = True, sharey= True):
    fig, axes = plt.subplots(rows, cols, figsize = figsize, sharey=sharey, sharex=sharex, constrained_layout=True)
    if super_title:
        fig.suptitle(super_title, fontsize = 40)
    return  fig, axes


def save_polyplot_fig(fig, output_pdf, share_labels = True, output_png=None):
    if share_labels:
        for ax in fig.get_axes():
            ax.label_outer()
    # fig.tight_layout()
    output_pdf.savefig(fig, dpi=500)
    if output_png:
        fig.savefig(output_png, dpi=500)
    plt.close(fig)


def plot_2dhist_subplot(fig, ax, hist2d, bins, title=None, xlabel=None, ylabel=None, zlog=False, min_hits=None, limits=None, invert_x_axis = False):
    cmap = cm.get_cmap("viridis")
    cmap.set_bad('w')
    if min_hits:
        hist2d = np.ma.array(hist2d, mask=hist2d<min_hits)
    if limits:
        zmin = limits[0]
        zmax = limits[1]
    else:
        zmin = np.min(hist2d)
        zmax = np.max(hist2d)
        if zmin==zmax:
            zmin = zmax-1
    levels = MaxNLocator(nbins=256).tick_values(zmin, zmax)
    norm = BoundaryNorm(levels, ncolors=256, clip=True)
    bounds = np.linspace(start=zmin, stop=zmax, num=256, endpoint=True)
    ticks = np.linspace(start=zmin, stop=zmax, num=11, endpoint=True)
    if zlog:
        if zmin==0:
            zmin = 0.1
        norm = colors.LogNorm(vmin=zmin, vmax=zmax)
        bounds = np.logspace(start=zmin, stop=zmax, num=256, endpoint=True)
        ticks = np.logspace(start=zmin, stop=zmax, num=11, endpoint=True)

    im = ax.imshow(hist2d.T, extent=[bins[0][0], bins[0][-1], bins[1][0], bins[1][-1]], origin="lower", aspect = "auto", interpolation = "none")
    if title:
        ax.set_title(title + " - %i entries" % hist2d.sum(), fontsize = 20)
    if xlabel:
        ax.set_xlabel(xlabel,fontsize = 20)
    if ylabel:
        ax.set_ylabel(ylabel,fontsize = 20)
    
    fig.colorbar(im, ax = ax, cmap=cmap, ticks = ticks, boundaries = bounds, norm=norm, fraction=0.04, pad=0.05)
    ax.grid()


def plot_magnetic_deflection(x_old, x_new, x_proj, z_0, z_1, z_2, old_slope, new_slope, x_center, z_center, radius, output_pdf):
    '''
    Visualize the 3-fold track propagation before, in and after the magnetic field.
    ----------------------------
    INPUT:
    '''
    
    fig = Figure()
    _ = FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.set_xlabel("z [cm]")
    ax.set_ylabel("x [cm]")
    ax.grid()
    ax.set_title("example of track propagation through magnetic field")
    pixel_color = "C0"
    circle_color = "C1"
    planes_color = "C2"
    linear_color = "darkviolet"

    z_before = np.linspace(0,z_0,100)
    z_circle = np.linspace(z_0, z_1,100)
    
    for i, index in enumerate(np.random.choice(x_old.shape[0], 3)):
        print("Plotting track %i" % index)
        z_after = np.linspace(z_1, z_2[index], 100)
        # for checking if a tangent was fitted, plot the circle for all z values
        z_circle_2 = np.linspace(0, z_2[index], 100)
        z_fit = np.linspace(0, z_2[index] - z_1, 100)
        y_vals = circle(z_circle, z_center[index], x_center[index], radius)
        
        if i == 1:
            ax.plot(z_before, (z_before * old_slope[index]) + x_old[index], color = pixel_color, label = "pixel track")
            ax.plot(z_circle, y_vals, color = circle_color, label = "circle")
            ax.plot(z_after, (z_fit * new_slope[index]) + y_vals[-1], color = linear_color, label = "linear projection")
            ax.axvline(x=z_2[index], linestyle = "dashed", alpha = 1, color = planes_color, label = "Scifi planes")
        else:
            ax.plot(z_before, (z_before * old_slope[index]) + x_old[index], color = pixel_color)
            ax.plot(z_circle, y_vals, color = circle_color)
            ax.plot(z_after, (z_fit * new_slope[index]) + y_vals[-1], color = linear_color)
            ax.axvline(x=z_2[index], linestyle = "dashed", alpha = 1, color = planes_color)

    ax.axvspan(z_0,z_1, alpha = 1, color = "gainsboro", label = "magnetic field")

    ax.legend()
    output_pdf.savefig(fig, dpi=1000)


def plot_magnet_example(plot_x, plane_tracks, z0, z2, field_z, path = None):
    '''
    Plot track propagation in magnetic field. Draws 3 random tracks from input array. Uses step by step calculated x positions
    ----------------
    INPUT : 
        plot_x : np.array of float
            propagated x values to plot directly. Either 1D (1 Track) or 2D with shape[1] = plane_tracks.shape[0] (all tracks have been propagated)
        plane_tracks : np.array
            tracks with x value before and after the field
        z0 : float
            z coordinate of field start
        z2 : np.array of float
            z coordinates of scifi plane, can be different for every cluster
        field_z : np.array of float
            z coordinates for calculated x values
        path : string
            path where output pdf file is stored
    OUTPUT : 
        nothing, pdf saved to disk
            
    '''
    fig = Figure()
    _ = FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.set_xlabel("z [cm]")
    ax.set_ylabel("x [cm]")
    ax.grid()
    ax.set_title("example of track propagation through magnetic field")
    pixel_color = "C0"
    circle_color = "C1"
    planes_color = "C2"
    linear_color = "darkviolet"


    for i, index in enumerate(np.random.choice(plot_x.shape[0], 3)):
        track = plane_tracks[index]
        if i ==0:
            ax.plot([0,z0], [track["x"],track["x"] + track["x_slope"]*z0],color = pixel_color, label = "pixel track")
            ax.plot(field_z, plot_x[:,index], color = circle_color, label = "propagation in field")
            ax.plot([field_z[-1], z2[index]], [track["x_new"], track["x_new"] + track["x_slope_new"] * (z2[index] - field_z[-1])], color = linear_color, label = "new track")
            ax.axvline(x=z2[index], linestyle = "dashed", alpha = 1, color = planes_color, label = "Scifi planes")
        else :
            ax.plot([0,z0], [track["x"],track["x"] + track["x_slope"]*z0],color = pixel_color)
            ax.plot(field_z, plot_x[:,index], color = circle_color)
            ax.plot([field_z[-1], z2[index]], [track["x_new"], track["x_new"] + track["x_slope_new"] * (z2[index] - field_z[-1])], color = linear_color)
            ax.axvline(x=z2[index], linestyle = "dashed", alpha = 1, color = planes_color)
    
    ax.axvspan(field_z[0], field_z[-1], alpha = 1, color = "gainsboro", label = "magnetic field")
    ax.set_title("Example of track propagation in Goliath magnetic field")
    ax.set_xlabel("z [cm]")
    ax.set_ylabel("x [cm]")
    ax.legend()
    
#     plt.xlim(300,450)
    fig.savefig(path, dpi = 1000)
