import uproot as up
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import matplotlib
import matplotlib.colors as colors
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from ship_pixel_tracking.tools.analysis_utils import save_table, load_table
from ship_pixel_tracking.ConvertRoot.cbmsim_to_h5 import load_SND_cbmsim_scifi, load_SND_cbmsim_montecarlo

def cartesian_to_spherical(x, y, z):
    ''' Does a transformation from cartesian to spherical coordinates.

    Convention: r = 0 --> phi = theta = 0

    Parameters
    ----------
    x, y, z : float
        Position in cartesian space.

    Returns
    -------
    Spherical coordinates phi, theta and r.
    '''
    r = np.sqrt(np.square(x) + np.square(y) + np.square(z))
    #r = np.ones_like(x, dtype=np.float64) #np.linalg.norm(np.array([x,y,z]))
    phi = np.zeros_like(r, dtype=np.float64)  # define phi = 0 for x = 0
    theta = np.zeros_like(r, dtype=np.float64)  # theta = 0 for r = 0
    # Avoid division by zero
    # https://en.wikipedia.org/wiki/Atan2
    
    phi = np.arctan2(y, x)
    phi[phi < 0] += 2. * np.pi  # map to phi = [0 .. 2 pi[
    theta = np.arccos(z / r)
    
    #phi[x != 0] = np.arctan2(y[x != 0], x[x != 0])
    #phi[phi < 0] += 2. * np.pi  # map to phi = [0 .. 2 pi[
    #theta[r != 0] = np.arccos(z[r != 0] / r[r != 0])
    return phi, theta, r


# calculate pseudorapidity
z = 480 # meter
z_range = np.linspace(z-5, z+5, 100)
x = 0.1
y = 0.02
#x_range = np.full_like(z_range, fill_value = x)
#y_range = np.full_like(z_range, fill_value = y)

x_range = np.linspace(0.001,1,100)
y_range = np.linspace(0.001,0.75,100)

#phi, theta, r = cartesian_to_spherical(x_range, y_range, z_range)

theta2 = np.arctan(y/z)
#print("phi", phi)
#print("theta", theta)
#print("r", r)
#print("theta2 = ", theta2)
#eta = np.log(np.tan(theta/2))
#print("eta",eta)
eta2 = np.log(np.tan(theta2/2))
#print("eta2 = %f" % eta2)


Xplot, Zplot = np.meshgrid(x_range, z_range)

phi, theta, r = cartesian_to_spherical(Xplot, y, Zplot)

eta = np.log(np.tan(theta/2))

eta = np.log(np.tan(theta/2))

# fig = plt.figure()
# ax = fig.gca(projection='3d')

# color_dimension =eta*-1 # change to desired fourth dimension
# minn, maxx = color_dimension.min(), color_dimension.max()
# levels = MaxNLocator(nbins=256).tick_values(minn, maxx)
# #norm = BoundaryNorm(levels, ncolors=256, clip=True)
# #norm = matplotlib.colors.Normalize(minn, maxx)
# #m = plt.cm.ScalarMappable(norm=norm, cmap='viridis')
# #m.set_array([])


# #norm=colors.Normalize(vmin = minn, vmax = maxx, clip = False)
# #norm = colors.Normalize()

# levels = MaxNLocator(nbins=256).tick_values(minn, maxx)
# norm = BoundaryNorm(levels, ncolors=256, clip=True)
# bounds = np.linspace(start=minn, stop=maxx, num=256, endpoint=True)
# ticks = np.linspace(start=minn, stop=maxx, num=11, endpoint=True)

# #fcolors = m.to_rgba(color_dimension)

# surf = ax.plot_surface(Xplot, Zplot, eta, cmap=cm.viridis,shade=False)
# ax.set_xlabel('x')
# ax.set_ylabel('y')
# ax.set_zlabel('z')

# fig.colorbar(surf, cmap = cm.viridis,  shrink=0.5, aspect=5)
# plt.show()

a,b= np.meshgrid(np.linspace(0.001,1,100), np.linspace(0.05,0.75,100) )
phi, theta, r = cartesian_to_spherical(a, b, 480)
eta = np.log(np.tan(theta/2))
print(eta.min(), eta.max())
fig = plt.figure()
ax = fig.gca(projection='3d')

color_dimension =eta*-1 # change to desired fourth dimension
minn, maxx = color_dimension.min(), color_dimension.max()
levels = MaxNLocator(nbins=256).tick_values(minn, maxx)
norm = BoundaryNorm(levels, ncolors=256, clip=True)
bounds = np.linspace(start=minn, stop=maxx, num=256, endpoint=True)
ticks = np.linspace(start=minn, stop=maxx, num=6, endpoint=True)

#fcolors = m.to_rgba(color_dimension)

surf = ax.plot_surface(a, b, eta, cmap=cm.viridis,shade=False)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('eta')
ax.set_title("Pixel in SND: possible $\mathrm{\eta}$ coverage")
ax.zaxis.labelpad = 12

fig.colorbar(surf, cmap = cm.viridis, shrink=0.75, aspect=10)
plt.show()