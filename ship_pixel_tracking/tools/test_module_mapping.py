# std::unordered_map<int, TVector3> * PixelModules::MakePositionMap() {
# // map unique detectorID to x,y,z position in LOCAL coordinate system. xy (0,0) is on the bottom left of each Front End,
# // the raw data counts columns from 1-80 from left to right and rows from 1-336 FROM TOP TO BOTTOM.
#
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import tables as tb
from ship_pixel_tracking.tools.plot_utils import plot_2d_hist ,plot_1d_hist

def make_position_map():
    mkm = 0.0001
    #
    z0ref=  -1300.*mkm
    z1ref=   5200.*mkm
    z2ref=  24120.*mkm
    z3ref=  30900.*mkm
    z4ref=  51000.*mkm
    z5ref=  57900.*mkm
    z6ref=  77900.*mkm
    z7ref=  84600.*mkm
    z8ref= 104620.*mkm
    z9ref= 111700.*mkm
    z10ref= 131620.*mkm
    z11ref= 138500.*mkm

    Zref = [z0ref, z1ref, z2ref, z3ref, z4ref, z5ref, z6ref, z7ref, z8ref, z9ref, z10ref, z11ref]

    x0ref= (-16800. + 15396.)*mkm       +z0ref*0.0031
    x1ref= -2310.*mkm       +z1ref*0.0031
    x2ref=  6960.*mkm       +z2ref*0.0031
    x3ref=  6940.*mkm       +z3ref*0.0031
    x4ref= (-16800 + 15285.)*mkm       +z4ref*0.0031
    x5ref= -2430.*mkm       +z5ref*0.0031
    x6ref=  6620.*mkm       +z6ref*0.0031
    x7ref=  6710.*mkm       +z7ref*0.0031
    x8ref= (-16800 + 15440.)*mkm       +z8ref*0.0031
    x9ref= -2505.*mkm       +z9ref*0.0031
    x10ref=  6455.*mkm      +z10ref*0.0031
    x11ref=  6320.*mkm      +z11ref*0.0031

    Xref = [x0ref, x1ref, x2ref, x3ref, x4ref, x5ref, x6ref, x7ref, x8ref, x9ref, x10ref, x11ref]

    y0ref=   -15.*mkm       +z0ref*0.0068
    y1ref=    20.*mkm       +z1ref*0.0068
    y2ref= (-8400 + 7930.)*mkm       +z2ref*0.0068
    y3ref= (8400 - 8990.)*mkm       +z3ref*0.0068
    y4ref=  -370.*mkm       +z4ref*0.0068
    y5ref=  -610.*mkm       +z5ref*0.0068
    y6ref=  (-8400 + 7200.)*mkm       +z6ref*0.0068
    y7ref= (8400 - 9285.)*mkm       +z7ref*0.0068
    y8ref=  -700.*mkm       +z8ref*0.0068
    y9ref=  -690.*mkm       +z9ref*0.0068
    y10ref=  (-8400 + 7660.)*mkm      +z10ref*0.0068
    y11ref= (8400 - 8850.)*mkm      +z11ref*0.0068

    Yref = [y0ref, y1ref, y2ref, y3ref, y4ref, y5ref, y6ref, y7ref, y8ref, y9ref, y10ref, y11ref]

    positionMap = {}

    for partitionID in range(3):
        for frontEndID in range(8):
            for column in range(1,81):
                for row in range(1,337):
                    map_index = 10000000*partitionID + 1000000*frontEndID + 1000*row + column
                    moduleID = (8*partitionID + frontEndID)//2
                    # calculate LOCAL x position of hit
                    if frontEndID%2==1 :
                        if column == 1:
                            x_local = -2.02
                        elif column == 80:
                            x_local = -0.0225
                        else:
                            x_local = -1.9825 + (column-2)*0.025
                        # x_local = -0.025 - (80-column)*0.025
                        # if (column == 80):
                        #      x_local -= 0.0225
                    elif frontEndID%2==0 :
                        if column == 80:
                            x_local = 2.02
                        elif column == 1:
                            x_local = 0.0225
                        else:
                            x_local = 1.9825 - (79-column)*0.025
                        # x_local = 0.0225 + (column-1)*0.025
                        # if (column == 80):
                        #      x_local += 0.025
                    # calculate LOCAL y position of hit
                    y_local = 1.6775 - 0.0050*(row-1)
                    # transform local to global coordinates
                    if (frontEndID == 0):
                        x = -y_local
                        y = x_local
                    if (frontEndID == 1 ):
                        x = -y_local
                        y = x_local
                    if frontEndID == 2 :
                        x = y_local
                        y = x_local
                    if frontEndID == 3 :
                        x = y_local
                        y = x_local
                    if frontEndID == 4 :
                        x = -x_local
                        y = -y_local
                    if (frontEndID == 5 ):
                        x = -x_local
                        y = -y_local
                    if (frontEndID == 6 ):
                        x = -x_local
                        y = y_local
                    if (frontEndID == 7 ):
                        x = -x_local
                        y = y_local
                    # positionMap[map_index] = [x - Xref[moduleID],y - Yref[moduleID],Zref[moduleID]]
                    positionMap[map_index] = [-x,y,Zref[moduleID]]
    return positionMap


def get_matched_hits(cbmsim_in_file, matched_events_file):
    with tb.open_file(matched_events_file, "r") as matched_evts_h5:
        events = matched_evts_h5.root.MatchedTimestamps[:]
    with tb.open_file(cbmsim_in_file) as cbmsim_h5:
        hits = cbmsim_h5.root.Hits[:]

    hits_out = []
    i=0
    for row in hits:
        while (row["event_time"] > events[i]["event_time"]) and (i<events.size - 1):
            i+=1
        if (row["event_time"] == events[i]["event_time"]):
            hits_out.append(row)
        elif (row["event_time"] < events[i]["event_time"]):
            continue
    return np.array(hits_out)


def plot_cbmsim_data(hits_array):
    mkm = 0.0001
    positionMap = make_position_map()
    print(hits_array[-1])
    pix_detID = hits_array["pixel_detID"]
    xbins = [-1.6800 + i*50*mkm for i in range(0,336)] + [i*50*mkm for i in range(0,337)]
    ybins = np.concatenate((np.array([-2.0450]),np.arange(-1.9950,-200*mkm,250*mkm),np.array([0]),np.arange(450*mkm,2.0200,250*mkm),np.array([2.0450])))
    bins = [np.arange(-2.5,2.5,0.025), np.arange(-2.2,2.2,0.025)]

    x, y = [],[]
    ids = []
    empty = 0
    for hit in pix_detID:
        try:
            x.append(positionMap[hit][0])
            y.append(positionMap[hit][1])
            ids.append(hit)
        except KeyError:
            # if hit!=0:
                # print(hit)
            empty +=1
            continue
    print("%s empty hits" % empty)
    hit_histogram, hit_x_edges, hit_y_edges = np.histogram2d(x,y,bins= [xbins,ybins])
    with PdfPages("/home/niko/cernbox/storage/matched_hit_histogram.pdf") as output_pdf:
        plot_2d_hist(hit_histogram, output_pdf = output_pdf, bins = [hit_x_edges, hit_y_edges], xlabel="hit x", ylabel="hit y",
                    title="s522366760: reconstructed pixel hits - %i entries" % hit_histogram.sum(), 
                    box = None, mean_eff = None,
                    output_png="/home/niko/cernbox/storage/matched_hit_histogram.png", zlog=False, min_hits=1, limits=None, scale_large_pixels = True, invert_x_axis = True)
        plot_1d_hist(pix_detID, output_pdf = output_pdf, bins =100, log = False, title = "s522366760: pix_detID - %s entries" % pix_detID.shape[0],
                    output_png = "/home/niko/cernbox/storage/matched_pixdetID_histogram.png")
        plot_1d_hist(hits_array["event"], output_pdf = output_pdf, bins =100, log = False, title = "s522366760: events - %s entries" % hits_array["event"].shape[0],
                    output_png = "/home/niko/cernbox/storage/matched_eventnumber_histogram.png")



if __name__ == '__main__':
    matched_hits = get_matched_hits("/home/niko/cernbox/storage/SPILLDATA_8000_0522366760_20180730_172232_pix_hits.h5",
                                    "/home/niko/cernbox/storage/_matched_spill_10_raw.h5"
                                    )
    plot_cbmsim_data(matched_hits)