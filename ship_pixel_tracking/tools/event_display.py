from pathlib import Path
import logging

import matplotlib.pyplot as plt
from matplotlib import colors, cm
from matplotlib.patches import Ellipse
import matplotlib
import matplotlib.gridspec as gridspec
import matplotlib.patches as patches
from matplotlib.ticker import FormatStrFormatter
import numpy as np
from ship_pixel_tracking.tools.analysis_utils import load_table, load_config, save_table, save_config
from ship_pixel_tracking.tools.plot_utils import plot_2d_hist, plot_1d_bar

from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.mplot3d import Axes3D

logging.basicConfig(level=logging.INFO, format="%(asctime)s - [%(levelname)-8s] %(message)s")

def get_matched_emulsion_tracks(matched_tracks_in_path, emulsion_in_path):
    """ 
    Use trackIDs of matched emulsion tracks to extract further information from the emulsion track file.
    Right now only the z coordinate of the vertex of the matched track is taken, but this can be extended to any information in the table easily.
    ---------------------
    INPUT:
        matched_tracks_in_path : str
            location of the .h5 file with the matched track info, needs "emu_trk" column
        emulsion_in_path : str
            location of the .h5 file with emulsion track coordinates
    OUTPUT:
        nothing, file written to disk in same folder as matched_tracks_in_path
    """

    matched_tracks = load_table(matched_tracks_in_path, table_name="MatchedTracks")
    emulsion_tracks = load_table(emulsion_in_path, table_name="EmuTracks")

    # create dtype for output file. Need to get old dtype and extend it.
    out_tracks_dtype = []
    for j, name in enumerate(matched_tracks.dtype.names):
        out_tracks_dtype.append((matched_tracks.dtype.names[j], matched_tracks.dtype[name].type))
    # add column for new data
    out_tracks_dtype.extend([('emu_z0', np.float32), ])
    out_tracks = np.full(fill_value=np.nan, shape=matched_tracks.shape, dtype=out_tracks_dtype)

    # to speed up the matching of trackIDs in the two files the smaller table (matched_tracks) is sorted for the trackID.
    # in this way we can make use of the fact that both tables are sorted.
    matched_tracks.sort(order="emu_trk")
    i = 0
    for k, track in enumerate(matched_tracks):
        while (emulsion_tracks[i]["trackID"] != track["emu_trk"]) and (i < (emulsion_tracks.shape[0]-1)):
            i += 1
        if emulsion_tracks[i]["trackID"] == track["emu_trk"]:
            # instead of hardcoding all dtypes one can use a loop to be more flexible and reduce lines of code
            for dtype in matched_tracks.dtype.names:
                out_tracks[k][dtype] = track[dtype]
            # here the new data is written to the array, can easily be extended
            out_tracks[k]["emu_z0"] = emulsion_tracks[i]["vz"]

    save_table(array_in=out_tracks, path=matched_tracks_in_path[:-3] + "_z_coordinate.h5", node_name="MatchedTracks")
    return (matched_tracks_in_path[:-3] + "_z_coordinate.h5")


def get_alignment(matched_tracks_in_path):
    """ 
    Extract alingment paramters from matched_tracks .h5 file. 
    The original root file was row based, each emulsion - pixel pair had its own row, 
    and alignment parameters are saved in every row, althouhg they are identical for each match.
    So the easiest way to extract is just to read in the corresponding columns from the table and get the first value of every column.
    These values are saved in an .yaml file for easy access afterwards.
    """
    matched_tracks = load_table(matched_tracks_in_path, table_name="MatchedTracks")
    alignment_dict = {}
    spills, index = np.unique(matched_tracks["spill"], return_index=True)
    for spill, index in zip(spills,index):
        spill_track = matched_tracks[index]
        alignment_values = spill_track[["x_0", "y_0", "z_0", "tx_0", "ty_0", "txy_0", "v_x", "v_y"]]

        alignment_dict[int(spill)] = {}
        for col in alignment_values.dtype.names:
            alignment_dict[int(spill)][col] = float(alignment_values[col])

    save_config(matched_tracks_in_path[:-3] + "_alignment.yaml", alignment_dict)
    return matched_tracks_in_path[:-3] + "_alignment.yaml"


def plot_translated_pixel_tracks(pix_tracks_in_path):
    '''
    simply plot track coordinates, for quick check of translation.
    '''

    pix_trks = load_table(pix_tracks_in_path, table_name="Tracks")

    with PdfPages(pix_tracks_in_path[:-3] + ".pdf") as out_pdf:

        x_hist, x_bins = np.histogram(pix_trks["x"], bins=100)
        plot_1d_bar(hist_in=x_hist, output_pdf=out_pdf, bar_edges=x_bins, xlabel="x in cm", title="CH1R6 - Algined pixel tracks in emulsion system")

        y_hist, y_bins = np.histogram(pix_trks["y"], bins=100)
        plot_1d_bar(hist_in=y_hist, output_pdf=out_pdf, bar_edges=y_bins, xlabel="y in cm", title="CH1R6 - Algined pixel tracks in emulsion system")

        z_hist, z_bins = np.histogram(pix_trks["z"], bins=100)
        plot_1d_bar(hist_in=z_hist, output_pdf=out_pdf, bar_edges=z_bins, xlabel="z in cm", title="CH1R6 - Algined pixel tracks in emulsion system")

        x_y_hist, xbins, ybins = np.histogram2d(pix_trks["x"], pix_trks["y"], bins=[100, 100], range=[[-1, 13], [-1, 11]])
        plot_2d_hist(hist2d=x_y_hist, output_pdf=out_pdf, bins=[xbins, ybins], xlabel="track x in cm",
                     ylabel="track y [cm]", title="CH1R6 translated pixel tracks", min_hits=1,
                     output_png=pix_tracks_in_path[:-3] + "_xy_hist.png")


def plot_emulsion_tracks(emu_tracks_in_path):
    '''
    simply plot track coordinates, for quick check
    '''
    emu_trks = load_table(emu_tracks_in_path, table_name="EmuTracks")

    with PdfPages(emu_tracks_in_path[:-3] + ".pdf") as out_pdf:

        x_hist, x_bins = np.histogram(emu_trks["x"], bins=100)
        plot_1d_bar(hist_in=x_hist, output_pdf=out_pdf, bar_edges=x_bins, xlabel="x [cm]", title="CH1R6 - Emulsion track segments")

        y_hist, y_bins = np.histogram(emu_trks["y"], bins=100)
        plot_1d_bar(hist_in=y_hist, output_pdf=out_pdf, bar_edges=y_bins, xlabel="y [cm]", title="CH1R6 - Emulsion track segments")

        z_hist, z_bins = np.histogram(emu_trks["z"], bins=100)
        plot_1d_bar(hist_in=z_hist, output_pdf=out_pdf, bar_edges=z_bins, xlabel="z [cm]", title="CH1R6 - Emulsion track segments")

        x_y_hist, xbins, ybins = np.histogram2d(emu_trks["x"], emu_trks["y"], bins=[100, 100], range=[[0, 13], [0, 10]])
        plot_2d_hist(hist2d=x_y_hist, output_pdf=out_pdf, bins=[xbins, ybins], xlabel="track x [cm]", min_hits=1,
                     ylabel="track y [cm]", title="CH1R6 emulsion track segments", output_png=emu_tracks_in_path[:-3] + "_xy_hist.png")

        x_y_hist2, xbins2, ybins2 = np.histogram2d(emu_trks["x2"], emu_trks["y2"], bins=[100, 100], range=[[0, 13], [0, 10]])
        plot_2d_hist(hist2d=x_y_hist2, output_pdf=out_pdf, bins=[xbins2, ybins2],
                     xlabel="track x2 [cm]", ylabel="track y2 [cm]", title="CH1R6 propagated emulsion track segments", min_hits=1)

        x_y_hist_delta, xbins_delta, ybins_delta = np.histogram2d((emu_trks["x2"] - emu_trks["x"])*1e4, (emu_trks["y2"] - emu_trks["y"])*1e4, bins=[100, 100])
        plot_2d_hist(hist2d=x_y_hist_delta, output_pdf=out_pdf, bins=[xbins_delta, ybins_delta],
                     xlabel="track x2 - x [$\mu m$]", ylabel="track y2 - y [$\mu m$]", title="CH1R6 relative change of track position", min_hits=1)


def plot_matched_track_info(matched_emu_trks_in_file):
    """
    Plots for basic check of matched track coordinates
    """
    emu_tracks = load_table(matched_emu_trks_in_file, table_name="MatchedTracks")
    with PdfPages("/media/niko/big_data/charm_testbeam_july18/match_emu/CH1R6_s10.pdf") as out_pdf:

        hist2d, binsx, binsy = np.histogram2d(emu_tracks["match_dxr"], emu_tracks["match_dyr"], bins=[100, 100])
        plot_2d_hist(hist2d=hist2d, output_pdf=out_pdf, bins=[binsx, binsy], xlabel="$\Delta$ x [cm]", ylabel="$\Delta$ y [cm]",
                     title="CH1R6 S10 residuals - %i entries" % hist2d.sum(), box=None, mean_eff=None, ax=None, fig=None,
                     output_png="/media/niko/big_data/charm_testbeam_july18/match_emu/CH1R6_s10_2d_residuals.png", zlog=False, min_hits=1, limits=None, scale_large_pixels=False, invert_x_axis=False)

        res_x_hist, res_x_bins = np.histogram(emu_tracks["match_dxr"]*10, bins=100)
        plot_1d_bar(hist_in=res_x_hist, output_pdf=out_pdf, bar_edges=res_x_bins, xlim=None, ylim=None, xlabel="$x_{emu} - x_{pix}$ [mm]", ylabel=None, ax=None, fig=None,
                    title="CH1R6 S10 x residuals - %i entries" % (res_x_hist.sum()), legend=None, yerr=None, error_kw=None, capsize=None, log=False, linewidth=0.005,
                    vlines=None,  gauss_fit=True, gauss_unit="mm")

        res_y_hist, res_y_bins = np.histogram(emu_tracks["match_dyr"]*10, bins=100)
        plot_1d_bar(hist_in=res_y_hist, output_pdf=out_pdf, bar_edges=res_y_bins, xlim=None, ylim=None, xlabel="$y_{emu} - y_{pix}$ [mm]", ylabel=None, ax=None, fig=None,
                    title="CH1R6 S10 y residuals - %i entries" % (res_y_hist.sum()), legend=None, yerr=None, error_kw=None, capsize=None, log=False, linewidth=0.005,
                    vlines=None,  gauss_fit=True, gauss_unit="mm")

        res_tx_hist, res_tx_bins = np.histogram(emu_tracks["match_dtxr"]*1000, bins=100)
        plot_1d_bar(hist_in=res_tx_hist, output_pdf=out_pdf, bar_edges=res_tx_bins, xlim=None, ylim=None, xlabel="$tx_{emu} - tx_{pix}$ [mRad]", ylabel=None, ax=None, fig=None,
                    title="CH1R6 S10 tx residuals - %i entries" % (res_x_hist.sum()), legend=None, yerr=None, error_kw=None, capsize=None, log=False, linewidth=0.005,
                    vlines=None,  gauss_fit=True, gauss_unit="mRad")

        res_ty_hist, res_ty_bins = np.histogram(emu_tracks["match_dtyr"]*1000, bins=100)
        plot_1d_bar(hist_in=res_ty_hist, output_pdf=out_pdf, bar_edges=res_ty_bins, xlim=None, ylim=None, xlabel="$ty_{emu} - ty_{pix}$ [mRad]", ylabel=None, ax=None, fig=None,
                    title="CH1R6 S10 ty residuals - %i entries" % (res_ty_hist.sum()), legend=None, yerr=None, error_kw=None, capsize=None, log=False, linewidth=0.005,
                    vlines=None,  gauss_fit=True, gauss_unit="mRad")

        res_bins = np.linspace(-0.15, 0.15, 100)
        plt.hist(emu_tracks["match_dyr"], bins=res_bins, label="$\Delta$ y")
        plt.hist(emu_tracks["match_dxr"], bins=res_bins, alpha=0.5, label="$\Delta$ x")
        plt.xlabel("$pos_{emu} - pos_{pix}$ [cm]")
        plt.legend()
        plt.grid()
        plt.title("CH1R6 S10 x and y residuals")
        out_pdf.savefig()
        plt.savefig("/media/niko/big_data/charm_testbeam_july18/match_emu/CH1R6_s10_residuals.png")

        plt.cla()
        plt.hist(emu_tracks["match_dtyr"], bins=100, label="$\Delta$ ty")
        plt.hist(emu_tracks["match_dtxr"], bins=100, alpha=0.5, label="$\Delta$ tx")
        plt.xlabel("$t_{emu} - t_{pix}$ [Rad]")
        plt.legend()
        plt.grid()
        plt.title("CH1R6 S10 tx and ty residuals")
        out_pdf.savefig()
        plt.savefig("/media/niko/big_data/charm_testbeam_july18/match_emu/CH1R6_s10_slope_residuals.png")

        plt.cla()
        plt.grid()
        plt.title("CH1R6 S10 matched timestamps - %i entries" % np.histogram(emu_tracks["match_time"])[0].sum())
        plt.hist(emu_tracks["match_time"]*25/1e9, bins=100)
        plt.xlabel("t [s]")
        out_pdf.savefig()
        plt.savefig("/media/niko/big_data/charm_testbeam_july18/match_emu/CH1R6_s10_timestamp.png")

        plt.cla()
        plt.grid()
        plt.title("CH1R6 S10 $\chi^2$/ndf - %i entries" % np.histogram(emu_tracks["match_time"])[0].sum())
        plt.hist(emu_tracks["match_chi2"]/4, bins=100)
        plt.xlabel("$\chi^2/ndf$")
        out_pdf.savefig()
        plt.savefig("/media/niko/big_data/charm_testbeam_july18/match_emu/CH1R6_s10_chi2.png")

        plt.cla()
        plt.hist(emu_tracks["match_dty"], bins=100, label="ty pull")
        plt.hist(emu_tracks["match_dtx"], bins=100, label="tx pull", alpha=0.5)
        plt.legend()
        plt.hist(emu_tracks["match_dy"], bins=100, alpha=0.5)


def translate_pix_coords(x_pix, y_pix, tx_pix, ty_pix, timestamp, spill=10):
    '''
    translate single coordinates similar to "translate_pixel_tracks"
    DEPRECIATED
    '''
    pix_emu_alignment = load_config("/media/niko/big_data/charm_testbeam_july18/match_emu/emu_alignment.yaml")
    algn_step = (timestamp*25/1e9) / 4.5 * 10
    algn = pix_emu_alignment["step%i" % algn_step]
    txy = algn["txy"] * 1e-3

    tx = np.cos(txy) * (tx_pix + algn["tx0"]*1e-3) - np.sin(txy) * (ty_pix + algn["ty0"]*1e-3)
    ty = np.cos(txy) * (ty_pix + algn["ty0"]*1e-3) + np.sin(txy) * (tx_pix + algn["tx0"]*1e-3)

    x = algn["x0"] + np.cos(txy) * (x_pix - algn["z0"] * tx) - np.sin(txy) * (y_pix - algn["z0"] * ty) + algn["v"] * (timestamp*25/1e9)
    y = algn["y0"] + spill*2 + np.cos(txy) * (y_pix - algn["z0"] * ty) + np.sin(txy) * (x_pix - algn["z0"] * tx)

    return (x, y, tx, ty)


def translate_pixel_hits(pix_hits_path, alignment_file_path, spill=None, steps=False,):
    '''
    Translate pixel hit data in emulsion rest frame. Alignment data from Christopher Betancourt, 10 steps for all values per spill.
    Which step to use is determined by the timestamp of the hit.
    The translated data is saved in the same folder as the input file.
    ---------------
    INPUT:
        pix_hits_path : str
            path to the pixel hits .h5 file
        alignment_file_path : str
            path to .yaml file with alignment values
    OUTPUT:
        nothing, aligned data is written to disk.
    '''
    pix_emu_alignment = load_config(alignment_file_path)
    pix_hits = load_table(pix_hits_path, table_name="Hits")

    if steps == True:
        pix_hits = pix_hits[pix_hits["spill"]==spill]
        algn_times = np.linspace(pix_hits["timestamp"].min(), pix_hits["timestamp"].max(), 11)
        for i in range(1, 11):

            pix_hits_mask = np.logical_and(pix_hits["timestamp"] <= algn_times[i], pix_hits["timestamp"] > algn_times[i-1])

            algn_step = i
            algn = pix_emu_alignment["step%i" % algn_step]
            txy = algn["txy"] * 1e-3

            tx = np.cos(txy) * algn["tx0"]*1e-3 - np.sin(txy) * algn["ty0"]*1e-3
            ty = np.cos(txy) * algn["ty0"]*1e-3 + np.sin(txy) * algn["tx0"]*1e-3

            x = algn["x0"] + np.cos(txy) * (-pix_hits["hitx"][pix_hits_mask] - algn["z0"] * tx) - np.sin(txy) * \
                (pix_hits["hity"][pix_hits_mask] - algn["z0"] * ty) + algn["v"] * (pix_hits["timestamp"][pix_hits_mask]*25/1e9)
            y = algn["y0"] + pix_hits["spill"][pix_hits_mask] * 2 + \
                np.cos(txy) * (pix_hits["hity"][pix_hits_mask] - algn["z0"] * ty) + np.sin(txy) * (-pix_hits["hitx"][pix_hits_mask] - algn["z0"] * tx)

    else:
        for spill in np.unique(pix_hits["spill"]):
            try:
                algn = pix_emu_alignment[int(spill)]
            except KeyError:
                # spill does not contain emulsion data, so no alignment values
                continue

            mask = pix_hits["spill"]==spill
            txy = algn["txy_0"] * 1e-3

            tx = np.cos(txy) * algn["tx_0"]*1e-3 - np.sin(txy) * algn["ty_0"]*1e-3
            ty = np.cos(txy) * algn["ty_0"]*1e-3 + np.sin(txy) * algn["tx_0"]*1e-3

            x = algn["x_0"] + np.cos(txy) * (-pix_hits[mask]["hitx"] - algn["z_0"] * tx) - np.sin(txy) * \
                (pix_hits["hity"][mask] - algn["z_0"] * ty) + algn["v_x"] * (pix_hits["timestamp"][mask]*25/1e9)
            y = algn["y_0"] + pix_hits["spill"][mask] * 2 + \
                np.cos(txy) * (pix_hits["hity"][mask] - algn["z_0"] * ty) + np.sin(txy) * \
                (-pix_hits["hitx"][mask] - algn["z_0"] * tx) + algn["v_y"] * (pix_hits["timestamp"][mask]*25/1e9)

            pix_hits["hitx"][mask] = x
            pix_hits["hity"][mask] = y
            pix_hits["hitz"][mask] += algn["z_0"]

    save_table(array_in=pix_hits, path=pix_hits_path[:-3] + "_emu_system.h5", node_name="Hits")
    return (pix_hits_path[:-3] + "_emu_system.h5")

def translate_pixel_plane(plane_x, plane_y, spill, timestamp, alignment_file_path = "/mount/niko/big_data/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched_alignment.yaml"):
    pix_emu_alignment = load_config(alignment_file_path)
    algn = pix_emu_alignment[int(spill)]

    txy = algn["txy_0"] * 1e-3

    tx = np.cos(txy) * algn["tx_0"]*1e-3 - np.sin(txy) * algn["ty_0"]*1e-3
    ty = np.cos(txy) * algn["ty_0"]*1e-3 + np.sin(txy) * algn["tx_0"]*1e-3

    plane_x_center = algn["x_0"] + np.cos(txy) * (-plane_x - algn["z_0"] * tx) - np.sin(txy) * \
        (plane_y - algn["z_0"] * ty) + algn["v_x"] * (timestamp*25/1e9)
    plane_y_center = algn["y_0"] + spill * 2 + \
        np.cos(txy) * (plane_y - algn["z_0"] * ty) + np.sin(txy) * \
        (-plane_x - algn["z_0"] * tx) + algn["v_y"] * (timestamp*25/1e9)

    return plane_x_center, plane_y_center


def translate_pixel_vertices(pix_vtx_path, alignment_file_path, spill=None, steps=False,):
    '''
    Translate pixel hit data in emulsion rest frame. Alignment data from Christopher Betancourt, 10 steps for all values per spill.
    Which step to use is determined by the timestamp of the hit.
    The translated data is saved in the same folder as the input file.
    ---------------
    INPUT:
        pix_hits_path : str
            path to the pixel hits .h5 file
        alignment_file_path : str
            path to .yaml file with alignment values
    OUTPUT:
        nothing, aligned data is written to disk.
    '''
    pix_emu_alignment = load_config(alignment_file_path)
    pix_vtcs = load_table(pix_vtx_path, table_name="Vertices")

    if steps == True:
        pix_vtcs = pix_vtcs[pix_vtcs["spill"]==spill]
        algn_times = np.linspace(pix_vtcs["timestamp"].min(), pix_vtcs["timestamp"].max(), 11)
        for i in range(1, 11):

            mask = np.logical_and(pix_vtcs["timestamp"] <= algn_times[i], pix_vtcs["timestamp"] > algn_times[i-1])

            algn_step = i
            algn = pix_emu_alignment["step%i" % algn_step]
            txy = algn["txy"] * 1e-3

            tx = np.cos(txy) * algn["tx0"]*1e-3 - np.sin(txy) * algn["ty0"]*1e-3
            ty = np.cos(txy) * algn["ty0"]*1e-3 + np.sin(txy) * algn["tx0"]*1e-3

            x = algn["x0"] + np.cos(txy) * (-pix_vtcs["hitx"][mask] - algn["z0"] * tx) - np.sin(txy) * \
                (pix_vtcs["hity"][mask] - algn["z0"] * ty) + algn["v"] * (pix_vtcs["timestamp"][mask]*25/1e9)
            y = algn["y0"] + pix_vtcs["spill"][mask] * 2 + \
                np.cos(txy) * (pix_vtcs["hity"][mask] - algn["z0"] * ty) + np.sin(txy) * (-pix_vtcs["hitx"][mask] - algn["z0"] * tx)

    else:
        algn_x, algn_y, algn_z, algn_vx, algn_vy, algn_tx, algn_ty, algn_txy = [],[],[], [], [], [], [], []
        for spill in np.unique(pix_vtcs["spill"]):
            try:
                algn0 = pix_emu_alignment[int(spill)]
            except KeyError:
                # spill does not contain emulsion data, so no alignment values
                continue
            
            algn_x.append(algn0["x_0"] -13*(spill%2))
            algn_y.append(algn0["y_0"])
            algn_z.append(algn0["z_0"])
            algn_vx.append(algn0["v_x"])
            algn_vy.append(algn0["v_y"])
            algn_tx.append(algn0["tx_0"])
            algn_ty.append(algn0["ty_0"])
            algn_txy.append(algn0["txy_0"])
            
        sigma_x_algn = np.std(np.abs(algn_x))
        sigma_y_algn = np.std(np.abs(algn_y))
        sigma_z_algn = np.std(np.abs(algn_z))
        sigma_vx_algn = np.std(np.abs(algn_vx))
        sigma_vy_algn = np.std(np.abs(algn_vy))
        sigma_tx_algn = np.std(np.abs(algn_tx))
        sigma_ty_algn = np.std(np.abs(algn_ty))
        sigma_txy_algn = np.std(np.abs(algn_txy))

        for spill in np.unique(pix_vtcs["spill"]):
            try:
                algn = pix_emu_alignment[int(spill)]
            except KeyError:
                # spill does not contain emulsion data, so no alignment values
                continue

            mask = pix_vtcs["spill"]==spill
            txy = algn["txy_0"]

            tx = np.cos(txy) * algn["tx_0"] - np.sin(txy) * algn["ty_0"]
            ty = np.cos(txy) * algn["ty_0"] + np.sin(txy) * algn["tx_0"]

            x = algn["x_0"] + np.cos(txy) * (-pix_vtcs["x"][mask]- algn["z_0"] * tx) - np.sin(txy) * \
                (pix_vtcs["y"][mask] - algn["z_0"] * ty) + algn["v_x"] * (pix_vtcs["timestamp"][mask]*25/1e9)
            y = algn["y_0"] + pix_vtcs["spill"][mask] * 2 + np.cos(txy) * (pix_vtcs["y"][mask] - algn["z_0"] * ty) + np.sin(txy) * (-pix_vtcs["x"][mask] - algn["z_0"] * tx) + algn["v_y"] * (pix_vtcs["timestamp"][mask]*25/1e9)

            pix_vtcs["x"][mask] = x
            pix_vtcs["y"][mask] = y
            pix_vtcs["z"][mask] += algn["z_0"]

            x0_err = sigma_x_algn
            txy_xerr = (np.sin(txy)*(algn["z_0"]*tx+pix_vtcs[mask]["x"]) + np.cos(txy)*(algn["z_0"]*ty-pix_vtcs[mask]["y"])) * sigma_txy_algn
            x_xerr = np.cos(txy) * np.sqrt(pix_vtcs[mask]["vertex_cov_XX"])
            y_xerr = np.sin(txy) * np.sqrt(pix_vtcs[mask]["vertex_cov_YY"])
            tx_xerr = np.cos(txy)*(-algn["z_0"]) * sigma_tx_algn
            ty_xerr = algn["z_0"] * np.sin(txy) * sigma_ty_algn
            vx_err = sigma_vx_algn * (pix_vtcs["timestamp"][mask]*25/1e9)
            t_xerr = 25*1e-9 *algn["v_x"]
            z0_xerr = sigma_z_algn * (ty*np.sin(txy) - tx*np.cos(txy))
            
            y0_err = sigma_y_algn
            z0_yerr = (-ty*np.cos(txy) - tx*np.sin(txy)) * sigma_z_algn
            x_yerr =(-np.sin(txy))*np.sqrt(pix_vtcs[mask]["vertex_cov_XX"])
            y_yerr = (np.cos(txy))*np.sqrt(pix_vtcs[mask]["vertex_cov_YY"])
            tx_yerr = (-algn["z_0"]*np.sin(txy)) * sigma_tx_algn
            ty_yerr =(-algn["z_0"]*np.cos(txy)) * sigma_ty_algn
            txy_yerr = (np.sin(txy)*(ty*algn["z_0"]-pix_vtcs[mask]["y"]) - np.cos(txy)*(algn["z_0"]*tx + pix_vtcs[mask]["x"])) * sigma_txy_algn
            vy_err = sigma_vy_algn * (pix_vtcs["timestamp"][mask]*25/1e9)
            t_yerr = 25*1e-9 *algn["v_y"]

            # z_tx_err = z_prime* (np.sin(ty)- np.cos(ty)) * sigma_tx_algn
            # z_ty_err = z_prime * (np.sin(ty)+ np.cos(ty)) * sigma_ty_algn
            # z_z0_err = np.sin(ty) -np.cos(ty) - np.sin(tx) - np.cos(tx) * sigma_z_algn
            # z_z_err = np.sin(ty) -np.cos(ty) - np.sin(tx) - np.cos(tx) * np.sqrt(pix_vtcs[mask]["vertex_cov_ZZ"])
            # print("algn_x", np.mean(algn_x))
            # print("sigma_txy_algn",  np.mean(sigma_txy_algn))
            # print("x0_err",  np.mean(x0_err))
            # print("txy_xerr", np.mean(txy_xerr))
            # print("x_xerr",  np.mean(x_xerr))
            # print("y_xerr",  np.mean(y_xerr))
            # print("tx_xerr",  np.mean(tx_xerr))
            # print("ty_xerr",  np.mean(ty_xerr))
            # print("vx_err",  np.mean(vx_err))
            # print("z0_xerr",  np.mean(z0_xerr))
            # print("t_xerr",  np.mean(t_xerr))
            sigma2_x = np.square(x_xerr)+ np.square(x0_err) + np.square(txy_xerr) + np.square(y_xerr) + np.square(tx_xerr) + np.square(ty_xerr) + np.square(vx_err) + np.square(z0_xerr) + np.square(t_xerr)
            sigma2_y = np.square(x_yerr)+ np.square(y0_err) + np.square(txy_yerr) + np.square(y_yerr) + np.square(tx_yerr) + np.square(ty_yerr) + np.square(vy_err) + np.square(z0_yerr)+ np.square(t_yerr)
            sigma2_z = np.square(sigma_z_algn) + pix_vtcs[mask]["vertex_cov_ZZ"]
            
            pix_vtcs["vertex_cov_XX"][mask] = sigma2_x
            pix_vtcs["vertex_cov_YY"][mask] = sigma2_y
            pix_vtcs["vertex_cov_ZZ"][mask] = sigma2_z

            # print( "sigma2_x", np.mean(sigma2_x))
            # print( "sigma2_y", np.mean(sigma2_y))
            # print( "sigma2_z", np.mean(sigma2_z))
            # raise

    save_table(array_in=pix_vtcs, path=pix_vtx_path[:-3] + "_emu_system.h5", node_name="Vertices")
    return (pix_vtx_path[:-3] + "_emu_system.h5")


def translate_pixel_tracks(pix_tracks_path, alignment_file_path, spill=None, steps=False):
    '''
    Translate pixel track data to emulsion rest frame. Alignment data from Christopher Betancourt, 10 alignment steps per spill.
    Which step to use is determined by the timestamp of the hit. In case of last version of file only one alignment for whole spill, but including velocity in y. 
    The translated data is saved in the same folder as the input file.
    ---------------
    INPUT:
        pix_tracks_path : str
            path to the pixel hits .h5 file
        alignment_file_path : str
            path to .yaml file with alignment values
        steps : bool
            if true use 10 steps, if false use new alignment with vy
    OUTPUT:
        nothing, aligned data is written to disk.
    '''
    pix_emu_alignment = load_config(alignment_file_path)

    pix_tracks = load_table(pix_tracks_path, table_name="Tracks")

    if steps == True:
        pix_tracks = pix_tracks[pix_tracks["spill"]==spill]
        algn_times = np.linspace(pix_tracks["timestamp"].min(), pix_tracks["timestamp"].max(), 11)
        for i in range(1, 11):

            pix_trks_mask = np.logical_and(pix_tracks["timestamp"] <= algn_times[i], pix_tracks["timestamp"] > algn_times[i-1])

            algn_step = i  # (timestamp*25/1e9) / 4.5 * 10
            algn = pix_emu_alignment["step%i" % algn_step]
            txy = algn["txy"] * 1e-3

            tx = np.cos(txy) * (-pix_tracks["x_slope"][pix_trks_mask] + algn["tx0"]*1e-3) - \
                np.sin(txy) * (pix_tracks["y_slope"][pix_trks_mask] + algn["ty0"]*1e-3)
            ty = np.cos(txy) * (pix_tracks["y_slope"][pix_trks_mask] + algn["ty0"]*1e-3) + \
                np.sin(txy) * (-pix_tracks["x_slope"][pix_trks_mask] + algn["tx0"]*1e-3)

            x = algn["x0"] + np.cos(txy) * (-pix_tracks["x"][pix_trks_mask] - algn["z0"] * tx) - np.sin(txy) * \
                (pix_tracks["y"][pix_trks_mask] - algn["z0"] * ty) + algn["v"] * (pix_tracks["timestamp"][pix_trks_mask]*25/1e9)
            y = algn["y0"] + pix_tracks["spill"][pix_trks_mask] * 2 + \
                np.cos(txy) * (pix_tracks["y"][pix_trks_mask] - algn["z0"] * ty) + np.sin(txy) * (-pix_tracks["x"][pix_trks_mask] - algn["z0"] * tx)

            pix_tracks["x_slope"][pix_trks_mask] = tx
            pix_tracks["y_slope"][pix_trks_mask] = ty

            pix_tracks["x"][pix_trks_mask] = x
            pix_tracks["y"][pix_trks_mask] = y

    else:
        for spill in np.unique(pix_tracks["spill"]):
            try:
                algn = pix_emu_alignment[int(spill)]
            except KeyError:
                # spill does not contain emulsion data, so no alignment values
                continue
            mask = pix_tracks["spill"]==spill
            txy = algn["txy_0"]# * 1e3

            tx = np.cos(txy) * (-pix_tracks["x_slope"][mask] + algn["tx_0"]) - np.sin(txy) * (pix_tracks["y_slope"][mask] + algn["ty_0"])
            ty = np.cos(txy) * (pix_tracks["y_slope"][mask]+ algn["ty_0"]) + np.sin(txy) * (-pix_tracks["x_slope"][mask] + algn["tx_0"])

            x = algn["x_0"] + np.cos(txy) * (-pix_tracks["x"][mask] - algn["z_0"] * tx) - np.sin(txy) * \
                (pix_tracks["y"][mask] - algn["z_0"] * ty) + algn["v_x"] * (pix_tracks["timestamp"][mask] * 25/1e9)
            y = algn["y_0"] + pix_tracks[mask]["spill"] * 2 + np.cos(txy) * (pix_tracks[mask]["y"] - algn["z_0"] * ty) + \
                np.sin(txy) * (-pix_tracks["x"][mask] - algn["z_0"] * tx) + algn["v_y"] * (pix_tracks["timestamp"][mask] * 25/1e9)

            pix_tracks["x_slope"][mask] = tx
            pix_tracks["y_slope"][mask] = ty

            pix_tracks["x"][mask] = x
            pix_tracks["y"][mask] = y

    save_table(array_in=pix_tracks, path=pix_tracks_path[:-3] + "_emu_system.h5", node_name="Tracks")
    return (pix_tracks_path[:-3] + "_emu_system.h5")


def translate_emulsion_tracks(emulsion_in_path):
    """
    Before alignment with pixel tracks the emulsion tracks are rotated by their own to align them to the beam
    """
    ty_algn = -4.63719e-03
    tx_algn = 2.15320e-03

    emu_tracks = load_table(emulsion_in_path, table_name="EmuTracks")

    emu_tracks["tx"] += tx_algn
    emu_tracks["ty"] += ty_algn

    # new_x = emu_tracks["x"] * np.cos(tx_algn) - emu_tracks["z"] * np.sin(tx_algn)
    # new_z = emu_tracks["x"] * np.sin(tx_algn) + emu_tracks["z"] * np.cos(tx_algn)
    # new_y = emu_tracks["y"] * np.cos(ty_algn) - new_z * np.sin(ty_algn)
    # new_zz = emu_tracks["y"] * np.sin(ty_algn) + new_z * np.cos(ty_algn)

    # new_x2 = emu_tracks["x2"] * np.cos(tx_algn) - emu_tracks["z2"] * np.sin(tx_algn)
    # new_z2 = emu_tracks["x2"] * np.sin(tx_algn) + emu_tracks["z2"] * np.cos(tx_algn)
    # new_y2 = emu_tracks["y2"] * np.cos(ty_algn) - new_z2 * np.sin(ty_algn)
    # new_z2z = emu_tracks["y2"] * np.sin(ty_algn) + new_z2 * np.cos(ty_algn)

    # new_vx = emu_tracks["vx"] * np.cos(tx_algn) - emu_tracks["vz"] * np.sin(tx_algn)
    # new_vz = emu_tracks["vx"] * np.sin(tx_algn) + emu_tracks["vz"] * np.cos(tx_algn)
    # new_vy = emu_tracks["vy"] * np.cos(ty_algn) - new_vz * np.sin(ty_algn)
    # new_vzz = emu_tracks["vy"] * np.sin(ty_algn) + new_vz * np.cos(ty_algn)

    # emu_tracks["x"] = new_x
    # emu_tracks["y"] = new_y
    # emu_tracks["z"] = new_zz

    # emu_tracks["x2"] = new_x2
    # emu_tracks["y2"] = new_y2
    # emu_tracks["z2"] = new_z2z

    # emu_tracks["vx"] = new_vx
    # emu_tracks["vy"] = new_vy
    # emu_tracks["vz"] = new_vzz

    save_table(array_in=emu_tracks, path=emulsion_in_path[:-3] + "_tx_aligned.h5", node_name="EmuTracks")
    print("aligned emulsion file saved at %s" % (emulsion_in_path[:-3] + "_tx_aligned.h5"))


def choose_tracks(emulsion_tracks, pix_tracks, matched_track_table, pix_hits, pix_vertices, match_chi2=None, max_z=None, min_z=None, timestamp=None, spill = None, max_emu_tracks = None):
    '''
    Randomly draw event from matched emulsion pixel tracks. First a random row of matched_tracks table is chosen, 
    then the timestamp of this row is picked and all tracks on pix_tracks with the same timestamp are chosen.
    Also all matched_track rows with this timestamp are chosen. 
    From these the emulsion track ids are determined so that the emulsion tracks can be drawn from the respective table.
    ---------------
    INPUT:
        emulsion_tracks : np.array
            structured array with emulsion track data, must contain trid, x, y, z, tx, ty
        pix_tracks : np.array
            structured array with pixel track data, must contain timestamp, x, y, x_slope, y_slope
        matched_track_table : np.array
            structured array with matched information. Must contain trid (==emu_track) and timestamp (==match_time)
        pix_hits : np.array
            structured array with pixel hit data, must contain timestamp hitx, hity, hitz
        match_chi2 : float
            maximum chi2 (NOT REDUCED CHI2!) for rows to be considered
        max_z: float
            maxium z vertex position in cm. The higher (more positive) the later track was created in emulsion, the shorter the travel distance in the emulsion block.
    OUTPUT:
        4 numpy arrays which contain:
            chosen emulsion tracks
            chosen pixel tracks
            chosen pixel hits
            matched pixel tracks
    '''

    # select tracks based on given conditions.
    if match_chi2 is not None:
        matched_track_table = matched_track_table[matched_track_table["match_chi2"] <= match_chi2]
    if max_z is not None:
        matched_track_table = matched_track_table[np.logical_and(matched_track_table["emu_z0"]>min_z, matched_track_table["emu_z0"] <= max_z)]

    if (max_emu_tracks is not None) and (timestamp is None):
        selected_vid, selected_indices, selected_counts = np.unique(matched_track_table["emu_vid"], return_index = True, return_counts = True)
        select_table = matched_track_table[selected_indices[np.logical_and(selected_counts>2, selected_counts<max_emu_tracks)]]
        logging.info("choosing event from %i entries" % select_table.shape[0])
    elif (max_emu_tracks is not None) and (timestamp is not None):
        logging.warning("WARNING: max n track selection can only work without specific timestamp!!")
        logging.warning("WARNING: max n track selection can only work without specific timestamp!!")
        logging.warning("WARNING: max n track selection can only work without specific timestamp!!")
        select_table = matched_track_table
    else:
        select_table = matched_track_table
    # select single events by the timestamp, interesting events in spill 10 are: 138311973 93265339 59931423 46589857 150901936 58472778 102484845 88880178 65745214
    # in case no specific event is given, choose random track
    if timestamp is None:
        # timestamps are only unique within a single spill. So if a specific timestamp is provided, one als needs to provide the spill number,
        # in the following only tracks from this spill will be considered
        chosen_index = np.random.choice(select_table.shape[0], 1)
        timestamp = select_table[chosen_index]["match_time"]
        spill = select_table[chosen_index]["spill"]
    
    matched_track_table = matched_track_table[matched_track_table["spill"]==spill]
    if pix_hits is not None:
        pix_hits = pix_hits[pix_hits["spill"]==spill]
    if pix_vertices is not None:
        pix_vertices = pix_vertices[pix_vertices["spill"]==spill]

    # get all emulsion track IDs matched to the same event
    event = matched_track_table[matched_track_table["match_time"] == timestamp]
    emulsion_track_IDs = event["emu_trk"]

    plot_pixel_tracks = []
    plot_pixel_hits = []
    plot_pixel_vertices = []
    pixel_vertex_ids = []
    vtx_tracks = []
    vertexIDs = []
    vertex_pos = {}
    nTracks_in_vtx = {}
    matched_emu_tracks = []
    plot_matched_pixel_tracks = []

    # collect pixel tracks based on timestamp
    n_matched_tracks = np.unique(matched_track_table[matched_track_table["match_time"] == timestamp]).shape[0]
    plot_pixel_tracks.extend(pix_tracks[pix_tracks["timestamp"] == timestamp])

    # get pixel hits with the same timestamp
    if pix_hits is not None:
        plot_pixel_hits.extend(pix_hits[pix_hits["timestamp"] == timestamp])
    if pix_vertices is not None:
        evt_vertices = pix_vertices[pix_vertices["timestamp"] == timestamp]
        vertex_ids, vtx_indices = np.unique(evt_vertices["vertexID"], return_index=True)
        plot_pixel_vertices.extend(evt_vertices[vtx_indices])
        pixel_vertex_ids.extend(vertex_ids.tolist())
    # collect emulsion tracks based on emu track ID
    for trackID in emulsion_track_IDs:
        matched_emu_tracks.extend(emulsion_tracks[emulsion_tracks["trackID"] == trackID])
        vertexIDs.extend(emulsion_tracks[emulsion_tracks["trackID"] == trackID]["vID"])

    # it turns out that tracks from different vertices can be matched, in order to display these correctly all vertices have to be identified
    for vertexID in np.unique(vertexIDs):
        emu_vtx_set = emulsion_tracks[emulsion_tracks["vID"] == vertexID]
        vtx_tracks.extend(emu_vtx_set)
        vertex_position = np.array([emu_vtx_set["vx"],emu_vtx_set["vy"],emu_vtx_set["vz"]])
        _, unq_pos_index, unq_counts = np.unique(vertex_position[0,:], return_index = True, return_counts = True)
        vertex_pos[vertexID] = vertex_position[:,unq_pos_index]
        nTracks_in_vtx[vertexID] = unq_counts

    # collect matched pixel tracks
    plot_matched_pixel_tracks.extend(event)

    # print event details
    logging.info("")
    logging.info("================== Spill %i - EVENT %9.i ==================" % (matched_track_table["spill"][0],timestamp))
    logging.info("     vertexIDs          :{}".format(' '.join(map(str,np.unique(vertexIDs).tolist()))) )
    for key in vertex_pos:
        logging.info("     vertexID %i" % key)
        for i, row in enumerate(vertex_pos[key].T):
            logging.info("          x=%7.4f  y= %7.4f  z= %7.4f, %2.i emulsion tracks"%(row[0],row[1],row[2], nTracks_in_vtx[key][i] ))
            # logging.info("     emulsion tracks    : %i " % )
    logging.info("")
    logging.info("     pixelvertexIDs          :{}".format(' '.join(map(str, pixel_vertex_ids))))
    for pix_vtx in plot_pixel_vertices:
        logging.info("     vertexID %i" % pix_vtx["vertexID"])
        logging.info("          x=%7.4f \u00B1 %.4f y= %7.4f \u00B1 %.4f  z= %7.4f \u00B1 %.4f, %2.i pixel tracks"%(pix_vtx["x"], np.sqrt(pix_vtx["vertex_cov_XX"]),pix_vtx["y"], np.sqrt(pix_vtx["vertex_cov_YY"]), pix_vtx["z"],np.sqrt(pix_vtx["vertex_cov_ZZ"]), pix_vtx["nTracks"] ))
    
    logging.info("     matched emulsion track IDs  :{}".format(' '.join(map(str, emulsion_track_IDs.tolist()))))#   , emulsion_track_IDs)
    logging.info("     pixel tracks                : %2.i" % len(plot_pixel_tracks))
    logging.info("     matched tracks              : %2.i = %.1f %% of pixel tracks" % (n_matched_tracks, n_matched_tracks/len(plot_pixel_tracks)*100))
    logging.info("                                      = %.1f %% of emulsion tracks" % (len(emulsion_track_IDs) / len(vtx_tracks)*100) )
    logging.info("     matching chi2               : " + "".join("{:.4f}, ".format(chi2) for chi2 in event["match_chi2"].tolist()))
    if pix_hits is not None:
        logging.info("     pixel hits                  : %i" % (np.unique(pix_hits[pix_hits["timestamp"] == timestamp]).shape[0]))
    logging.info("")
    logging.info("================== unmatched tracks =================")
    logging.info("             emulsion   : %2.i (= %.1f %%)" % (len(vtx_tracks) - len(emulsion_track_IDs), (len(vtx_tracks) - len(emulsion_track_IDs))/len(vtx_tracks) * 100))
    logging.info("             pixel      : %2.i (= %.1f %%)" % (len(plot_pixel_tracks) - len(plot_matched_pixel_tracks),
                                                  ((len(plot_pixel_tracks) - len(plot_matched_pixel_tracks)) / len(plot_pixel_tracks)) * 100))

    return np.array(matched_emu_tracks), np.array(vtx_tracks), np.array(plot_pixel_tracks), np.array(plot_pixel_hits), np.array(plot_matched_pixel_tracks), np.array(plot_pixel_vertices)


def plot_event_subplots(pix_tracks_path, emulsion_tracks_path, pix_hits_path = None, matched_tracks_path=None, pix_vertices_path = None, match_chi2=10., max_z=-1., min_z=None, timestamp=None, spill = None, max_emu_tracks = None, colortheme = "dark", fontsize = 24, print_only = False, plot_pixel_planes = True):
    '''
    plot event display with 3d plot and all 3 projections. Randomly pick one matched event from matched track table.
    Use emulsion track data from matched track table, first and last track segment. Plot matched pixel tracks in different color.
    All pixel hits are plotted.
    --------------
    INPUT:
        pix_tracks_path : string
            location of the pixel tracks .h5 file, needs columns x,y,z x_slope, y_slope spill and timestamp
        pix_hits_path : string
            location of pixel hits file, needs hitx hity hitz spill and timestamp column
        matched_tracks_path : string
            location of file with matched emulsion and pixel tracks. converted from .root file.
            column "emu_trk" is emulsion trackID "match_time" is pixel timestamp
        emulsion_tracks_path : string
            location of .h5 file converted from emulsion sf branch. 
    OUTPUT:
        pdf file saved at location of matched_tracks_path
        3d event display (plt.show())
    '''

    # helper function to access table in input files
    pix_tracks = load_table(pix_tracks_path, table_name="Tracks")
    if pix_hits_path:
        pix_hits = load_table(pix_hits_path, table_name="Hits")
    else:
        pix_hits = None

    matched_track_table = load_table(matched_tracks_path, table_name="MatchedTracks")
    emulsion_tracks = load_table(emulsion_tracks_path, table_name="EmuTracks")
    if pix_vertices_path:
        pix_vertices = load_table(pix_vertices_path)
    else:
        pix_vertices = None
    # choose tracks function randomly picks one of the matched tracks and finds corresponding pixel hits, pixel tracks and emulsion tracks
    matched_emu, missed_emu, pixel_tracks, pixel_hits, matched_pix_tracks, pixel_vertices = choose_tracks(
        emulsion_tracks, pix_tracks, matched_track_table, pix_hits, pix_vertices, match_chi2=match_chi2, max_z=max_z, min_z=min_z, timestamp=timestamp, spill = spill, max_emu_tracks = max_emu_tracks)
    if print_only:
        return
    fig = plt.figure(figsize=plt.figaspect(3/5.)*fontsize/10)
    # gridspec defines the location and number of subplots. It creates columns and rows.
    # On creating the subplots one can just index the gridspec (like numpy) to specify the location.
    # Here a 3x3 grid was chosen, the 3d plot uses 2 columns and 3 rows, while the 2d plots are 1x1
    gs = gridspec.GridSpec(3, 3, hspace=0.25)
    # dark background style changes grid, axes, legend etc...
    if colortheme == "dark":
        plt.style.use('dark_background')
        fig.set_facecolor('black')
    # suptitle is for the whole figure
    plt.suptitle("CH1R6 spill %i eventtime %i" % (pixel_tracks["spill"][0], pixel_tracks["timestamp"][0]), fontsize = fontsize)
    # facecolor changes the part of the viewer which is rendered around the actual plot
    
    # since fig.tight_layout() does not work with 3d plot, one has to adjust the positioning by hand
    plt.subplots_adjust(left=0, bottom=0.1, right=0.94, top=0.9)

    # adjust axes limits for 3d plot. Dynamically based on track position so that event is maximized
    # adjust x axis to right handed coordinate system ( x increases towards left)
    limit_addition = 1.
    x_lims = pixel_tracks["x"].mean() + limit_addition, pixel_tracks["x"].mean() - limit_addition
    z_lims = pixel_tracks["y"].mean() - limit_addition, pixel_tracks["y"].mean() + limit_addition

    ax = fig.add_subplot(gs[0, 2])  # x vs y
    ax.set_xlabel("x in cm", fontsize = fontsize)
    ax.set_ylabel("y in cm", fontsize = fontsize)
    ax.set_xlim(x_lims)
    # ax.set_ylim([z_lims[0])
    ax.tick_params(axis='both', which='major', labelsize=fontsize)

    # put x axis label on the right to save horizontal space
    ax.xaxis.set_label_coords(1.1, -0.08)

    ax2 = fig.add_subplot(gs[1, 2])  # x vs z
    ax2.set_xlabel("x in cm", fontsize = fontsize)
    ax2.set_ylabel("z in cm", fontsize = fontsize)
    ax2.set_xlim(x_lims)
    ax2.tick_params(axis='both', which='major', labelsize=fontsize)

    # put x axis label on the right to save horizontal space
    ax2.xaxis.set_label_coords(1.1, -0.08)

    ax4 = fig.add_subplot(gs[2, 2])  # y vs z
    ax4.set_xlabel("y in cm", fontsize = fontsize)
    ax4.set_ylabel("z in cm", fontsize = fontsize)
    ax4.set_xlim(z_lims)
    ax4.tick_params(axis='both', which='major', labelsize=fontsize)

    # put x axis label on the right to save horizontal space
    ax4.xaxis.set_label_coords(1.1, -0.08)

    # make 3d plot
    # here one can see how gridspec is used : gs[0:,]means first to last row and gs[,0:2] means first to second column
    ax3 = fig.add_subplot(gs[0:, 0:2], projection='3d')  # 3d
    ax3.set_xlabel("x in cm", fontsize = fontsize)
    ax3.set_ylabel("z in cm", fontsize = fontsize)
    ax3.set_zlabel("y in cm", fontsize = fontsize)
    ax3.tick_params(axis = 'both', which='major', labelsize=fontsize)
    ax3.xaxis.labelpad = fontsize
    ax3.yaxis.labelpad = fontsize 
    ax3.zaxis.labelpad = fontsize
    ax3.xaxis.set_major_locator(plt.MaxNLocator(6))
    ax3.yaxis.set_major_locator(plt.MaxNLocator(6))
    ax3.zaxis.set_major_locator(plt.MaxNLocator(6))
    

    # remove white color of axes writing
    ax3.w_xaxis.pane.fill = False
    ax3.w_yaxis.pane.fill = False
    ax3.w_zaxis.pane.fill = False
    ax3.grid(False)
    # adjust viewing position of 3d plot
    if plot_pixel_planes is not True:
        ax3.view_init(elev=35., azim=-135.)
        ax3.set_xlim3d(x_lims)  # x coordinate
        # ax3.set_ylim3d(-2.8,7) # z coordinate
        ax3.set_zlim3d(z_lims)  # y coordinate

    # z positions for pixel planes from pixel standalone alignment
    z_algn = 1.879810
    z = [-0.13, 0.52, 2.412, 3.09, 5.1, 5.79, 7.79, 8.46, 10.462, 11.17, 13.162, 13.85]
    z_pix_tracks = [z_value + z_algn for z_value in z[1:]]

    # propagate pixel track coordinates to create points for plotting
    track_x = np.array([pixel_tracks["x"] + pixel_tracks["x_slope"] * z_pos for z_pos in z_pix_tracks])
    track_y = np.array([pixel_tracks["y"] + pixel_tracks["y_slope"] * z_pos for z_pos in z_pix_tracks])
    track_z = np.array([pixel_tracks["z"] + z_pos for z_pos in z_pix_tracks])

    # choose colors table for dark background
    if colortheme=="dark" :
        colors = ['#8dd3c7', '#feffb3', '#bfbbd9', '#fa8174', '#81b1d2', '#fdb462', '#b3de69', '#bc82bd', '#ccebc4', '#ffed6f']
        hit_color = colors[0]
        emu_color = colors[5]
        pix_color = colors[7]
        matched_color = colors[3]
        emu_plane_color = colors[1]
        linewidth = 0.7
        alpha = 0.7
    else:
        cmap = cm.get_cmap('tab10')
        hit_color = cmap(0)
        emu_color = cmap(4)
        pix_color = cmap(7)
        matched_color = cmap(3)
        emu_plane_color = cmap(5)
        linewidth = 1.75
        alpha = 1
    vertex_color = "0.85" #"#1e9c89"
    vertex_edge = "#1e9c89"

    # display emulsion planes
    emulsion_thickness = np.linspace(0, 0.0455, 5)
    # create legend, but only for first iteration make label object. otherwise labels will be stacked
    for i in range(30):
        if i == 0:
            label = "emulsion plane"
        else:
            label = None
        # emulsion is 455 um thick, calculate z positions of planes accordingly
        z_values = -i*0.1455 - emulsion_thickness
        x_emu_plane, z_emu_plane, y_emu_plane = np.meshgrid(np.linspace(x_lims[0], x_lims[1], 5), z_values, np.linspace(z_lims[0], z_lims[1], 5))

        ax2.plot(np.linspace(x_lims[0], x_lims[1], 5), z_values, color=emu_plane_color, alpha=0.2)
        ax4.plot(np.linspace(z_lims[0], z_lims[1], 5), z_values, color=emu_plane_color, alpha=0.2)
        # use wireframe instead of plot_surface for more transparency
        ax3.plot_wireframe(x_emu_plane[0, :, :], z_emu_plane[0, :, :], y_emu_plane[0, :, :],
                           alpha=0.2, color=emu_plane_color, zorder=-1, rstride=5, cstride=5, label=label)
        # ax3.plot_surface(x_emu_plane[0, :, :], z_emu_plane[0, :, :], y_emu_plane[0, :, :],
        #                    alpha=0.2, color=emu_plane_color)
    
    # plot pixel planes
    pixel_frame_center_x, pixel_frame_center_y = translate_pixel_plane(0, 0, spill, timestamp)

    if plot_pixel_planes is True:
        Alpha= [0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031, 0.0031]
        Beta = [0.0068, 0.0068, 0.0068, 0.0068, 0.0068, 0.0068, 0.0068, 0.0068, 0.0068, 0.0068, 0.0068, 0.0068]
        plot_z_planes =  [z_value + z_algn for z_value in z[:]]

        x_ref = [1.5396  + plot_z_planes[0] * Alpha[0],
                -0.2310 + plot_z_planes[1] * Alpha[1],
                0.6960  + plot_z_planes[2] * Alpha[2],
                0.6940  + plot_z_planes[3] * Alpha[3],
                1.5285  + plot_z_planes[4] * Alpha[4],
                -0.2430 + plot_z_planes[5] * Alpha[5],
                0.6620  + plot_z_planes[6] * Alpha[6],
                0.6710  + plot_z_planes[7] * Alpha[7],
                1.5440  + plot_z_planes[8] * Alpha[8],
                -0.2505 + plot_z_planes[9] * Alpha[9],
                0.6455  + plot_z_planes[10] * Alpha[10],
                0.6320  + plot_z_planes[11] * Alpha[11],
                ]

        y_ref = [-0.0015  + plot_z_planes[0]  * Beta[0],
                -0.020   + plot_z_planes[1]  * Beta[1],
                0.7930   + plot_z_planes[2] * Beta[2],
                0.8990  + plot_z_planes[3]  * Beta[3],
                -0.0370  + plot_z_planes[4] * Beta[4],
                -0.0610  + plot_z_planes[5] * Beta[5],
                0.7200   + plot_z_planes[6] * Beta[6],
                0.9285  + plot_z_planes[7] * Beta[7],
                -0.0700  + plot_z_planes[8] * Beta[8],
                -0.0690  + plot_z_planes[9] * Beta[9],
                0.7660   + plot_z_planes[10] * Beta[10],
                0.8850  + plot_z_planes[11] * Beta[11],
                ]
        module_x_width = (78*2*250 + 2*450 + 2*500)*1e-4
        module_y_width = (336*50)*1e-4
        
        # ax3.scatter3D(pixel_frame_center_x, z_algn, pixel_frame_center_y, s=30, edgecolors=hit_color, facecolor="orange", alpha=1, label="pixel center")
        plane_x = []
        plane_y = []

        ax3.set_xlim3d(pixel_frame_center_x - 3, pixel_frame_center_x + 3)  # x coordinate
        ax3.set_ylim3d(-5, plot_z_planes[-1] + 2) # z coordinate
        ax3.set_zlim3d([pixel_frame_center_y - 2, pixel_frame_center_y + 2])  # y coordinate
        # ax3.view_init(elev=14., azim=-155)
        ax3.view_init(elev=12., azim=-173)

        for plane, z_plane in enumerate(plot_z_planes):
            if plane in[0,4,8]:
                plane_x_center = pixel_frame_center_x + x_ref[plane]
                plane_y_center = pixel_frame_center_y + y_ref[plane]
                x = [plane_x_center - module_y_width/2, plane_x_center + module_y_width/2]
                y = [plane_y_center - module_x_width/2, plane_y_center + module_x_width/2]
            elif plane in [1,5,9]:
                plane_x_center = pixel_frame_center_x + x_ref[plane]
                plane_y_center = pixel_frame_center_y + y_ref[plane]
                x = [plane_x_center - module_y_width/2, plane_x_center + module_y_width/2]
                y = [plane_y_center - module_x_width/2, plane_y_center + module_x_width/2]
            elif plane in [2,6,10]:
                plane_x_center = pixel_frame_center_x + x_ref[plane]
                plane_y_center = pixel_frame_center_y - y_ref[plane]
                x = [plane_x_center - module_x_width/2 , plane_x_center + module_x_width/2]
                y = [plane_y_center - module_y_width/2, plane_y_center + module_y_width/2]
            else:
                plane_x_center = pixel_frame_center_x + x_ref[plane]
                plane_y_center = pixel_frame_center_y + y_ref[plane]
                x = [plane_x_center - module_x_width/2 , plane_x_center + module_x_width/2]
                y = [plane_y_center - module_y_width/2, plane_y_center + module_y_width/2]
            plane_x.append(x)
            plane_y.append(y)
            z = [z_plane - 0.015, z_plane + 0.015]
            x_pix_plane, z_pix_plane, y_pix_plane = np.meshgrid(x, z, y)
            if plane ==0:
                pix_plane_label3d = "pixel planes"
            else:
                pix_plane_label3d = None
            surface = ax3.plot_surface(x_pix_plane[0, :, :], z_pix_plane[0, :, :], y_pix_plane[0, :, :], color= "grey", alpha = 0.07, edgecolor = "black", label =pix_plane_label3d )
            surface._facecolors2d = surface._facecolor3d
            surface._edgecolors2d = surface._edgecolor3d

            # rect = patches.Rectangle([x[0],y[0]], x[1]-x[0], y[1] - y[0], facecolor="grey", alpha=0.05, edgecolor = "black")

            # ax.add_patch(rect)
            # ax.axhspan(y[0], y[1], x[0], x[1], color = "grey")
            ax2.hlines(z_plane, x[0], x[1], color = "grey", alpha = 0.5)
            ax4.hlines(z_plane, y[0], y[1],  color="grey", alpha=0.5)

            # print("plane", plane, "delta x = %.4f delta y = %.4f"%(plane_x_center - pixel_frame_center_x, plane_y_center - pixel_frame_center_y), x_ref[plane], y_ref[plane])
            # print("----")
        

    for index in range(track_x.shape[1]):
        # see above
        if index == 0:
            label = "pixel track"
        else:
            label = None
        # pixel track plotting used thinner lines and transparency to make plot lighter and give focus to matched tracks
        ax.plot(track_x[:, index], track_y[:, index], color=pix_color, linewidth=linewidth, alpha=alpha)
        ax2.plot(track_x[:, index], track_z[:, index], color=pix_color, linewidth=linewidth, alpha=alpha)
        ax4.plot(track_y[:, index], track_z[:, index], color=pix_color, linewidth=linewidth, alpha=alpha)
        ax3.plot3D(track_x[:, index], track_z[:, index], track_y[:, index], color=pix_color, linestyle="-", linewidth=linewidth, alpha=alpha, label=label)

    for track in range(missed_emu.shape[0]):
        # se above
        if track == 0:
            label = "emulsion track"
        else:
            label = None
        # emulsion track plotting, use only two points.
        ax.plot([missed_emu["x"][track], missed_emu["vx"][track]], [missed_emu["y"][track], missed_emu["vy"][track]], color=emu_color, linewidth=linewidth, alpha=alpha)
        ax2.plot([missed_emu["x"][track], missed_emu["vx"][track]], [missed_emu["z"][track], missed_emu["vz"][track]], color=emu_color, linewidth=linewidth, alpha=alpha)
        ax4.plot([missed_emu["y"][track], missed_emu["vy"][track]], [missed_emu["z"][track], missed_emu["vz"][track]], color=emu_color, linewidth=linewidth, alpha=alpha)
        ax3.plot3D([missed_emu["x"][track], missed_emu["vx"][track]], [missed_emu["z"][track], missed_emu["vz"][track]],
                   [missed_emu["y"][track], missed_emu["vy"][track]], color=emu_color, label=label, linewidth=linewidth, alpha=alpha)

    for track in range(matched_emu.shape[0]):
        ax.plot([matched_emu["x"][track], matched_emu["vx"][track]], [matched_emu["y"][track],
                                                                      matched_emu["vy"][track]], color=matched_color, linestyle=(0, (5, 10)), linewidth = linewidth+1)
        ax2.plot([matched_emu["x"][track], matched_emu["vx"][track]], [matched_emu["z"][track],
                                                                       matched_emu["vz"][track]], color=matched_color, linestyle=(0, (5, 10)), linewidth = linewidth+1)
        ax4.plot([matched_emu["y"][track], matched_emu["vy"][track]], [matched_emu["z"][track],
                                                                       matched_emu["vz"][track]], color=matched_color, linestyle=(0, (5, 10)), linewidth = linewidth+1)
        ax3.plot3D([matched_emu["x"][track], matched_emu["vx"][track]], [matched_emu["z"][track], matched_emu["vz"][track]],
                   [matched_emu["y"][track], matched_emu["vy"][track]], color=matched_color, linewidth = linewidth+1, linestyle=(-2.5, (5, 2.5)))
        # project matched emulsion track through pixel telescope for debugging :
        # ax2.plot([matched_emu["x"][track] + matched_emu["tx"][track]*z_pix_tracks[-1], matched_emu["x"][track], matched_emu["vx"][track]], [z_pix_tracks[-1], matched_emu["z"][track], matched_emu["vz"][track]], color="tab:blue", linestyle=(0, (5, 10)))
        # ax4.plot([matched_emu["y"][track] + matched_emu["ty"][track]*z_pix_tracks[-1], matched_emu["y"][track], matched_emu["vy"][track]], [z_pix_tracks[-1], matched_emu["z"][track], matched_emu["vz"][track]], color="tab:blue", linestyle=(0, (5, 10)))


    for track in range(matched_pix_tracks.shape[0]):
        if track == 0:
            label = "matched track"
        else:
            label = None
        # print("last pixel x %.5f" % (matched_pix_tracks["match_pix_x"][track] + matched_pix_tracks["match_pix_tx"][track]*(z_pix_tracks[-1])))
        # plot matched tracks (should cover the already plotted pixel tracks)
        ax.plot([matched_pix_tracks["match_pix_x"][track], matched_pix_tracks["match_pix_x"][track] + matched_pix_tracks["match_pix_tx"][track]*z_pix_tracks[-1]],
                [matched_pix_tracks["match_pix_y"][track], matched_pix_tracks["match_pix_y"][track] + matched_pix_tracks["match_pix_ty"][track]*z_pix_tracks[-1]], color=matched_color, linestyle=(0, (5, 5)), linewidth = linewidth+1)
        ax2.plot([matched_pix_tracks["match_pix_x"][track], matched_pix_tracks["match_pix_x"][track] +
                  matched_pix_tracks["match_pix_tx"][track]*(z_pix_tracks[-1])], [0, z_pix_tracks[-1]], color=matched_color, linewidth = linewidth+1, linestyle=(0, (5, 5)))
        ax4.plot([matched_pix_tracks["match_pix_y"][track], matched_pix_tracks["match_pix_y"][track] +
                  matched_pix_tracks["match_pix_ty"][track]*(z_pix_tracks[-1])], [0, z_pix_tracks[-1]], color=matched_color, linewidth = linewidth+1, linestyle=(0, (5, 5)))
        # project matched pixel track through emulsion for debugging:
        # ax2.plot([matched_pix_tracks["match_pix_x"][track] + matched_pix_tracks["match_pix_tx"][track] * matched_pix_tracks["emu_z0"][track], matched_pix_tracks["match_pix_x"][track], matched_pix_tracks["match_pix_x"][track] +
        #           matched_pix_tracks["match_pix_tx"][track]*(z_pix_tracks[-1])], [matched_pix_tracks["emu_z0"][track] ,0, z_pix_tracks[-1]], color=matched_color, linestyle=(0, (5, 10)))
        # ax4.plot([matched_pix_tracks["match_pix_y"][track] + matched_pix_tracks["match_pix_ty"][track] * matched_pix_tracks["emu_z0"][track], matched_pix_tracks["match_pix_y"][track], matched_pix_tracks["match_pix_y"][track] +
        #           matched_pix_tracks["match_pix_ty"][track]*(z_pix_tracks[-1])], [matched_pix_tracks["emu_z0"][track], 0, z_pix_tracks[-1]], color=matched_color, linestyle=(0, (5, 10)))
        ax3.plot3D([matched_pix_tracks["match_pix_x"][track], matched_pix_tracks["match_pix_x"][track] + matched_pix_tracks["match_pix_tx"][track]*(z_pix_tracks[-1])], [0, z_pix_tracks[-1]],
                   [matched_pix_tracks["match_pix_y"][track], matched_pix_tracks["match_pix_y"][track] + matched_pix_tracks["match_pix_ty"][track]*(z_pix_tracks[-1])], color=matched_color, label=label,linewidth = linewidth+1, linestyle=(-2.5, (5, 2.5)))

    if pix_hits is not None:
        # plot pixel hits. Create empty circles by choosing facecolors= "None" and edgecolors to the color you want. s determines size of the circle
        ax.scatter(pixel_hits["hitx"], pixel_hits["hity"], s=9, edgecolors=hit_color, facecolors=hit_color, alpha=alpha)
        ax2.scatter(pixel_hits["hitx"], pixel_hits["hitz"], s=9, edgecolors=hit_color, facecolors=hit_color, alpha=alpha)
        ax4.scatter(pixel_hits["hity"], pixel_hits["hitz"], s=9, edgecolors=hit_color, facecolors=hit_color, alpha=alpha)
        ax3.scatter3D(pixel_hits["hitx"], pixel_hits["hitz"], pixel_hits["hity"], s=9, edgecolors=hit_color, facecolor=hit_color, alpha=alpha, label="pixel hit")
    if pix_vertices is not None:
        # ax.scatter(pixel_vertices["x"],pixel_vertices["y"], s=10, edgecolors="k",  alpha=1)
        # ax2.scatter(pixel_vertices["x"],pixel_vertices["z"], s=10, edgecolors="k",  alpha=1)
        # ax4.scatter(pixel_vertices["y"],pixel_vertices["z"], s=10, edgecolors="k",alpha=1)
        ax3.scatter3D(pixel_vertices["x"],pixel_vertices["z"],pixel_vertices["y"], s=20, edgecolors=vertex_edge, linewidth = 2, facecolor=vertex_color, alpha=alpha, label = "pixel vertex")
        for i in range(0,(pixel_vertices.size)):
            vtx = pixel_vertices[i]
            
            ax.add_patch(Ellipse([vtx["x"],vtx["y"]],np.sqrt(vtx["vertex_cov_XX"]), np.sqrt(vtx["vertex_cov_XX"]), edgecolor = vertex_edge, linewidth = 2, facecolor = vertex_color))
            ax.set_xlim([vtx["x"]-np.sqrt(vtx["vertex_cov_XX"])*2, vtx["x"]+np.sqrt(vtx["vertex_cov_XX"])*2 ])
            ax.set_ylim([vtx["y"]-np.sqrt(vtx["vertex_cov_YY"])*1, vtx["y"]+np.sqrt(vtx["vertex_cov_YY"])*1])
            ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
            ax.xaxis.set_major_locator(plt.MaxNLocator(4))

            ax2.add_patch(Ellipse([vtx["x"],vtx["z"]],np.sqrt(vtx["vertex_cov_XX"]), np.sqrt(vtx["vertex_cov_ZZ"]), edgecolor = vertex_edge, linewidth = 2, facecolor = vertex_color))
            ax2.set_xlim([vtx["x"]-np.sqrt(vtx["vertex_cov_XX"])*2, vtx["x"]+np.sqrt(vtx["vertex_cov_XX"])*2 ])
            ax2.set_ylim([vtx["z"]-np.sqrt(vtx["vertex_cov_ZZ"])*1, vtx["z"]+np.sqrt(vtx["vertex_cov_ZZ"])*2 ])
            ax2.xaxis.set_major_locator(plt.MaxNLocator(4))
            ax2.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

            ax4.add_patch(Ellipse([vtx["y"],vtx["z"]],np.sqrt(vtx["vertex_cov_YY"]), np.sqrt(vtx["vertex_cov_ZZ"]), edgecolor = vertex_edge, linewidth = 2, facecolor = vertex_color))
            ax4.set_xlim([vtx["y"]-np.sqrt(vtx["vertex_cov_YY"])*2, vtx["y"]+np.sqrt(vtx["vertex_cov_YY"])*2 ])
            ax4.set_ylim([vtx["z"]-np.sqrt(vtx["vertex_cov_ZZ"])*1, vtx["z"]+np.sqrt(vtx["vertex_cov_ZZ"])*2 ])
            ax4.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
            ax4.xaxis.set_major_locator(plt.MaxNLocator(4))
            
    ax3.legend(prop={'size': fontsize}, markerscale = 6, loc= "upper right" )
    plt.savefig(matched_tracks_path[:-3] + "_event_display.pdf")
    plt.show()


if __name__ == "__main__":

    '''
    ts         spill
    128973341  11
    83875305   11
    152786481  12
    131410163  9
    178768554  11
    143070640  10
    90503879   11
    138148099  9
    39583218  10 <---------
    64776028 11 !!

    164987503 9 <-- Vertex
    148026126 9 <-- Vertex
    168407127 10 <--- BEST VERTEX

    76280269 10 <--- check with 6 hits/track
    '''

    home = str(Path.home())
    align = False
    if align:
        alignment_yaml = get_alignment(home + "/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched.h5")

        # translated_track_path =  translate_pixel_tracks(home + "/cernbox/SHiP/charm_xsec_july18/pix_only_vtx/pix_tracks_6hits_no_share_vertex_z_7.5_0_chi_25.h5",
        #                     alignment_yaml)
        translated_track_path =  translate_pixel_tracks(home + "/git/ship_tracking/data/run_2793/pix_tracks.h5",
                            alignment_yaml)
        translated_hits_path = translate_pixel_hits(home + "/cernbox/SHiP/charm_xsec_july18/pix_only_vtx/pix_hits_6hits_no_share_vertex_z_7.5_0_chi_25.h5",
                            alignment_yaml)
        translated_pix_vtx_path = translate_pixel_vertices(home + "/git/ship_tracking/data/run_2793/pix_vertices.h5", alignment_yaml)
        logging.info("saved pixel vertices at %s" % translated_pix_vtx_path)
        plot_translated_pixel_tracks(translated_track_path)
        plot_emulsion_tracks(home + "/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf.h5")

        matched_tracks_path = get_matched_emulsion_tracks(matched_tracks_in_path=home + "/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched.h5",
                                    emulsion_in_path=home + "/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf_trackID_vtx_merged.h5")

        translate_emulsion_tracks(home + "/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf_trackID_vtx_merged.h5")

    else:
        translated_track_path = "/home/niko/git/ship_tracking/data/run_2793/pix_tracks_emu_system.h5"
        translated_hits_path = "/home/niko/cernbox/SHiP/charm_xsec_july18/pix_only_vtx/pix_hits_6hits_no_share_vertex_z_7.5_0_chi_25_emu_system.h5"
        translated_pix_vtx_path = "/home/niko/git/ship_tracking/data/run_2793/pix_vertices_emu_system.h5"
        matched_tracks_path = "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched_z_coordinate.h5"


    plot_event_subplots(pix_tracks_path=translated_track_path,
                        pix_hits_path = None,# translated_hits_path,
                        matched_tracks_path=matched_tracks_path,
                        emulsion_tracks_path=home + "/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf_trackID_vtx_merged_tx_aligned.h5",
                        pix_vertices_path = translated_pix_vtx_path,
                        match_chi2=None, max_z=None, timestamp= 168407127, spill = 10, max_emu_tracks = None, colortheme = "light", print_only=False) # 39583218
