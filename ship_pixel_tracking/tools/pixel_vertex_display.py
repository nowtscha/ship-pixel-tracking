from pathlib import Path
import os

import matplotlib.pyplot as plt
from matplotlib import colors, cm
import matplotlib
import matplotlib.gridspec as gridspec
import numpy as np
from ship_pixel_tracking.tools.analysis_utils import load_table, load_config, save_table, save_config
from ship_pixel_tracking.tools.plot_utils import plot_2d_hist, plot_1d_bar, create_polyplot_fig, save_polyplot_fig

from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.mplot3d import Axes3D


plane_z = [-1300*1e-4, 5200*1e-4, 24120*1e-4, 30900*1e-4, 51000*1e-4, 57900*1e-4, 77900*1e-4, 84600*1e-4, 104620*1e-4, 111700*1e-4,131620*1e-4, 138500*1e-4]


def choose_event(pix_vertices_in, pix_tracks_in, pix_hits_in=None, event_number = None, spill = None, timestamp=None, min_vtcs = 1):

    if event_number:
        event_vtx = pix_vertices_in[pix_vertices_in["event_number"]==event_number]
        event_tracks = pix_tracks_in[pix_tracks_in["event_number"]==event_number]
        if isinstance(pix_hits_in, (np.ndarray, np.generic)):
            event_hits = pix_hits_in[pix_hits_in["event_number"]==event_number]
    elif timestamp:
        spill_vtx = pix_vertices_in[pix_vertices_in["spill"]==spill]
        spill_tracks = pix_tracks_in[pix_tracks_in["spill"]==spill]
        event_vtx = spill_vtx[spill_vtx["timestamp"]==timestamp]
        event_tracks = spill_tracks[spill_tracks["timestamp"]==timestamp]
        if isinstance(pix_hits_in, (np.ndarray, np.generic)):
            spill_hits = pix_hits_in[pix_hits_in["spill"]==spill]
            event_hits = spill_hits[spill_hits["timestamp"]==timestamp]
    else:
        #randomly choose event with at least min_vtcs vertices
        select_vtcs = pix_vertices_in[pix_vertices_in["nvtx_in_evt"]>=min_vtcs]
        chosen_index = np.random.choice(select_vtcs.shape[0], 1)
        timestamp = select_vtcs[chosen_index]["match_time"]

        event_vtx = pix_vertices_in[pix_vertices_in["timestamp"]==timestamp]
        event_tracks = pix_tracks_in[pix_tracks_in["timestamp"]==timestamp]
        if isinstance(pix_hits_in, (np.ndarray, np.generic)):
            event_hits = pix_hits_in[pix_hits_in["timestamp"]==timestamp]

    vtx_tracks = {}
    event_track_mask = np.ones(shape = event_tracks.shape, dtype = bool)
    for vtx in np.unique(event_vtx["vertexID"]):
        track_ids = event_vtx[event_vtx["vertexID"]==vtx]["trackID"]
        # TODO: better selection without second for loop?
        tracklist = []
        for trackID in track_ids:
            tracklist.append(event_tracks[event_tracks["trackID"]==trackID])
            event_track_mask[event_tracks["trackID"]==trackID] = 0

        vtx_tracks[vtx] = np.array(tracklist)

    event_vertices = event_vtx[np.unique(event_vtx["vertexID"], return_index = True)[1]]

    if 'event_hits' in locals():
        # event_hits exists.
        return event_vertices, vtx_tracks, event_hits, event_tracks[event_track_mask]
    else:
        return event_vertices, vtx_tracks


def plot_event(pix_vertices, pix_tracks, out_path, pix_hits=None, remaining_tracks = None, zoom=False):
    '''
    plot event display with pixel vertices.
    '''
    cmap = matplotlib.cm.get_cmap('viridis')

    color_0 = cmap(0.1)
    color_1 = cmap(0.2)
    color_5 = cmap(0.375)
    color_2 = cmap(0.55)
    color_3 = cmap(0.75)
    color_4 = cmap(0.9)

    colors = [color_1, color_2, color_3, color_4, color_5]
    fig, axes = plt.subplots(2, 1, sharey=False, sharex=False, squeeze=True, gridspec_kw = {'height_ratios':[1,1]}, figsize = (6,5))
    fig.suptitle("Event %s" %event_vertices[0]["timestamp"])

    axes[0].set_title("z-x view")
    axes[1].set_title("z-y view")
    axes[0].set_xlabel("z in cm")
    axes[0].set_ylabel("x in cm")
    axes[1].set_xlabel("z in cm")
    axes[1].set_ylabel("y in cm")

    # negative z values
    max_z = pix_vertices["z"].max()* 0.85
    min_z = pix_vertices["z"].min()* 1.05

    max_x = pix_vertices["x"].max()
    min_x = pix_vertices["x"].min()

    if max_x<0:
        xlims_0 = [min_x*1.1, max_x * 0.9]
    else:
        xlims_0 = [min_x*0.9, max_x * 1.1]

    max_y = pix_vertices["y"].max()
    min_y = pix_vertices["y"].min()
    if max_y<0:
        xlims_1 = [min_y * 1.1, max_y*0.9]
    else:
        xlims_1 = [min_y*0.9, max_y * 1.1]
    
    j=0
    for plane in plane_z:
        label = None
        axes[0].axvline(plane, linestyle = "-", linewidth = 0.75, alpha = 0.5, color = "grey", label = label, zorder=1)
        axes[1].axvline(plane, linestyle = "-", linewidth = 0.75, alpha = 0.5, color = "grey", label = label, zorder = 1)
    
    if pix_hits is not None:
        axes[0].plot(pix_hits["hitz"], pix_hits["hitx"], color = "grey", linestyle = "None", marker = ".", markersize = 1.5, alpha = 0.75, label = "hit")
        axes[1].plot(pix_hits["hitz"], pix_hits["hity"], color = "grey", linestyle = "None", marker = ".", markersize = 1.5, alpha = 0.75, label = None)
    if remaining_tracks is not None:
        tracks_x = np.array([remaining_tracks["x"]+remaining_tracks["x_slope"]*-5.5, remaining_tracks["x"]+remaining_tracks["x_slope"]*plane_z[-1]])
        tracks_y = np.array([remaining_tracks["y"]+remaining_tracks["y_slope"]*-5.5, remaining_tracks["y"]+remaining_tracks["y_slope"]*plane_z[-1]])
        tracks_z = np.zeros_like(tracks_x)
        tracks_z[0,:] = min_z
        tracks_z[1,:] = plane_z[-1]
        axes[0].plot(tracks_z, tracks_x, color = "grey", linewidth = 0.75, label = ["track w/o vtx"]+[None]*(tracks_x.shape[1]-1))
        axes[1].plot(tracks_z, tracks_y, color = "grey", linewidth = 0.75, label = None) #["track w/o vtx"]+[None]*(tracks_x.shape[1]-1))
        for i in range(tracks_x.shape[1]):
            axes[0].annotate("%i" % remaining_tracks[i]["trackID"], (plane_z[-1], tracks_x[1,i]), (plane_z[-4], i*0.5), arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))
            axes[1].annotate("%i" % remaining_tracks[i]["trackID"], (plane_z[-1], tracks_y[1,i]), (plane_z[-4], i*0.5), arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))
    x_bounds, y_bounds = [],[]
    
    for vtx, tracks in pix_tracks.items():
        vtx_row = pix_vertices[pix_vertices["vertexID"]==vtx]
        vtx_label = "vertex %i\n$\chi^2_{red}$ = %.3f" % (vtx,vtx_row["vertex_chi2_red"])
        axes[0].errorbar(vtx_row["z"], vtx_row["x"], marker = "o", markersize = 2, linestyle = "None", yerr = np.sqrt(vtx_row["vertex_cov_XX"]), xerr = np.sqrt(vtx_row["vertex_cov_ZZ"]), elinewidth = 1, capsize = 1, label = vtx_label, color = colors[j])
        axes[1].errorbar(vtx_row["z"], vtx_row["y"], marker = "o", markersize = 2, linestyle = "None", yerr = np.sqrt(vtx_row["vertex_cov_YY"]), xerr = np.sqrt(vtx_row["vertex_cov_ZZ"]), elinewidth = 1, capsize = 1, label = None, color = colors[j])
        i=0
        for track in tracks:
            if pix_hits is not None:
                label = None
                track_hits = pix_hits[pix_hits["trackID"]==track["trackID"]]
                axes[0].plot(track_hits["hitz"], track_hits["hitx"], color = colors[j], markeredgecolor = color_0, markeredgewidth = 0.2, linestyle = "None", marker = "o", markersize = 1.5, label = label)
                axes[1].plot(track_hits["hitz"], track_hits["hity"], color = colors[j], markeredgecolor = color_0, markeredgewidth = 0.2, linestyle = "None", marker = "o", markersize = 1.5, label = None)
            
            track_x = np.array([track["x"]+track["x_slope"]*(vtx_row["z"]-np.sqrt(vtx_row["vertex_cov_ZZ"])*5), track["x"]+track["x_slope"]*plane_z[-1]]).flatten()
            track_y = np.array([track["y"]+track["y_slope"]*(vtx_row["z"]-np.sqrt(vtx_row["vertex_cov_ZZ"])*5), track["y"]+track["y_slope"]*plane_z[-1]]).flatten()
            track_x_update = np.array([track["x_update"]+track["x_slope_update"]*(vtx_row["z"]-np.sqrt(vtx_row["vertex_cov_ZZ"])*5), track["x_update"]+track["x_slope_update"]*plane_z[-1]]).flatten()
            track_y_update = np.array([track["y_update"]+track["y_slope_update"]*(vtx_row["z"]-np.sqrt(vtx_row["vertex_cov_ZZ"])*5), track["y_update"]+track["y_slope_update"]*plane_z[-1]]).flatten()
            track_z = np.array([float(vtx_row["z"]-np.sqrt(vtx_row["vertex_cov_ZZ"])*5), plane_z[-1]])

            track_x_error = np.array([np.sqrt(np.square(track["x_error"]) + np.square(track["x_slope_error"] * vtx_row["z"])),
            np.sqrt(np.square(track["x_error"]) + np.square(track["x_slope_error"]* plane_z[-1]))]).flatten()

            track_y_error = np.array([np.sqrt(np.square(track["y_error"]) + np.square(track["y_slope_error"] * vtx_row["z"])),
            np.sqrt(np.square(track["y_error"]) + np.square(track["y_slope_error"]* plane_z[-1]))]).flatten()

            # x_bounds.append(track["x_update"]+track["x_slope_update"]*max_z)
            # y_bounds.append(track["y_update"]+track["y_slope_update"]*max_z)

            if i==0:
                label = "track"
                label2 = "updated track"
            else:
                label = None
                label2 = None
            i+=1

            x_errorbounds = [track_x - track_x_error, track_x + track_x_error]
            y_errorbounds = [track_y - track_y_error, track_y + track_y_error]
            axes[0].plot(track_z, track_x, color = colors[j], label = label, linewidth = 1)
            axes[1].plot(track_z, track_y, color = colors[j], label = None, linewidth = 1)
            axes[0].fill_between(track_z, x_errorbounds[0], x_errorbounds[1], color = colors[j], alpha = 0.2)
            axes[1].fill_between(track_z, y_errorbounds[0], y_errorbounds[1], color = colors[j], alpha = 0.2)
            axes[0].plot(track_z, track_x_update, color = colors[j], label = label2, linestyle ="--", linewidth = 0.75)
            axes[1].plot(track_z, track_y_update, color = colors[j], label = None, linestyle ="--", linewidth = 0.75)
            
            x_bounds.extend([x_errorbounds[0][0] ,x_errorbounds[1][0]])
            y_bounds.extend([y_errorbounds[0][0] ,y_errorbounds[1][0]])
        # axes[0].annotate("vtx $\chi^2_{red}$ = %.4f" %vtx_row["vertex_chi2_red"], (vtx_row["z"], vtx_row["x"]), (vtx_row["z"]*j*0.75, vtx_row["x"]+j*0.25), arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0.3', color='k'), fontsize = 8)
        j+=1

    if zoom:
        max_track_x = np.max(x_bounds)
        min_track_x = np.min(x_bounds)

        min_track_y = np.min(y_bounds)
        max_track_y = np.max(y_bounds)

        x_lims = [np.min((min_track_x, xlims_0[0])), np.max((max_track_x, xlims_0[1]))]
        y_lims = [np.min((min_track_y, xlims_1[0])), np.max((max_track_y, xlims_1[1]))]

        axes[0].set_xlim(min_z, max_z)
        axes[0].set_ylim(x_lims)
        axes[1].set_xlim(min_z, max_z)
        axes[1].set_ylim(y_lims)

    fig.legend(bbox_to_anchor=(1.2, 0.88),prop={'size': 8})
    plt.tight_layout()
    plt.savefig(out_path, bbox_inches='tight')



if __name__ == "__main__":

    '''
    ts         spill
    128973341  11
    83875305   11
    152786481  12
    131410163  9
    178768554  11
    143070640  10
    90503879   11
    138148099  9
    39583218  10 <---------
    64776028 11 !!

    164987503 9 <-- Vertex
    '''
    event_dict = {  128973341 : 11, 
                    83875305 : 11, 
                    152786481 : 12, 
                    131410163 : 9, 
                    178768554 : 11, 
                    143070640 : 10, 
                    # 90503879 : 10, 
                    138148099 : 9, 
                    39583218 : 10, 
                    64776028 : 11, 
                    164987503 : 9
                    }

    home = str(Path.home())

    vertices = load_table(home + "/git/ship_tracking/data/run_2793/pix_vertices.h5")
    tracks = load_table(home + "/git/ship_tracking/data/run_2793/pix_tracks.h5")
    hits = load_table(home + "/git/ship_tracking/data/run_2793/pix_hits.h5")
    
    save_path = "/home/niko/cernbox/storage/pix_vtx_displays/"
    if not os.path.exists(os.path.dirname(save_path)):
        os.makedirs(os.path.dirname(save_path))

    for timestamp, spill in event_dict.items():
        print("time", timestamp, " : spill", spill)
        event_vertices, event_tracks, event_hits, remaining_tracks = choose_event(vertices, tracks, pix_hits_in = hits, timestamp =timestamp, spill=spill )
        plot_event(event_vertices, event_tracks, out_path=save_path + "event_display_%s_spill_%s.pdf" %(timestamp, spill), pix_hits = event_hits, remaining_tracks = remaining_tracks )
        plot_event(event_vertices, event_tracks, out_path=save_path + "event_display_%s_spill_%s_zoom.pdf" % (timestamp, spill), pix_hits = event_hits, remaining_tracks = remaining_tracks, zoom=True )
