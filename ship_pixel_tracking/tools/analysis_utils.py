import numpy as np
import re
import os
import multiprocessing
import logging
import tables as tb
from tqdm import tqdm
from beam_telescope_analysis.tools import analysis_utils as bta_ana_utils
from scipy import optimize, stats
from scipy.optimize import curve_fit, least_squares
from scipy.special import erf
from numba import njit
# from tables import table
from tqdm import tqdm
from yaml import safe_load, safe_dump


def read_csv_to_h5(in_file_path, out_file_path=None, chunksize = 500000):
    import pandas as pd
    if out_file_path is None:
        out_file_path = in_file_path[:-4] + ".h5"
    table_out = pd.HDFStore(out_file_path, complevel=5)
    for chunk in tqdm(pd.read_csv(in_file_path, chunksize=chunksize)):
        table_out.append("Converted", chunk, data_columns=True, index=False, complevel=5)
    table_out.create_table_index("Converted", columns=True, optlevel=9, kind="full")
    table_out.close()


def convert_cov_csv(covs_in_path):
    covs_dtype = [("trk", np.int32),("x_x", np.float16),("x_y", np.float16), ("x_tx", np.float16),("x_ty", np.float16),
                ("y_y", np.float16), ("y_tx", np.float16), ("y_ty", np.float16), ("tx_tx", np.float16),("tx_ty", np.float16),("ty_ty", np.float16),
                ("x_x_2", np.float16),("x_y_2", np.float16), ("x_tx_2", np.float16),("x_ty_2", np.float16), ("y_y_2", np.float16),
                ("y_tx_2", np.float16), ("y_ty_2", np.float16), ("tx_tx_2", np.float16),("tx_ty_2", np.float16),("ty_ty_2", np.float16)
            ]
    covs = np.loadtxt(covs_in_path, dtype = covs_dtype, delimiter = ",")
    save_table(covs, "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/CH1R6_emu_sf_covs.h5", node_name="CovMat")


def get_busy_events(pix_tracks_in, min_tracks, max_tracks):
    events, indices, counts = np.unique(pix_tracks_in["event_number"],return_index = True, return_counts = True)
    mask = np.ones(shape = pix_tracks_in.shape)
    mask[indices[np.logical_and(counts==1)]] = 0
    return pix_tracks_in[mask]


def fill_global_trackID(pix_tracks_in):
    pix_tracks = load_table(pix_tracks_in)
    for i in range(pix_tracks.size):
        pix_tracks[i]["global_trackID"] = i

    save_table(pix_tracks, pix_tracks_in, node_name = "Tracks")


def _make_matrix_psd_mod(A, atol=1e-5, rtol=1e-8): 
    """Find the nearest positive-definite matrix to input
    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].
    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd
    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6

    rtol and atol for MICROMETER VALUES !!!!!!!!!!
    """
    A3 = np.zeros((A.shape))
    B = (A + A.T) / 2
    _, s, V = np.linalg.svd(B)
    H = np.dot(V.T, np.dot(np.diag(s), V))
    A2 = (B + H) / 2
    A3 = (A2 + A2.T) / 2
    if np.all(np.linalg.eigvalsh(A3) >= 0.0):
        tol_A3 = atol + rtol * np.absolute(A3)
        if np.any(np.absolute(A - A3) > tol_A3):  # Check if corrected (psd) matrix did not change too much.
            raise RuntimeError('Output matrix differs too much from input matrix during nearest PSD')
    else:
        spacing = np.spacing(np.linalg.norm(A))
        I_d = np.eye(A.shape[0])
        k = 1
        while not np.all(np.linalg.eigvalsh(A3) >= 0.0):
            mineig = np.min(np.real(np.linalg.eigvalsh(A3)))
            A3 += I_d * (-mineig * k**2 + spacing)
            k += 1
        tol_A3 = atol + rtol * np.absolute(A3)
        if np.any(np.absolute(A - A3) > tol_A3):  # Check if corrected (psd) matrix did not change too much.
            raise RuntimeError('Output matrix differs too much from input matrix during nearest PSD')
    return A3


def _make_matrix_psd(A, atol=1e-8, rtol=1e-12):
    """Find the nearest positive-definite matrix to input
    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].
    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd
    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6
    """
    A3 = np.zeros((A.shape))
    for i in range(A.shape[0]):
        B = (A[i] + A[i].T) / 2
        _, s, V = np.linalg.svd(B)
        H = np.dot(V.T, np.dot(np.diag(s), V))
        A2 = (B + H) / 2
        A3[i] = (A2 + A2.T) / 2
        if np.all(np.linalg.eigvalsh(A3[i]) >= 0.0):
            tol_A3 = atol + rtol * np.absolute(A3[i])
            if np.any(np.absolute(A[i] - A3[i]) > tol_A3):  # Check if corrected (psd) matrix did not change too much.
                raise RuntimeError('Output matrix differs too much from input matrix during nearest PSD')
            continue
        else:
            spacing = np.spacing(np.linalg.norm(A[i]))
            I_d = np.eye(A[i].shape[0])
            k = 1
            while not np.all(np.linalg.eigvalsh(A3[i]) >= 0.0):
                mineig = np.min(np.real(np.linalg.eigvalsh(A3[i])))
                A3[i] += I_d * (-mineig * k**2 + spacing)
                k += 1
            tol_A3 = atol + rtol * np.absolute(A3[i])
            if np.any(np.absolute(A[i] - A3[i]) > tol_A3):  # Check if corrected (psd) matrix did not change too much.
                raise RuntimeError('Output matrix differs too much from input matrix during nearest PSD')
    return A3


def _covariance(x,y):
    mu_x, mu_y = x.mean(), y.mean()
    return np.sum((x - mu_x)*(y - mu_y))/(len(x)-1)

def cov_mat(X):
    '''
    covariance matrix for 4-parameter vector with arbitrary number of measurements
    '''

    return np.array([[_covariance(X[:,0], X[:,0]),_covariance(X[:,0], X[:,1]), _covariance(X[:,0], X[:,2]), _covariance(X[:,0], X[:,3])],
                    [_covariance(X[:,1], X[:,0]),_covariance(X[:,1], X[:,1]), _covariance(X[:,1], X[:,2]), _covariance(X[:,1], X[:,3])],
                    [_covariance(X[:,2], X[:,0]),_covariance(X[:,2], X[:,1]), _covariance(X[:,2], X[:,2]), _covariance(X[:,2], X[:,3])],
                    [_covariance(X[:,3], X[:,0]),_covariance(X[:,3], X[:,1]), _covariance(X[:,3], X[:,2]), _covariance(X[:,3], X[:,3])]])

def count_column(array_in):
    '''
    count occurences of entry in array in and return array of same size as input with counts
    '''
    counts = np.zeros(shape = array_in.shape[0])
    value = array_in[0]
    count = 0
    for i, row in enumerate(array_in):
        if row != value:
            counts[i-count:i] = count
            count = 1
            value = row
        elif row == value:
            count +=1
    return counts
            

def transform_to_emulsion(tracks_in, matched_tracks_in=None, alignment_file=None):
    '''
    INPUT:
        tracks_in :         numpy structured array
            pixel track data in pixel rest frame
        matched_tracks_in : numpy structured array
            already matched data, used for alignment values and dtype of output_array
        alignment_file : string
            path to alignment file. Preferred over matched_tracks_in array
    OUTPUT:
        out_array : numpy structured array 
            contains transformed pixel data and alignment coordinates. NO EMULSION TRACK DATA OR EVENT INFOS

    '''

    if matched_tracks_in is not None:
        out_array = np.empty(shape = tracks_in.shape, dtype = matched_tracks_in.dtype)
        spills, indices = np.unique(matched_tracks_in["spill"], return_index=True)
    elif alignment_file is not None:
        out_array = np.full(fill_value = 0, shape = tracks_in.shape, dtype = matched_emulsion_pixel_dtype)
        spills = np.unique(tracks_in["spill"])
        # ugly hack to have only one for loop below
        indices = spills
        alignment = load_config(alignment_file)
    else:
        raise RuntimeError("either matched tracks table or alignment file have to be provided")
    if (matched_tracks_in is not None) and (alignment_file is not None):
        print("example array AND alignment file provided, choosing alignment file")

    for spill, index in zip(spills, indices):
        if matched_tracks_in:
            algn = matched_tracks_in[index]
        else:
            try :
                algn = alignment[spill]
            except KeyError:
                # spill does not contain emulsion data, so no alignment values
                print("no alignment values for spill %i continue to next spill" % spill)
                continue
        mask = tracks_in["spill"] == spill
        txy = algn["txy_0"]

        tx = np.cos(txy) * (-tracks_in[mask]["x_slope"] + algn["tx_0"]) - np.sin(txy) * (tracks_in[mask]["y_slope"] + algn["ty_0"])

        ty = np.cos(txy) * (tracks_in[mask]["y_slope"] + algn["ty_0"]) + np.sin(txy) * (-tracks_in[mask]["x_slope"] + algn["tx_0"])

        x = algn["x_0"] + np.cos(txy) * (-tracks_in[mask]["x"] - algn["z_0"] * tx) - np.sin(txy) * (tracks_in[mask]["y"] - algn["z_0"] * ty) + algn["v_x"] * (tracks_in[mask]["timestamp"] * 25/1e9)

        y = algn["y_0"] + tracks_in[mask]["spill"] * 2. + np.cos(txy) * (tracks_in[mask]["y"] - algn["z_0"] * ty) + np.sin(txy) * (-tracks_in[mask]["x"] - algn["z_0"] * tx) + algn["v_y"] * (tracks_in[mask]["timestamp"] * 25/1e9)
        
        out_array["match_pix_x"][mask]= x
        out_array["match_pix_y"][mask] = y
        out_array["match_pix_tx"][mask] = tx
        out_array["match_pix_ty"][mask] = ty
        out_array["x_0"][mask] = algn["x_0"]
        out_array["y_0"][mask] = algn["y_0"]
        out_array["z_0"][mask] = algn["z_0"]
        out_array["tx_0"][mask] = algn["tx_0"]
        out_array["ty_0"][mask] = algn["ty_0"]
        out_array["txy_0"][mask] = algn["txy_0"]
        out_array["v_x"][mask] = algn["v_x"]
        out_array["v_y"][mask] = algn["v_y"]
        out_array["match_time"][mask] = tracks_in[mask]["timestamp"]
        out_array["spill"][mask] = tracks_in[mask]["spill"]
        out_array["pix_x_error"] = tracks_in[mask]["x_error"]
        out_array["pix_y_error"] = tracks_in[mask]["y_error"]
        out_array["pix_tx_error"] = tracks_in[mask]["x_slope_error"]
        out_array["pix_ty_error"] = tracks_in[mask]["y_slope_error"]
        out_array["pix_x_y_error"] = tracks_in[mask]["x_y_error"]
        out_array["pix_x_tx_error"] = tracks_in[mask]["x_tx_error"]
        out_array["pix_x_ty_error"] = tracks_in[mask]["x_ty_error"]
        out_array["pix_y_tx_error"] = tracks_in[mask]["y_tx_error"]
        out_array["pix_y_ty_error"] = tracks_in[mask]["y_ty_error"]
        out_array["pix_tx_ty_error"] = tracks_in[mask]["tx_ty_error"]
        out_array["pix_track_chi2"] = tracks_in[mask]["red_chi2"]
        try:
            out_array["global_trackID"] = tracks_in[mask]["global_trackID"]
        except ValueError:
            print("no column global_trackID")
            continue
        # out_array["matched"] = 0

    return out_array, out_array["spill"]!=0


def transform_matched_to_pixel_frame(matched_tracks_in, lefthanded=True):
    '''
    Back transformation for pixel tracks from emulsion rest frame (moving with vx and vy !) to pixel rest frame. 
    Use alignment values from Chris. Use input array with alignment info per row. Replace match_pix_x etc. by new values.
    The transformation to the emulsion frame was done in the following order :
        1. add offsets to angles tx, ty
        2. rotate angles tx, ty with angle txy -> tx' , ty'

        3. invert x
        4. substract offset of z0*tx' , z0*ty' to x,y
        5. rotate x,y
        6. add offsets x0 and vx*ts, y0 and vy*ts -> result is x',y'

    So the backtransformation has to be done in reverse oder:
        1. substract offsets x0 vx*ts, y0 vy*ts
        2. rotate x', y' with negative t_xy
        3. add offset z0*tx', z0*ty' -> x,y
        4. invert x
        
        5. rotate tx' and ty' with negative t_xy
        6. substract offset of angles tx' and ty' -> tx, ty

    ----------------------
    INPUT:
        matched_tracks_in : numpy structured array0
            emulsion and pixel trackdata. Must contain columns  "match_pix_x" for , tx, ty and the alignment values
            x_0, y_0, z_0, tx_0, ty_0, txy_0, v_x, v_y, as well as "match_time" and "spill"
        lefthanded : bool
            wether or not to invert x coordinate. Emulsion data is in right handed coordinate system, pixel data in lefthanded
    OUTPUT:
        out_array : numpy structured array
            copy of input array with new x,y,tx,ty 
    '''
    x = matched_tracks_in["match_pix_x"]
    y = matched_tracks_in["match_pix_y"]
    tx = matched_tracks_in["match_pix_tx"]
    ty = matched_tracks_in["match_pix_ty"]

    if lefthanded:
        x_mirror = -1
    else:
        x_mirror = 1

    tx_rest = (tx )*np.cos(-1*matched_tracks_in["txy_0"]) - (ty)*np.sin(-1*matched_tracks_in["txy_0"])
    tx_rest -= matched_tracks_in["tx_0"]
    tx_rest *= x_mirror
    ty_rest = (tx)*np.sin(-1*matched_tracks_in["txy_0"]) + (ty )*np.cos(-1*matched_tracks_in["txy_0"])
    ty_rest -= matched_tracks_in["ty_0"]
   
    x_prime = (x - (matched_tracks_in["match_time"]*1e-9*25)*matched_tracks_in["v_x"] - matched_tracks_in["x_0"])
    y_prime = y - (matched_tracks_in["match_time"]*1e-9*25)*matched_tracks_in["v_y"] - matched_tracks_in["y_0"] - matched_tracks_in["spill"]*2.

    x_rest = (x_prime)*np.cos(-1*matched_tracks_in["txy_0"]) - (y_prime)*np.sin(-1*matched_tracks_in["txy_0"])
    y_rest = (x_prime)*np.sin(-1*matched_tracks_in["txy_0"]) + (y_prime)*np.cos(-1*matched_tracks_in["txy_0"])

    x_rest += tx*matched_tracks_in["z_0"]
    y_rest += ty * matched_tracks_in["z_0"]

    out_array = np.empty_like(matched_tracks_in)

    for dtype in matched_tracks_in.dtype.names:
        out_array[:][dtype] = matched_tracks_in[:][dtype]

    out_array["match_pix_x"] = x_rest*x_mirror
    out_array["match_pix_y"] = y_rest
    out_array["match_pix_tx"] = tx_rest
    out_array["match_pix_ty"] = ty_rest

    return out_array


def select_single_track_events(merged_pixel_cluster_path):

    merged_pixel_clusters = load_table(in_file=merged_pixel_cluster_path, table_name="MergedClusters")

    for dut in range(12):
        merged_pixel_clusters = merged_pixel_clusters[np.logical_or(
            merged_pixel_clusters["n_cluster_dut_%i" % dut] <= 1, np.isnan(merged_pixel_clusters["n_cluster_dut_%i" % dut]))]

    save_table(array_in=merged_pixel_clusters, path=merged_pixel_cluster_path[:-3] + "_single_clusters.h5", node_name="MergedClusters")


def select_single_cluster(dut_cluster_path):

    pixel_clusters = load_table(in_file=dut_cluster_path, table_name="Clusters")
    pixel_clusters = pixel_clusters[pixel_clusters["n_cluster"] <= 1]
    save_table(array_in=pixel_clusters, path=dut_cluster_path[:-3] + "_single.h5", node_name="Clusters")

def _correct_emu_alignment(in_file=None, in_array=None, spill=None):
    if isinstance(in_file,str):
        matched_tracks = load_table(in_file, table_name = "MatchedTracks")
        if isinstance(spill, int):
            matched_tracks["v_x"] *= (-1)**(spill%2)
            if spill%2!=0:
                matched_tracks["x_0"] += 13.
        else:
            matched_tracks["v_x"] *= (-1)**(matched_tracks["spill"]%2)
            matched_tracks["x_0"][matched_tracks["spill"]%2 != 0] += 13.
        save_table(matched_tracks, in_file, append=False, node_name="MatchedTracks")
        return
    elif isinstance(in_array, (np.ndarray, np.generic)):
        if isinstance(spill, int):
            in_array["v_x"] *= (-1)**(spill%2)
            if spill%2 != 0:
                in_array["x_0"]+= 13.
        else:
            in_array["v_x"] *= (-1)**(in_array["spill"]%2)
            in_array["x_0"][in_array["spill"]%2 != 0] += 13.
        return


def load_table(in_file, table_name=None, spill=None, unique=False, ):
    '''
    open pytables file (.h5) and return numpy (structured) array
    ---------------
    INPUT:
        in_file: string
            path to .h5 file to open. File must contain a pytables Table
        spill: int
            spill identifier. If given table needs to have column "spill"
            only rows with spill matching given spill identifier are returned.
        unique: Bool
            if given only rows with first occurence of new event_number are returned. column "event_number" necessary
        table_name: string
            Name of the table in the file to load.
    OUTPUT:
        table_out: numpy (structured) array. If unique is True only with unique event_numbers
    '''
    #TODO: chunking
    table_out = None
    logging.info("loading %s" % in_file)
    with tb.open_file(in_file, "r") as in_file_h5:
        if table_name:
            for table in in_file_h5.walk_nodes('/', classname='Table'):
                if table.name == table_name:
                    table_out = table[:]
            if not isinstance(table_out, (np.ndarray, np.generic)):
                return
        else:
            for table in in_file_h5.walk_nodes('/', classname='Table'):
                table_out = table[:]
                break
    if spill:
        table_out = table_out[table_out["spill"] == spill]
    if unique:
        _, unique_indices = np.unique(table_out["event_number"], return_index=True)
        return table_out[unique_indices]
    else:
        return table_out


def load_array(in_file_path, table_name=None):
    '''
    open pytables file (.h5) and return numpy array
    ---------------
    INPUT:
        in_file: string
            path to .h5 file to open. File must contain a pytables Table
        spill: int
            spill identifier. If given table needs to have column "spill"
            only rows with spill matching given spill identifier are returned.
        unique: Bool
            if given only rows with first occurence of new event_number are returned. column "event_number" necessary
        table_name: string
            Name of the table in the file to load.
    OUTPUT:
        table_out: numpy (structured) array. If unique is True only with unique event_numbers
    '''
    #TODO: chunking
    table_out = None
    logging.info("loading %s" % in_file_path)
    with tb.open_file(in_file_path, "r") as in_file_h5:
        if table_name:
            for table in in_file_h5.walk_nodes('/', classname='CArray'):
                if table.name == table_name:
                    table_out = table[:]
            if not isinstance(table_out, (np.ndarray, np.generic)):
                return
        else:
            for table in in_file_h5.walk_nodes('/', classname='CArray'):
                table_out = table[:]
                break

    return table_out


def sort_table(array_in, columns):
    return array_in.sort(order=columns)


def count_matches(matched_tracks):
    '''
    count number of emulsion tracks matched to pixel tracks in event. Expect 1:1 correlation
    Data shows that tracks from the same pixel event can be matched to different vertices but NOT vice versa.
    The algorithm needs the array to be sorted by timestamp and emu_vid.
    ---------------
    INPUT:
        matched_tracks : np.array
            structured array with matched tracks, must contain vertex id and timestamp 
    OUTPUT:
        count_matches : np.array
            contains 4 columns, timestamp, vertex_id, number of pixel tracks per timestamp, number of emulsion tracks per timestamp
    '''

    ts = np.unique(matched_tracks["match_time"])
    vtx = np.unique(matched_tracks["emu_vid"])
    n_ts = ts.shape[0]
    n_vtx = vtx.shape[0]
    matched_tracks.sort(order=["match_time", "emu_vid"])
    count_matches = np.full(fill_value=np.nan, shape=(n_ts,), dtype=[("timestamp", np.int64),
                                                                     ("vertex_ID", np.int32), ("pix_counts", np.int32), ("emu_counts", np.int32)])

    vid = matched_tracks[0]["emu_vid"]
    match_time = matched_tracks[0]["match_time"]

    n_pix_tracks = 0
    n_pix_tracks2 = 0
    n_emu_tracks = 0
    j = 0

    # look at every track pair
    for i in range(0, matched_tracks.shape[0]):
        # count all tracks in event
        if matched_tracks[i]["match_time"] == match_time:
            if matched_tracks[i]["emu_vid"] == vid:
                n_pix_tracks += 1
                n_emu_tracks += 1
            else:
                # same timestamp associated to different vertex. Now count number of pixel tracks with same timestamp associated to that vertex.
                # vertex with the most associated tracks "wins"
                # TODO: consider more than 2 vertices
                n_pix_tracks2 += 1
        else:
            # new event, store data
            count_matches[j]["timestamp"] = match_time
            count_matches[j]["pix_counts"] = max(n_pix_tracks, n_pix_tracks2)
            count_matches[j]["emu_counts"] = n_emu_tracks
            j += 1
            # in case of new event reset variables. Since we are already in new event, n_pix_tracks and n_emu_tracks are set to 1.
            n_pix_tracks, n_pix_tracks2, n_emu_tracks = 1, 0, 1
            match_time = matched_tracks[i]["match_time"]
            vid = matched_tracks[i]["emu_vid"]

    return count_matches


def count_matches_emulsion(matched_tracks):
    '''
    count number of pixel tracks matched to emulsion vertex Expect 1:1 correlation
    Data shows that tracks from the same pixel event can be matched to different vertices but NOT vice versa.
    The algorithm needs the array to be sorted by timestamp and emu_vid.
    ---------------
    INPUT:
        matched_tracks : np.array
            structured array with matched tracks, must contain vertex id and timestamp 
    OUTPUT:
        count_matches : np.array
            contains 4 columns, timestamp, vertex_id, number of pixel tracks per timestamp, number of emulsion tracks per timestamp
    '''

    ts = np.unique(matched_tracks["match_time"])
    vtx = np.unique(matched_tracks["emu_vid"])
    n_ts = ts.shape[0]
    n_vtx = vtx.shape[0]
    matched_tracks.sort(order=["emu_vid","match_time"])
    count_matches = np.full(fill_value=np.nan, shape=(n_vtx,), dtype=[("timestamp", np.int64),
                                                                     ("vertex_ID", np.int32), ("pix_counts", np.int32), 
                                                                     ("emu_counts", np.int32), ("pix_off", np.int32),
                                                                     ("pix_rate_correct", np.float64)])

    vid = matched_tracks[0]["emu_vid"]
    match_time = matched_tracks[0]["match_time"]

    n_pix_tracks = 0
    n_pix_tracks2 = 0
    n_emu_tracks2 = 0
    n_emu_tracks = 0
    j = 0

    # look at every track pair
    for i in range(0, matched_tracks.shape[0]):
        
        # count all tracks in event
        if matched_tracks[i]["emu_vid"] == vid:
            n_emu_tracks += 1
            if matched_tracks[i]["match_time"] == match_time:
                n_pix_tracks += 1
                # n_emu_tracks += 1
            else:
                # same vertex associated to different timestamp. Now count number of emulsion tracks with same vertex associated to that timestamp.
                # vertex with the most associated tracks "wins"
                # TODO: consider more than 2 vertices
                n_pix_tracks2 += 1
                # n_emu_tracks2 += 1
        else:
            # new event, store data
            count_matches[j]["vertex_ID"] = vid
            count_matches[j]["pix_counts"] = max(n_pix_tracks, n_pix_tracks2)
            count_matches[j]["emu_counts"] = max(n_emu_tracks, n_emu_tracks2)
            count_matches[j]["pix_off"] =  min(n_pix_tracks, n_pix_tracks2)
            j += 1
            # in case of new event reset variables. Since we are already in new event, n_pix_tracks and n_emu_tracks are set to 1.
            n_pix_tracks, n_pix_tracks2, n_emu_tracks, n_emu_tracks2 = 1, 0, 1, 0
            match_time = matched_tracks[i]["match_time"]
            vid = matched_tracks[i]["emu_vid"]
        # end of array
        if i == matched_tracks.shape[0]-1:
            count_matches[j]["vertex_ID"] = vid
            count_matches[j]["pix_counts"] = max(n_pix_tracks, n_pix_tracks2)
            count_matches[j]["emu_counts"] = max(n_emu_tracks, n_emu_tracks2)
            count_matches[j]["pix_off"] =  min(n_pix_tracks,n_pix_tracks2)

    # count_matches["pix_rate_correct"] = count_matches["pix_off"]/ count_matches["emu_counts"]
    # count_matches["pix_rate_correct"][count_matches["pix_off"]==0] = 1.

    count_matches["pix_rate_correct"] = count_matches["pix_counts"] / count_matches["emu_counts"]
    return count_matches


def calculate_matching_eff(matched_tracks, all_tracks, x_dim=13., y_dim=10., n_bins=200):
    '''
    calculates matching efficiency map in emulsion rest frame. Map starts at x,y = 0,0
    Here the efficiency is defined as the number of successfully matched tracks from one detector divided by the number of all tracks reconstructed in this detector
    ---------------
    INPUT:
        matched_tracks : np.array with 2 columns an N rows
            columns contain x and y position of matched tracks
        all_tracks : np.array of same structure as matched_tracks
            array contains x and y position of all tracks
        x_dim : float
            upper limit of x values to consider in cm
        y_dim : float
            see x_dim
        n_bins : int
            number of bins in y dimension, x bins are calculated according to the ratio x_dim / y_dim
    OUTPUT:
        machting_efficiency : np.ma.array
            masked numpy array of size (x_dim / y_dim * n_bins, n_bins) contains efficiency histogram
        x_bins : np.array
            bin_edges for efficiency histogram
        y_bins : np.array
            see x_bins
    '''
    y_bins = np.linspace(0, y_dim, n_bins)
    x_bins = np.linspace(0, x_dim, int(n_bins * x_dim/y_dim))

    total_track_density = np.zeros(shape=(len(x_bins)-1, len(y_bins)-1))
    total_matched_density = np.zeros(shape=(len(x_bins)-1, len(y_bins)-1))

    total_track_density += np.histogram2d(all_tracks[0, :], all_tracks[1, :], bins=[x_bins, y_bins])[0]
    total_matched_density += np.histogram2d(matched_tracks[0, :], matched_tracks[1, :], bins=[x_bins, y_bins])[0]

    matching_efficiency = np.zeros(shape=total_track_density.shape, dtype=np.float32)
    matching_efficiency[total_track_density != 0] += total_matched_density[total_track_density !=
                                                                           0].astype(np.float32) / total_track_density[total_track_density != 0].astype(np.float32)
    
    # mask eff histogram where there are no entries for better plotting
    matching_efficiency = np.ma.array(matching_efficiency, mask=total_track_density == 0)
    total_track_density = np.ma.array(total_track_density, mask = total_track_density == 0)
    total_matched_density = np.ma.array(total_matched_density, mask = total_track_density == 0)

    return matching_efficiency, total_track_density, total_matched_density, x_bins, y_bins


def save_table(array_in, path, append=False, node_name="Hits"):

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    if append:
        mode = "a"
    else:
        mode = "w"
    if not isinstance(array_in, list):
        array_in = [array_in]
        node_name = [node_name]
    elif not isinstance(node_name, list):
        raise TypeError("No node names provided for array list")

    with tb.open_file(path, mode) as out_file_h5:
        for i, array in enumerate(array_in):
            if not append:
                table_out = out_file_h5.create_table(where=out_file_h5.root, name=node_name[i], 
                            description=array.dtype, filters=tb.Filters(complib='blosc',complevel=5, fletcher32=False))
            else:
                for table in out_file_h5.walk_nodes('/', classname='Table'):
                    if table.name == node_name[i]:
                        table_out = table
                # table_out = out_file_h5.get_node(where = "/", name = node_name[i])
                
            table_out.append(array)
            table_out.flush()
            logging.info("saved %s at %s" % (node_name[i], path))


def save_array(array_in, path, append=False, node_name="Hits"):

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    if append:
        mode = "a"
    else:
        mode = "w"
    if not isinstance(array_in, list):
        array_in = [array_in]
        node_name = [node_name]
    elif not isinstance(node_name, list):
        raise TypeError("No node names provided for array list")

    with tb.open_file(path, mode) as out_file_h5:
        for i, array in enumerate(array_in):
            table_out = out_file_h5.create_carray(where=out_file_h5.root, name=node_name[i],
                                                filters=tb.Filters(complib='blosc',
                                                                    complevel=5, fletcher32=False),
                                                atom=tb.Atom.from_dtype(array.dtype), shape=array.shape)
            table_out[:] = array
            logging.info("saved %s at %s" % (node_name[i], path))


def write_csv_track_files(array_in, path, nevents = -1, start_event = -1, new_event_numbers = False, max_tracks = -1, min_tracks = -1):
    
    '''
    Function to write tracking data in TrackML csv format for acts framework. One csv per event is written to the specified output folder.
    The naming scheme is as follows: "event000000001-tracks.csv"
    First column of the input array has to be the event_number. All columns but the first one are written to the csv file.
    ------------------------
    INPUT:
        array_in : nummpy structured array
            must contain event number. For usage with acts must contain x,y,theta, phi, q/p, t, and the corresponding errors.
        path : string
            Specifies where to create the folder with csv files.
        nevents : int
            number of events to be written. If given positive a random sample of size nevents is drawn from input array
        start_event : int
            skip event numbers smaller than start_event
        new_event_numbers : bool
            If given assign monotonically increasing event numbers to output file. It seems that the acts example does not tolerate missing event numbers
        max_tracks : int
            if given positive only consider events with a number of tracks less or equal to max_tracks
        min_tracks : int
            if given positve only consider events with a number of tracks larger or equal to min_tracks
    OUTPUT:
        none, files written to disk
    '''

    if not os.path.exists(path):
        os.makedirs(path)

    dtype_names = array_in.dtype.names
    events, event_indices, counts = np.unique(array_in[array_in[dtype_names[0]]>start_event][dtype_names[0]], return_index=True, return_counts = True)
    
    if max_tracks > 0:
        events = events[counts<=max_tracks]
        event_indices = event_indices[counts<=max_tracks]
        counts = counts[counts<=max_tracks]
    if min_tracks >0:
        events = events[counts>=min_tracks]
        event_indices = event_indices[counts>=min_tracks]
        counts = counts[counts>=min_tracks]
    if nevents>0:
        choose_indices = np.random.choice(events.size, nevents)
        events = events[choose_indices]
        event_indices = event_indices[choose_indices]
        counts = counts[choose_indices]

    # need list of column name strings for header, no tuple
    write_columns = [name for name in dtype_names[1:]]
    array_write = array_in[write_columns]
    
    header = ','.join(map(str, dtype_names[1:]))
    # acts does not tolerate missing event numbers
    if new_event_numbers:
        events = np.arange(0,events.size)

    for i, index in enumerate(tqdm(event_indices)):
        filename = path + "event" + f'{events[i]:09}' + "-tracks.csv"
        np.savetxt(filename, array_write[index:index+counts[i]], delimiter = ",", header = header, comments='')

    logging.info("wrote one csv file per event to %s" % path)


def merge_spills(in_files, out_path, out_node_name, merge_runs = False):
    '''
    For higher statistics merge matched events of several spills.
    --------------------
    INPUT:
        in_files : list of strings
            path to input files of single spills which will be merged
        out_path : string
            location where merged table will be saved
        out_node_name : string
            for easier use name of the table to read in and name of the created merged table
    '''
    if not os.path.exists(os.path.dirname(out_path)):
        os.makedirs(os.path.dirname(out_path))

    init = True
    last_evt_nr = 0
    last_spill_nr = 0

    with tb.open_file(out_path, "w") as out_file_h5:
        for input_file in in_files:
            logging.info("load input file %s" % input_file)
            
            array_out = load_table(input_file, table_name=out_node_name)
            if isinstance(array_out, (np.ndarray, np.generic)):
                if merge_runs :
                        array_out["event_number"] += last_evt_nr
                        # array_out["spill"] += last_spill_nr
                        last_evt_nr = array_out[-1]["event_number"]
                        # last_spill_nr = array_out[-1]["spill"]
                if init:
                    table_out = out_file_h5.create_table(where=out_file_h5.root, name=out_node_name, description=array_out.dtype,
                                                    filters=tb.Filters(complib='blosc',
                                                                        complevel=5, fletcher32=False)
                    )
                    init = False
                table_out.append(array_out)
                table_out.flush()

# @njit
def get_matched_tracks_pixel(tracks_in, matched_events):
    '''
    get pixel Tracks with event numbers which were matched by timstamp with another detector.
    ---------------
    INPUT:
        tracks_in: structured numpy.array
            array with tracks from TestBeamFullPix.c must contain "event_number"
        matched_events: structured numpy.array
            array with matched events, must contain "matched_event_number"
    OUTPUT:
        tracks_out: structured numpy.array
            same as tracks in, but only contains rows with matching "event_number". 
            Also updates the event number to the common "matched_event_number"
    '''

    tracks_out = []  # List()
    i = 0
    for row in tracks_in:
        while (row["event_number"] > matched_events[i]["pixel_event"]) and (i < matched_events.size - 1):
            i += 1
        if (row["event_number"] == matched_events[i]["pixel_event"]):
            tracks_out.append(row)
            tracks_out[-1]["event_number"] = matched_events[i]["matched_event_number"]
        elif (row["event_number"] < matched_events[i]["pixel_event"]):
            continue
    return np.array(tracks_out)


def circle(x, x_0, y_0, r):
    '''
    Circle equation with (x_0, y_0) as center point with radius r.
    Due to ambiguity of r^2 one has to check for the case at hand which sign to use for the square root.
    '''
    return y_0 + np.sign(r) * np.sqrt(r**2 - (x - x_0)**2)
    

def line(x, m, b):
    return m*x + b

def radius(x1, x2, y1, y2, y_slope):
    '''
    Return radius of circle crossing two points x1/y1 and x2/y2. The two points are connected by a line of normal slope y_slope
    '''
    ty = np.arctan(y_slope) + np.pi/2
    return -1 * np.square((x2 - x1))/2/((x2 + x1) *np.cos(ty) + (y2 - y1)*np.sin(ty))


def radius2(x_in, x_out, L):
    '''
    compute circle radius from knwon entry and exit point and Magnetic field length.
    Does not compute circle center.
    ---------------------
    INPUT:
        x_in : float or numpy array of floats
            entry point of track into field
        x_out: float or numpy array of floats
            exit point of track from field
        L : float
            field length
    '''

    d = x_out - x_in
    return (np.square(L) + np.square(d))/(2 * d)


def parabola(x,a,b,c):
    return a*(x**2) + (b*x) + c


def gauss(x, amp, cen, sigma):
    """Gaussian lineshape."""
    return amp * np.exp(-(x-cen)**2 / (2.*sigma**2))

'''
Boxfunction and background fit provided by Ivan Caicedo.
'''
def boxfc(x_data, *parameters):
    """Box function"""
    box_Amp, box_mean, box_width, box_sigma = parameters
    return (box_Amp/2)*( erf(((x_data-box_mean)+(box_width/2))/(np.sqrt(2)*box_sigma)) + erf(((box_width/2)-(x_data-box_mean))/(np.sqrt(2)*box_sigma)) )


def gaussbck(x_data, *parameters):
    """Gaussian background"""
    Amp_gauss, mu_gauss, sigma_gauss, constant = parameters
    return Amp_gauss*np.exp(-(x_data-mu_gauss)**2/(2.*sigma_gauss**2)) + constant


def boxfc_withgaussbck(x_data, bck_params, *parameters):
    """Box + Gaussian background"""
    box_Amp, box_mean, box_width, box_sigma = parameters
    bck_Amp_gauss, bck_mu_gauss, bck_sigma_gauss, constant = bck_params
    return (box_Amp/2)*( erf(((x_data-box_mean)+(box_width/2))/(np.sqrt(2)*box_sigma)) + erf(((box_width/2)-(x_data-box_mean))/(np.sqrt(2)*box_sigma)) )+ bck_Amp_gauss*np.exp(-(x_data-bck_mu_gauss)**2/(2.*bck_sigma_gauss**2)) + constant


def fit_boxfc(x_data, y_data, params_guess = None, empty_bins_in_chi2=False):
    """Fit Box"""
    x_data = np.array(x_data)
    y_data = np.array(y_data)

    mean = bta_ana_utils.get_mean_from_histogram(y_data, x_data)
    rms = bta_ana_utils.get_rms_from_histogram(y_data, x_data)

    # y_maxima=x_data[np.where(y_data[:]==np.max(y_data))[0]]
    if not params_guess:
        print("guessing parameters")
        params_guess = np.array([np.max(y_data),mean, 50, rms])
    
    try:
        params_from_fit, pcov = curve_fit(boxfc, x_data, y_data, p0=params_guess)
        chi2 = red_chisquare(y_data, boxfc(x_data, *params_from_fit), np.sqrt(y_data), params_from_fit, use_empty_bins = empty_bins_in_chi2)
    except RuntimeError:
        logging.error('Fit did not work boxfc: %s %s %s %s', str(params_guess[0]), str(params_guess[1]), str(params_guess[2]), str(params_guess[3]))
        return params_guess, np.full(fill_value = float("inf"), shape = (4,4)), float("inf")
    return params_from_fit, pcov, chi2


def fit_gaussbck(x_data, y_data, initial_values=[None, None, None, None]):
    """Fit Gaussian background"""
    x_data = np.array(x_data)
    y_data = np.array(y_data)

    # mean = bta_ana_utils.get_mean_from_histogram(y_data, x_data)
    # rms = bta_ana_utils.get_rms_from_histogram(y_data, x_data)

    y_maxima=x_data[np.where(y_data[:]==np.max(y_data))[0]]
    y_bck=x_data[y_data[-1]]
    
    if initial_values[0]==None:
        params_guess = np.array([np.max(y_data), y_maxima[0], 50 ,y_bck])
    else:
        # params_guess = np.array([np.max(y_data), initial_values[1], initial_values[2],y_bck])
        params_guess = np.array(initial_values)
    # print(params_guess)
    try:
        params_from_fit = curve_fit(gaussbck, x_data, y_data, p0=params_guess)
    except RuntimeError:
        print ('Fit did not work gaussian background: %s ', str(params_guess))
        return params_guess
    return params_from_fit[0] 


def fit_boxfc_withgaussbck(x_data, y_data, params):
    """Fit Box + Gaussian background"""
    x_data = np.array(x_data)
    y_data = np.array(y_data)
    y_maxima=x_data[np.where(y_data[:]==np.max(y_data))[0]]
    
    params_guess = np.array([params[0], params[1], params[2], params[3], params[4],params[5],params[6],params[7]])
    try:
        params_from_fit=curve_fit(lambda x_data, *parameters_boxfc : boxfc_withgaussbck(x_data, params_guess[4:8], *parameters_boxfc), x_data, y_data, p0=params_guess[0:4])
    except RuntimeError:
        print('Fit did not work boxfc+background: %s ', str(params_guess))
        return params_guess
    return params_from_fit[0] 


def fit_gauss(hist_in, bin_center, empty_bins_in_chi2 = False):
    '''
    simple gauss fit to 1d histogram.
    ------------------
    INPUT:
        hist_in : np.array
            1D histogram
        bin_center : np.array
            former bin_edges. Now centered for fitting
        empty_bins_in_chi2 : bool
            if False empty bins will be masked for chi2/ndf calculation
    OUTPUT:
        popt : np.array of of shape (3,1)
            fitted parameters for gauss function
        pcov : np.array of shape (3,3) 
            covariance matrix
        reduced_chi2 : float
            chi2/ndf
    '''

    try:
        mean = bta_ana_utils.get_mean_from_histogram(hist_in, bin_center)
        rms = bta_ana_utils.get_rms_from_histogram(hist_in, bin_center)
        popt, pcov = curve_fit(bta_ana_utils.gauss, bin_center, hist_in, p0=[np.amax(hist_in), mean, rms] )
        # TODO: correct sigmas for histogram ?
        reduced_chi2 = red_chisquare(hist_in, bta_ana_utils.gauss(bin_center, *popt), np.sqrt(hist_in), popt, use_empty_bins = empty_bins_in_chi2)
    except RuntimeError:
        popt = [float("inf"), float("inf"), float("inf")]
        pcov = np.full(fill_value = float("inf"), shape = (3,3))
        reduced_chi2 = float("inf")
        logging.warning("could not fit histogram")

    return popt, pcov, reduced_chi2


def fit_residuals(tracks_in_file_path, scifi_planes=[1, 2, 3, 4, 5, 7, 8], n_residual_bins=1000, residual_range=[-5000, 5000]):
    '''
    histogram residuals and do gauss fit, no plotting
    ------------
    INPUT:
        tracks_in_file_path : str
            path to merged_tracks_file with column delta_x
        scifi_planes: list of int
            number of planes for which fit is done
        n_residual_bins : int
            binning of histogram for fit
        residual_range: list of 2 entries
            minimum and maximum value to be considered in histogram
    OUTPUT:
        fit_res : list of [array, 3x3 array, float]
            contains fit results, errors and chi2/ndf
    '''

    merged_tracks = load_table(tracks_in_file_path, spill=None, table_name="MergedTracks")

    dimension = {1: "x", 2: "x", 3: "y", 4: "y", 5: "y", 7: "x", 8: "x"}
    fit_res = []

    for plane in scifi_planes:
        logging.info("fitting residuals for plane %i" % plane)
        selected_tracks = merged_tracks[np.logical_and(merged_tracks["plane"] == plane, merged_tracks["best_match"] == 1)]

        delta_x_hist, x_bins = np.histogram(selected_tracks["delta_%s" % dimension[plane]]*1e4, bins=n_residual_bins, range=residual_range,)
        
        hist_in = delta_x_hist[1:-1]
        bin_center = (x_bins[1:] + x_bins[:-1]) / 2.0
        bin_center = bin_center[1:-1]

        fit, error, reduced_chi2 = fit_gauss(hist_in, bin_center, )
        fit_res.append([fit, error, reduced_chi2])
    return fit_res


def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    From https://stackoverflow.com/questions/5967500/how-to-correctly-sort-a-string-with-a-number-inside
    """

    def atoi(text):
        return int(text) if text.isdigit() else text

    def natural_keys(text):
        '''
        alist.sort(key=natural_keys) sorts in human order
        http://nedbatchelder.com/blog/200712/human_sorting.html
        (See Toothy's implementation in the comments)
        '''
        return [atoi(c) for c in re.split(r'(\d+)', text)]
    l.sort(key=natural_keys)


def cartesian_to_spherical(x, y, z):
    ''' Does a transformation from cartesian to spherical coordinates. For transforming track slopes provide x-slope and y-slope as x,y coordinates and 1 as z coordinate. The result will be theta and phi from the track of given slopes on a sphere of individual radius being slightly larger 1.

    Convention: r = 0 --> phi = theta = 0

    Parameters
    ----------
    x, y, z : float
        Position in cartesian space.

    Returns
    -------
    Spherical coordinates phi, theta and r.
    '''
    r = np.sqrt(np.square(x) + np.square(y) + np.square(z))
    #r = np.ones_like(x, dtype=np.float64) #np.linalg.norm(np.array([x,y,z]))
    phi = np.zeros_like(r, dtype=np.float64)  # define phi = 0 for x = 0
    theta = np.zeros_like(r, dtype=np.float64)  # theta = 0 for r = 0
    # Avoid division by zero
    # https://en.wikipedia.org/wiki/Atan2
    phi[x != 0] = np.arctan2(y[x != 0], x[x != 0])
    phi[phi < 0] += 2. * np.pi  # map to phi = [0 .. 2 pi[
    theta[r != 0] = np.arccos(z[r != 0] / r[r != 0])
    return phi, theta, r


def red_chisquare(observed, expected, observed_error, popt, use_empty_bins = True):
    '''
    Calculate chi2/ndf for fitted model.
    -------------
    INPUT:
        observed : np.array
            measured values
        expected : np.array
            expected values based on fitted model
        obseved_error : np.array
            measurement error, one for every observed value
        popt : array
            fitted paramters
        use_empty_bins : bool
            if false mask empty bins for calculation. If not done, empty bins will result in chi2/ndf = inf
    OUTPUT : 
        float : calculated chi2/ndf
    '''
    mask = np.ones(len(observed), np.bool)
    if not use_empty_bins:
        mask[observed==0] = 0
    return np.sum(((observed[mask] - expected[mask]) / observed_error[mask])**2 / (len(observed_error[mask]) - len(popt) - 1))

def _mask_empty_bins(hist_in, bin_center):
    '''
    remove empty bins on outer edges for fitting histogram.
    Only for 1D arrays at the moment, does not remove empty bins in between!
    -----------------
    INPUT:
        hist_in : np.array
            1D histogram
        bin_center : np.array
            former bin_edges but centered for easier fit and calculation
    OUTPUT:
        hist_mask : np.array
            masked input histogram
        bin_center_mask : np.array
            masked input bin_centers
    '''

    mask_indices = []
    # start at left (lower) edge
    for i in range(hist_in.shape[0]):
        if hist_in[i] == 0:
            mask_indices.append(i)
        else:
            break
    # look from right (upper) edge
    for i in range(1,hist_in.shape[0]+1):
        if hist_in[-i] == 0:
            mask_indices.append(hist_in.shape[0] - i)
        else:
            break
    
    mask = np.ones(len(hist_in), np.bool)
    mask[mask_indices] = 0
    hist_mask = hist_in[mask]
    bin_center_mask = bin_center[mask]
    return hist_mask, bin_center_mask



def load_config(config_file_path):
    configuration_dict = {}
    if not config_file_path:
        pass
    if os.path.isfile(os.path.abspath(config_file_path)):
        logging.info('Loading configuration from file %s', os.path.abspath(config_file_path))
        with open(os.path.abspath(config_file_path), mode='r') as f:
            configuration_dict.update(safe_load(f))
    else:  # YAML string
        configuration_dict.update(safe_load(config_file_path))
    return configuration_dict


def save_config(config_file_path, config_dict):
    with open(config_file_path, mode='w') as f:
        safe_dump(config_dict, f, default_flow_style=False)


def get_files(folder, string):
    '''
    crawl folder for files with name matching given string recursively, use human sorting for output list.
    -----------
    INPUT:
        folder, str: partent directory where to start searching
        string, str: phrase which is looked for in the file name
    OUTPUT:
        raw_data_file_list, list: list with absolute paths of files which match given string
    '''

    raw_data_file_list = []
    for dirpath, _, filenames in os.walk(folder):
        for f in filenames:
            if string in f:
                raw_data_file_list.append(os.path.abspath(os.path.join(dirpath, f)))
    # raw_data_file_list.sort()
    sort_nicely(raw_data_file_list)
    return raw_data_file_list


def rotate_around_z(x, y, angle):
    '''
    Rotation around z axis (upstream direction)
    -----------------
    INPUT:
        angle: float
            angle which to rotate in mRad
    '''
    x_out = x*np.cos(angle) - y*np.sin(angle)
    y_out = x*np.sin(angle) + y*np.cos(angle)
    return x_out, y_out


def abcformel(a, b, c):

    radikand = np.square(b) - 4 * a *c
    if (radikand < 0 ):
        x1 = 0 
        x2 = 0
        print("Negative radicand")
        return
    x1 = ( -b + np.sqrt(radikand)) / (2 * a)
    x2 = ( -b - np.sqrt(radikand)) / (2 * a)
    return x1, x2


def getradius(xi, ti, xo, L = 2.0667):

    a = 1 / np.sqrt(1 + np.square(ti))
    d = xo-xi

    # p2 = np.square(a) - 1 + np.square(ti * a)
    # p1 = L * ti * a - 2 * d * a
    # p0 = np.square(d) + np.square(L)

    p2 = np.square(a) - 1 + np.square(ti * a)
    p1 = (-2) * a * (L * ti + d) 
    p0 = np.square(L) + np.square(d)

    r1, r2, = abcformel(p2, p1, p0)
    return r1, r2


def getmomentum_2(xi, ti, xo, B, L = 2.0667):

    c = 2.997e8 # speed of light in air (little slower than in vacuum)

    r1 ,r2 = getradius(xi/100, ti, xo/100,  L)
    p1 = r1*B*c
    p2 = r2*B*c
    
    if np.abs(p2 - 400) < np.abs(p1 - 400):
        return p2
    else:
        return p1


def parallel_fit(x, y, p0, y_error, fit_function, n_workers=None):
    """ This Function distributes the fit over all available cores using independent processes. 
        Fit results are returned in a list of length n_cores.
    """

    if n_workers == None:
        try:
            cpu_count = os.cpu_count  # Py3
        except AttributeError:
            cpu_count = multiprocessing.cpu_count  # Py2
        n_processes = cpu_count()
    else:
        n_processes = n_workers

    results = []
    my_queue = multiprocessing.Queue()
    my_ps = []

    def my_curve_fit(dataset, p0, q=None, **kwargs):
        """This function takes same args/kwargs as curve_fit and returns dict containing fit_id as key and (popts, perr) as vaule"""

        # Do fit and error calculation from covaraince matrix
        popt, pcov = curve_fit(**kwargs)
        perr = np.sqrt(np.diag(pcov))

        # Store result in dict: we need the *dataset* as identifier
        res = {dataset: (popt, perr)}

        # If there is no Queue-object return. Otherwise put in Queue
        if q is None:
            return res

        q.put(res)

    def parallel_fit_processes(n_processes, x, y, yerr, fit_func):
        """This function performs a parallel fit of the data using individual multiprocessing. Process instances"""

        def chunk_fit(chunk_offset, x, y, yerr):
            """Helper function which does fit for a chunk of the original data"""

            for dataset in range(x.shape[0]):

                # args for curve_fit; dataset+chunk_offset is the actual number of the current dataset with respect to the original data
                fit_args = (dataset+chunk_offset, my_queue)

                # kwargs for curve_fit
                fit_kwargs = dict(f=fit_func, xdata=x[dataset], ydata=y[dataset], sigma=yerr[dataset], absolute_sigma=True)

                # Dot fit and put result into Queue
                my_curve_fit(*fit_args, **fit_kwargs)

        # # Timing
        # start = time()

        # Init idx, calc step size and start loop over number of processes
        idx_chunk = 0
        step_chunk = int(x.shape[0] / n_processes)
        for n in range(n_processes):

            # Get chunk of data for current process
            if n != n_processes - 1:
                xc, yc, yerrc = x[idx_chunk:idx_chunk+step_chunk], y[idx_chunk:idx_chunk+step_chunk], yerr[idx_chunk:idx_chunk+step_chunk]
            # Last chunk ist just from idx on
            else:
                xc, yc, yerrc = x[idx_chunk:], y[idx_chunk:], yerr[idx_chunk:]

            # Spawn process instance and start
            p = multiprocessing.Process(target=chunk_fit, args=(idx_chunk, xc, yc, yerrc))
            p.start()

            # Append process to list to interact with it
            my_ps.append(p)

            # Increment chunk idx
            idx_chunk += step_chunk

        # Collect the results; arbitrary order
        for _ in range(x.shape[0]):
            results.append(my_queue.get())

        # join the processes: this must not be done in the same loop as start since process will launch and then wait for finish before the next process can be launched
        for p in my_ps:
            p.join()

        # # Timing
        # end = time()

        # # Timing
        # return end - start
    parallel_fit_processes(n_processes, x, y, yerr=y_error, fit_func=fit_function)
    return results


def fit_circle(x, y):
    '''
    From scipy cookbook: https://scipy-cookbook.readthedocs.io/items/Least_Squares_Circle.html
    Method 2 least squares
    ------------------
    INPUT:
        x, y : np.arrays of floats, have to have same length
            Points to fit circle to
    OUTPUT:
        xc, yc, : float
            circle center
        R: float
            circle radius
    '''

    # TODO: return fit errors
    x_m = np.mean(x)
    y_m = np.mean(y)

    def calc_R(xc, yc):
        """ calculate the distance of each 2D points from the center (xc, yc) """
        return np.sqrt((x-xc)**2 + (y-yc)**2)

    def f_2(c):
        """ calculate the algebraic distance between the data points and the mean circle centered at c=(xc, yc) """
        Ri = calc_R(*c)
        return Ri - Ri.mean()

    center_estimate = x_m, y_m
    center, ier = least_squares(f_2, center_estimate)

    xc, yc = center
    Ri = calc_R(*center)
    R = Ri.mean()
    residu = sum((Ri - R)**2)

    return xc, yc, R


def get_deflection(momentum, x, x_error, x_slope, x_slope_error, By=None, field_z=None, charge=1.,momentum_scalar = True):
    '''
    Use known field map for Goliath By to calculate deflection step wise. 
    Bin size of map is 5 cm in all 3 dimensions, beam width is ca. 2 cm , so do not need to consider other x and y positions in B field.
    Fit slope of output track using last 15 x positions. Tangent on last used deflection circle is wrong due to changing B-Field.
    ------------------
    INPUT:
        momentum : float, np.array of float
            momentum of particle to track in field
        x : float or np.array of float
            track x position at entry to magnet
        x_error : float or np.array of float
            error on track x
        x_slope : float or np.array of float
            track x_slope before field
        x_slope_error : float or np.array of float
            error on track x_slope before field
        By : np.array of float
            measured B values for Goliath field
        field_z : np.array of float
            z positions of By field measurements, same coordinate system as tracks
    OUTPUT:
        new_x : float or np.array of float (depending on input)
            track x position at the end of the magnet
        new_x_error : float or np.array of float (depending on input)
            error on output track x
        fit_slope : float or np.array of float (depending on input)
            slope of the output track (fitted)
        fit_slope_error : float or np.array of float (depending on input)
            error on the new slope
        plot_x : np.array of float (shape depending on input)
            all calculated x positions for the single B field values 
    '''

    if (By is None) or (field_z is None):

        field_z_default = np.array([ 3.73,   8.73,  13.73,  18.73,  23.73,  28.73,  33.73,  38.73,  43.73,  48.73,
                        53.73,  58.73,  63.73,  68.73,  73.73,  78.73,  83.73,  88.73,  93.73,  98.73,
                        103.73, 108.73, 113.73, 118.73, 123.73, 128.73, 133.73, 138.73, 143.73, 148.73,
                        153.73, 158.73, 163.73, 168.73, 173.73, 178.73, 183.73, 188.73, 193.73, 198.73,
                        203.73, 208.73, 213.73, 218.73, 223.73, 228.73, 233.73, 238.73, 243.73, 248.73,
                        253.73, 258.73, 263.73, 268.73, 273.73, 278.73, 283.73, 288.73, 293.73, 298.73,
                        303.73, 308.73, 313.73, 318.73, 323.73, 328.73, 333.73, 338.73, 343.73, 348.73,
                        353.73, 358.73, 363.73, 368.73])

        By = np.array([-0.1185646, -0.1472104, -0.1807546, -0.2192168, -0.2633682, -0.3126212,
                        -0.3669946, -0.425686,  -0.4891762, -0.5568198, -0.6279358, -0.7021102,
                        -0.7794526, -0.8581002, -0.9345912, -1.009857,  -1.0804222, -1.1461178,
                        -1.2021938, -1.251542,  -1.2937396, -1.3289748, -1.3579024, -1.3819836,
                        -1.4018306, -1.4179538, -1.4308048, -1.4413368, -1.4498608, -1.4565272,
                        -1.4618054, -1.465877,  -1.469,     -1.4711374, -1.4725578, -1.4732642,
                        -1.4732396, -1.4725664, -1.4711934, -1.4690206, -1.46594,   -1.461857,
                        -1.4565832, -1.4498776, -1.441346,  -1.430814,  -1.4177112, -1.4015442,
                        -1.3816976, -1.3575308, -1.3281134, -1.2925762, -1.2503856, -1.2005098,
                        -1.1426408, -1.077467,  -1.006628,  -0.9311426, -0.8531716, -0.7750776,
                        -0.6987264, -0.624223,  -0.5529024, -0.485227,  -0.4216612, -0.36274,
                        -0.3083242, -0.25918,   -0.215198,  -0.1763882, -0.1425964, -0.1140354,
                        -0.0898272])

        field_z_default -= 2.5

        pixel_global_z = 1008.33  # cm
        scifi_global_z = 1463.46  # cm
        goliath_global_z_center = 1240.67 # cm from EDMS 1836813 and EDMS 1825777

        distance_pix_scifi = scifi_global_z - pixel_global_z  # 455.11 cm
        magnetic_field_start_z = distance_pix_scifi/2  - field_z_default.max()/2
        magnetic_field_end_z = magnetic_field_start_z + field_z_default.max()
        magnet_to_scifi = distance_pix_scifi - magnetic_field_end_z

        field_z = field_z_default +  magnetic_field_start_z

    diff_field_z = np.diff(field_z)
    
    plot_x, x_errs, normal_slope, normal_slope_error = _calc_field_steps(By, field_z, x_slope, x_slope_error, x, x_error, momentum, diff_field_z, charge, momentum_scalar)
    plot_x = np.array(plot_x)
    x_errs = np.array(x_errs)
   
    # need to get output track slope
    # slope_out = (plot_x[-1,:] - plot_x[-2,:])/(5.)
    new_x = plot_x[-1]
    # new_x_error = 0.08731 # this is the average error if one assumes a linear track and propagates over the distance of 450 cm . This is strongly dominated by the error on the x slope.
    new_x_error = np.sqrt(np.square(x_error) + np.square(x_slope_error * (field_z.max() - field_z.min())))
    normal_slope_error = x_slope_error
    return(new_x, new_x_error, normal_slope, normal_slope_error, plot_x)


@njit
def _calc_field_steps(By, field_z, normal_slope, normal_slope_error, new_x, new_x_error, momentum, diff_field_z, charge, momentum_scalar=True):
    
    plot_x = [new_x]
    x_errs = [new_x_error]
    i = 0
    track_angle = np.arctan(normal_slope) + np.pi/2
    for i in range(By.shape[0]):
        # track_angle = np.arctan(normal_slope) + np.pi/2
        if momentum_scalar:
            B = By[i] * np.sin(track_angle)# TODO use cos ?
            # calculate deflection radius
            radius = momentum*1e9/B/2.997e8/charge * 100
            # radius = 3.3356 * momentum/B * 100
        else:
            # vector components of momentum known, only momentum perpendicular to field is used
            radius = momentum*1e9/By[i]/2.997e8/charge * 100

        # slope_error = 1/(np.square(normal_slope)+1) *  normal_slope_error
        # calculate center of circle with deflection radius
        z_center = field_z[i] - (np.cos(track_angle) * radius)
        x_center = new_x - (np.sin(track_angle) * radius)

        # x position after field step
        new_x = x_center + np.sign(radius) * np.sqrt(np.square(radius) - np.square(field_z[i+1] - z_center))
        # if np.any(np.isnan(new_x)):
        #     print("new_x is Nan")
        #     print("momentum", momentum[np.isnan(new_x)])
        #     print("charge", charge[np.isnan(new_x)])
        #     print("x_center", x_center[np.isnan(new_x)])
        #     print("z_center", z_center[np.isnan(new_x)])
        #     print("sqrt", np.sqrt(np.square(radius) - np.square(field_z[i+1] - z_center))[np.isnan(new_x)])
        #     print("square", np.square(field_z[i+1] - z_center)[np.isnan(new_x)])
        #     print("radius",np.square(radius)[np.isnan(new_x)] )
        #     print("------------")
        # new_x = circle(field_z[i+1], z_center, x_center, radius)
        # z_center_error = radius * np.sin(track_angle) * slope_error
        # x_center_error = np.sqrt(np.square(new_x_error) + np.square(np.cos(track_angle)*radius * slope_error))
        # print("relative z_center error" , z_center_error / z_center)
        # print("relative x_center error" , x_center_error / x_center)

        # new_x_error = np.sqrt(np.square(x_center_error) + np.square((field_z[i+1] - z_center) / (np.sqrt(np.square(radius) + np.square(field_z[i+1] - z_center))) * z_center_error ))
        
        # slope_term = slope_error * (radius * np.sin(track_angle)/(np.square(radius) - 2*(radius * np.cos(track_angle) - (field_z[i] ) + field_z[i+1])) - radius * np.cos(track_angle))
        # slope_term = radius * (field_z[i+1] - z_center)/np.sqrt(radius**2 - np.square(field_z[i+1] - z_center)) * z_center_error
        # correlation_term =  2 * (slope_term/slope_error * np.cov(new_x, track_angle, rowvar=False)[0][1] + slope_term/slope_error * np.cov(new_x, track_angle, rowvar=False)[1][0])
        # new_x_error = np.sqrt(np.square(new_x_error) + np.square(slope_term) + correlation_term)
        # new_x_error = np.sqrt(np.square(new_x_error) + np.square(slope_term) + correlation_term)

        # New track angle after field step
        # normal_slope = 1/((x_center - new_x) /(field_z[i+1] - z_center))
        track_angle -= 2 * np.arcsin(diff_field_z[i]/2/radius)

        plot_x.append(new_x)
        x_errs.append(new_x_error)

    normal_slope = np.arctan(track_angle - np.pi/2)
    normal_slope_error = (field_z[i+1] - z_center)/np.square(x_center - new_x) * new_x_error

    return plot_x, x_errs, normal_slope, normal_slope_error


def histogram_data_in_custom_bins(x,y,statistic="mean", n_bins=10, eff_errors = False):

    '''
    histogram given y data in bins defined by x data, essentially a wrapper for scipys binned statistic
    --------------
    INPUT
        x : numpy array
        y : numpy array of same dimension as x
        statistic: str or function, as in scipy.stats.binned_statistic
        n_bins : int
        eff_errors: bool, whether or not to calculate bayesian efficiency errors on calculated histogram bins.
    OUTPUT
        bin_edges : x data of histogram
        bin_means : histogram entry (y data)
        effs : only returned if eff_errors = True, mean efficiciency per bin
        stds : only returned if eff_errors = True, lower and upper error for efficiency bins
    '''
    bin_means, bin_edges, binnumber = stats.binned_statistic(x, y, statistic=statistic, bins=n_bins)

    if eff_errors:

        stds = []
        effs = []
        for actual_bin in tqdm(np.unique(binnumber)):  # Iterate over each bin
            values_in_bin = y[binnumber==actual_bin]  # Get all elements inside bin n
            lim_e_m, lim_e_p = find_inter(values_in_bin[values_in_bin==1].size, values_in_bin.size, 0.68)
            eff = values_in_bin[values_in_bin==1].size/ values_in_bin.size
            stds.append(np.abs([lim_e_m - eff, lim_e_p - eff]))
            effs.append(eff)
        
        return bin_edges, bin_means, np.array(effs), np.array(stds)

    else:
        return bin_edges, bin_means


def get_eff_pdf(eff, k, N):
    # taken from beam telescope analysis https://github.com/SiLab-Bonn/beam_telescope_analysis
    ''' Returns the propability density function for the efficiency
        estimator eff = k/N, where k are the passing events and N the
        total number of events.

        http://lss.fnal.gov/archive/test-tm/2000/fermilab-tm-2286-cd.pdf
        page 5

        This function gives plot 1 of paper, when multiplied by Gamma(N+1)
    '''

    # The paper has the function defined by gamma functions. These explode quickly
    # leading to numerical instabilities. The beta function does the same...
    # np.float(gamma(N + 2)) / np.float((gamma(k + 1) * gamma(N - k + 1))) * eff**k * (1. - eff)**(N - k)

    return stats.beta.pdf(eff, k + 1, N - k + 1)

def get_eff_prop_int(eff, k, N):
    # taken from beam telescope analysis https://github.com/SiLab-Bonn/beam_telescope_analysis
    ''' C.d.f. of beta function = P.d.f. integral -infty..eff '''
    return stats.beta.cdf(eff, k + 1, N - k + 1)

def interval_integ(a, b, k, N):
    # taken from beam telescope analysis https://github.com/SiLab-Bonn/beam_telescope_analysis
    ''' Return the integral of the efficiency pdf using limits [a, b]:
    '''
    return get_eff_prop_int(b, k, N) - get_eff_prop_int(a, k, N)

def find_inter(k, N, interval):
    # taken from beam telescope analysis https://github.com/SiLab-Bonn/beam_telescope_analysis
    ''' Calculates Integral(pdf(k, N))_err-^err+ = interval with
    | err- - err+| != min.
    '''

    def minimizeMe(x):
        a, b = x[0], x[1]
        return b - a

    def get_start_values(k, N):
        # Discretize issue for numerical calculation
        eff = np.linspace(0.8 * float(k) / N, 1.2 * float(k) / N, 1000000)
        eff = eff[eff <= 1.]
        # Normalize by discretization
        p = get_eff_pdf(eff, k, N=N)
        max_i = np.argmin(np.abs(eff - float(k) / N))

        for y in np.linspace(p[max_i] * 0.9, 0, 1000):
            if max_i > 0:
                idx_l = np.abs(y - p[:max_i]).argmin()
            else:
                idx_l = 0

            if max_i < p.shape[0]:
                idx_r = np.abs(y - p[max_i:]).argmin() + max_i
            else:
                idx_r = p.shape[0] - 1

            if p[idx_l:idx_r].sum() * np.diff(eff)[0] > interval:
                break
        return eff[idx_l], eff[idx_r]

    # Quick determination of start value to enhance convergence
    max_a = float(k) / N  # a < maximum eff
    min_b = float(k) / N  # b > maximum eff

    a0, b0 = get_start_values(k, N)

    cons = ({'type': 'eq', 'fun': lambda x: np.abs(interval_integ(x[0], x[1], k, N) - interval)})

    # Find b
    res = optimize.minimize(fun=minimizeMe, method='SLSQP', x0=(a0, b0),
                            bounds=((0., max_a), (min_b, 1.)),
                            constraints=cons)

    return res.x


def fit_hist(hist_in, bins_in, func_in, p0, mask_nan = True, error_in = None, provided_popt = False):
    
    # y input for fit can not contain any NaNs
    if mask_nan:
        mask = ~np.isnan(hist_in)
    else :
        mask = np.ones_like(hist_in)
        
    hist_in = hist_in[mask]
    # center histogram bins

    bin_centers = (bins_in[1:] + bins_in[:-1]) / 2.0
    bin_centers = bin_centers[mask]
    # fit 
    if provided_popt==True:
        popt = p0
        pcov = np.ones_like(p0)
    else : 
        popt, pcov = curve_fit(func_in, bin_centers, hist_in, p0, sigma = error_in, absolute_sigma = False)
    if error_in is not None:
        redchi2 = red_chisquare(hist_in, func_in(bin_centers, *popt), error_in, popt, use_empty_bins = False)
    else:
        redchi2 = red_chisquare(hist_in, func_in(bin_centers, *popt), np.sqrt(hist_in), popt, use_empty_bins = False)
    # create plot data
    
    x_start = bin_centers[0] + (bin_centers[1] - bin_centers[0])/2.
    x_end = bin_centers[-1] + (bin_centers[-1] - bin_centers[-2])/2.
    x = np.linspace(x_start, x_end, num=1000)
    y = func_in(x, *popt)
    return x,y, popt, pcov, redchi2



scifi_tracks_dtype = [("event_number", np.int64),
                   ("spill", np.int32),
                   ("trigger_shift", np.int64),
                   ("x", np.float64),
                   ("y", np.float64),
                   ("z", np.float64),
                   ("x_slope", np.float64),
                   ("y_slope", np.float64),
                   ("x_error", np.float64),
                   ("y_error", np.float64),
                   ("x_slope_error", np.float64),
                   ("y_slope_error", np.float64),
                   ("chi2", np.float64),
                   ("x1", np.float64),
                   ("y1", np.float64),
                   ("z1x", np.float64),
                   ("z1y", np.float64),
                   ("x2", np.float64),
                   ("y2", np.float64),
                   ("z2x", np.float64),
                   ("z2y", np.float64),
                   ("x3", np.float64),
                   ("y3", np.float64),
                   ("z3x", np.float64),
                   ("z3y", np.float64),
                   ("x4", np.float64),
                   ("y4", np.float64),
                   ("z4x", np.float64),
                   ("z4y", np.float64),
                   ("x5", np.float64),
                   ("y5", np.float64),
                   ("z5x", np.float64),
                   ("z5y", np.float64),
                   ("x6", np.float64),
                   ("y6", np.float64),
                   ("z6x", np.float64),
                   ("z6y", np.float64),
                   ("x7", np.float64),
                   ("y7", np.float64),
                   ("z7x", np.float64),
                   ("z7y", np.float64),
                   ("x8", np.float64),
                   ("y8", np.float64),
                   ("z8x", np.float64),
                   ("z8y", np.float64),
                   ("tot_1", np.int32),
                   ("tot_2", np.int32),
                   ("tot_3", np.int32),
                   ("tot_4", np.int32),
                   ("tot_5", np.int32),
                   ("tot_7", np.int32),
                   ("tot_8", np.int32),
                   ("cluster_size_1", np.int32),
                   ("cluster_size_2", np.int32),
                   ("cluster_size_3", np.int32),
                   ("cluster_size_4", np.int32),
                   ("cluster_size_5", np.int32),
                   ("cluster_size_7", np.int32),
                   ("cluster_size_8", np.int32),
                   ("hit_1_used", np.int32),
                   ("hit_2_used", np.int32),
                   ("hit_3_used", np.int32),
                   ("hit_4_used", np.int32),
                   ("hit_5_used", np.int32),
                   ("hit_7_used", np.int32),
                   ("hit_8_used", np.int32),
                   ("track_ID", np.int16),
                   ]

merged_clusters_dtype = [("event_number", np.int64),
                         ("pix_trackID", np.int16),
                         ("plane", np.int32),
                         ("delta_x", np.float64),
                         ("delta_y", np.float64),
                         ("best_match", np.int32),
                         ("delta_r", np.float64),
                         ("spill", np.int32),
                         ("x", np.float64),
                         ("y", np.float64),
                         ("z", np.float64),
                         ("x_slope", np.float64),
                         ("y_slope", np.float64),
                         ("x_error", np.float64),
                         ("y_error", np.float64),
                         ("x_slope_error", np.float64),
                         ("y_slope_error", np.float64),
                         ("pix_chi2", np.float64),
                         ("x_new", np.float64),
                         ("x_new_error", np.float64),
                         ("x_project", np.float64),
                         ("y_project", np.float64),
                         ("x_project_error", np.float64),
                         ("y_project_error", np.float64),
                         ("offset_after", np.float64),
                         ("x_slope_new", np.float64),
                         ("x_slope_new_error", np.float64),
                         ("delta_slope", np.float64),
                         ("timestamp", np.int64),
                         ("n_pix_tracks", np.int64),
                         ("scifi_timestamp", np.int64),
                         ("x_sc", np.float64),
                         ("y_sc", np.float64),
                         ("z_sc", np.float64),
                         ("scifi_tot", np.int32),
                         ("scifi_cluster_size", np.int32),
                         ("scifi_hit_used", np.int16),
                         ("x1", np.float64),
                         ("y1", np.float64),
                         ("z1x", np.float64),
                         ("z1y", np.float64),
                         ("x2", np.float64),
                         ("y2", np.float64),
                         ("z2x", np.float64),
                         ("z2y", np.float64),
                         ("x3", np.float64),
                         ("y3", np.float64),
                         ("z3x", np.float64),
                         ("z3y", np.float64),
                         ("x4", np.float64),
                         ("y4", np.float64),
                         ("z4x", np.float64),
                         ("z4y", np.float64),
                         ("x5", np.float64),
                         ("y5", np.float64),
                         ("z5x", np.float64),
                         ("z5y", np.float64),
                         ("x6", np.float64),
                         ("y6", np.float64),
                         ("z6x", np.float64),
                         ("z6y", np.float64),
                         ("x7", np.float64),
                         ("y7", np.float64),
                         ("z7x", np.float64),
                         ("z7y", np.float64),
                         ("x8", np.float64),
                         ("y8", np.float64),
                         ("z8x", np.float64),
                         ("z8y", np.float64),
                         ("n_plane_hits", np.int32),
                         ("momentum_calc", np.float64),
                         ("radius_calc", np.float64),
                         ]

merged_tracks_dtype = [("event_number", np.int64),
                       ("pix_trackID", np.int16),
                       ("sc_trackID", np.int16),
                       ("plane", np.int32),
                       ("delta_x", np.float64),
                       ("delta_y", np.float64),
                       ("best_match", np.int32),
                       ("delta_r", np.float64),
                       ("spill", np.int32),
                       ("x", np.float64),
                       ("y", np.float64),
                       ("z", np.float64),
                       ("x_slope", np.float64),
                       ("y_slope", np.float64),
                       ("x_error", np.float64),
                       ("y_error", np.float64),
                       ("x_slope_error", np.float64),
                       ("y_slope_error", np.float64),
                       ("pix_chi2", np.float64),
                       ("timestamp", np.int64),
                       ("n_pix_tracks", np.int64),
                       ("scifi_timestamp", np.int64),
                       ("x_sc", np.float64),
                       ("y_sc", np.float64),
                       ("z_sc", np.float64),
                       ("x_slope_sc", np.float64),
                       ("y_slope_sc", np.float64),
                       ("x_sc_error", np.float64),
                       ("y_sc_error", np.float64),
                       ("x_slope_sc_error", np.float64),
                       ("y_slope_sc_error", np.float64),
                       ("x1", np.float64),
                       ("y1", np.float64),
                       ("z1x", np.float64),
                       ("z1y", np.float64),
                       ("x2", np.float64),
                       ("y2", np.float64),
                       ("z2x", np.float64),
                       ("z2y", np.float64),
                       ("x3", np.float64),
                       ("y3", np.float64),
                       ("z3x", np.float64),
                       ("z3y", np.float64),
                       ("x4", np.float64),
                       ("y4", np.float64),
                       ("z4x", np.float64),
                       ("z4y", np.float64),
                       ("x5", np.float64),
                       ("y5", np.float64),
                       ("z5x", np.float64),
                       ("z5y", np.float64),
                       ("x6", np.float64),
                       ("y6", np.float64),
                       ("z6x", np.float64),
                       ("z6y", np.float64),
                       ("x7", np.float64),
                       ("y7", np.float64),
                       ("z7x", np.float64),
                       ("z7y", np.float64),
                       ("x8", np.float64),
                       ("y8", np.float64),
                       ("z8x", np.float64),
                       ("z8y", np.float64),
                       ("sc_tot_1", np.int32),
                       ("sc_tot_2", np.int32),
                       ("sc_tot_3", np.int32),
                       ("sc_tot_4", np.int32),
                       ("sc_tot_5", np.int32),
                       ("sc_tot_7", np.int32),
                       ("sc_tot_8", np.int32),
                       ("sc_cluster_size_1", np.int32),
                       ("sc_cluster_size_2", np.int32),
                       ("sc_cluster_size_3", np.int32),
                       ("sc_cluster_size_4", np.int32),
                       ("sc_cluster_size_5", np.int32),
                       ("sc_cluster_size_7", np.int32),
                       ("sc_cluster_size_8", np.int32),
                       ("sc_hit_1_used", np.int32),
                       ("sc_hit_2_used", np.int32),
                       ("sc_hit_3_used", np.int32),
                       ("sc_hit_4_used", np.int32),
                       ("sc_hit_5_used", np.int32),
                       ("sc_hit_7_used", np.int32),
                       ("sc_hit_8_used", np.int32),
                       ("x_project", np.float64),
                       ("y_project", np.float64),
                       ("x_project_error", np.float64),
                       ("y_project_error", np.float64),
                       ("x_sc_project", np.float64),
                       ("y_sc_project", np.float64),
                       ("x_sc_project_error", np.float64),
                       ("y_sc_project_error", np.float64),
                       ("offset_after", np.float64),
                       ("x_slope_new", np.float64),
                       ("x_slope_new_error", np.float64),
                       ("delta_slope", np.float64),
                       ("radius_calc", np.float64),
                       ("momentum_calc", np.float64)
                       ]

combined_tracks_dtype = [("event_number", np.int64),
                   ("spill", np.int32),
                   ("timestamp", np.int64),
                   ("x", np.float64),
                   ("y", np.float64),
                   ("z", np.float64),
                   ("x_project", np.float64),
                   ("x_project_slope", np.float64),
                   ("x_project_error", np.float64),
                   ("x_project_slope_error", np.float64),
                   ("x_deflect", np.float64),
                   ("x_deflect_error", np.float64),
                   ("x_slope_deflect", np.float64),
                   ("x_slope_deflect_error", np.float64),
                   ("x_slope", np.float64),
                   ("y_slope", np.float64),
                   ("x_error", np.float64),
                   ("y_error", np.float64),
                   ("x_slope_error", np.float64),
                   ("y_slope_error", np.float64),
                   ("z_deflect", np.float64),
                   ("track_x_new", np.float64), 
                   ("track_x_slope_new", np.float64), 
                   ("track_x_error_new", np.float64), 
                   ("track_x_slope_error_new", np.float64),
                   ("track_y_new", np.float64), 
                   ("track_y_slope_new", np.float64), 
                   ("track_y_error_new", np.float64), 
                   ("track_y_slope_error_new", np.float64),
                   ("track_z_new", np.float64), 
                   ("chi2", np.float64),
                   ("x1", np.float64),
                   ("y1", np.float64),
                   ("z1x", np.float64),
                   ("z1y", np.float64),
                   ("x2", np.float64),
                   ("y2", np.float64),
                   ("z2x", np.float64),
                   ("z2y", np.float64),
                   ("x3", np.float64),
                   ("y3", np.float64),
                   ("z3x", np.float64),
                   ("z3y", np.float64),
                   ("x4", np.float64),
                   ("y4", np.float64),
                   ("z4x", np.float64),
                   ("z4y", np.float64),
                   ("x5", np.float64),
                   ("y5", np.float64),
                   ("z5x", np.float64),
                   ("z5y", np.float64),
                   ("x6", np.float64),
                   ("y6", np.float64),
                   ("z6x", np.float64),
                   ("z6y", np.float64),
                   ("x7", np.float64),
                   ("y7", np.float64),
                   ("z7x", np.float64),
                   ("z7y", np.float64),
                   ("x8", np.float64),
                   ("y8", np.float64),
                   ("z8x", np.float64),
                   ("z8y", np.float64),
                   ("tot_1", np.int32),
                   ("tot_2", np.int32),
                   ("tot_3", np.int32),
                   ("tot_4", np.int32),
                   ("tot_5", np.int32),
                   ("tot_7", np.int32),
                   ("tot_8", np.int32),
                   ("cluster_size_1", np.int32),
                   ("cluster_size_2", np.int32),
                   ("cluster_size_3", np.int32),
                   ("cluster_size_4", np.int32),
                   ("cluster_size_5", np.int32),
                   ("cluster_size_7", np.int32),
                   ("cluster_size_8", np.int32),
                   ("n_hits_1", np.int32),
                   ("n_hits_2", np.int32),
                   ("n_hits_3", np.int32),
                   ("n_hits_4", np.int32),
                   ("n_hits_5", np.int32),
                   ("n_hits_7", np.int32),
                   ("n_hits_8", np.int32),
                   ("n_planes", np.int16),
                   ("n_planes_x", np.int16),
                   ("n_planes_y", np.int16),
                   ("track_ID", np.int16),
                   ("delta_x_1", np.float64),
                   ("delta_x_2", np.float64),
                   ("delta_x_7", np.float64),
                   ("delta_x_8", np.float64),
                   ("delta_y_3", np.float64),
                   ("delta_y_4", np.float64),
                   ("delta_y_5", np.float64),
                   ]


matched_emulsion_pixel_dtype = [('emu_vid', '<i4'),
                                ('emu_trk', '<i4'),
                                ('emu_track_chi2', '<f4'),
                                ('pix_trk', '<i4'),
                                ('match_chi2', '<f4'),
                                ('match_time', '<i8'),
                                ('match_emu_x', '<f4'),
                                ('match_emu_y', '<f4'),
                                ('match_emu_tx', '<f4'),
                                ('match_emu_ty', '<f4'),
                                ('match_emu_nseg', '<i4'),
                                ('match_pix_x', '<f4'), 
                                ('match_pix_y', '<f4'),
                                ('match_pix_tx', '<f4'),
                                ('match_pix_ty', '<f4'),
                                ('match_dx', '<f4'),
                                ('match_dy', '<f4'),
                                ('match_dtx', '<f4'),
                                ('match_dty', '<f4'),
                                ('match_dxr', '<f4'),
                                ('match_dyr', '<f4'),
                                ('match_dtxr', '<f4'),
                                ('match_dtyr', '<f4'),
                                ('x_0', '<f4'),
                                ('y_0', '<f4'),
                                ('z_0', '<f4'),
                                ('tx_0', '<f4'), 
                                ('ty_0', '<f4'),
                                ('txy_0', '<f4'),
                                ('v_x', '<f4'),
                                ('v_y', '<f4'),
                                ('spill', '<i4'),
                                ('matched', '<i4'),
                                ('pix_x_error', '<f4'),
                                ('pix_y_error', '<f4'),
                                ('pix_tx_error', '<f4'),
                                ('pix_ty_error', '<f4'),
                                ('pix_x_y_error', '<f4'),
                                ('pix_x_tx_error', '<f4'),
                                ('pix_x_ty_error', '<f4'),
                                ('pix_y_tx_error', '<f4'),
                                ('pix_y_ty_error', '<f4'),
                                ('pix_tx_ty_error', '<f4'),
                                ('pix_track_chi2', '<f4'),
                                ("global_trackID", np.int64),
                                ]


emu_matching_input_dtype = [('trackID', '<i4'), 
                            ('x', '<f4'), 
                            ('y', '<f4'),
                            ('z', '<f4'), 
                            ('tx', '<f4'), 
                            ('ty', '<f4'), 
                            ('x2', '<f4'), 
                            ('y2', '<f4'), 
                            ('z2', '<f4'), 
                            ('vID', '<i4'), 
                            ('vx', '<f4'), 
                            ('vy', '<f4'), 
                            ('vz', '<f4'), 
                            ('nTracks', '<i4'), 
                            ('quarter', '<i4'), 
                            ('nseg', '<i4'), 
                            ('track_chi2', '<f4'),
                            ('match_emu_x', '<f4'),
                            ('match_emu_y', '<f4'),
                            ('match_emu_tx', '<f4'),
                            ('match_emu_ty', '<f4'),
                            ('emu_vid', '<i4'),
                            ('emu_trk', '<i4'),
                            ('spill', '<i4'),
                            ("matched", np.int16)]
