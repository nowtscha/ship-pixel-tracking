import os
import logging
import subprocess
import concurrent.futures as cf

from ship_pixel_tracking.analysis.calculate_efficiency import getEfficiency, make_efficiency_input_files
from ship_pixel_tracking.ConvertRoot.pix_tracks_to_h5 import open_pixel_tracks_root

runs  = [2815]
for run in runs:
    # planes = [0,1,2,3,4,5,6,7,8,9,10,11]
    planes = [1,2,3,5,6,7,9,10,11]
    #planes = [6] #,2,3,5,6,7,9,10,11]
    nevents = -1
    max_hits = -1
    last_shared_module = -1

    new_tracks = True

    if new_tracks:
        processes = []
        for plane in planes:
            tracking_call = 'root -l -b -q \"/home/niko/git/ship_tracking/ship_pixel_tracking/runTestBeamFullPix.C(%i,%i,%i,%i,%i,%i, %i)\"' %(run, nevents, -1, -1, plane, max_hits, last_shared_module)
            logging.info("fitting tracks for plane %i" %plane)
            processes.append(subprocess.Popen(tracking_call, shell=True))
            processes[0].wait()
        for process in processes[1:]:
            process.wait()

        in_files = ["/home/niko/git/ship_tracking/data/run_%s/pix_tracks_skip_plane_%i.root" %(run, plane) for plane in planes]
        track_files, hit_files = [], []
        
        with cf.ProcessPoolExecutor() as executor:
            conversion_threads = []
            for in_file in in_files:
                conversion_threads.append(executor.submit(open_pixel_tracks_root, in_file, 10000, None))
            cf.wait(conversion_threads)
            for thread in conversion_threads:
                returns = thread.result()
                track_files.append(returns[0])
                hit_files.append(returns[1])

        eff_input_file = make_efficiency_input_files(track_files, hit_files, planes = planes)
    else:
        eff_input_file = "/home/niko/git/ship_tracking/data/run_%s/efficiency_input.h5" % run

    getEfficiency(eff_tracks_file = eff_input_file, 
                    cut_distance=[500.,500.], res_range = [[-500., 500.],[-500.,500.]],
                    efficiency_regions= [None,
                                        [[-5000,10000],[-15000,15000]], 
                                        [[-7000,12000],[-15000,0]], 
                                        [[-26000,12000],[700,17000]], 
                                        None, 
                                        [[-6996.,9804.],[-20000.,20000.]], 
                                        [[-3500,4700],[-6000,0.]], 
                                        [[-3500,5000],[700,12500]],
                                        None, 
                                        [[-5000,10000],[-15000,15000]],
                                        [[-3500,4700],[-6000,0.]],
                                        [[-2500,3500],[0,12500]]
                                        ],
                    min_occupancy= 1, track_quality = 5., additional_plots = False, plot_planes = planes)
                    # min_angle = [0.001, 0.001])