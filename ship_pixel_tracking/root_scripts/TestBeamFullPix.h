//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Feb  6 16:23:26 2019 by ROOT version 6.14/06
// from TTree tree/tree
// found on file: data/run_2793/Merged_spills.root
//////////////////////////////////////////////////////////
//

#ifndef TestBeamFullPix_h
#define TestBeamFullPix_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include "TEfficiency.h"
#include <TVector3.h>
#include <TMatrixDSym.h>
#include <TVectorD.h>
#include <iostream>


#include <boost/config.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/bron_kerbosch_all_cliques.hpp>
#include <boost/graph/undirected_graph.hpp>


// Header file for the classes stored in the TTree if any.

class TestBeamFullPix {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  TH2F * h_dist_d;
  TH1D * h_unused_dx;
  TH1D * h_unused_dy;
  TH1F * h_candHits;
  TH1F * h_hitsInEv;
  TH1F * h_hitX[12];
  TH1F * h_hitY[12];
  TH1F * h_clusterCharge[12];
  TH1F * h_df1trX[12];
  TH1F * h_df1trY[12];
  TH1F * h_dfUp[12];
  TH1F * h_dfDo[12];
  TH1F * h_dZX[12];
  TH1F * h_dZY[12];
  TH1F * h_nhits[6];
  TH2F * h_xy_ineff[6];
  TH2F * h_xy_eff[6];

  TH1F * h_mplhit;
  TH1F * h_nTrkPerHit;
  TH1F * h_quality;
  TH1F * h_trkFoundX;
  TH1F * h_trkFoundY;
  TH1F * h_trkFoundAngX;
  TH1F * h_trkFoundAngY;
  TH1F * h_test1;
  TH1F * h_test2;
  TH1F * h_isolPl;
  TH1F * h_ndf;
  TH1F * h_ntracks;
  TH1F * h_vertex_X[6];
  TH1F * h_vertex_Y[6];
  TH1F * h_vertex_Z[6];
  TH1F * h_2t_vertex_x;
  TH1F * h_2t_vertex_y;
  TH1F * h_2t_vertex_z;
  TH1F * h_vertex_Z_distances[6];
  TH1F * h_nvertex_per_event;
  TH1F * h_nvertex_per_event0;
  TH2F * h_xy_vertex[6];
  TH2F * h_xy_vertex_emu_beginning;
  TH2F * h_xy_vertex_emu_end;
  TH2F * h_xy_vertex_backscatter;
  TH1D * h_dInVrt;
  TH1F * h_matchedEmul;
  TH1F * h_matchDistX;
  TH1F * h_matchDistY1;
  TH1F * h_matchDistY2;
  TH1F * h_matchDistR;
  TH1F * h_matchAng;
  TH1F * h_matchInEv;
  TH1F * h_matchTrk;
  TH1F * h_accu;
  TH1F * h_accu1;

  TH2F * h_matchXY;
  TH1F * h_hitpattern;
  TH2F * h_purity_tracks[12];
  TH2F * h_purity_hits[12];
  TH2F * h_purity[12];
  TH2F * h_failed_tracks[12];

  TH2F * h_eff_tracks[12];
  TH2F * h_eff_hits[12];
  TH2F * h_eff[12];
  // TEfficiency * h_eff[12];

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  ULong64_t       trigger_time_stamp;
  ULong64_t       event_number;
  std::vector<double>  *x0;
  std::vector<double>  *x1;
  std::vector<double>  *x2;
  std::vector<double>  *x3;
  std::vector<double>  *x4;
  std::vector<double>  *x5;
  std::vector<double>  *x6;
  std::vector<double>  *x7;
  std::vector<double>  *x8;
  std::vector<double>  *x9;
  std::vector<double>  *x10;
  std::vector<double>  *x11;
  std::vector<double>  *y0;
  std::vector<double>  *y1;
  std::vector<double>  *y2;
  std::vector<double>  *y3;
  std::vector<double>  *y4;
  std::vector<double>  *y5;
  std::vector<double>  *y6;
  std::vector<double>  *y7;
  std::vector<double>  *y8;
  std::vector<double>  *y9;
  std::vector<double>  *y10;
  std::vector<double>  *y11;
  std::vector<float>   *charge0;
  std::vector<float>   *charge1;
  std::vector<float>   *charge2;
  std::vector<float>   *charge3;
  std::vector<float>   *charge4;
  std::vector<float>   *charge5;
  std::vector<float>   *charge6;
  std::vector<float>   *charge7;
  std::vector<float>   *charge8;
  std::vector<float>   *charge9;
  std::vector<float>   *charge10;
  std::vector<float>   *charge11;
  std::vector<unsigned int> *nHits0;
  std::vector<unsigned int> *nHits1;
  std::vector<unsigned int> *nHits2;
  std::vector<unsigned int> *nHits3;
  std::vector<unsigned int> *nHits4;
  std::vector<unsigned int> *nHits5;
  std::vector<unsigned int> *nHits6;
  std::vector<unsigned int> *nHits7;
  std::vector<unsigned int> *nHits8;
  std::vector<unsigned int> *nHits9;
  std::vector<unsigned int> *nHits10;
  std::vector<unsigned int> *nHits11;
  UShort_t        nCluster0;
  UShort_t        nCluster1;
  UShort_t        nCluster2;
  UShort_t        nCluster3;
  UShort_t        nCluster4;
  UShort_t        nCluster5;
  UShort_t        nCluster6;
  UShort_t        nCluster7;
  UShort_t        nCluster8;
  UShort_t        nCluster9;
  UShort_t        nCluster10;
  UShort_t        nCluster11;
  std::vector<Long64_t> *clusterShape0;
  std::vector<Long64_t> *clusterShape1;
  std::vector<Long64_t> *clusterShape2;
  std::vector<Long64_t> *clusterShape3;
  std::vector<Long64_t> *clusterShape4;
  std::vector<Long64_t> *clusterShape5;
  std::vector<Long64_t> *clusterShape6;
  std::vector<Long64_t> *clusterShape7;
  std::vector<Long64_t> *clusterShape8;
  std::vector<Long64_t> *clusterShape9;
  std::vector<Long64_t> *clusterShape10;
  std::vector<Long64_t> *clusterShape11;
  std::vector<float>   *xerr0;
  std::vector<float>   *xerr1;
  std::vector<float>   *xerr2;
  std::vector<float>   *xerr3;
  std::vector<float>   *xerr4;
  std::vector<float>   *xerr5;
  std::vector<float>   *xerr6;
  std::vector<float>   *xerr7;
  std::vector<float>   *xerr8;
  std::vector<float>   *xerr9;
  std::vector<float>   *xerr10;
  std::vector<float>   *xerr11;
  std::vector<float>   *yerr0;
  std::vector<float>   *yerr1;
  std::vector<float>   *yerr2;
  std::vector<float>   *yerr3;
  std::vector<float>   *yerr4;
  std::vector<float>   *yerr5;
  std::vector<float>   *yerr6;
  std::vector<float>   *yerr7;
  std::vector<float>   *yerr8;
  std::vector<float>   *yerr9;
  std::vector<float>   *yerr10;
  std::vector<float>   *yerr11;
  Int_t           hitflag;
  Int_t           spill;

  // List of branches
  TBranch        *b_tts;   //!
  TBranch        *b_event;   //!
  TBranch        *b_x0;   //!
  TBranch        *b_x1;   //!
  TBranch        *b_x2;   //!
  TBranch        *b_x3;   //!
  TBranch        *b_x4;   //!
  TBranch        *b_x5;   //!
  TBranch        *b_x6;   //!
  TBranch        *b_x7;   //!
  TBranch        *b_x8;   //!
  TBranch        *b_x9;   //!
  TBranch        *b_x10;   //!
  TBranch        *b_x11;   //!
  TBranch        *b_y0;   //!
  TBranch        *b_y1;   //!
  TBranch        *b_y2;   //!
  TBranch        *b_y3;   //!
  TBranch        *b_y4;   //!
  TBranch        *b_y5;   //!
  TBranch        *b_y6;   //!
  TBranch        *b_y7;   //!
  TBranch        *b_y8;   //!
  TBranch        *b_y9;   //!
  TBranch        *b_y10;   //!
  TBranch        *b_y11;   //!
  TBranch        *b_charge0;   //!
  TBranch        *b_charge1;   //!
  TBranch        *b_charge2;   //!
  TBranch        *b_charge3;   //!
  TBranch        *b_charge4;   //!
  TBranch        *b_charge5;   //!
  TBranch        *b_charge6;   //!
  TBranch        *b_charge7;   //!
  TBranch        *b_charge8;   //!
  TBranch        *b_charge9;   //!
  TBranch        *b_charge10;   //!
  TBranch        *b_charge11;   //!
  TBranch        *b_nHits0;   //!
  TBranch        *b_nHits1;   //!
  TBranch        *b_nHits2;   //!
  TBranch        *b_nHits3;   //!
  TBranch        *b_nHits4;   //!
  TBranch        *b_nHits5;   //!
  TBranch        *b_nHits6;   //!
  TBranch        *b_nHits7;   //!
  TBranch        *b_nHits8;   //!
  TBranch        *b_nHits9;   //!
  TBranch        *b_nHits10;   //!
  TBranch        *b_nHits11;   //!
  TBranch        *b_nCluster0;   //!
  TBranch        *b_nCluster1;   //!
  TBranch        *b_nCluster2;   //!
  TBranch        *b_nCluster3;   //!
  TBranch        *b_nCluster4;   //!
  TBranch        *b_nCluster5;   //!
  TBranch        *b_nCluster6;   //!
  TBranch        *b_nCluster7;   //!
  TBranch        *b_nCluster8;   //!
  TBranch        *b_nCluster9;   //!
  TBranch        *b_nCluster10;   //!
  TBranch        *b_nCluster11;   //!
  TBranch        *b_clusterShape0;   //!
  TBranch        *b_clusterShape1;   //!
  TBranch        *b_clusterShape2;   //!
  TBranch        *b_clusterShape3;   //!
  TBranch        *b_clusterShape4;   //!
  TBranch        *b_clusterShape5;   //!
  TBranch        *b_clusterShape6;   //!
  TBranch        *b_clusterShape7;   //!
  TBranch        *b_clusterShape8;   //!
  TBranch        *b_clusterShape9;   //!
  TBranch        *b_clusterShape10;   //!
  TBranch        *b_clusterShape11;   //!
  TBranch        *b_xerr0;   //!
  TBranch        *b_xerr1;   //!
  TBranch        *b_xerr2;   //!
  TBranch        *b_xerr3;   //!
  TBranch        *b_xerr4;   //!
  TBranch        *b_xerr5;   //!
  TBranch        *b_xerr6;   //!
  TBranch        *b_xerr7;   //!
  TBranch        *b_xerr8;   //!
  TBranch        *b_xerr9;   //!
  TBranch        *b_xerr10;   //!
  TBranch        *b_xerr11;   //!
  TBranch        *b_yerr0;   //!
  TBranch        *b_yerr1;   //!
  TBranch        *b_yerr2;   //!
  TBranch        *b_yerr3;   //!
  TBranch        *b_yerr4;   //!
  TBranch        *b_yerr5;   //!
  TBranch        *b_yerr6;   //!
  TBranch        *b_yerr7;   //!
  TBranch        *b_yerr8;   //!
  TBranch        *b_yerr9;   //!
  TBranch        *b_yerr10;   //!
  TBranch        *b_yerr11;   //!
  TBranch        *b_flag;   //!
  TBranch        *b_spill;   //!

  TestBeamFullPix(unsigned short iRun = -1, int iSkipPlane = -1, int clustering = 1);
  virtual ~TestBeamFullPix();
  unsigned short fRun;
  int fSkipPlane;
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop(unsigned short iRun, Long64_t nEvents = -1, Int_t whichSpill = -1, Long64_t startEvent = 0, Int_t iSkipPlane = -1, Int_t imax_hits = -1, Int_t last_shared_module = -1, Int_t clustering = 1);
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);
  void  Align1tr(std::vector< std::vector<double>> &X, std::vector< std::vector<double>> &Y);
  void  CheckEfficiency(std::vector< std::vector<double>> &X, std::vector< std::vector<double>> &Y);
  struct fittedTrack {
    bool Good = false;
    Long64_t timestamp{0};
    Float_t Chi2{1.e9};
    int Ndf{0};
    TVector3 pos;
    TVector3 mom;
    TMatrixDSym COV{6};
    int trackID{0};
    Long_t globalTrackID{-1};
    TVector3 pos_update;
    TVector3 mom_update;
    TMatrixDSym COV_update{6};
  };
  struct pixHit {
    TVectorD hitXY{2};
    Long64_t timestamp{0};
    Long64_t eventNumber{0};
    double planeZ{0.};
    float clusterCharge{ -1.};
    UInt_t clusterSize{0};
    TMatrixDSym hitCov{2};
    int planeID{0};
    int localIndex{ -1};
    int Used{0};
    int trackID{0};
    Long_t globalTrackID{-1};
  };
  struct planehit {
    Long64_t event_number{0};
    Long64_t timestamp{0};
    double_t x;
    double_t y;
    int planeID;
  };
  struct pixTrack {
    pixHit Hit;
    TVector2 pos;
    TVector2 angle;
  };
  struct vertex2Track {
    double chi2{1.e9};
    double distance;
    TVector3 r;
    TMatrixD COV{3,3};
    std::vector<int> trackID;
  };
  struct MultiVtx {
        bool Good=false;
        double chi2{1.e9}; // chi2 for fit of r
        double redchi2{1.e9};
        int ndf;
        int nTracks;
        double distance; // euclidean distance in point of closest approach (POCA)
        TVector3 r; // POCA
        TMatrixD vertexCov{3,3};
        std::vector<double> chi2PerTrk;
        // TMatrixDSym COV{3}; // covariance matrix for fit of r
        std::vector<int> trackID; // trackIDs of Tracks in vertex
        bool refined = false;
    };
  struct trackPars {
      TVector3 pos;
      TVector3 mom;
      TMatrixDSym COV{2};
    };

  struct clique_visitor{
    clique_visitor(std::vector< std::vector<int> > & input): m_allCliques(input){ input.clear();}
    
    template <typename Clique, typename Graph>
    void clique(const Clique& clq, Graph& )
    {
      std::vector<int> new_clique(0);
      for(auto i = clq.begin(); i != clq.end(); ++i) new_clique.push_back(*i);
      m_allCliques.push_back(new_clique);
    }

    std::vector< std::vector<int> > & m_allCliques;

  };

  std::vector< std::vector<pixHit> >  * pixEvent{nullptr};
  std::vector< std::multimap<double, pixHit> >  * npixEvent{nullptr};
  std::unique_ptr<boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS>> m_compatibilityGraph{};

  void CreatePixEvent(std::vector< std::vector<double>> &X, std::vector< std::vector<double>> &Y, std::vector<std::vector<float>> &Xerr, std::vector<std::vector<float>> &Yerr, std::vector< std::vector<float>> &clusterChargetmp, std::vector< std::vector<unsigned int>> &clusterSizetmp, Long64_t event_number, ULong64_t timestamp);
  void PrintPixEvent(std::vector< std::vector<double>> &X, std::vector< std::vector<double>> &Y);
  void WriteHistograms();
  void GetTracks(std::vector<fittedTrack> & fittedTracks, Int_t SkipPlane, std::set<int> shareHitModules, Long_t &globTrackID);
  void GetTracksPlane2Plane(std::vector<fittedTrack> & fittedTracks);
  void FillPurityTrackHist(std::vector<fittedTrack> &track);
  void GetEfficiency(std::vector<fittedTrack> & fittedTracks);
  int HitsInPlane2PlaneCone(pixHit hitI, std::vector<pixHit> & nextPlaneHits);
  void find2TrackVertices(std::vector<fittedTrack> Tracks, std::vector<vertex2Track> &vertices, std::map<long int, vertex2Track> &map2tVtx);
  int findTrackIndex(TVector3 mom, std::vector<fittedTrack> Tracks);
  int storeVertices(std::vector<fittedTrack> Tracks, std::vector<vertex2Track> &vertices);

  void matchEmulsionAng(std::vector<fittedTrack> & Tracks, int evtnum);
  void matchEmulsion(std::vector<fittedTrack> & Tracks, int );
  fittedTrack FitTrack(std::vector<pixHit> &trkHits);
  fittedTrack FitTrack3D(std::vector<pixHit> &trkHits);
  bool TrackImproved(std::vector<pixHit> &trkHits, fittedTrack &Trk);
  double fit2TVertex(fittedTrack t1, fittedTrack t2, double &Xv, double &Yv, double &Zv, double &eXv, double &eYv, double &eZv, double &chi2v);
  void twoVertex(fittedTrack t1, fittedTrack t2, vertex2Track& d);
  void SetMovingXY(ULong64_t trigger_time_stamp, Int_t spill);

  double DistancePointTrack(TVector3 point, fittedTrack tr);
  double SumDistancePointTracks(TVector3 point, std::vector<fittedTrack> Tracks);
  // TVector3 MultiTrackVertex(std::vector<fittedTrack> Tracks);
  void FillCompGraph(std::vector<vertex2Track> &vertices2Track, std::vector<vertex2Track> &good2tVtx, long int nTracks, std::vector<double> z_lims, double chi2_lim);
  void FitMultiVtx(std::vector<MultiVtx> &multiVtcs, std::vector<vertex2Track> &selected2Tvertices, std::map<long int, vertex2Track> &map2tVtx, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims);
  TVector3 estimVrtPos( int nTrk, std::vector<int> &selTrk, std::map<long int, vertex2Track> & vrt);
  //TODO: finish functions
  void fitVertex(std::vector<fittedTrack> &Tracks, MultiVtx &vtx, TVector3 estimate);
  // void fitVertexEigen(std::vector<fittedTrack> &Tracks, MultiVtx &vtx, TVector3 estimate);
  void fitVertexLin(std::vector<fittedTrack> &Tracks, MultiVtx &vtx, TVector3 estimate);

  void fitVertex_old(MultiVtx &vertex, std::vector<fittedTrack> &Tracks, TVector3 estimate);
  int nTrkCommon(std::vector<MultiVtx> &vertexSet, int V1, int V2);
  double mergeAndRefitVertices( MultiVtx & v1, MultiVtx & v2, MultiVtx & newvrt, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims, std::map<Int_t, trackPars> &newPars);
  double refineVerticesWithCommonTracks( MultiVtx &v1, MultiVtx &v2, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims, std::map<Int_t, trackPars> &newPars);
  void FitVtxandTracks(std::vector<fittedTrack> &fittedTracks, MultiVtx &vtx, TVector3 estimate, std::map<Int_t, trackPars> &newPars);

  TestBeamFullPix::MultiVtx RefineVertex(MultiVtx &v1, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims, std::map<Int_t, trackPars> &newPars);

};

#endif

#ifdef TestBeamFullPix_cxx
TestBeamFullPix::TestBeamFullPix(unsigned short iRun, int iSkipPlane, int clustering) : fChain(0), fRun(iRun), fSkipPlane(iSkipPlane) {

  TString str = "../data/run_" + std::to_string(fRun) + "/run" + std::to_string(fRun) + ".root";
  if (clustering!=1) str = "../data/run_" + std::to_string(fRun) + "/run" + std::to_string(fRun) + "_no_clustering.root";
  // TString str = "data/run_"+std::to_string(fRun)+"/Merged_spills.root";
  TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(str);
  if (!f || !f->IsOpen()) f = new TFile(str);
  TTree *tree = 0;
  f->GetObject("Clusters", tree);
  Init(tree);
}

TestBeamFullPix::~TestBeamFullPix() {
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t TestBeamFullPix::GetEntry(Long64_t entry) {
// Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t TestBeamFullPix::LoadTree(Long64_t entry) {
// Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void TestBeamFullPix::Init(TTree *tree) {
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set object pointer
  x0 = 0;
  x1 = 0;
  x2 = 0;
  x3 = 0;
  x4 = 0;
  x5 = 0;
  x6 = 0;
  x7 = 0;
  x8 = 0;
  x9 = 0;
  x10 = 0;
  x11 = 0;
  y0 = 0;
  y1 = 0;
  y2 = 0;
  y3 = 0;
  y4 = 0;
  y5 = 0;
  y6 = 0;
  y7 = 0;
  y8 = 0;
  y9 = 0;
  y10 = 0;
  y11 = 0;
  xerr0 = 0;
  xerr1 = 0;
  xerr2 = 0;
  xerr3 = 0;
  xerr4 = 0;
  xerr5 = 0;
  xerr6 = 0;
  xerr7 = 0;
  xerr8 = 0;
  xerr9 = 0;
  xerr10 = 0;
  xerr11 = 0;
  yerr0 = 0;
  yerr1 = 0;
  yerr2 = 0;
  yerr3 = 0;
  yerr4 = 0;
  yerr5 = 0;
  yerr6 = 0;
  yerr7 = 0;
  yerr8 = 0;
  yerr9 = 0;
  yerr10 = 0;
  yerr11 = 0;
  charge0 = 0;
  charge1 = 0;
  charge2 = 0;
  charge3 = 0;
  charge4 = 0;
  charge5 = 0;
  charge6 = 0;
  charge7 = 0;
  charge8 = 0;
  charge9 = 0;
  charge10 = 0;
  charge11 = 0;
  nHits0 = 0;
  nHits1 = 0;
  nHits2 = 0;
  nHits3 = 0;
  nHits4 = 0;
  nHits5 = 0;
  nHits6 = 0;
  nHits7 = 0;
  nHits8 = 0;
  nHits9 = 0;
  nHits10 = 0;
  nHits11 = 0;
  clusterShape0 = 0;
  clusterShape1 = 0;
  clusterShape2 = 0;
  clusterShape3 = 0;
  clusterShape4 = 0;
  clusterShape5 = 0;
  clusterShape6 = 0;
  clusterShape7 = 0;
  clusterShape8 = 0;
  clusterShape9 = 0;
  clusterShape10 = 0;
  clusterShape11 = 0;
  xerr0 = 0;
  xerr1 = 0;
  xerr2 = 0;
  xerr3 = 0;
  xerr4 = 0;
  xerr5 = 0;
  xerr6 = 0;
  xerr7 = 0;
  xerr8 = 0;
  xerr9 = 0;
  xerr10 = 0;
  xerr11 = 0;
  yerr0 = 0;
  yerr1 = 0;
  yerr2 = 0;
  yerr3 = 0;
  yerr4 = 0;
  yerr5 = 0;
  yerr6 = 0;
  yerr7 = 0;
  yerr8 = 0;
  yerr9 = 0;
  yerr10 = 0;
  yerr11 = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("trigger_time_stamp", &trigger_time_stamp, &b_tts);
  fChain->SetBranchAddress("event_number", &event_number, &b_event);
  fChain->SetBranchAddress("x0", &x0, &b_x0);
  fChain->SetBranchAddress("x1", &x1, &b_x1);
  fChain->SetBranchAddress("x2", &x2, &b_x2);
  fChain->SetBranchAddress("x3", &x3, &b_x3);
  fChain->SetBranchAddress("x4", &x4, &b_x4);
  fChain->SetBranchAddress("x5", &x5, &b_x5);
  fChain->SetBranchAddress("x6", &x6, &b_x6);
  fChain->SetBranchAddress("x7", &x7, &b_x7);
  fChain->SetBranchAddress("x8", &x8, &b_x8);
  fChain->SetBranchAddress("x9", &x9, &b_x9);
  fChain->SetBranchAddress("x10", &x10, &b_x10);
  fChain->SetBranchAddress("x11", &x11, &b_x11);
  fChain->SetBranchAddress("y0", &y0, &b_y0);
  fChain->SetBranchAddress("y1", &y1, &b_y1);
  fChain->SetBranchAddress("y2", &y2, &b_y2);
  fChain->SetBranchAddress("y3", &y3, &b_y3);
  fChain->SetBranchAddress("y4", &y4, &b_y4);
  fChain->SetBranchAddress("y5", &y5, &b_y5);
  fChain->SetBranchAddress("y6", &y6, &b_y6);
  fChain->SetBranchAddress("y7", &y7, &b_y7);
  fChain->SetBranchAddress("y8", &y8, &b_y8);
  fChain->SetBranchAddress("y9", &y9, &b_y9);
  fChain->SetBranchAddress("y10", &y10, &b_y10);
  fChain->SetBranchAddress("y11", &y11, &b_y11);
  fChain->SetBranchAddress("xerr0", &xerr0, &b_xerr0);
  fChain->SetBranchAddress("xerr1", &xerr1, &b_xerr1);
  fChain->SetBranchAddress("xerr2", &xerr2, &b_xerr2);
  fChain->SetBranchAddress("xerr3", &xerr3, &b_xerr3);
  fChain->SetBranchAddress("xerr4", &xerr4, &b_xerr4);
  fChain->SetBranchAddress("xerr5", &xerr5, &b_xerr5);
  fChain->SetBranchAddress("xerr6", &xerr6, &b_xerr6);
  fChain->SetBranchAddress("xerr7", &xerr7, &b_xerr7);
  fChain->SetBranchAddress("xerr8", &xerr8, &b_xerr8);
  fChain->SetBranchAddress("xerr9", &xerr9, &b_xerr9);
  fChain->SetBranchAddress("xerr10", &xerr10, &b_x10);
  fChain->SetBranchAddress("xerr11", &xerr11, &b_x11);
  fChain->SetBranchAddress("yerr0", &yerr0, &b_yerr0);
  fChain->SetBranchAddress("yerr1", &yerr1, &b_yerr1);
  fChain->SetBranchAddress("yerr2", &yerr2, &b_yerr2);
  fChain->SetBranchAddress("yerr3", &yerr3, &b_yerr3);
  fChain->SetBranchAddress("yerr4", &yerr4, &b_yerr4);
  fChain->SetBranchAddress("yerr5", &yerr5, &b_yerr5);
  fChain->SetBranchAddress("yerr6", &yerr6, &b_yerr6);
  fChain->SetBranchAddress("yerr7", &yerr7, &b_yerr7);
  fChain->SetBranchAddress("yerr8", &yerr8, &b_yerr8);
  fChain->SetBranchAddress("yerr9", &yerr9, &b_yerr9);
  fChain->SetBranchAddress("yerr10", &yerr10, &b_yerr10);
  fChain->SetBranchAddress("yerr11", &yerr11, &b_yerr11);
  fChain->SetBranchAddress("charge0", &charge0, &b_charge0);
  fChain->SetBranchAddress("charge1", &charge1, &b_charge1);
  fChain->SetBranchAddress("charge2", &charge2, &b_charge2);
  fChain->SetBranchAddress("charge3", &charge3, &b_charge3);
  fChain->SetBranchAddress("charge4", &charge4, &b_charge4);
  fChain->SetBranchAddress("charge5", &charge5, &b_charge5);
  fChain->SetBranchAddress("charge6", &charge6, &b_charge6);
  fChain->SetBranchAddress("charge7", &charge7, &b_charge7);
  fChain->SetBranchAddress("charge8", &charge8, &b_charge8);
  fChain->SetBranchAddress("charge9", &charge9, &b_charge9);
  fChain->SetBranchAddress("charge10", &charge10, &b_charge10);
  fChain->SetBranchAddress("charge11", &charge11, &b_charge11);
  fChain->SetBranchAddress("nHits0", &nHits0, &b_nHits0);
  fChain->SetBranchAddress("nHits1", &nHits1, &b_nHits1);
  fChain->SetBranchAddress("nHits2", &nHits2, &b_nHits2);
  fChain->SetBranchAddress("nHits3", &nHits3, &b_nHits3);
  fChain->SetBranchAddress("nHits4", &nHits4, &b_nHits4);
  fChain->SetBranchAddress("nHits5", &nHits5, &b_nHits5);
  fChain->SetBranchAddress("nHits6", &nHits6, &b_nHits6);
  fChain->SetBranchAddress("nHits7", &nHits7, &b_nHits7);
  fChain->SetBranchAddress("nHits8", &nHits8, &b_nHits8);
  fChain->SetBranchAddress("nHits9", &nHits9, &b_nHits9);
  fChain->SetBranchAddress("nHits10", &nHits10, &b_nHits10);
  fChain->SetBranchAddress("nHits11", &nHits11, &b_nHits11);
  fChain->SetBranchAddress("nCluster0", &nCluster0, &b_nCluster0);
  fChain->SetBranchAddress("nCluster1", &nCluster1, &b_nCluster1);
  fChain->SetBranchAddress("nCluster2", &nCluster2, &b_nCluster2);
  fChain->SetBranchAddress("nCluster3", &nCluster3, &b_nCluster3);
  fChain->SetBranchAddress("nCluster4", &nCluster4, &b_nCluster4);
  fChain->SetBranchAddress("nCluster5", &nCluster5, &b_nCluster5);
  fChain->SetBranchAddress("nCluster6", &nCluster6, &b_nCluster6);
  fChain->SetBranchAddress("nCluster7", &nCluster7, &b_nCluster7);
  fChain->SetBranchAddress("nCluster8", &nCluster8, &b_nCluster8);
  fChain->SetBranchAddress("nCluster9", &nCluster9, &b_nCluster9);
  fChain->SetBranchAddress("nCluster10", &nCluster10, &b_nCluster10);
  fChain->SetBranchAddress("nCluster11", &nCluster11, &b_nCluster11);
  fChain->SetBranchAddress("clusterShape0", &clusterShape0, &b_clusterShape0);
  fChain->SetBranchAddress("clusterShape1", &clusterShape1, &b_clusterShape1);
  fChain->SetBranchAddress("clusterShape2", &clusterShape2, &b_clusterShape2);
  fChain->SetBranchAddress("clusterShape3", &clusterShape3, &b_clusterShape3);
  fChain->SetBranchAddress("clusterShape4", &clusterShape4, &b_clusterShape4);
  fChain->SetBranchAddress("clusterShape5", &clusterShape5, &b_clusterShape5);
  fChain->SetBranchAddress("clusterShape6", &clusterShape6, &b_clusterShape6);
  fChain->SetBranchAddress("clusterShape7", &clusterShape7, &b_clusterShape7);
  fChain->SetBranchAddress("clusterShape8", &clusterShape8, &b_clusterShape8);
  fChain->SetBranchAddress("clusterShape9", &clusterShape9, &b_clusterShape9);
  fChain->SetBranchAddress("clusterShape10", &clusterShape10, &b_clusterShape10);
  fChain->SetBranchAddress("clusterShape11", &clusterShape11, &b_clusterShape11);
  fChain->SetBranchAddress("xerr0", &xerr0, &b_xerr0);
  fChain->SetBranchAddress("xerr1", &xerr1, &b_xerr1);
  fChain->SetBranchAddress("xerr2", &xerr2, &b_xerr2);
  fChain->SetBranchAddress("xerr3", &xerr3, &b_xerr3);
  fChain->SetBranchAddress("xerr4", &xerr4, &b_xerr4);
  fChain->SetBranchAddress("xerr5", &xerr5, &b_xerr5);
  fChain->SetBranchAddress("xerr6", &xerr6, &b_xerr6);
  fChain->SetBranchAddress("xerr7", &xerr7, &b_xerr7);
  fChain->SetBranchAddress("xerr8", &xerr8, &b_xerr8);
  fChain->SetBranchAddress("xerr9", &xerr9, &b_xerr9);
  fChain->SetBranchAddress("xerr10", &xerr10, &b_xerr10);
  fChain->SetBranchAddress("xerr11", &xerr11, &b_xerr11);
  fChain->SetBranchAddress("yerr0", &yerr0, &b_yerr0);
  fChain->SetBranchAddress("yerr1", &yerr1, &b_yerr1);
  fChain->SetBranchAddress("yerr2", &yerr2, &b_yerr2);
  fChain->SetBranchAddress("yerr3", &yerr3, &b_yerr3);
  fChain->SetBranchAddress("yerr4", &yerr4, &b_yerr4);
  fChain->SetBranchAddress("yerr5", &yerr5, &b_yerr5);
  fChain->SetBranchAddress("yerr6", &yerr6, &b_yerr6);
  fChain->SetBranchAddress("yerr7", &yerr7, &b_yerr7);
  fChain->SetBranchAddress("yerr8", &yerr8, &b_yerr8);
  fChain->SetBranchAddress("yerr9", &yerr9, &b_yerr9);
  fChain->SetBranchAddress("yerr10", &yerr10, &b_yerr10);
  fChain->SetBranchAddress("yerr11", &yerr11, &b_yerr11);
  fChain->SetBranchAddress("hitflag", &hitflag, &b_flag);
  fChain->SetBranchAddress("spill", &spill, &b_spill);
  Notify();
}

Bool_t TestBeamFullPix::Notify() {
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void TestBeamFullPix::Show(Long64_t entry) {
// Print contents of entry.
// If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
#endif // #ifdef TestBeamFullPix_cxx
