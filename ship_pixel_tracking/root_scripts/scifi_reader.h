//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Feb 13 18:45:17 2019 by ROOT version 6.14/02
// from TTree data/data tree
// found on file: RUN_0900_02793.root
//////////////////////////////////////////////////////////

#ifndef Reader_h
#define Reader_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class Reader {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<unsigned int> *BoardID;
   vector<unsigned int> *STiCID;
   vector<unsigned int> *Ch;
   vector<unsigned int> *TOT;
   vector<unsigned long> *HitTime;
   Int_t           Eventnum;
   vector<unsigned long> *TriggerID;
   vector<unsigned int> *FineTime;
   vector<unsigned long> *TriggerT;
   UInt_t          Spillnum;

   // List of branches
   TBranch        *b_BoardID;   //!
   TBranch        *b_STiCID;   //!
   TBranch        *b_Ch;   //!
   TBranch        *b_TOT;   //!
   TBranch        *b_HitTime;   //!
   TBranch        *b_Eventnum;   //!
   TBranch        *b_TriggerID;   //!
   TBranch        *b_FineTime;   //!
   TBranch        *b_TriggerT;   //!
   TBranch        *b_Spillnum;   //!

   Reader(TTree *tree=0);
   virtual ~Reader();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Reader_cxx
Reader::Reader(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      // TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("RUN_0900_02793.root");
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/media/niko/big_data/charm_testbeam_july18/scifi_offline/run_2827/RUN_0900_02827_1F250B06.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/media/niko/big_data/charm_testbeam_july18/scifi_offline/run_2827/RUN_0900_02827_1F250B06.root");
      }
      f->GetObject("data",tree);

   }
   Init(tree);
}

Reader::~Reader()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Reader::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Reader::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Reader::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   BoardID = 0;
   STiCID = 0;
   Ch = 0;
   TOT = 0;
   HitTime = 0;
   TriggerID = 0;
   FineTime = 0;
   TriggerT = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("BoardID", &BoardID, &b_BoardID);
   fChain->SetBranchAddress("STiCID", &STiCID, &b_STiCID);
   fChain->SetBranchAddress("Ch", &Ch, &b_Ch);
   fChain->SetBranchAddress("TOT", &TOT, &b_TOT);
   fChain->SetBranchAddress("HitTime", &HitTime, &b_HitTime);
   fChain->SetBranchAddress("Eventnum", &Eventnum, &b_Eventnum);
   fChain->SetBranchAddress("TriggerID", &TriggerID, &b_TriggerID);
   fChain->SetBranchAddress("FineTime", &FineTime, &b_FineTime);
   fChain->SetBranchAddress("TriggerT", &TriggerT, &b_TriggerT);
   fChain->SetBranchAddress("Spillnum", &Spillnum, &b_Spillnum);
   Notify();
}

Bool_t Reader::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Reader::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Reader::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
void Position(int ch_layer, int layer, double &x, double &y, double &z);
#endif // #ifdef Reader_cxx
