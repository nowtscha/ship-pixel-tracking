#define EventDisplay_cxx
#include "EventDisplay.h"
#include <TH2.h>
#include <TH3.h>
#include <TPolyMarker3D.h>
#include <TPolyMarker.h>
#include <TPolyLine3D.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <iostream>

const float cm=1.;
const float mm=0.1;
const float mkm=0.0001;

void EventDisplay::Loop(Long64_t ev, bool useSequentialEventIndex=false, bool draw2dTracks=true)
{
   if (fChain == 0) return;
   Long64_t nentries = fChain->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;

   const float range[3][2]= {-2.825, 1.425, -2.125, 2.125, -11., 15.};
   const TString axisTitle[3] = {"x[cm]", "y[cm]", "z[cm]"};

   TPolyMarker3D *pm_hit_use[12];
   TPolyMarker3D *pm_hit_unu[12];
   TPolyLine3D *tr[1000];
   TLine *tr_2d[3][1000];

   gStyle->SetTitleSize(0.03, "xyz"); 
   gStyle->SetTitleOffset(2.5, "xyz"); 
   gStyle->SetLabelSize(0.03, "xyz");

   TH3D *h4 = new TH3D("h4","Pixel Event View;x[cm]; z[cm]; y[cm]", 2, range[0][0], range[0][1],
                                                                    2, range[2][0], range[2][1],
                                                                    2, range[1][0], range[1][1]);
   h4->GetXaxis()->CenterTitle();
   h4->GetYaxis()->CenterTitle();
   h4->GetZaxis()->CenterTitle();
   TH2D *h2[3];
   for (unsigned i=0; i<3; i++) {
     int ixa(2), iya(1);
     if (i==2) iya=0;
     if (i==0) ixa=0;
     if (draw2dTracks) {
       h2[i] = new TH2D(Form("h2%d",i),Form("h2%d; %s; %s",i,axisTitle[ixa].Data(),axisTitle[iya].Data()),
                        1000, range[ixa][0], range[ixa][1], 1000, range[iya][0], range[iya][1]);
       h2[i]->GetXaxis()->CenterTitle();
       h2[i]->GetYaxis()->CenterTitle();
     }
   }

//   int col[6]={kBlack, kBlue, kMagenta, kRed, kCyan, kGreen+2};
   int col[6]={kCyan+2, kBlue+2, kMagenta+2, kRed+2, kYellow+2, kGreen+2};
   const float  z[12]= {-0.13, 0.520, 2.412, 3.09, 5.10, 5.79, 7.79, 8.46, 10.462, 11.170, 13.162, 13.85};
   const float xu[12]= {-2.38,-.6082,-.6893,-2.7041,-2.3819,-.6171,-2.6864,-2.69703,-2.4142,-.6227,-2.6865,-2.6751};
   const float xl[12]= {-.70,1.067,1.2969,1.3016,-.7065,1.0675,1.3195,1.31128,-.7388,1.0544,1.3215,1.3253};
   const float yu[12]= {-2.02,-2.02,-1.6536,.0363,-1.999,-1.9801,-1.6199,.02687,-2.0014,-1.989,-1.7054,.0501};
   const float yl[12]= {2.02,2.02,.0318,1.7237,2.003,2.0243,.06913,1.7222,1.9989,1.9944,-.0112,1.6294};

   TCanvas *c1 = new TCanvas("c1", "c1",0,0,1000,1000);
   if (draw2dTracks) c1->Divide(2,2);
   gStyle->SetHistTopMargin(0);
   TPad *p0 = (TPad*)c1->cd(1); p0->SetTheta(20.); p0->SetPhi(-45.);
//   TPad *p1 = (TPad*)c1->cd(2); p1->SetTheta(0.01); p1->SetPhi(0.01);
//   TPad *p2 = (TPad*)c1->cd(3); p2->SetTheta(0.01); p2->SetPhi(-90.01);
//   TPad *p3 = (TPad*)c1->cd(4); p3->SetTheta(89.99); p3->SetPhi(-89.99);

   Long64_t start_loop, stop_loop;
   start_loop = (useSequentialEventIndex) ? ev : 0;
   stop_loop = (useSequentialEventIndex) ? ev+1 : nentries;
//   for (Long64_t jentry=0; jentry<nentries; jentry++) {
//   for (Long64_t jentry=ev; jentry<ev+1; jentry++) {
   for (Long64_t jentry=start_loop; jentry<stop_loop; jentry++) {

      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;

      if (!useSequentialEventIndex && (evnum != ev)) continue;
      std::cout << "Displaying ntuple entry " << jentry << ", with event number " << evnum << " in spill " << pspill << std::endl;

      const unsigned sz = hitx->size();
      const unsigned nTracks = x->size();

      cout << "Processing event: " << ev << " with " << sz << " hits and " << nTracks << " tracks" << endl;
//      cout << "Processing event: " << evnum << " with " << sz << " hits and " << nTracks << " tracks" << endl;

      for (int i=0; i<12; i++) {
         pm_hit_use[i] = new TPolyMarker3D(); pm_hit_use[i]->SetMarkerSize(0.75); pm_hit_use[i]->SetMarkerColor(col[i/2]); pm_hit_use[i]->SetMarkerStyle(20+i%2);
         pm_hit_unu[i] = new TPolyMarker3D(); pm_hit_unu[i]->SetMarkerSize(0.5); pm_hit_unu[i]->SetMarkerColor(col[i/2]); pm_hit_unu[i]->SetMarkerStyle(24+i%2);
      }
      float f[] = {range[2][0], 15., 15.5};
      float dx,dy;
//      float trackx,tracky;
      for (unsigned i = 0; i < sz; i++) {
         float dd = 0.02;
         int ip = hitplane->at(i);
         float x(hitx->at(i)), y(hity->at(i)), z(hitz->at(i));
         if (hitused->at(i)) {
            switch (clusterSize->at(i)) {
               case 1: 
                 pm_hit_use[ip]->SetNextPoint(x, z, y);
                 break;
               case 2: 
                 if ((ip/2)%2 == 0) {
                    pm_hit_use[ip]->SetNextPoint(x, z, y+dd/2.);
                    pm_hit_use[ip]->SetNextPoint(x, z, y-dd/2.);
                 } else {
                    pm_hit_use[ip]->SetNextPoint(x+dd/2., z, y);
                    pm_hit_use[ip]->SetNextPoint(x-dd/2., z, y);
                 } 
                 break;
               case 3: 
                 if ((ip/2)%2 == 0) {
                    pm_hit_use[ip]->SetNextPoint(x, z, y+dd);
                    pm_hit_use[ip]->SetNextPoint(x, z, y);
                    pm_hit_use[ip]->SetNextPoint(x, z, y-dd);
                 } else {
                    pm_hit_use[ip]->SetNextPoint(x+dd, z, y);
                    pm_hit_use[ip]->SetNextPoint(x, z, y);
                    pm_hit_use[ip]->SetNextPoint(x-dd, z, y);
                 } 
                 break;
               case 4:
                 if ((ip/2)%2 == 0) {
                    pm_hit_use[ip]->SetNextPoint(x, z, y+1.5*dd);
                    pm_hit_use[ip]->SetNextPoint(x, z, y+.5*dd);
                    pm_hit_use[ip]->SetNextPoint(x, z, y-.5*dd);
                    pm_hit_use[ip]->SetNextPoint(x, z, y-1.5*dd);
                 } else {
                    pm_hit_use[ip]->SetNextPoint(x+1.5*dd, z, y);
                    pm_hit_use[ip]->SetNextPoint(x+0.5*dd, z, y);
                    pm_hit_use[ip]->SetNextPoint(x-0.5*dd, z, y);
                    pm_hit_use[ip]->SetNextPoint(x-1.5*dd, z, y);
                 } 
                 break;
               default:
                 for (unsigned k=0; k<5; k++) {
                    for (unsigned j=0; j<5; j++) {
                        pm_hit_use[ip]->SetNextPoint(x+k*dd/2., z, y+j*dd/2.);
                        pm_hit_use[ip]->SetNextPoint(x+k*dd/2., z, y-j*dd/2.);
                        pm_hit_use[ip]->SetNextPoint(x-k*dd/2., z, y+j*dd/2.);
                        pm_hit_use[ip]->SetNextPoint(x-k*dd/2., z, y-j*dd/2.);
                    } 
                 }                
            }
         } else pm_hit_unu[ip]->SetNextPoint(x, z, y);
      }

      for (unsigned itr = 0; itr < nTracks; itr++) {
        tr[itr] = new TPolyLine3D(2);
        for (int i=0; i<2; i++) tr[itr]->SetPoint(i, x->at(itr)+f[i]*tx->at(itr), f[i], y->at(itr)+f[i]*ty->at(itr));
        if (draw2dTracks) {
          for (unsigned i = 0; i < sz; i++) {
  //          trackx = x->at(itr) + z[i]*tx->at(itr);
  //          tracky = y->at(itr) + z[i]*ty ->at(itr);
  //          dx = trackx - hitx->at(i);
  //          dy = tracky - hity->at(i);
            dx = x->at(itr) + z[i]*tx->at(itr) - hitx->at(i);
            dy = y->at(itr) + z[i]*ty->at(itr) - hity->at(i);
            printf("Event %lld hit %d plane %d used %d x=%8.5f y=%8.5f z=%8.5f dx=%8.5f um dy=%8.5f um   \n", ev, i, hitplane->at(i), (int)hitused->at(i),
                    hitx->at(i), hity->at(i), hitz->at(i), dx/mkm, dy/mkm);
          }
        }
      }
      for (unsigned i = 0; i < vx->size(); i++) {
        cout << "Vertex " << i << "vx = " << vx->at(i) << " vy = " << vy->at(i) << " vz = " << vz->at(i) << " vdist = " << vdist->at(i) << endl;
      }
//      for (unsigned long i = 0; i < x->size(); i++) cout << i << " " << x->at(i) << endl;
      // float f[] = {-0.5, 15.};
      //   for (unsigned itr = 0; itr < nTracks; itr++) {
      //   tr[itr] = new TPolyLine3D(2);
      //   for (int i=0; i<2; i++) tr[itr]->SetPoint(i, x->at(itr)+f[i]*tx->at(itr), f[i], y->at(itr)+f[i]*ty->at(itr));
      // }

      TPolyMarker *pu[3][12];
      for (unsigned jj=0; jj<12; jj++) {
        int n = pm_hit_use[jj]->GetN();
        for (unsigned k=0; k<3; k++) {
          pu[k][jj] = new TPolyMarker(n);
          pu[k][jj]->SetMarkerSize(0.2);
        }
        for (unsigned i=0; i<n; i++) {
          float x, y, z;
          pm_hit_use[jj]->GetPoint(i, x, z, y);
          pu[0][jj]->SetPoint(i,x,y);
          pu[1][jj]->SetPoint(i,z,y);
          pu[2][jj]->SetPoint(i,z,x);
        }
      } 

      for (unsigned itr = 0; itr < nTracks; itr++) {
        float x0, y0, z0, x1, y1, z1;
        z0 = -10.5;
        z1 = 14.5;
        x0 = tx->at(itr)*z0 + x->at(itr);
        x1 = tx->at(itr)*z1 + x->at(itr);
        y0 = ty->at(itr)*z0 + y->at(itr);
        y1 = ty->at(itr)*z1 + y->at(itr);
        if (draw2dTracks) {
          tr_2d[0][itr] = new TLine(x0,y0,x1,y1);
          tr_2d[1][itr] = new TLine(z0,y0,z1,y1);
          tr_2d[2][itr] = new TLine(z0,x0,z1,x1);
          for (unsigned i = 0; i<3; i++) {
             tr_2d[i][itr]->SetLineColor(kGreen+2);
           }
        }
      }

      cout << "Finished filling all stuff" << endl;
      cout << "Now plotting" << endl;

      for (unsigned kk=1; kk<5; kk++) {
         int k = kk - 2;
         c1->cd(kk);
         if (kk == 1) h4->Draw(); // Draw box
         else {
           if (draw2dTracks) h2[k]->Draw();
         }

// Draw detectors
         if (kk == 1) {
            TPolyLine3D *pix[24];
            for (unsigned i = 0; i < 12; i++) {
               pix[i] = new TPolyLine3D(5);
               pix[i]->SetPoint(0, xu[i], z[i], yu[i]);
               pix[i]->SetPoint(1, xu[i], z[i], yl[i]);
               pix[i]->SetPoint(2, xl[i], z[i], yl[i]);
               pix[i]->SetPoint(3, xl[i], z[i], yu[i]);
               pix[i]->SetPoint(4, xu[i], z[i], yu[i]);
               pix[i]->SetLineColor(col[i/2]);
               pix[i]->SetLineWidth(1);
               pix[i]->SetLineStyle(4);
               pix[i]->Draw("same");
            }
         } 

// Draw Tracks;
         TLatex *la[nTracks];
         for (unsigned itr = 0; itr < nTracks; itr++) { 
            if (kk == 1) { 
              tr[itr]->Draw("same"); // Draw all reconstructed tracks
            } else {
              if (draw2dTracks) tr_2d[k][itr]->Draw("same"); // Draw all reconstructed tracks
            }
            float ff, centered(0.);
            if (k==1) { 
//              ff = -(x->at(itr)+f[2]*tx->at(itr));
              ff = y->at(itr)+f[2]*ty->at(itr);
//              centered = 0.045*(h4->GetXaxis()->GetXmax() - h4->GetXaxis()->GetXmin());
            } else if (k==2) {
//              ff = y->at(itr)+f[2]*ty->at(itr);
              ff = x->at(itr)+f[2]*tx->at(itr);
            }
//            la[itr] = new TLatex(.64, -centered+0.265*ff, Form("%3d",itr));
            la[itr] = new TLatex(f[2], ff, Form("%3d",itr));
            la[itr]->SetTextSize(0.017);
            la[itr]->SetTextAlign(32);
            la[itr]->Draw();
         }

// Draw Hits;
         for (int i = 11; i > -1; i--) {
            if (kk == 1) {
              pm_hit_use[i]->Draw("same"); // Draw all used pixel hits
              pm_hit_unu[i]->Draw("same"); // Draw all unused pixel hits
              cout << (*pm_hit_use[i]).Size() << " used and " << (*pm_hit_unu[i]).Size() <<" unused hits in plane " << i << endl ;
              cout << "--------------------------------" << endl ;
            } else {
                if (draw2dTracks) pu[k][i]->Draw("same");
            }
         }


      }  
   }
}
