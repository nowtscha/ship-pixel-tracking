#define VrtEmulsion_cxx
#include "VrtEmulsion.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>


struct EmulVrt{
 float VX;
 float VY;
 float VZ=0.;
 int ntrkraw=0;
 std::vector<EmulTrk> TrkInVrt;
};
std::vector< EmulVrt > VrtEmulRun;

void VrtEmulsion::Loop()
{
//   In a ROOT session, you can do:
//      root> .L VrtEmulsion.C
//      root> VrtEmulsion t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   double cs=cos(0.);
   double sn=sin(0.);
   EmulTrk EMULT;
   EmulVrt VERTEX;
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if ((jentry%10000) == 0) printf("Processing event %10lld\n", jentry);
      if (ientry < 0) break;
      //nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
      nb = b_ntracks->GetEntry(ientry);   nbytes += nb;
      if(ntracks<35) continue;
      if(ntracks>45) continue;
      nb = b_volumetrack__eProb->GetEntry(ientry);  nbytes += nb;
      nb = b_volumetrack__eTX->GetEntry(ientry);    nbytes += nb;
      nb = b_volumetrack__eTY->GetEntry(ientry);    nbytes += nb;
      nb = b_volumetrack__eX->GetEntry(ientry);     nbytes += nb;
      nb = b_volumetrack__eY->GetEntry(ientry);     nbytes += nb;
      nb = b_volumetrack__eZ->GetEntry(ientry);     nbytes += nb;
      nb = b_vx->GetEntry(ientry);     nbytes += nb;
      nb = b_vy->GetEntry(ientry);     nbytes += nb;
      nb = b_vz->GetEntry(ientry);     nbytes += nb;
      nb = b_segment_->GetEntry(ientry);       nbytes += nb;
      nb = b_segment_eProb->GetEntry(ientry);  nbytes += nb;
      nb = b_segment_eTX->GetEntry(ientry);    nbytes += nb;
      nb = b_segment_eTY->GetEntry(ientry);    nbytes += nb;
      nb = b_segment_eX->GetEntry(ientry);     nbytes += nb;
      nb = b_segment_eY->GetEntry(ientry);     nbytes += nb;
      nb = b_segment_eZ->GetEntry(ientry);     nbytes += nb;
      VERTEX.VX=vx;
      VERTEX.VY=vy;
      VERTEX.VZ=vz;
      VERTEX.ntrkraw=ntracks;
      VERTEX.TrkInVrt.clear();
      /*/////---
      for(int it=0; it<ntracks; it++){
        if(volumetrack__eProb[it]<0.05)continue;
        if(volumetrack__eZ[it]<0.)continue;
        EMULT.X  =  volumetrack__eX[it];
        EMULT.Y  =  volumetrack__eY[it];
        double PX = (volumetrack__eTX[it]+0.0023);
        double PY = (volumetrack__eTY[it]-0.0059);
        EMULT.PX=PX*cs-PY*sn;
        EMULT.PY=PX*sn+PY*cs;
        VERTEX.TrkInVrt.push_back(EMULT);
      }
      */////---
      for(int it=0; it<segment_; it++){
        if(segment_eProb[it]<0.05)continue;
        if(segment_eZ[it]<0.)continue;
        EMULT.X  =  segment_eX[it];
        EMULT.Y  =  segment_eY[it];
        double PX = (segment_eTX[it]+0.0023);
        double PY = (segment_eTY[it]-0.0059);
        if(fabs(PX)>0.15)continue;
        if(fabs(PY)>0.15)continue;
        EMULT.PX=PX*cs-PY*sn;
        EMULT.PY=PX*sn+PY*cs;
        VERTEX.TrkInVrt.push_back(EMULT);
      }
      /////----
      if(VERTEX.TrkInVrt.size()<22)continue;
      VrtEmulRun.push_back(VERTEX);

   }
   for(auto &ir:VrtEmulRun)std::cout<<ir.TrkInVrt.size()<<" ntraw="<<ir.ntrkraw
   <<" z="<<ir.VZ<<" y="<<ir.VY<<" x="<<ir.VX<<'\n';
}
