
#ifndef VectorsToTreeCharmx_h
#define VectorsToTreeCharmx_h
#endif

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include<TMatrixDSym.h>
#include "TTree.h"
#include <iostream>
#include <vector>
#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ class std::vector<std::vector<int> >+;
#endif


// Header file for the classes stored in the TTree if any.

class TestBeam {

public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Long64_t         event_number;
   Long64_t         trigger_time_stamp;
   Double_t         x_dut_0;
   Double_t         x_dut_1;
   Double_t         x_dut_2;
   Double_t         x_dut_3;
   Double_t         x_dut_4;
   Double_t         x_dut_5;
   Double_t         x_dut_6;
   Double_t         x_dut_7;
   Double_t         x_dut_8;
   Double_t         x_dut_9;
   Double_t         x_dut_10;
   Double_t         x_dut_11;
   Double_t         y_dut_0;
   Double_t         y_dut_1;
   Double_t         y_dut_2;
   Double_t         y_dut_3;
   Double_t         y_dut_4;
   Double_t         y_dut_5;
   Double_t         y_dut_6;
   Double_t         y_dut_7;
   Double_t         y_dut_8;
   Double_t         y_dut_9;
   Double_t         y_dut_10;
   Double_t         y_dut_11;
   Double_t         z_dut_0;
   Double_t         z_dut_1;
   Double_t         z_dut_2;
   Double_t         z_dut_3;
   Double_t         z_dut_4;
   Double_t         z_dut_5;
   Double_t         z_dut_6;
   Double_t         z_dut_7;
   Double_t         z_dut_8;
   Double_t         z_dut_9;
   Double_t         z_dut_10;
   Double_t         z_dut_11;
   Float_t         charge_dut_0;
   Float_t         charge_dut_1;
   Float_t         charge_dut_2;
   Float_t         charge_dut_3;
   Float_t         charge_dut_4;
   Float_t         charge_dut_5;
   Float_t         charge_dut_6;
   Float_t         charge_dut_7;
   Float_t         charge_dut_8;
   Float_t         charge_dut_9;
   Float_t         charge_dut_10;
   Float_t         charge_dut_11;
   UInt_t          n_hits_dut_0;
   UInt_t          n_hits_dut_1;
   UInt_t          n_hits_dut_2;
   UInt_t          n_hits_dut_3;
   UInt_t          n_hits_dut_4;
   UInt_t          n_hits_dut_5;
   UInt_t          n_hits_dut_6;
   UInt_t          n_hits_dut_7;
   UInt_t          n_hits_dut_8;
   UInt_t          n_hits_dut_9;
   UInt_t          n_hits_dut_10;
   UInt_t          n_hits_dut_11;
   Short_t         cluster_ID_dut_0;
   Short_t         cluster_ID_dut_1;
   Short_t         cluster_ID_dut_2;
   Short_t         cluster_ID_dut_3;
   Short_t         cluster_ID_dut_4;
   Short_t         cluster_ID_dut_5;
   Short_t         cluster_ID_dut_6;
   Short_t         cluster_ID_dut_7;
   Short_t         cluster_ID_dut_8;
   Short_t         cluster_ID_dut_9;
   Short_t         cluster_ID_dut_10;
   Short_t         cluster_ID_dut_11;
   Long64_t        cluster_shape_dut_0;
   Long64_t        cluster_shape_dut_1;
   Long64_t        cluster_shape_dut_2;
   Long64_t        cluster_shape_dut_3;
   Long64_t        cluster_shape_dut_4;
   Long64_t        cluster_shape_dut_5;
   Long64_t        cluster_shape_dut_6;
   Long64_t        cluster_shape_dut_7;
   Long64_t        cluster_shape_dut_8;
   Long64_t        cluster_shape_dut_9;
   Long64_t        cluster_shape_dut_10;
   Long64_t        cluster_shape_dut_11;
   UInt_t          n_cluster_dut_0;
   UInt_t          n_cluster_dut_1;
   UInt_t          n_cluster_dut_2;
   UInt_t          n_cluster_dut_3;
   UInt_t          n_cluster_dut_4;
   UInt_t          n_cluster_dut_5;
   UInt_t          n_cluster_dut_6;
   UInt_t          n_cluster_dut_7;
   UInt_t          n_cluster_dut_8;
   UInt_t          n_cluster_dut_9;
   UInt_t          n_cluster_dut_10;
   UInt_t          n_cluster_dut_11;
   UInt_t          hit_flag;
   Float_t         x_err_dut_0;
   Float_t         x_err_dut_1;
   Float_t         x_err_dut_2;
   Float_t         x_err_dut_3;
   Float_t         x_err_dut_4;
   Float_t         x_err_dut_5;
   Float_t         x_err_dut_6;
   Float_t         x_err_dut_7;
   Float_t         x_err_dut_8;
   Float_t         x_err_dut_9;
   Float_t         x_err_dut_10;
   Float_t         x_err_dut_11;
   Float_t         y_err_dut_0;
   Float_t         y_err_dut_1;
   Float_t         y_err_dut_2;
   Float_t         y_err_dut_3;
   Float_t         y_err_dut_4;
   Float_t         y_err_dut_5;
   Float_t         y_err_dut_6;
   Float_t         y_err_dut_7;
   Float_t         y_err_dut_8;
   Float_t         y_err_dut_9;
   Float_t         y_err_dut_10;
   Float_t         y_err_dut_11;
   Float_t         z_err_dut_0;
   Float_t         z_err_dut_1;
   Float_t         z_err_dut_2;
   Float_t         z_err_dut_3;
   Float_t         z_err_dut_4;
   Float_t         z_err_dut_5;
   Float_t         z_err_dut_6;
   Float_t         z_err_dut_7;
   Float_t         z_err_dut_8;
   Float_t         z_err_dut_9;
   Float_t         z_err_dut_10;
   Float_t         z_err_dut_11;


   // List of branches
   TBranch        *b_event_number;   //!
   TBranch        *b_trigger_time_stamp;   //!
   TBranch        *b_x_dut_0;   //!
   TBranch        *b_x_dut_1;   //!
   TBranch        *b_x_dut_2;   //!
   TBranch        *b_x_dut_3;   //!
   TBranch        *b_x_dut_4;   //!
   TBranch        *b_x_dut_5;   //!
   TBranch        *b_x_dut_6;   //!
   TBranch        *b_x_dut_7;   //!
   TBranch        *b_x_dut_8;   //!
   TBranch        *b_x_dut_9;   //!
   TBranch        *b_x_dut_10;   //!
   TBranch        *b_x_dut_11;   //!
   TBranch        *b_y_dut_0;   //!
   TBranch        *b_y_dut_1;   //!
   TBranch        *b_y_dut_2;   //!
   TBranch        *b_y_dut_3;   //!
   TBranch        *b_y_dut_4;   //!
   TBranch        *b_y_dut_5;   //!
   TBranch        *b_y_dut_6;   //!
   TBranch        *b_y_dut_7;   //!
   TBranch        *b_y_dut_8;   //!
   TBranch        *b_y_dut_9;   //!
   TBranch        *b_y_dut_10;   //!
   TBranch        *b_y_dut_11;   //!
   TBranch        *b_z_dut_0;   //!
   TBranch        *b_z_dut_1;   //!
   TBranch        *b_z_dut_2;   //!
   TBranch        *b_z_dut_3;   //!
   TBranch        *b_z_dut_4;   //!
   TBranch        *b_z_dut_5;   //!
   TBranch        *b_z_dut_6;   //!
   TBranch        *b_z_dut_7;   //!
   TBranch        *b_z_dut_8;   //!
   TBranch        *b_z_dut_9;   //!
   TBranch        *b_z_dut_10;   //!
   TBranch        *b_z_dut_11;   //!
   TBranch        *b_charge_dut_0;   //!
   TBranch        *b_charge_dut_1;   //!
   TBranch        *b_charge_dut_2;   //!
   TBranch        *b_charge_dut_3;   //!
   TBranch        *b_charge_dut_4;   //!
   TBranch        *b_charge_dut_5;   //!
   TBranch        *b_charge_dut_6;   //!
   TBranch        *b_charge_dut_7;   //!
   TBranch        *b_charge_dut_8;   //!
   TBranch        *b_charge_dut_9;   //!
   TBranch        *b_charge_dut_10;   //!
   TBranch        *b_charge_dut_11;   //!
   TBranch        *b_n_hits_dut_0;   //!
   TBranch        *b_n_hits_dut_1;   //!
   TBranch        *b_n_hits_dut_2;   //!
   TBranch        *b_n_hits_dut_3;   //!
   TBranch        *b_n_hits_dut_4;   //!
   TBranch        *b_n_hits_dut_5;   //!
   TBranch        *b_n_hits_dut_6;   //!
   TBranch        *b_n_hits_dut_7;   //!
   TBranch        *b_n_hits_dut_8;   //!
   TBranch        *b_n_hits_dut_9;   //!
   TBranch        *b_n_hits_dut_10;   //!
   TBranch        *b_n_hits_dut_11;   //!
   TBranch        *b_cluster_ID_dut_0;   //!
   TBranch        *b_cluster_ID_dut_1;   //!
   TBranch        *b_cluster_ID_dut_2;   //!
   TBranch        *b_cluster_ID_dut_3;   //!
   TBranch        *b_cluster_ID_dut_4;   //!
   TBranch        *b_cluster_ID_dut_5;   //!
   TBranch        *b_cluster_ID_dut_6;   //!
   TBranch        *b_cluster_ID_dut_7;   //!
   TBranch        *b_cluster_ID_dut_8;   //!
   TBranch        *b_cluster_ID_dut_9;   //!
   TBranch        *b_cluster_ID_dut_10;   //!
   TBranch        *b_cluster_ID_dut_11;   //!
   TBranch        *b_cluster_shape_dut_0;   //!
   TBranch        *b_cluster_shape_dut_1;   //!
   TBranch        *b_cluster_shape_dut_2;   //!
   TBranch        *b_cluster_shape_dut_3;   //!
   TBranch        *b_cluster_shape_dut_4;   //!
   TBranch        *b_cluster_shape_dut_5;   //!
   TBranch        *b_cluster_shape_dut_6;   //!
   TBranch        *b_cluster_shape_dut_7;   //!
   TBranch        *b_cluster_shape_dut_8;   //!
   TBranch        *b_cluster_shape_dut_9;   //!
   TBranch        *b_cluster_shape_dut_10;   //!
   TBranch        *b_cluster_shape_dut_11;   //!
   TBranch        *b_n_cluster_dut_0;   //!
   TBranch        *b_n_cluster_dut_1;   //!
   TBranch        *b_n_cluster_dut_2;   //!
   TBranch        *b_n_cluster_dut_3;   //!
   TBranch        *b_n_cluster_dut_4;   //!
   TBranch        *b_n_cluster_dut_5;   //!
   TBranch        *b_n_cluster_dut_6;   //!
   TBranch        *b_n_cluster_dut_7;   //!
   TBranch        *b_n_cluster_dut_8;   //!
   TBranch        *b_n_cluster_dut_9;   //!
   TBranch        *b_n_cluster_dut_10;   //!
   TBranch        *b_n_cluster_dut_11;   //!
   TBranch        *b_hit_flag;   //!
   TBranch        *b_x_err_dut_0;   //!
   TBranch        *b_x_err_dut_1;   //!
   TBranch        *b_x_err_dut_2;   //!
   TBranch        *b_x_err_dut_3;   //!
   TBranch        *b_x_err_dut_4;   //!
   TBranch        *b_x_err_dut_5;   //!
   TBranch        *b_x_err_dut_6;   //!
   TBranch        *b_x_err_dut_7;   //!
   TBranch        *b_x_err_dut_8;   //!
   TBranch        *b_x_err_dut_9;   //!
   TBranch        *b_x_err_dut_10;   //!
   TBranch        *b_x_err_dut_11;   //!
   TBranch        *b_y_err_dut_0;   //!
   TBranch        *b_y_err_dut_1;   //!
   TBranch        *b_y_err_dut_2;   //!
   TBranch        *b_y_err_dut_3;   //!
   TBranch        *b_y_err_dut_4;   //!
   TBranch        *b_y_err_dut_5;   //!
   TBranch        *b_y_err_dut_6;   //!
   TBranch        *b_y_err_dut_7;   //!
   TBranch        *b_y_err_dut_8;   //!
   TBranch        *b_y_err_dut_9;   //!
   TBranch        *b_y_err_dut_10;   //!
   TBranch        *b_y_err_dut_11;
   TBranch        *b_z_err_dut_0;   //!
   TBranch        *b_z_err_dut_1;   //!
   TBranch        *b_z_err_dut_2;   //!
   TBranch        *b_z_err_dut_3;   //!
   TBranch        *b_z_err_dut_4;   //!
   TBranch        *b_z_err_dut_5;   //!
   TBranch        *b_z_err_dut_6;   //!
   TBranch        *b_z_err_dut_7;   //!
   TBranch        *b_z_err_dut_8;   //!
   TBranch        *b_z_err_dut_9;   //!
   TBranch        *b_z_err_dut_10;   //!
   TBranch        *b_z_err_dut_11;



   TestBeam(TTree *tree=0);
   virtual ~TestBeam();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   void FillHits(int);

   struct pixHit{
     TVectorD hitXY{2};
     float planeZ{0.};
     TMatrixDSym hitCov{2};
     int planeID{0};
     int localIndex{-1};
     bool Used{false};
   };
   std::vector< std::vector< std::vector<pixHit> > > * pixFILL{nullptr};

   // struct fittedTrack{
   //   TVector3 pos;
   //   TVector3 mom;
   //   TMatrixDSym COV;
   // };
};
