
#ifndef VectorsToTreeCharmx_h
#define VectorsToTreeCharmx_h
#endif

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include<TMatrixDSym.h>
#include "TTree.h"
#include <iostream>
#include <vector>
#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ class std::vector<std::vector<int> >+;
#endif


// Header file for the classes stored in the TTree if any.

class TestBeam {

public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Long64_t   event_number_old;
   Long64_t   timestamp_old;
   Int_t      spill_old;
   Short_t    pix_trackID_old;
   Double_t   x_old;
   Double_t   y_old;
   Double_t   z_old;
   Float_t    x_slope_old;
   Float_t    y_slope_old;
   Long64_t   rpc_timestamp_old;
   Short_t    rpc_trackID_old;
   Float_t    slopexz_old;
   Float_t    slopeyz_old;
   Double_t   x1_old;
   Double_t   y1_old;
   Double_t   z1x_old;
   Double_t   z1y_old;
   Double_t   x2_old;
   Double_t   y2_old;
   Double_t   z2x_old;
   Double_t   z2y_old;
   Double_t   x3_old;
   Double_t   y3_old;
   Double_t   z3x_old;
   Double_t   z3y_old;
   Double_t   x4_old;
   Double_t   y4_old;
   Double_t   z4x_old;
   Double_t   z4y_old;
   Double_t   x5_old;
   Double_t   y5_old;
   Double_t   z5x_old;
   Double_t   z5y_old;


   // List of branches
   TBranch    *b_event_number_old;
   TBranch    *b_timestamp_old;
   TBranch    *b_pix_trackID_old;
   TBranch    *b_spill_old;
   TBranch    *b_x_old;
   TBranch    *b_y_old;
   TBranch    *b_z_old;
   TBranch    *b_x_slope_old;
   TBranch    *b_y_slope_old;
   TBranch    *b_rpc_timestamp_old;
   TBranch    *b_rpc_trackID_old;
   TBranch    *b_slopexz_old;
   TBranch    *b_slopeyz_old;
   TBranch    *b_x1_old;
   TBranch    *b_y1_old;
   TBranch    *b_z1x_old;
   TBranch    *b_z1y_old;
   TBranch    *b_x2_old;
   TBranch    *b_y2_old;
   TBranch    *b_z2x_old;
   TBranch    *b_z2y_old;
   TBranch    *b_x3_old;
   TBranch    *b_y3_old;
   TBranch    *b_z3x_old;
   TBranch    *b_z3y_old;
   TBranch    *b_x4_old;
   TBranch    *b_y4_old;
   TBranch    *b_z4x_old;
   TBranch    *b_z4y_old;
   TBranch    *b_x5_old;
   TBranch    *b_y5_old;
   TBranch    *b_z5x_old; 
   TBranch    *b_z5y_old;


   TestBeam(TTree *tree=0);
   virtual ~TestBeam();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   void FillHits(int);

};
