//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jul 10 16:18:44 2019 by ROOT version 6.15/01
// from TTree p/Pixel tracks
// found on file: data/run_2781/pix_tracks_spill_all.root
//////////////////////////////////////////////////////////

#ifndef EventDisplay_h
#define EventDisplay_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"


class EventDisplay {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Long64_t        evnum;
   ULong64_t       timestamp;
   UShort_t        pspill;
   vector<float>   *hitx;
   vector<float>   *hity;
   vector<float>   *hitz;
   vector<float>   *clusterCharge;
   vector<unsigned int> *clusterSize;
   vector<int>     *trackID;
   vector<unsigned short> *hitplane;
   vector<int>     *hitused;
   vector<float>   *x;
   vector<float>   *y;
   vector<float>   *z;
   vector<float>   *ex;
   vector<float>   *ey;
   vector<float>   *tx;
   vector<float>   *ty;
   vector<float>   *etx;
   vector<float>   *ety;
   vector<float>   *chi2;
   vector<float>   *vx;
   vector<float>   *vy;
   vector<float>   *vz;
   vector<float>   *vdist;
   vector<float>   *vchi2;
   vector<vector<int> > *vitrk;
   vector<float>   *rvx;
   vector<float>   *rvy;
   vector<float>   *rvz;
   vector<float>   *rvchi2;
   vector<float>   *rvndf;
   vector<int>     *rvid;
   vector<int>     *rvntrk;
   vector<float>   *rvminweight;

   // List of branches
   TBranch        *b_evnum;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_pspill;   //!
   TBranch        *b_hitx;   //!
   TBranch        *b_hity;   //!
   TBranch        *b_hitz;   //!
   TBranch        *b_clusterCharge;   //!
   TBranch        *b_clusterSize;   //!
   TBranch        *b_trackID;   //!
   TBranch        *b_hitplane;   //!
   TBranch        *b_hitused;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_ex;   //!
   TBranch        *b_ey;   //!
   TBranch        *b_tx;   //!
   TBranch        *b_ty;   //!
   TBranch        *b_etx;   //!
   TBranch        *b_ety;   //!
   TBranch        *b_chi2;   //!
   TBranch        *b_vx;   //!
   TBranch        *b_vy;   //!
   TBranch        *b_vz;   //!
   TBranch        *b_vdist;   //!
   TBranch        *b_vchi2;   //!
   TBranch        *b_vitrk;   //!
   TBranch        *b_rvx;   //!
   TBranch        *b_rvy;   //!
   TBranch        *b_rvz;   //!
   TBranch        *b_rvchi2;   //!
   TBranch        *b_rvndf;   //!
   TBranch        *b_rvid;   //!
   TBranch        *b_rvntrk;   //!
   TBranch        *b_rvminweight;   //!

   EventDisplay(TTree *tree=0);
   virtual ~EventDisplay();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(Long64_t ev, bool useSequentialEventIndex, bool draw2dTracks); 
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef EventDisplay_cxx
EventDisplay::EventDisplay(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data/run_2793/pix_tracks_spill_all.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data/run_2793/pix_tracks_spill_all.root");
      }
      f->GetObject("p",tree);

   }
   Init(tree);
}

EventDisplay::~EventDisplay()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t EventDisplay::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t EventDisplay::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void EventDisplay::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   hitx = 0;
   hity = 0;
   hitz = 0;
   clusterCharge = 0;
   clusterSize = 0;
   trackID = 0;
   hitplane = 0;
   hitused = 0;
   x = 0;
   y = 0;
   z = 0;
   ex = 0;
   ey = 0;
   tx = 0;
   ty = 0;
   etx = 0;
   ety = 0;
   chi2 = 0;
   vx = 0;
   vy = 0;
   vz = 0;
   vdist = 0;
   vchi2 = 0;
   vitrk = 0;
   rvx = 0;
   rvy = 0;
   rvz = 0;
   rvchi2 = 0;
   rvndf = 0;
   rvid = 0;
   rvntrk = 0;
   rvminweight = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("evnum", &evnum, &b_evnum);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("pspill", &pspill, &b_pspill);
   fChain->SetBranchAddress("hitx", &hitx, &b_hitx);
   fChain->SetBranchAddress("hity", &hity, &b_hity);
   fChain->SetBranchAddress("hitz", &hitz, &b_hitz);
   fChain->SetBranchAddress("clusterCharge", &clusterCharge, &b_clusterCharge);
   fChain->SetBranchAddress("clusterSize", &clusterSize, &b_clusterSize);
   fChain->SetBranchAddress("trackID", &trackID, &b_trackID);
   fChain->SetBranchAddress("hitplane", &hitplane, &b_hitplane);
   fChain->SetBranchAddress("hitused", &hitused, &b_hitused);
   fChain->SetBranchAddress("x", &x, &b_x);
   fChain->SetBranchAddress("y", &y, &b_y);
   fChain->SetBranchAddress("z", &z, &b_z);
   fChain->SetBranchAddress("ex", &ex, &b_ex);
   fChain->SetBranchAddress("ey", &ey, &b_ey);
   fChain->SetBranchAddress("tx", &tx, &b_tx);
   fChain->SetBranchAddress("ty", &ty, &b_ty);
   fChain->SetBranchAddress("etx", &etx, &b_etx);
   fChain->SetBranchAddress("ety", &ety, &b_ety);
   fChain->SetBranchAddress("chi2", &chi2, &b_chi2);
   fChain->SetBranchAddress("vx", &vx, &b_vx);
   fChain->SetBranchAddress("vy", &vy, &b_vy);
   fChain->SetBranchAddress("vz", &vz, &b_vz);
   fChain->SetBranchAddress("vdist", &vdist, &b_vdist);
   fChain->SetBranchAddress("vchi2", &vchi2, &b_vchi2);
   fChain->SetBranchAddress("vitrk", &vitrk, &b_vitrk);
   fChain->SetBranchAddress("rvx", &rvx, &b_rvx);
   fChain->SetBranchAddress("rvy", &rvy, &b_rvy);
   fChain->SetBranchAddress("rvz", &rvz, &b_rvz);
   fChain->SetBranchAddress("rvchi2", &rvchi2, &b_rvchi2);
   fChain->SetBranchAddress("rvndf", &rvndf, &b_rvndf);
   fChain->SetBranchAddress("rvid", &rvid, &b_rvid);
   fChain->SetBranchAddress("rvntrk", &rvntrk, &b_rvntrk);
   fChain->SetBranchAddress("rvminweight", &rvminweight, &b_rvminweight);
   Notify();
}

Bool_t EventDisplay::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void EventDisplay::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t EventDisplay::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   entry = entry + 1 - 1;
   return 1;
}
#endif // #ifdef EventDisplay_cxx
