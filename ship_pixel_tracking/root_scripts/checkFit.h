#ifndef checkFit_h
#define checkFit_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include "TEfficiency.h"
#include <TVector3.h>
#include <TMatrixDSym.h>
#include <TVectorD.h>
#include <iostream>


class SimVertex {
public :

    struct fittedTrack {
    bool Good = false;
    Long64_t timestamp{0};
    Float_t Chi2{1.e9};
    int Ndf{0};
    TVector3 pos;
    TVector3 mom;
    TMatrixDSym COV{6};
    int trackID{0};
    };

    struct MultiVtx {
        bool Good=false;
        double chi2{1.e9}; // chi2 for fit of r
        double redchi2{1.e9};
        int ndf;
        int nTracks;
        double distance; // euclidean distance in point of closest approach (POCA)
        TVector3 r; // POCA
        TMatrixD vertexCov{3,3};
        std::vector<double> chi2PerTrk;
        // TMatrixDSym COV{3}; // covariance matrix for fit of r
        std::vector<int> trackID; // trackIDs of Tracks in vertex
    };

    void Loop(Int_t nevents);
    void FitVtxandTracks(std::vector<fittedTrack> &fittedTracks, MultiVtx &vtx, TVector3 estimate, std::vector<double>& newTrackPars);

};


#endif // #ifdef checkFit_cxx


