//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Dec  7 15:37:20 2018 by ROOT version 6.14/04
// from TTree emulsion/Reconstructed vertices, tracks and segments in emulsion
// found on file: vertices_withtracks.root
//////////////////////////////////////////////////////////

#ifndef VrtEmulsion_h
#define VrtEmulsion_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TClonesArray.h"
#include "TObject.h"

class VrtEmulsion {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxvolumetrack_ = 57;
   static constexpr Int_t kMaxsegment = 231;

   // Declaration of leaf types
   Float_t         vx;
   Float_t         vy;
   Float_t         vz;
   Int_t           ntracks;
   Int_t           volumetrack__;
   UInt_t          volumetrack__fUniqueID[kMaxvolumetrack_];   //[volumetrack._]
   UInt_t          volumetrack__fBits[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__ePID[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eID[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eVid[kMaxvolumetrack_][2];   //[volumetrack._]
   Int_t           volumetrack__eAid[kMaxvolumetrack_][2];   //[volumetrack._]
   Int_t           volumetrack__eFlag[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eTrack[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eX[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eY[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eZ[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eTX[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eTY[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eSZ[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eChi2[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eProb[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eW[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eVolume[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eDZ[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eDZem[kMaxvolumetrack_];   //[volumetrack._]
   Float_t         volumetrack__eP[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eMCTrack[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eMCEvt[kMaxvolumetrack_];   //[volumetrack._]
   UInt_t          volumetrack__eScanID_fUniqueID[kMaxvolumetrack_];   //[volumetrack._]
   UInt_t          volumetrack__eScanID_fBits[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eScanID_eBrick[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eScanID_ePlate[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eScanID_eMajor[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           volumetrack__eScanID_eMinor[kMaxvolumetrack_];   //[volumetrack._]
   Int_t           trid[57];   //[ntracks]
   Int_t           nseg[57];   //[ntracks]
   Int_t           npl[57];   //[ntracks]
   Int_t           n0[57];   //[ntracks]
   Int_t           segment_;
   UInt_t          segment_fUniqueID[kMaxsegment];   //[segment_]
   UInt_t          segment_fBits[kMaxsegment];   //[segment_]
   Int_t           segment_ePID[kMaxsegment];   //[segment_]
   Int_t           segment_eID[kMaxsegment];   //[segment_]
   Int_t           segment_eVid[kMaxsegment][2];   //[segment_]
   Int_t           segment_eAid[kMaxsegment][2];   //[segment_]
   Int_t           segment_eFlag[kMaxsegment];   //[segment_]
   Int_t           segment_eTrack[kMaxsegment];   //[segment_]
   Float_t         segment_eX[kMaxsegment];   //[segment_]
   Float_t         segment_eY[kMaxsegment];   //[segment_]
   Float_t         segment_eZ[kMaxsegment];   //[segment_]
   Float_t         segment_eTX[kMaxsegment];   //[segment_]
   Float_t         segment_eTY[kMaxsegment];   //[segment_]
   Float_t         segment_eSZ[kMaxsegment];   //[segment_]
   Float_t         segment_eChi2[kMaxsegment];   //[segment_]
   Float_t         segment_eProb[kMaxsegment];   //[segment_]
   Float_t         segment_eW[kMaxsegment];   //[segment_]
   Float_t         segment_eVolume[kMaxsegment];   //[segment_]
   Float_t         segment_eDZ[kMaxsegment];   //[segment_]
   Float_t         segment_eDZem[kMaxsegment];   //[segment_]
   Float_t         segment_eP[kMaxsegment];   //[segment_]
   Int_t           segment_eMCTrack[kMaxsegment];   //[segment_]
   Int_t           segment_eMCEvt[kMaxsegment];   //[segment_]
   UInt_t          segment_eScanID_fUniqueID[kMaxsegment];   //[segment_]
   UInt_t          segment_eScanID_fBits[kMaxsegment];   //[segment_]
   Int_t           segment_eScanID_eBrick[kMaxsegment];   //[segment_]
   Int_t           segment_eScanID_ePlate[kMaxsegment];   //[segment_]
   Int_t           segment_eScanID_eMajor[kMaxsegment];   //[segment_]
   Int_t           segment_eScanID_eMinor[kMaxsegment];   //[segment_]

   // List of branches
   TBranch        *b_vx;   //!
   TBranch        *b_vy;   //!
   TBranch        *b_vz;   //!
   TBranch        *b_ntracks;   //!
   TBranch        *b_volumetrack__;   //!
   TBranch        *b_volumetrack__fUniqueID;   //!
   TBranch        *b_volumetrack__fBits;   //!
   TBranch        *b_volumetrack__ePID;   //!
   TBranch        *b_volumetrack__eID;   //!
   TBranch        *b_volumetrack__eVid;   //!
   TBranch        *b_volumetrack__eAid;   //!
   TBranch        *b_volumetrack__eFlag;   //!
   TBranch        *b_volumetrack__eTrack;   //!
   TBranch        *b_volumetrack__eX;   //!
   TBranch        *b_volumetrack__eY;   //!
   TBranch        *b_volumetrack__eZ;   //!
   TBranch        *b_volumetrack__eTX;   //!
   TBranch        *b_volumetrack__eTY;   //!
   TBranch        *b_volumetrack__eSZ;   //!
   TBranch        *b_volumetrack__eChi2;   //!
   TBranch        *b_volumetrack__eProb;   //!
   TBranch        *b_volumetrack__eW;   //!
   TBranch        *b_volumetrack__eVolume;   //!
   TBranch        *b_volumetrack__eDZ;   //!
   TBranch        *b_volumetrack__eDZem;   //!
   TBranch        *b_volumetrack__eP;   //!
   TBranch        *b_volumetrack__eMCTrack;   //!
   TBranch        *b_volumetrack__eMCEvt;   //!
   TBranch        *b_volumetrack__eScanID_fUniqueID;   //!
   TBranch        *b_volumetrack__eScanID_fBits;   //!
   TBranch        *b_volumetrack__eScanID_eBrick;   //!
   TBranch        *b_volumetrack__eScanID_ePlate;   //!
   TBranch        *b_volumetrack__eScanID_eMajor;   //!
   TBranch        *b_volumetrack__eScanID_eMinor;   //!
   TBranch        *b_trid;   //!
   TBranch        *b_nseg;   //!
   TBranch        *b_npl;   //!
   TBranch        *b_n0;   //!
   TBranch        *b_segment_;   //!
   TBranch        *b_segment_fUniqueID;   //!
   TBranch        *b_segment_fBits;   //!
   TBranch        *b_segment_ePID;   //!
   TBranch        *b_segment_eID;   //!
   TBranch        *b_segment_eVid;   //!
   TBranch        *b_segment_eAid;   //!
   TBranch        *b_segment_eFlag;   //!
   TBranch        *b_segment_eTrack;   //!
   TBranch        *b_segment_eX;   //!
   TBranch        *b_segment_eY;   //!
   TBranch        *b_segment_eZ;   //!
   TBranch        *b_segment_eTX;   //!
   TBranch        *b_segment_eTY;   //!
   TBranch        *b_segment_eSZ;   //!
   TBranch        *b_segment_eChi2;   //!
   TBranch        *b_segment_eProb;   //!
   TBranch        *b_segment_eW;   //!
   TBranch        *b_segment_eVolume;   //!
   TBranch        *b_segment_eDZ;   //!
   TBranch        *b_segment_eDZem;   //!
   TBranch        *b_segment_eP;   //!
   TBranch        *b_segment_eMCTrack;   //!
   TBranch        *b_segment_eMCEvt;   //!
   TBranch        *b_segment_eScanID_fUniqueID;   //!
   TBranch        *b_segment_eScanID_fBits;   //!
   TBranch        *b_segment_eScanID_eBrick;   //!
   TBranch        *b_segment_eScanID_ePlate;   //!
   TBranch        *b_segment_eScanID_eMajor;   //!
   TBranch        *b_segment_eScanID_eMinor;   //!

   VrtEmulsion(TTree *tree=0);
   virtual ~VrtEmulsion();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef VrtEmulsion_cxx
VrtEmulsion::VrtEmulsion(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data/vertices_withtracks.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data/vertices_withtracks.root");
      }
      f->GetObject("emulsion",tree);

   }
   Init(tree);
}

VrtEmulsion::~VrtEmulsion()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t VrtEmulsion::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t VrtEmulsion::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void VrtEmulsion::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("vx", &vx, &b_vx);
   fChain->SetBranchAddress("vy", &vy, &b_vy);
   fChain->SetBranchAddress("vz", &vz, &b_vz);
   fChain->SetBranchAddress("ntracks", &ntracks, &b_ntracks);
   fChain->SetBranchAddress("volumetrack.", &volumetrack__, &b_volumetrack__);
   fChain->SetBranchAddress("volumetrack..fUniqueID", volumetrack__fUniqueID, &b_volumetrack__fUniqueID);
   fChain->SetBranchAddress("volumetrack..fBits", volumetrack__fBits, &b_volumetrack__fBits);
   fChain->SetBranchAddress("volumetrack..ePID", volumetrack__ePID, &b_volumetrack__ePID);
   fChain->SetBranchAddress("volumetrack..eID", volumetrack__eID, &b_volumetrack__eID);
   fChain->SetBranchAddress("volumetrack..eVid[2]", volumetrack__eVid, &b_volumetrack__eVid);
   fChain->SetBranchAddress("volumetrack..eAid[2]", volumetrack__eAid, &b_volumetrack__eAid);
   fChain->SetBranchAddress("volumetrack..eFlag", volumetrack__eFlag, &b_volumetrack__eFlag);
   fChain->SetBranchAddress("volumetrack..eTrack", volumetrack__eTrack, &b_volumetrack__eTrack);
   fChain->SetBranchAddress("volumetrack..eX", volumetrack__eX, &b_volumetrack__eX);
   fChain->SetBranchAddress("volumetrack..eY", volumetrack__eY, &b_volumetrack__eY);
   fChain->SetBranchAddress("volumetrack..eZ", volumetrack__eZ, &b_volumetrack__eZ);
   fChain->SetBranchAddress("volumetrack..eTX", volumetrack__eTX, &b_volumetrack__eTX);
   fChain->SetBranchAddress("volumetrack..eTY", volumetrack__eTY, &b_volumetrack__eTY);
   fChain->SetBranchAddress("volumetrack..eSZ", volumetrack__eSZ, &b_volumetrack__eSZ);
   fChain->SetBranchAddress("volumetrack..eChi2", volumetrack__eChi2, &b_volumetrack__eChi2);
   fChain->SetBranchAddress("volumetrack..eProb", volumetrack__eProb, &b_volumetrack__eProb);
   fChain->SetBranchAddress("volumetrack..eW", volumetrack__eW, &b_volumetrack__eW);
   fChain->SetBranchAddress("volumetrack..eVolume", volumetrack__eVolume, &b_volumetrack__eVolume);
   fChain->SetBranchAddress("volumetrack..eDZ", volumetrack__eDZ, &b_volumetrack__eDZ);
   fChain->SetBranchAddress("volumetrack..eDZem", volumetrack__eDZem, &b_volumetrack__eDZem);
   fChain->SetBranchAddress("volumetrack..eP", volumetrack__eP, &b_volumetrack__eP);
   fChain->SetBranchAddress("volumetrack..eMCTrack", volumetrack__eMCTrack, &b_volumetrack__eMCTrack);
   fChain->SetBranchAddress("volumetrack..eMCEvt", volumetrack__eMCEvt, &b_volumetrack__eMCEvt);
   fChain->SetBranchAddress("volumetrack..eScanID.fUniqueID", volumetrack__eScanID_fUniqueID, &b_volumetrack__eScanID_fUniqueID);
   fChain->SetBranchAddress("volumetrack..eScanID.fBits", volumetrack__eScanID_fBits, &b_volumetrack__eScanID_fBits);
   fChain->SetBranchAddress("volumetrack..eScanID.eBrick", volumetrack__eScanID_eBrick, &b_volumetrack__eScanID_eBrick);
   fChain->SetBranchAddress("volumetrack..eScanID.ePlate", volumetrack__eScanID_ePlate, &b_volumetrack__eScanID_ePlate);
   fChain->SetBranchAddress("volumetrack..eScanID.eMajor", volumetrack__eScanID_eMajor, &b_volumetrack__eScanID_eMajor);
   fChain->SetBranchAddress("volumetrack..eScanID.eMinor", volumetrack__eScanID_eMinor, &b_volumetrack__eScanID_eMinor);
   fChain->SetBranchAddress("trid", trid, &b_trid);
   fChain->SetBranchAddress("nseg", nseg, &b_nseg);
   fChain->SetBranchAddress("npl", npl, &b_npl);
   fChain->SetBranchAddress("n0", n0, &b_n0);
   fChain->SetBranchAddress("segment", &segment_, &b_segment_);
   fChain->SetBranchAddress("segment.fUniqueID", segment_fUniqueID, &b_segment_fUniqueID);
   fChain->SetBranchAddress("segment.fBits", segment_fBits, &b_segment_fBits);
   fChain->SetBranchAddress("segment.ePID", segment_ePID, &b_segment_ePID);
   fChain->SetBranchAddress("segment.eID", segment_eID, &b_segment_eID);
   fChain->SetBranchAddress("segment.eVid[2]", segment_eVid, &b_segment_eVid);
   fChain->SetBranchAddress("segment.eAid[2]", segment_eAid, &b_segment_eAid);
   fChain->SetBranchAddress("segment.eFlag", segment_eFlag, &b_segment_eFlag);
   fChain->SetBranchAddress("segment.eTrack", segment_eTrack, &b_segment_eTrack);
   fChain->SetBranchAddress("segment.eX", segment_eX, &b_segment_eX);
   fChain->SetBranchAddress("segment.eY", segment_eY, &b_segment_eY);
   fChain->SetBranchAddress("segment.eZ", segment_eZ, &b_segment_eZ);
   fChain->SetBranchAddress("segment.eTX", segment_eTX, &b_segment_eTX);
   fChain->SetBranchAddress("segment.eTY", segment_eTY, &b_segment_eTY);
   fChain->SetBranchAddress("segment.eSZ", segment_eSZ, &b_segment_eSZ);
   fChain->SetBranchAddress("segment.eChi2", segment_eChi2, &b_segment_eChi2);
   fChain->SetBranchAddress("segment.eProb", segment_eProb, &b_segment_eProb);
   fChain->SetBranchAddress("segment.eW", segment_eW, &b_segment_eW);
   fChain->SetBranchAddress("segment.eVolume", segment_eVolume, &b_segment_eVolume);
   fChain->SetBranchAddress("segment.eDZ", segment_eDZ, &b_segment_eDZ);
   fChain->SetBranchAddress("segment.eDZem", segment_eDZem, &b_segment_eDZem);
   fChain->SetBranchAddress("segment.eP", segment_eP, &b_segment_eP);
   fChain->SetBranchAddress("segment.eMCTrack", segment_eMCTrack, &b_segment_eMCTrack);
   fChain->SetBranchAddress("segment.eMCEvt", segment_eMCEvt, &b_segment_eMCEvt);
   fChain->SetBranchAddress("segment.eScanID.fUniqueID", segment_eScanID_fUniqueID, &b_segment_eScanID_fUniqueID);
   fChain->SetBranchAddress("segment.eScanID.fBits", segment_eScanID_fBits, &b_segment_eScanID_fBits);
   fChain->SetBranchAddress("segment.eScanID.eBrick", segment_eScanID_eBrick, &b_segment_eScanID_eBrick);
   fChain->SetBranchAddress("segment.eScanID.ePlate", segment_eScanID_ePlate, &b_segment_eScanID_ePlate);
   fChain->SetBranchAddress("segment.eScanID.eMajor", segment_eScanID_eMajor, &b_segment_eScanID_eMajor);
   fChain->SetBranchAddress("segment.eScanID.eMinor", segment_eScanID_eMinor, &b_segment_eScanID_eMinor);
   Notify();
}

Bool_t VrtEmulsion::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void VrtEmulsion::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
#endif // #ifdef VrtEmulsion_cxx
