//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Nov  9 17:17:24 2018 by ROOT version 6.14/04
// from TTree tracks/tracks
// found on file: linked_tracks_16_squarecm.root
//////////////////////////////////////////////////////////

#ifndef Emulsion_h
#define Emulsion_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TObject.h"
#include "TClonesArray.h"

class Emulsion {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxt = 1;
   static constexpr Int_t kMaxs = 29;
   static constexpr Int_t kMaxsf = 29;

   // Declaration of leaf types
   Int_t           trid;
   Int_t           nseg;
   Int_t           npl;
   Int_t           n0;
   Float_t         xv;
   Float_t         yv;
   Float_t         w;
 //EdbSegP         *t_;
   UInt_t          t_TObject_fUniqueID;
   UInt_t          t_TObject_fBits;
   Int_t           t_ePID;
   Int_t           t_eID;
   Int_t           t_eVid[2];
   Int_t           t_eAid[2];
   Int_t           t_eFlag;
   Int_t           t_eTrack;
   Float_t         t_eX;
   Float_t         t_eY;
   Float_t         t_eZ;
   Float_t         t_eTX;
   Float_t         t_eTY;
   Float_t         t_eSZ;
   Float_t         t_eChi2;
   Float_t         t_eProb;
   Float_t         t_eW;
   Float_t         t_eVolume;
   Float_t         t_eDZ;
   Float_t         t_eDZem;
   Float_t         t_eP;
   Int_t           t_eMCTrack;
   Int_t           t_eMCEvt;
   UInt_t          t_eScanID_fUniqueID;
   UInt_t          t_eScanID_fBits;
   Int_t           t_eScanID_eBrick;
   Int_t           t_eScanID_ePlate;
   Int_t           t_eScanID_eMajor;
   Int_t           t_eScanID_eMinor;
   Int_t           s_;
   UInt_t          s_fUniqueID[kMaxs];   //[s_]
   UInt_t          s_fBits[kMaxs];   //[s_]
   Int_t           s_ePID[kMaxs];   //[s_]
   Int_t           s_eID[kMaxs];   //[s_]
   Int_t           s_eVid[kMaxs][2];   //[s_]
   Int_t           s_eAid[kMaxs][2];   //[s_]
   Int_t           s_eFlag[kMaxs];   //[s_]
   Int_t           s_eTrack[kMaxs];   //[s_]
   Float_t         s_eX[kMaxs];   //[s_]
   Float_t         s_eY[kMaxs];   //[s_]
   Float_t         s_eZ[kMaxs];   //[s_]
   Float_t         s_eTX[kMaxs];   //[s_]
   Float_t         s_eTY[kMaxs];   //[s_]
   Float_t         s_eSZ[kMaxs];   //[s_]
   Float_t         s_eChi2[kMaxs];   //[s_]
   Float_t         s_eProb[kMaxs];   //[s_]
   Float_t         s_eW[kMaxs];   //[s_]
   Float_t         s_eVolume[kMaxs];   //[s_]
   Float_t         s_eDZ[kMaxs];   //[s_]
   Float_t         s_eDZem[kMaxs];   //[s_]
   Float_t         s_eP[kMaxs];   //[s_]
   Int_t           s_eMCTrack[kMaxs];   //[s_]
   Int_t           s_eMCEvt[kMaxs];   //[s_]
   UInt_t          s_eScanID_fUniqueID[kMaxs];   //[s_]
   UInt_t          s_eScanID_fBits[kMaxs];   //[s_]
   Int_t           s_eScanID_eBrick[kMaxs];   //[s_]
   Int_t           s_eScanID_ePlate[kMaxs];   //[s_]
   Int_t           s_eScanID_eMajor[kMaxs];   //[s_]
   Int_t           s_eScanID_eMinor[kMaxs];   //[s_]
   Int_t           sf_;
   UInt_t          sf_fUniqueID[kMaxsf];   //[sf_]
   UInt_t          sf_fBits[kMaxsf];   //[sf_]
   Int_t           sf_ePID[kMaxsf];   //[sf_]
   Int_t           sf_eID[kMaxsf];   //[sf_]
   Int_t           sf_eVid[kMaxsf][2];   //[sf_]
   Int_t           sf_eAid[kMaxsf][2];   //[sf_]
   Int_t           sf_eFlag[kMaxsf];   //[sf_]
   Int_t           sf_eTrack[kMaxsf];   //[sf_]
   Float_t         sf_eX[kMaxsf];   //[sf_]
   Float_t         sf_eY[kMaxsf];   //[sf_]
   Float_t         sf_eZ[kMaxsf];   //[sf_]
   Float_t         sf_eTX[kMaxsf];   //[sf_]
   Float_t         sf_eTY[kMaxsf];   //[sf_]
   Float_t         sf_eSZ[kMaxsf];   //[sf_]
   Float_t         sf_eChi2[kMaxsf];   //[sf_]
   Float_t         sf_eProb[kMaxsf];   //[sf_]
   Float_t         sf_eW[kMaxsf];   //[sf_]
   Float_t         sf_eVolume[kMaxsf];   //[sf_]
   Float_t         sf_eDZ[kMaxsf];   //[sf_]
   Float_t         sf_eDZem[kMaxsf];   //[sf_]
   Float_t         sf_eP[kMaxsf];   //[sf_]
   Int_t           sf_eMCTrack[kMaxsf];   //[sf_]
   Int_t           sf_eMCEvt[kMaxsf];   //[sf_]
   UInt_t          sf_eScanID_fUniqueID[kMaxsf];   //[sf_]
   UInt_t          sf_eScanID_fBits[kMaxsf];   //[sf_]
   Int_t           sf_eScanID_eBrick[kMaxsf];   //[sf_]
   Int_t           sf_eScanID_ePlate[kMaxsf];   //[sf_]
   Int_t           sf_eScanID_eMajor[kMaxsf];   //[sf_]
   Int_t           sf_eScanID_eMinor[kMaxsf];   //[sf_]

   // List of branches
   TBranch        *b_trid;   //!
   TBranch        *b_nseg;   //!
   TBranch        *b_npl;   //!
   TBranch        *b_n0;   //!
   TBranch        *b_xv;   //!
   TBranch        *b_yv;   //!
   TBranch        *b_w;   //!
   TBranch        *b_t_TObject_fUniqueID;   //!
   TBranch        *b_t_TObject_fBits;   //!
   TBranch        *b_t_ePID;   //!
   TBranch        *b_t_eID;   //!
   TBranch        *b_t_eVid;   //!
   TBranch        *b_t_eAid;   //!
   TBranch        *b_t_eFlag;   //!
   TBranch        *b_t_eTrack;   //!
   TBranch        *b_t_eX;   //!
   TBranch        *b_t_eY;   //!
   TBranch        *b_t_eZ;   //!
   TBranch        *b_t_eTX;   //!
   TBranch        *b_t_eTY;   //!
   TBranch        *b_t_eSZ;   //!
   TBranch        *b_t_eChi2;   //!
   TBranch        *b_t_eProb;   //!
   TBranch        *b_t_eW;   //!
   TBranch        *b_t_eVolume;   //!
   TBranch        *b_t_eDZ;   //!
   TBranch        *b_t_eDZem;   //!
   TBranch        *b_t_eP;   //!
   TBranch        *b_t_eMCTrack;   //!
   TBranch        *b_t_eMCEvt;   //!
   TBranch        *b_t_eScanID_fUniqueID;   //!
   TBranch        *b_t_eScanID_fBits;   //!
   TBranch        *b_t_eScanID_eBrick;   //!
   TBranch        *b_t_eScanID_ePlate;   //!
   TBranch        *b_t_eScanID_eMajor;   //!
   TBranch        *b_t_eScanID_eMinor;   //!
   TBranch        *b_s_;   //!
   TBranch        *b_s_fUniqueID;   //!
   TBranch        *b_s_fBits;   //!
   TBranch        *b_s_ePID;   //!
   TBranch        *b_s_eID;   //!
   TBranch        *b_s_eVid;   //!
   TBranch        *b_s_eAid;   //!
   TBranch        *b_s_eFlag;   //!
   TBranch        *b_s_eTrack;   //!
   TBranch        *b_s_eX;   //!
   TBranch        *b_s_eY;   //!
   TBranch        *b_s_eZ;   //!
   TBranch        *b_s_eTX;   //!
   TBranch        *b_s_eTY;   //!
   TBranch        *b_s_eSZ;   //!
   TBranch        *b_s_eChi2;   //!
   TBranch        *b_s_eProb;   //!
   TBranch        *b_s_eW;   //!
   TBranch        *b_s_eVolume;   //!
   TBranch        *b_s_eDZ;   //!
   TBranch        *b_s_eDZem;   //!
   TBranch        *b_s_eP;   //!
   TBranch        *b_s_eMCTrack;   //!
   TBranch        *b_s_eMCEvt;   //!
   TBranch        *b_s_eScanID_fUniqueID;   //!
   TBranch        *b_s_eScanID_fBits;   //!
   TBranch        *b_s_eScanID_eBrick;   //!
   TBranch        *b_s_eScanID_ePlate;   //!
   TBranch        *b_s_eScanID_eMajor;   //!
   TBranch        *b_s_eScanID_eMinor;   //!
   TBranch        *b_sf_;   //!
   TBranch        *b_sf_fUniqueID;   //!
   TBranch        *b_sf_fBits;   //!
   TBranch        *b_sf_ePID;   //!
   TBranch        *b_sf_eID;   //!
   TBranch        *b_sf_eVid;   //!
   TBranch        *b_sf_eAid;   //!
   TBranch        *b_sf_eFlag;   //!
   TBranch        *b_sf_eTrack;   //!
   TBranch        *b_sf_eX;   //!
   TBranch        *b_sf_eY;   //!
   TBranch        *b_sf_eZ;   //!
   TBranch        *b_sf_eTX;   //!
   TBranch        *b_sf_eTY;   //!
   TBranch        *b_sf_eSZ;   //!
   TBranch        *b_sf_eChi2;   //!
   TBranch        *b_sf_eProb;   //!
   TBranch        *b_sf_eW;   //!
   TBranch        *b_sf_eVolume;   //!
   TBranch        *b_sf_eDZ;   //!
   TBranch        *b_sf_eDZem;   //!
   TBranch        *b_sf_eP;   //!
   TBranch        *b_sf_eMCTrack;   //!
   TBranch        *b_sf_eMCEvt;   //!
   TBranch        *b_sf_eScanID_fUniqueID;   //!
   TBranch        *b_sf_eScanID_fBits;   //!
   TBranch        *b_sf_eScanID_eBrick;   //!
   TBranch        *b_sf_eScanID_ePlate;   //!
   TBranch        *b_sf_eScanID_eMajor;   //!
   TBranch        *b_sf_eScanID_eMinor;   //!

   Emulsion(TTree *tree=0);
   virtual ~Emulsion();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Emulsion_cxx
Emulsion::Emulsion(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data/linked_tracks_full.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data/linked_tracks_full.root");
      }
      f->GetObject("tracks",tree);

   }
   Init(tree);
}

Emulsion::~Emulsion()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Emulsion::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Emulsion::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Emulsion::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("trid", &trid, &b_trid);
   fChain->SetBranchAddress("nseg", &nseg, &b_nseg);
   fChain->SetBranchAddress("npl", &npl, &b_npl);
   fChain->SetBranchAddress("n0", &n0, &b_n0);
   fChain->SetBranchAddress("xv", &xv, &b_xv);
   fChain->SetBranchAddress("yv", &yv, &b_yv);
   fChain->SetBranchAddress("w", &w, &b_w);
   fChain->SetBranchAddress("t.TObject.fUniqueID", &t_TObject_fUniqueID, &b_t_TObject_fUniqueID);
   fChain->SetBranchAddress("t.TObject.fBits", &t_TObject_fBits, &b_t_TObject_fBits);
   fChain->SetBranchAddress("t.ePID", &t_ePID, &b_t_ePID);
   fChain->SetBranchAddress("t.eID", &t_eID, &b_t_eID);
   fChain->SetBranchAddress("t.eVid[2]", t_eVid, &b_t_eVid);
   fChain->SetBranchAddress("t.eAid[2]", t_eAid, &b_t_eAid);
   fChain->SetBranchAddress("t.eFlag", &t_eFlag, &b_t_eFlag);
   fChain->SetBranchAddress("t.eTrack", &t_eTrack, &b_t_eTrack);
   fChain->SetBranchAddress("t.eX", &t_eX, &b_t_eX);
   fChain->SetBranchAddress("t.eY", &t_eY, &b_t_eY);
   fChain->SetBranchAddress("t.eZ", &t_eZ, &b_t_eZ);
   fChain->SetBranchAddress("t.eTX", &t_eTX, &b_t_eTX);
   fChain->SetBranchAddress("t.eTY", &t_eTY, &b_t_eTY);
   fChain->SetBranchAddress("t.eSZ", &t_eSZ, &b_t_eSZ);
   fChain->SetBranchAddress("t.eChi2", &t_eChi2, &b_t_eChi2);
   fChain->SetBranchAddress("t.eProb", &t_eProb, &b_t_eProb);
   fChain->SetBranchAddress("t.eW", &t_eW, &b_t_eW);
   fChain->SetBranchAddress("t.eVolume", &t_eVolume, &b_t_eVolume);
   fChain->SetBranchAddress("t.eDZ", &t_eDZ, &b_t_eDZ);
   fChain->SetBranchAddress("t.eDZem", &t_eDZem, &b_t_eDZem);
   fChain->SetBranchAddress("t.eP", &t_eP, &b_t_eP);
   fChain->SetBranchAddress("t.eMCTrack", &t_eMCTrack, &b_t_eMCTrack);
   fChain->SetBranchAddress("t.eMCEvt", &t_eMCEvt, &b_t_eMCEvt);
   fChain->SetBranchAddress("t.eScanID.fUniqueID", &t_eScanID_fUniqueID, &b_t_eScanID_fUniqueID);
   fChain->SetBranchAddress("t.eScanID.fBits", &t_eScanID_fBits, &b_t_eScanID_fBits);
   fChain->SetBranchAddress("t.eScanID.eBrick", &t_eScanID_eBrick, &b_t_eScanID_eBrick);
   fChain->SetBranchAddress("t.eScanID.ePlate", &t_eScanID_ePlate, &b_t_eScanID_ePlate);
   fChain->SetBranchAddress("t.eScanID.eMajor", &t_eScanID_eMajor, &b_t_eScanID_eMajor);
   fChain->SetBranchAddress("t.eScanID.eMinor", &t_eScanID_eMinor, &b_t_eScanID_eMinor);
   fChain->SetBranchAddress("s", &s_, &b_s_);
   fChain->SetBranchAddress("s.fUniqueID", s_fUniqueID, &b_s_fUniqueID);
   fChain->SetBranchAddress("s.fBits", s_fBits, &b_s_fBits);
   fChain->SetBranchAddress("s.ePID", s_ePID, &b_s_ePID);
   fChain->SetBranchAddress("s.eID", s_eID, &b_s_eID);
   fChain->SetBranchAddress("s.eVid[2]", s_eVid, &b_s_eVid);
   fChain->SetBranchAddress("s.eAid[2]", s_eAid, &b_s_eAid);
   fChain->SetBranchAddress("s.eFlag", s_eFlag, &b_s_eFlag);
   fChain->SetBranchAddress("s.eTrack", s_eTrack, &b_s_eTrack);
   fChain->SetBranchAddress("s.eX", s_eX, &b_s_eX);
   fChain->SetBranchAddress("s.eY", s_eY, &b_s_eY);
   fChain->SetBranchAddress("s.eZ", s_eZ, &b_s_eZ);
   fChain->SetBranchAddress("s.eTX", s_eTX, &b_s_eTX);
   fChain->SetBranchAddress("s.eTY", s_eTY, &b_s_eTY);
   fChain->SetBranchAddress("s.eSZ", s_eSZ, &b_s_eSZ);
   fChain->SetBranchAddress("s.eChi2", s_eChi2, &b_s_eChi2);
   fChain->SetBranchAddress("s.eProb", s_eProb, &b_s_eProb);
   fChain->SetBranchAddress("s.eW", s_eW, &b_s_eW);
   fChain->SetBranchAddress("s.eVolume", s_eVolume, &b_s_eVolume);
   fChain->SetBranchAddress("s.eDZ", s_eDZ, &b_s_eDZ);
   fChain->SetBranchAddress("s.eDZem", s_eDZem, &b_s_eDZem);
   fChain->SetBranchAddress("s.eP", s_eP, &b_s_eP);
   fChain->SetBranchAddress("s.eMCTrack", s_eMCTrack, &b_s_eMCTrack);
   fChain->SetBranchAddress("s.eMCEvt", s_eMCEvt, &b_s_eMCEvt);
   fChain->SetBranchAddress("s.eScanID.fUniqueID", s_eScanID_fUniqueID, &b_s_eScanID_fUniqueID);
   fChain->SetBranchAddress("s.eScanID.fBits", s_eScanID_fBits, &b_s_eScanID_fBits);
   fChain->SetBranchAddress("s.eScanID.eBrick", s_eScanID_eBrick, &b_s_eScanID_eBrick);
   fChain->SetBranchAddress("s.eScanID.ePlate", s_eScanID_ePlate, &b_s_eScanID_ePlate);
   fChain->SetBranchAddress("s.eScanID.eMajor", s_eScanID_eMajor, &b_s_eScanID_eMajor);
   fChain->SetBranchAddress("s.eScanID.eMinor", s_eScanID_eMinor, &b_s_eScanID_eMinor);
   fChain->SetBranchAddress("sf", &sf_, &b_sf_);
   fChain->SetBranchAddress("sf.fUniqueID", sf_fUniqueID, &b_sf_fUniqueID);
   fChain->SetBranchAddress("sf.fBits", sf_fBits, &b_sf_fBits);
   fChain->SetBranchAddress("sf.ePID", sf_ePID, &b_sf_ePID);
   fChain->SetBranchAddress("sf.eID", sf_eID, &b_sf_eID);
   fChain->SetBranchAddress("sf.eVid[2]", sf_eVid, &b_sf_eVid);
   fChain->SetBranchAddress("sf.eAid[2]", sf_eAid, &b_sf_eAid);
   fChain->SetBranchAddress("sf.eFlag", sf_eFlag, &b_sf_eFlag);
   fChain->SetBranchAddress("sf.eTrack", sf_eTrack, &b_sf_eTrack);
   fChain->SetBranchAddress("sf.eX", sf_eX, &b_sf_eX);
   fChain->SetBranchAddress("sf.eY", sf_eY, &b_sf_eY);
   fChain->SetBranchAddress("sf.eZ", sf_eZ, &b_sf_eZ);
   fChain->SetBranchAddress("sf.eTX", sf_eTX, &b_sf_eTX);
   fChain->SetBranchAddress("sf.eTY", sf_eTY, &b_sf_eTY);
   fChain->SetBranchAddress("sf.eSZ", sf_eSZ, &b_sf_eSZ);
   fChain->SetBranchAddress("sf.eChi2", sf_eChi2, &b_sf_eChi2);
   fChain->SetBranchAddress("sf.eProb", sf_eProb, &b_sf_eProb);
   fChain->SetBranchAddress("sf.eW", sf_eW, &b_sf_eW);
   fChain->SetBranchAddress("sf.eVolume", sf_eVolume, &b_sf_eVolume);
   fChain->SetBranchAddress("sf.eDZ", sf_eDZ, &b_sf_eDZ);
   fChain->SetBranchAddress("sf.eDZem", sf_eDZem, &b_sf_eDZem);
   fChain->SetBranchAddress("sf.eP", sf_eP, &b_sf_eP);
   fChain->SetBranchAddress("sf.eMCTrack", sf_eMCTrack, &b_sf_eMCTrack);
   fChain->SetBranchAddress("sf.eMCEvt", sf_eMCEvt, &b_sf_eMCEvt);
   fChain->SetBranchAddress("sf.eScanID.fUniqueID", sf_eScanID_fUniqueID, &b_sf_eScanID_fUniqueID);
   fChain->SetBranchAddress("sf.eScanID.fBits", sf_eScanID_fBits, &b_sf_eScanID_fBits);
   fChain->SetBranchAddress("sf.eScanID.eBrick", sf_eScanID_eBrick, &b_sf_eScanID_eBrick);
   fChain->SetBranchAddress("sf.eScanID.ePlate", sf_eScanID_ePlate, &b_sf_eScanID_ePlate);
   fChain->SetBranchAddress("sf.eScanID.eMajor", sf_eScanID_eMajor, &b_sf_eScanID_eMajor);
   fChain->SetBranchAddress("sf.eScanID.eMinor", sf_eScanID_eMinor, &b_sf_eScanID_eMinor);
   Notify();
}

Bool_t Emulsion::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Emulsion::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
#endif // #ifdef Emulsion_cxx
