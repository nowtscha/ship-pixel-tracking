//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue May 28 15:26:39 2019 by ROOT version 6.14/06
// from TTree p/Pixel tracks
// found on file: data/run_2793/pix_tracks_7.root
//////////////////////////////////////////////////////////

#ifndef StudyResiduals_h
#define StudyResiduals_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class StudyResiduals {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Long64_t        evnum;
   ULong64_t       timestamp;
   UShort_t        pspill;
   vector<float>   *hitx;
   vector<float>   *hity;
   vector<float>   *hitz;
   vector<unsigned short> *hitplane;
   vector<bool>    *hitused;
   vector<float>   *x;
   vector<float>   *y;
   vector<float>   *ex;
   vector<float>   *ey;
   vector<float>   *tx;
   vector<float>   *ty;
   vector<float>   *etx;
   vector<float>   *ety;
   vector<float>   *chi2;

   // List of branches
   TBranch        *b_evnum;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_pspill;   //!
   TBranch        *b_hitx;   //!
   TBranch        *b_hity;   //!
   TBranch        *b_hitz;   //!
   TBranch        *b_hitplane;   //!
   TBranch        *b_hitused;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_ex;   //!
   TBranch        *b_ey;   //!
   TBranch        *b_tx;   //!
   TBranch        *b_ty;   //!
   TBranch        *b_etx;   //!
   TBranch        *b_ety;   //!
   TBranch        *b_chi2;   //!

   StudyResiduals(TTree *tree=0);
   virtual ~StudyResiduals();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef StudyResiduals_cxx
StudyResiduals::StudyResiduals(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("data/run_2793/pix_tracks_7.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("data/run_2793/pix_tracks_7.root");
      }
      f->GetObject("p",tree);

   }
   Init(tree);
}

StudyResiduals::~StudyResiduals()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t StudyResiduals::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t StudyResiduals::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void StudyResiduals::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   hitx = 0;
   hity = 0;
   hitz = 0;
   hitplane = 0;
   hitused = 0;
   x = 0;
   y = 0;
   ex = 0;
   ey = 0;
   tx = 0;
   ty = 0;
   etx = 0;
   ety = 0;
   chi2 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("evnum", &evnum, &b_evnum);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("pspill", &pspill, &b_pspill);
   fChain->SetBranchAddress("hitx", &hitx, &b_hitx);
   fChain->SetBranchAddress("hity", &hity, &b_hity);
   fChain->SetBranchAddress("hitz", &hitz, &b_hitz);
   fChain->SetBranchAddress("hitplane", &hitplane, &b_hitplane);
   fChain->SetBranchAddress("hitused", &hitused, &b_hitused);
   fChain->SetBranchAddress("x", &x, &b_x);
   fChain->SetBranchAddress("y", &y, &b_y);
   fChain->SetBranchAddress("ex", &ex, &b_ex);
   fChain->SetBranchAddress("ey", &ey, &b_ey);
   fChain->SetBranchAddress("tx", &tx, &b_tx);
   fChain->SetBranchAddress("ty", &ty, &b_ty);
   fChain->SetBranchAddress("etx", &etx, &b_etx);
   fChain->SetBranchAddress("ety", &ety, &b_ety);
   fChain->SetBranchAddress("chi2", &chi2, &b_chi2);
   Notify();
}

Bool_t StudyResiduals::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void StudyResiduals::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t StudyResiduals::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef StudyResiduals_cxx
