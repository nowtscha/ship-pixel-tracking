
#ifndef VectorsToTreeCharmx_h
#define VectorsToTreeCharmx_h
#endif

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TVectorD.h>
#include<TMatrixDSym.h>
#include "TTree.h"
#include <iostream>
#include <vector>
#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ class std::vector<std::vector<int> >+;
#endif


// Header file for the classes stored in the TTree if any.

class TestBeam {

public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Long64_t         event_number;
   Double_t         x;
   Double_t         y;
   Double_t         z;
   Double_t         px;
   Double_t         py;
   Double_t         pz;
   Double_t         EnergyLoss;
   UInt_t           plane;
   Int_t            PdgCode;
   Int_t            spill;

   // List of branches
   TBranch        *b_event_number;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_EnergyLoss;   //!
   TBranch        *b_plane;   //!
   TBranch        *b_PdgCode;   //!
   TBranch        *b_spill;   //!


   TestBeam(TTree *tree=0);
   virtual ~TestBeam();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   void FillHits(int);

   struct pixHit{
     TVectorD hitXY{2};
     float planeZ{0.};
     TMatrixDSym hitCov{2};
     int planeID{0};
     int localIndex{-1};
     bool Used{false};
   };
   std::vector< std::vector< std::vector<pixHit> > > * pixFILL{nullptr};

   // struct fittedTrack{
   //   TVector3 pos;
   //   TVector3 mom;
   //   TMatrixDSym COV;
   // };
};
