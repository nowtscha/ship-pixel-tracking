#ifndef vertexMC_h
#define vertexMC_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include "TEfficiency.h"
#include <TVector3.h>
#include <TMatrixDSym.h>
#include <TVectorD.h>
#include <iostream>

#include <boost/config.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/bron_kerbosch_all_cliques.hpp>
#include <boost/graph/undirected_graph.hpp>


class SimVertex {
public :

    struct fittedTrack {
    bool Good = false;
    Long64_t timestamp{0};
    Float_t Chi2{1.e9};
    int Ndf{0};
    int accepted{0};
    TVector3 pos;
    TVector3 mom;
    TMatrixDSym COV{6};
    int trackID{0};
    Long_t globalTrackID{-1};
    TVector3 pos_update;
    TVector3 mom_update;
    TMatrixDSym COV_update{6};
    int vtxID{-1};
    int vtxIDtruth{-1};
    int vtxIDtruthglob{-1};
    TVector3 vtxTruthpos;
    TVector3 fitvtxpos;
    };

    struct vertex2Track {
        double chi2{1.e9};
        double distance;
        TVector3 r;
        TMatrixD COV{3,3};
        std::vector<int> trackID;
    };

    struct trackPars {
      TVector3 pos;
      TVector3 mom;
      TMatrixDSym COV{2};
      TVector3 vtxpos;
    };

    struct MultiVtx {
        bool Good=false;
        double chi2{1.e9}; // chi2 for fit of r
        double redchi2{1.e9};
        int ndf;
        int nTracks;
        double distance; // euclidean distance in point of closest approach (POCA)
        TVector3 r; // POCA
        TMatrixD vertexCov{3,3};
        std::vector<double> chi2PerTrk;
        // TMatrixDSym COV{3}; // covariance matrix for fit of r
        std::vector<int> trackID; // trackIDs of Tracks in vertex
        std::vector<Long_t> trackIDglob; // global trackIDs of Tracks in vertex
        bool refined = false;
        int ID{-1};
        int globID{-1};
    };

    struct clique_visitor{
        clique_visitor(std::vector< std::vector<int> > & input): m_allCliques(input){ input.clear();}
        
        template <typename Clique, typename Graph>
        void clique(const Clique& clq, Graph& )
        {
            std::vector<int> new_clique(0);
            for(auto i = clq.begin(); i != clq.end(); ++i) new_clique.push_back(*i);
            m_allCliques.push_back(new_clique);
        }

        std::vector< std::vector<int> > & m_allCliques;

    };

    std::unique_ptr<boost::adjacency_list<boost::listS, boost::vecS, boost::undirectedS>> m_compatibilityGraph{};

    void Loop(Long_t nevents, Long_t n_mc_vtx, Long_t n_mc_tracks);
    void FitVtxandTracks_sample_vtx(std::vector<fittedTrack> &fittedTracks, MultiVtx &vtx, TVector3 estimate, std::vector<double>& newTrackPars);
    void find2TrackVertices(std::vector<fittedTrack> Tracks, std::vector<vertex2Track> &vertices, std::map<long int, vertex2Track> &map2tVtx);
    double fit2TVertex(fittedTrack t1, fittedTrack t2, double &Xv, double &Yv, double &Zv, double &eXv, double &eYv, double &eZv, double &chi2v);
    void FillCompGraph(std::vector<vertex2Track> &vertices2Track, std::vector<vertex2Track> &good2tVtx, long int nTracks, std::vector<double> z_lims, double chi2_lim);
    void FitMultiVtx(std::vector<MultiVtx> &multiVtcs, std::vector<vertex2Track> &selected2Tvertices, std::map<long int, vertex2Track> &map2tVtx, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims, Long_t &globvtxID);
    double refineVerticesWithCommonTracks( MultiVtx &v1, MultiVtx &v2, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims, std::map<Int_t, trackPars> &newPars);
    double mergeAndRefitVertices( MultiVtx & v1, MultiVtx & v2, MultiVtx & newvrt, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims, std::map<Int_t, trackPars> &newPars, Long_t globvtxID);
    SimVertex::MultiVtx RefineVertex(MultiVtx &v1, std::vector<fittedTrack> &fittedTracks, std::vector<double> &zlims, std::map<Int_t, trackPars> &newPars, Long_t &globvtxID);
    TVector3 estimVrtPos( int nTrk, std::vector<int> &selTrk, std::map<long int, vertex2Track> & vrt);
    int nTrkCommon(std::vector<MultiVtx> &vertexSet, int V1, int V2);
    void FitVtxandTracks(std::vector<fittedTrack> &fittedTracks, MultiVtx &vtx, TVector3 estimate, std::map<Int_t, trackPars> &newPars);
};


#endif // #ifdef checkFit_cxx


