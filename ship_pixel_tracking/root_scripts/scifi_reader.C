#define Reader_cxx
#include "scifi_reader.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TMath.h"

void Reader::Loop()
{
    if (fChain == 0)
        return;
    Long64_t nentries = fChain->GetEntriesFast();
    Long64_t nbytes = 0, nb = 0;
    UInt_t oldspill = 0;
    double oldtrig = 0.;

    TFile *nfile = TFile::Open("/media/niko/big_data/charm_testbeam_july18/scifi_offline/run_2827/2827_spill_1F250B06.root", "RECREATE");
    TTree *scifi_out = new TTree("scifi_out", "SciFi data");

    std::vector<unsigned int> board_ID;
    std::vector<unsigned int> STiC_ID;
    std::vector<unsigned int> ch;
    std::vector<double> x;
    std::vector<double> y;
    std::vector<double> z;
    std::vector<unsigned int> tot;
    std::vector<float> hit_time;
    Int_t event_number;
    UInt_t trigger_ID;
    std::vector<float> fine_time;
    Float_t trigger_shift;
    UInt_t spill;

    scifi_out->Branch("event_number", &event_number, "event_number/I");
    scifi_out->Branch("board_ID", &board_ID);
    scifi_out->Branch("STiC_ID", &STiC_ID);
    scifi_out->Branch("ch", &ch);
    scifi_out->Branch("x", &x);
    scifi_out->Branch("y", &y);
    scifi_out->Branch("z", &z);
    scifi_out->Branch("tot", &tot);
    scifi_out->Branch("hit_time", &hit_time);
    scifi_out->Branch("trigger_ID", &trigger_ID);
    scifi_out->Branch("fine_time", &fine_time);
    scifi_out->Branch("trigger_shift", &trigger_shift, "trigger_shift/F");
    scifi_out->Branch("spill", &spill);

    TH1D *htsci = new TH1D("htsci", "htsci", 1000, 0., 1000000.);
    TH1D *htsci2 = new TH1D("htsci2", "htsci", 1000, 0., 10000.);
    TH1D *htsci3 = new TH1D("htsci3", "htsci", 1000, 1., 6.);

    TH2D *h_bc[5], *h_bs[5], *h_cs[5];

    for (int i = 0; i < 5; i++)
    {
        h_bc[i] = new TH2D(Form("h_bc%d", i), "BoardID vs Ch", 66, -1.5, 64.5, 26, -0.5, 25.5);
        h_bs[i] = new TH2D(Form("h_bs%d", i), "BoardID vs STiCID", 10, -1.5, 8.5, 26, -0.5, 25.5);
        h_cs[i] = new TH2D(Form("h_cs%d", i), "Ch vs STiCID", 10, -1.5, 8.5, 66, -1.5, 64.5);
    }
    double xtmp, ytmp, ztmp;
    for (Long64_t jentry = 0; jentry < nentries; jentry++)
    {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0)
            break;
        nb = fChain->GetEntry(jentry);
        nbytes += nb;
        // if (Cut(ientry) < 0) continue;

        if (Spillnum != oldspill)
        {
            std::cout << Spillnum << std::endl;
            oldspill = Spillnum;
        }
        trigger_shift = 0;
        trigger_ID = 0;
        event_number = 0;
        spill = 0;
        board_ID.clear();
        STiC_ID.clear();
        ch.clear();
        tot.clear();
        hit_time.clear();
        fine_time.clear();
        x.clear();
        y.clear();
        z.clear();

        uint32_t ch_layer;
        Int_t length = Ch->size();
        Int_t b;
        for (int i = 0; i < length; i++)
        {
            board_ID.push_back(BoardID->at(i));
            STiC_ID.push_back(STiCID->at(i));
            ch.push_back(Ch->at(i));
            tot.push_back(TOT->at(i));

            int layer = ((*BoardID)[i] - 1) / 3;
            if (jentry == 0)
                cout << "l= " << layer << "\t b= " << (*BoardID)[i] << "\t stic= " << (*STiCID)[i] << "\t ch= " << (*Ch)[i] << endl;
            uint32_t board_inMod = ((*BoardID)[i]) % 3;
            ch_layer = ((board_inMod)*512) + (((int)((*STiCID)[i] / 2)) * 128 + ((*Ch)[i] * 2) + ((*STiCID)[i] % 2));
            Position(ch_layer, layer, xtmp, ytmp, ztmp);
            x.push_back(xtmp);
            y.push_back(ytmp);
            z.push_back(ztmp);
            h_bc[0]->Fill(Ch->at(i), BoardID->at(i));
            h_bs[0]->Fill(STiCID->at(i), BoardID->at(i));
            h_cs[0]->Fill(STiCID->at(i), Ch->at(i));
            if (TOT->at(i) < 255)
            {
                h_bc[1]->Fill(Ch->at(i), BoardID->at(i));
                h_bs[1]->Fill(STiCID->at(i), BoardID->at(i));
                h_cs[1]->Fill(STiCID->at(i), Ch->at(i));
            }

            double HitTimeShiftMsb = (double)(((HitTime->at(i) & 0xFFFF8000) / (pow(2, 15) - 1)) * pow(2, 15));
            double HitTimeShiftLsb = (double)(HitTime->at(i) & 0x7FFF);
            double HitTimeShift = (HitTimeShiftMsb + HitTimeShiftLsb) * (1 / 0.680);
            double finecnt = (double)(FineTime->at(i) * 1 / (0.680 * 32));
            double HitTime = HitTimeShift + finecnt;
            hit_time.push_back(HitTime);
            fine_time.push_back(finecnt);
            //           std::cout << jentry << " " << i << " " << HitTime/25. << std::endl;
        }
        //       for (auto const& i : *TriggerID) {
        int i = 7;
        unsigned long itri = TriggerID->at(i);
        double TrigTimeShiftMsb = (double)(((TriggerT->at(i) & 0xFFFF8000) / (pow(2, 15) - 1)) * pow(2, 15));
        double TrigTimeShiftLsb = (double)(TriggerT->at(i) & 0x7FFF);
        double TrigTimeShift = 0.04 * (TrigTimeShiftMsb + TrigTimeShiftLsb) * (1 / 0.680);
        //       printf("Entry %6lld Event %10d TriggerID %10lu TriggerTime/25ns %10.0f length %5d\n",
        //               jentry, Eventnum, itri, TrigTimeShift, length);
        double diff = TrigTimeShift - oldtrig;

        trigger_shift = TrigTimeShift;
        event_number = Eventnum;
        trigger_ID = itri;
        spill = Spillnum;
        scifi_out->Fill();

        // printf("%10.0f %10.0f %5d\n", TrigTimeShift, diff, length);
        htsci->Fill(diff);
        htsci2->Fill(diff);
        htsci3->Fill(TMath::Log10(diff));
        oldtrig = TrigTimeShift;
        //       }
    }
    scifi_out->Write();
    for (int i = 0; i < 5; i++)
    {
        h_bc[i]->Write();
        h_bs[i]->Write();
        h_cs[i]->Write();
    }
    htsci->Write();
    htsci2->Write();
    htsci3->Write();

    delete nfile;
}
void Position(int ch_layer, int layer, double &x, double &y, double &z)
{
    double gap_die = 0.220;  // mm
    double gap_SiPM = 0.400; // mm
    double ch_width = 0.250; // mm
    x = -10000;
    y = -10000;
    z = -10000;

    //angle in radiants for layers U and V (5 deg)
    double angle=2.5/180*TMath::Pi();

    //compute half layer width to be entered in the middle of the plane
    double half_layer = 1536 / 2 * 0.250 + 6 * gap_die + 5.5 * gap_SiPM;

    //z positions inside a station
    double z_pos[8];
    z_pos[0] = 581.3250;
    z_pos[1] = 584.8138;
    z_pos[2] = 588.3025;
    z_pos[3] = 591.7912;
    z_pos[4] = 595.2800;
    z_pos[5] = 598.7688;
    z_pos[6] = 602.2575;
    z_pos[7] = 605.7463;

    // z position defined as:
    z = z_pos[layer];
    ;

    //how many half dies
    int mult = int(ch_layer / 64);

    //shift to add to the position due to dead regions
    double shift = int(mult / 2) * gap_die + int(mult / 2) * gap_SiPM + (mult % 2) * gap_die;

    //first station only x defined for X and U planes
    if (layer == 0)
        x = -(ch_layer * 0.250 + shift - half_layer);
    if (layer == 1)
        x = -(ch_layer * 0.250 + shift - half_layer) - 200*sin(angle);

    //first station only y defined for Y and V planes
    if (layer == 2)
        y = -(ch_layer * 0.250 + shift - half_layer) + 200*sin(angle);
    if (layer == 3)
        y = -(ch_layer * 0.250 + shift - half_layer);

    //second station only y defined for Y and V planes
    if (layer == 4)
        y = -(ch_layer * 0.250 + shift - half_layer);
    if (layer == 5)
        y = -(ch_layer * 0.250 + shift - half_layer) - 200*sin(angle);

    //second station only x defined for X and U planes
    if (layer == 6)
        x = (ch_layer * 0.250 + shift - half_layer) + 200*sin(angle);
    if (layer == 7)
        x = (ch_layer * 0.250 + shift - half_layer);
}
