import numpy as np
import tables as tb
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_1d_bar, plot_2d_hist, plot_xy, plot_residuals_vs_position, create_polyplot_fig, save_polyplot_fig, plot_2dhist_subplot
from ship_pixel_tracking.tools.analysis_utils import load_table, save_table, save_array, load_array
from tqdm import tqdm
from numba import njit
import logging
import random

import scipy.cluster.hierarchy as hcluster
from matplotlib import colors, cm

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')




def calc_dist_all_combinations(vertices):
    event_numbers, event_start_indices, event_lengths = np.unique(vertices["event_number"], return_index = True, return_counts = True)
    dist = []
    logging.info("loop through events")
    for i,index in enumerate(tqdm(event_start_indices)):
        event = vertices[index:index+event_lengths[i]]
        mask = np.zeros(shape = event.shape, dtype = bool)
        vID = event[0]["vertexID"]
        for i,row in enumerate(event[1:]):
            if row["vertexID"] == vID:
                mask[i] = True
            else:vID = row["vertexID"]
        event = event[mask]  
        pos = np.array([event["x"],event["y"], event["z"]]).T
        
        for row in pos:
            for row2 in pos:
                dist.append([row, row2])

    logging.info("calculate euclidian distance")
    dist = np.array(dist)
    euclidian_dist = np.sqrt(np.sum((dist[:,0,:] - dist[:,1,:]) ** 2, axis=1)) #np.linalg.norm(dist[:,0,:] - dist[:,1,:], axis = 1)
    return euclidian_dist, event_lengths


def calc_vtx_distances(vertices):
    '''

    '''

    event_numbers, event_start_indices, event_lengths = np.unique(vertices["event_number"], return_index = True, return_counts = True)
    dist = []
    for i,index in enumerate(tqdm(event_start_indices)):
        event = vertices[index:index+event_lengths[i]]
        # vertex array contains vertex pos for every vtx track. For 3d vtx distance we only need each vtx position once. The following loop
        # finds and masks duplicates for calculation
        mask = np.zeros(shape = event.shape, dtype = bool)
        vID = event[0]["vertexID"]
        for i,row in enumerate(event[1:]):
            if row["vertexID"] == vID:
                mask[i] = True
            else:
                vID = row["vertexID"]
        event = event[mask]  
        vtx_positions = np.array([event["x"],event["y"], event["z"]]).T

        # we do not need all possible combinations, but only all unique combinations. Therefore the inner loop starts one enty later than the outer one
        last_vertex_index=0
        for row in vtx_positions:
            down_vtx_positions = vtx_positions[1+last_vertex_index:]
            for row2 in down_vtx_positions:
                dist.append([row, row2 ])
            last_vertex_index+=1

    dist = np.array(dist)
    logging.info("calculate euclidian distance")
    # np.sum and sqrt is slightly faster than linalg.norm
    euclidian_dist = np.sqrt(np.sum((dist[:,0,:] - dist[:,1,:]) ** 2, axis=1)) #np.linalg.norm(dist[:,0,:] - dist[:,1,:], axis = 1)
    return euclidian_dist, event_lengths


def find_common_vtx(vertices, max_dist=50):

    # add new column for new vertexID
    out_vtx_dtypes = []
    for j, name in enumerate(vertices.dtype.names):
        out_vtx_dtypes.append((vertices.dtype.names[j], vertices.dtype[name].type))
    out_vtx_dtypes.extend([('multi_vertexID', np.int16), ])
    vertex_array = np.full(fill_value = np.nan, shape = vertices.shape, dtype = out_vtx_dtypes)
    vertex_array["multi_vertexID"] = -1
    event_numbers, event_start_indices, event_lengths = np.unique(vertices["event_number"], return_index = True, return_counts = True)

    logging.info("loop through events")
    for i,index in enumerate(tqdm(event_start_indices)):
        # remove duplicate 2-track vertex lines
        event_indices = [0]
        full_event = vertices[index:index+event_lengths[i]]
        mask = np.zeros(shape = full_event.shape, dtype = bool)
        vID = full_event[0]["vertexID"]
        for j,row in enumerate(full_event[1:]):
            if row["vertexID"] == vID:
                mask[j+1] = True
            else:
                vID = row["vertexID"]
                event_indices.append(j+1)
        
        event = full_event[mask]
        pos = np.array([event["x"],event["y"], event["z"]]).T * 1e4

        # in case the event has only one 2-track vertex, the euclidian distance can not be calculated and flusterdata returns a ValueError. So manually assign cluster =1
        if pos.shape[0] == 1:
            clusters=[1]
        else:
            clusters = hcluster.fclusterdata(pos, max_dist, criterion="distance")
        # except ValueError:
        #     clusters = [1]
            
        # assign clusterID to vertices, fill new array
        for k, cluster in enumerate(clusters):
            vertex_array[index+event_indices[k]]["multi_vertexID"] = cluster
        for dtype in full_event.dtype.names:
            vertex_array[index:index+event_lengths[i]][dtype] = full_event[:][dtype]

    # cmap = cm.get_cmap('viridis')
    # markers = [".", ",", "v", "^", "<", "1", "2", "3", "4", "8", "s", "p", "P", "*", "h", "H", "+", "x", "X", "d", "D", "_",2,4,5,6,7,8,9,10,11]
    # random.shuffle(markers)
    # plot_colors = []
    # plot_markers = []

    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # for i, cluster in enumerate(clusters):

    #     plot_colors.append(cmap(1/n_cluster*cluster))
    # #     plot_markers.append(markers[cluster])
    # #     points = [*np.transpose(pos[i])]
    # #     ax.scatter(points[0], points[2], points[1], marker=markers[cluster], c="tab:blue", s=100)

    # ax.scatter(*np.transpose(pos), c=plot_colors, s=100, alpha = 1)
    # title = "threshold: %.1f $\mathrm{\mu}$m, number of clusters: %d" % (max_dist, n_cluster)
    # ax.view_init(elev=35., azim=45.)
    # ax.set_xlabel("x [$\mathrm{\mu}$m]")
    # ax.set_ylabel("z [$\mathrm{\mu}$m]")
    # ax.set_zlabel("y [$\mathrm{\mu}$m]")
    # ax.set_title(title)
    # plt.show()

    vertex_array.sort(order =["event_number", "multi_vertexID"])
    return vertex_array



if __name__ == '__main__':
    vtx_file = "/Users/niko/Desktop/charm_exp_2018/CH1R6/calculate_vertices/pix_vertices.h5"
    vertices = load_table(vtx_file)

    multi_vtcs = find_common_vtx(vertices, max_dist = 50)
    distances, lengths = calc_dist_all_combinations(vertices)

    save_table(multi_vtcs, path = "/Users/niko/Desktop/charm_exp_2018/CH1R6/calculate_vertices/pix_multi_vertices.h5", node_name ="Vertices")
    save_array([distances, lengths], path = "/Users/niko/Desktop/charm_exp_2018/CH1R6/calculate_vertices/pix_vertices_distances.h5", node_name =["distances","lengths"])

    # distances = load_array(in_file_path = "/Users/niko/Desktop/charm_exp_2018/CH1R6/calculate_vertices/pix_vertices_distances.h5", table_name="distances")
    # lengths = load_array(in_file_path = "/Users/niko/Desktop/charm_exp_2018/CH1R6/calculate_vertices/pix_vertices_distances.h5", table_name="lengths")
    # multi_vtcs = load_table("/Users/niko/Desktop/charm_exp_2018/CH1R6/calculate_vertices/pix_multi_vertices.h5")

    with PdfPages("/Users/niko/Desktop/charm_exp_2018/CH1R6/calculate_vertices/pix_vertices_distances.pdf") as out_pdf:
        hist, bins = np.histogram(np.clip(distances,0,0.599999), bins = np.linspace(0,0.5,200))
        plot_1d_bar(hist_in=hist, output_pdf=out_pdf, bar_edges=bins, log=False, title= "CH1R6 2-track vertex distances in events", xlabel="euclidian distance [cm]")
        nvtx_hist, nvtx_bins = np.histogram(np.clip(lengths,1,80), bins = np.arange(1,81))
        plot_1d_bar(hist_in = nvtx_hist, output_pdf=out_pdf, bar_edges=nvtx_bins, title ="CH1R6 2-track vertices - %i entries" %nvtx_hist.sum(), xlabel ="# of vtx/event")
        multi_vtx_hist, multi_vtx_bins = np.histogram(multi_vtcs["multi_vertexID"], bins = np.arange(0,100))
        plot_1d_bar(hist_in = multi_vtx_hist, output_pdf=out_pdf, bar_edges=multi_vtx_bins, title ="CH1R6 vertices - %i entries" %multi_vtx_hist.sum(), xlabel ="vertices / event")
