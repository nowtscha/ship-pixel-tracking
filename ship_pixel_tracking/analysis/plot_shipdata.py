import numpy as np
import tables as tb
import re
import os.path
import logging

from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_1d_bar, plot_2d_hist


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

def _plot_hits(hit_array, output_pdf, out_file = None, choose_module1=True, all_detectors = False):
    hit_array = hit_array[hit_array["hitz"]!=0]

    binsx = np.linspace(hit_array["hitx"].min(),hit_array["hitx"].max(),120)
    binsy = np.linspace(hit_array["hity"].min(),hit_array["hity"].max(),120)
    if choose_module1:
        histx, _ = np.histogram(hit_array["hitx"][np.logical_and(hit_array["hitz"]>126.5,hit_array["hitz"]<128)], bins = binsx)
        histy, _ = np.histogram(hit_array["hitx"][np.logical_and(hit_array["hitz"]>126.5,hit_array["hitz"]<128)], bins = binsy)
    else:
        histx, _ = np.histogram(hit_array["hitx"], bins = binsx)
        histy, _ = np.histogram(hit_array["hity"], bins = binsy)

    logging.info("create bar diagrams")
    if all_detectors:
        plot_1d_bar(hist_in=histx, output_pdf=output_pdf, bar_edges = binsx, xlim=None, xlabel = "x [cm]", title = "CH2R1 S10 all hits - %s entries" % histx.sum(), legend = None, log = False, output_png =out_file + "_x.png")
        plot_1d_bar(hist_in=histy, output_pdf=output_pdf, bar_edges = binsy, xlim=None, xlabel = "y [cm]", title = "CH2R1 S10 all hits - %s entries" % histy.sum(), legend = None, log = False, output_png =out_file + "_y.png")
    else:
        plot_1d_bar(hist_in=histx, output_pdf=output_pdf, bar_edges = binsx, xlim=None, xlabel = "x [cm]", title = "CH2R1 S10 pixel hits - %s entries" % histx.sum(), legend = None, log = False, output_png =out_file + "_x.png")
        plot_1d_bar(hist_in=histy, output_pdf=output_pdf, bar_edges = binsy, xlim=None, xlabel = "y [cm]", title = "CH2R1 S10 pixel hits - %s entries" % histy.sum(), legend = None, log = False, output_png =out_file + "_y.png")

    logging.info("plotting single events")

    for eventnumber in [88,111,4374,4652,6595,12628]:
        single_event = hit_array[hit_array["event"]==eventnumber]
        dthits = single_event["hitx"][single_event["subdetector"]==3].shape[0]
        pixhits = single_event["hitx"][single_event["subdetector"]==1].shape[0]
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.grid()
        ax.set_title("CH2R1 S10 - event %s" % eventnumber)
        ax.set_ylabel("x [cm]")
        ax.set_xlabel("z [cm]")
        if all_detectors:
            ax.set_title("CH2R1 S10 - event %s all detectors" % eventnumber)
            ax.plot(single_event["hitz"],single_event["hitx"], linestyle = "None", marker = "o", markersize = 5, label = "DT hits: %s\nPix hits: %s" % (dthits, pixhits))
            ax.legend()
        else:
            ax.set_title("CH2R1 S10 - event %s pixel" % eventnumber)
            ax.set_ylim(-2.2,2.2)
            ax.plot(single_event["hitz"],single_event["hitx"], linestyle = "None", marker = "o", markersize = 5)
        output_pdf.savefig(fig)
        if out_file is not None:
            fig.savefig(out_file + "_event_%s.png" %eventnumber)

        xbins = np.arange(-1.5,2.9,0.025)
        ybins = np.arange(-2.1,2.1,0.025)
        bins = np.array((xbins,ybins))
        if all_detectors:
            xy_hist,_,_ = np.histogram2d(single_event["hitx"], single_event["hity"], bins = bins)
            plot_2d_hist(hist2d = xy_hist, output_pdf=output_pdf, bins = bins, xlabel = "x [cm]", ylabel = "y [cm]", title = "Event %s" % eventnumber,output_png =out_file + "_%s_xy_hist_all.png" % eventnumber )
        else:
            xy_hist,_,_ = np.histogram2d(single_event["hitx"], single_event["hity"], bins = bins)
            plot_2d_hist(hist2d = xy_hist, output_pdf=output_pdf, bins = bins, xlabel = "x [cm]", ylabel = "y [cm]", title = "Event %s" % eventnumber,output_png =out_file + "_%s_xy_hist_pix.png" %eventnumber )

    logging.info("create 2d histograms")
    # xbins = np.concatenate((np.array([-2.0450]),np.arange(-1.9950,-0.0200,0.0250),np.array([0]),np.arange(0.0450,2.0200,0.0250),np.array([2.0450])))
    # ybins = np.array([-1.6800 + i*0.005 for i in range(0,336)] + [i*0.005 for i in range(0,337)])
    xbins = np.arange(-1.5,2.9,0.025)
    ybins = np.arange(-2.1,2.1,0.025)
    zbins = np.linspace(125,142,100)
    if all_detectors:
        xbins = np.arange(-125,125,0.5)
        ybins = np.arange(-10,70,0.5)
        zbins = np.linspace(110,800,350)
    bins = np.array((zbins,xbins))
    xz_hist,_,_ = np.histogram2d(hit_array["hitz"], hit_array["hitx"], bins = bins)

    plot_2d_hist(hist2d = xz_hist, output_pdf=output_pdf, bins = bins, xlabel = "z [cm]", ylabel = "x [cm]", title = "CH2R1 S10 Occupancy - %i entries"% xz_hist.sum(),output_png =out_file + "_xz_hist.png" )
    bins = np.array((zbins,ybins))
    yz_hist,_,_ = np.histogram2d(hit_array["hitz"], hit_array["hity"], bins = bins)
    plot_2d_hist(hist2d = yz_hist, output_pdf=output_pdf, bins = bins, xlabel = "z [cm]", ylabel = "y [cm]", title = "CH2R1 S10 Occupancy - %i entries"% yz_hist.sum(), output_png =out_file + "_yz_hist.png")


def plot_hits(hit_file_in):
    output_folder = os.path.split(hit_file_in)[0]
    out_file = os.path.join(output_folder,"pixel_hits")
    with tb.open_file(hit_file_in) as in_file_h5:
        hits = in_file_h5.root.Hits[:]
        with PdfPages(out_file + ".pdf") as output_pdf:
            logging.info("plotting pixel hits")
            pixel_hits = hits[hits["subdetector"]==1]
            _plot_hits(hit_array = pixel_hits, output_pdf = output_pdf, out_file = out_file, choose_module1=False, all_detectors = False)
        logging.info("output saved at %s" % out_file + ".pdf")
        out_file = os.path.join(output_folder,"all_hits")
        with PdfPages(out_file + ".pdf") as output_pdf:
            logging.info("plotting all hits")
            _plot_hits(hit_array = hits, output_pdf = output_pdf, out_file = out_file, choose_module1=False, all_detectors = True)
        logging.info("output saved at %s" % out_file + ".pdf")

if __name__ == '__main__':
    plot_hits("/Users/Niko/Desktop/charm_exp_2018/CH2R1_spill_10_fairship_positions_hits.h5")
