import numpy as np
import tables as tb
import logging
import os
import warnings


from numba import njit
from numba.typed import List
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_xy, plot_xy_2axis
from beam_telescope_analysis.tools import analysis_utils


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def load_rpc_tracks(in_file):
    '''
    load pytables file of rpc tracks with timestamps and fitted x,y track positions
    '''
    logging.info("load rpc tracks from %s" % in_file)
    with tb.open_file(in_file, "r") as in_file_h5:
        rpc_tracks = in_file_h5.root.RPCTracks[:]
    return rpc_tracks


def load_cluster_table(in_file, spill=None, unique=False, table_name=None):
    #TDOO: chunking
    logging.info("load pixel cluster from %s" % in_file)
    with tb.open_file(in_file, "r") as in_file_h5:
        for table in in_file_h5.walk_nodes('/', classname='Table'):
            if table.name == table_name:
                clusters = table[:]
    if unique:
        _, unique_indices = np.unique(clusters["event_number"], return_index=True)
        return clusters[unique_indices]
    else:
        return clusters


def get_unique_events(clusters):
    _, unique_indices = np.unique(clusters["event_number"], return_index=True)
    return clusters[unique_indices]


def choose_matched_events(cluster_in, common_events):
    cluster_out = []  # List()
    i = 0
    for row in cluster_in:
        while (row["event_number"] > common_events[i]["pixel_event"]) and (i < common_events.size - 1):
            i += 1
        if (row["event_number"] == common_events[i]["pixel_event"]):
            cluster_out.append(row)
        elif (row["event_number"] < common_events[i]["pixel_event"]):
            continue
    return np.array(cluster_out)


@njit()
def find_closest_ts(cluster_table, rpc_ts, common_in, delta_t=10000, fit_params=np.array([0., 1.])):
    '''
            find the closest match of a pixel timestamp to any rpc timestamp and save the time difference
            allowed maximal distance delta_t in ns
            ALL TIMESTAMPS ARE SAVED IN UNITS OF 1 ns
    '''
    j = 0
    matched_event_index = 0
    for i, timestamp in enumerate(cluster_table["timestamp"]):
        matched_event_index = j
        while (j < rpc_ts.shape[0]-1) and (np.abs(timestamp*25 - rpc_ts[j]["timestamp"]*10 - timestamp*25*fit_params[1]*1e-9 - fit_params[0]) > delta_t):
            j += 1
        # in case pixel event can not be matched, continue with next pixel event and start at last valid rpc index
        if j == (rpc_ts.shape[0]-1) and (i < cluster_table.shape[0]):
            j = matched_event_index
            continue

        common_in[j]["rpc_event"] = rpc_ts[j]["event_number"]
        common_in[j]["rpc_ts"] = rpc_ts[j]["timestamp"]*10
        common_in[j]["pixel_event"] = cluster_table[i]["event_number"]
        common_in[j]["pixel_ts"] = cluster_table[i]["timestamp"]*25
        common_in[j]["delta_t"] = timestamp*25 - rpc_ts[j]["timestamp"]*10 - timestamp*25*fit_params[1]*1e-9 - fit_params[0]

    # remove empty rows and fill new matched event number
    common_out = common_in[common_in["rpc_event"] != -1]
    for k, row in enumerate(common_out):
        row["matched_event_number"] = k
    return common_out


def match_rpc(rpc_ts_in=None, cluster_table_in=None, clusters_in=None, spill=None):
    '''
            match timestamps of rpc with pixel on per spill basis
            writes output file in same folder as clusters_in
            ----------
            INPUT:
            rpc_ts_in: string
                    path to rpc timestamp file
            clusters_in: string
                    path to pixel merged cluster file
            spill: int
                    number of pixel spill to match
    '''

    # Run 2863 - CHARM0
    # s1f27ef8d -> spill 522710925 -> spill 27

    # rpc_ts = np.loadtxt(rpc_ts_in)

    logging.info("matching spill %s" % spill)

    out_folder_path = os.path.dirname(os.path.realpath(rpc_ts_in))
    matched_clusters_out_path = clusters_in[:clusters_in.find("spill")+5] + "_%s_matched_rpc.h5" % spill
    rpc_output = rpc_ts_in[:-4] + "_matched.dat"
    pdf_path = out_folder_path + "_spill_%s_ts_distances.pdf" % spill

    rpc_ts = load_rpc_tracks(rpc_ts_in)
    cluster_table = cluster_table_in[cluster_table_in["spill"] == spill]

    with PdfPages(pdf_path) as output_pdf:
        for delta_t in [10000]:
            common_events_in = np.full(fill_value=-1, shape=rpc_ts.shape[0], dtype=[("rpc_event", np.int64), ("rpc_ts", np.int64),
                                                                                    ("pixel_event", np.int64), ("pixel_ts", np.int64), ("delta_t", np.int64), ("matched_event_number", np.int64)])
            logging.info("checking with delta t of %s" % delta_t)
            unique_clusters = get_unique_events(cluster_table)

            common_events_out = find_closest_ts(cluster_table=unique_clusters, rpc_ts=rpc_ts, common_in=common_events_in, delta_t=delta_t)

            if common_events_out.shape[0] == 0:
                warnings.warn("no matching events in spill %s - continue with next spill" % spill)
                return

            plot_xy(common_events_out["rpc_event"], common_events_out["pixel_event"], output_pdf=output_pdf,
                    xlim=None, xlabel="rpc event", ylim=None, ylabel="pixel event",
                    title="Run 2863: RPC pixel event correlation - %s entries" % common_events_out.shape[0],
                    legend="events with $\Delta t$ < %s ns" % delta_t, output_png=None)

            plot_xy(common_events_out["delta_t"], common_events_out["pixel_ts"]*1e-9, output_pdf=output_pdf,
                    xlim=[-(delta_t*1.05), delta_t*1.05], title="delta_t vs. timestamp",
                    xlabel="$\Delta$ t [ns]", ylabel="pixel timestamp [s]", markersize=1)

            popt, pcov = plot_xy(common_events_out["pixel_ts"]*1e-9, common_events_out["delta_t"], output_pdf=output_pdf, xlim=[0, 5],
                                 ylim=[-10000, 2000], title="delta_t vs. timestamp", ylabel="$\Delta$ t [ns]",
                                 xlabel="pixel timestamp [s]",
                                 markersize=1, linfit=True, legend="$\Delta t$",
                                 fit_data=[common_events_out["pixel_ts"][common_events_out["delta_t"] < 0]*1e-9,
                                           common_events_out["delta_t"][common_events_out["delta_t"] < 0]
                                           ],
                                 output_png=pdf_path[:-4] + "_delta_t_fit.png")
            plot_xy_2axis(x=common_events_out["pixel_ts"]*1e-9, y=common_events_out["delta_t"], array_in=cluster_table["timestamp"]*25*1e-9, bins=50,
                          output_pdf=output_pdf, xlim=[0, 5], ylim=[-10000, 2000],
                          title="corrected delta_t vs. timestamp", ylabel="$\Delta$ t [ns]",
                          xlabel="pixel timestamp [s]", markersize=1, legend="$\Delta t$",
                          linfit=True,
                          fit_data=[common_events_out["pixel_ts"][common_events_out["delta_t"] < 0]*1e-9,
                                    common_events_out["delta_t"][common_events_out["delta_t"] < 0]
                                    ],
                          output_png=pdf_path[:-4] + "_delta_t_hist_and_fit.png")
            corr_events_in = np.full(fill_value=-1, shape=rpc_ts.shape[0], dtype=[("rpc_event", np.int64), ("rpc_ts", np.int64),
                                                                                  ("pixel_event", np.int64), ("pixel_ts", np.int64), ("delta_t", np.int64), ("matched_event_number", np.int64)])
            common_events_corr = find_closest_ts(cluster_table=unique_clusters, rpc_ts=rpc_ts, common_in=corr_events_in, delta_t=750, fit_params=np.array(popt))

            plot_xy(common_events_corr["pixel_ts"]*1e-9, common_events_corr["delta_t"], output_pdf=output_pdf, xlim=[0, 5],
                    ylim=[-1000, 1000], title="corrected delta_t vs. timestamp", ylabel="$\Delta$ t [ns]",
                    xlabel="pixel timestamp [s]", markersize=1, legend="$\Delta t$",
                    output_png=pdf_path[:-4] + "_delta_t_corr.png")

            # plot_1d_hist(array_in = common_events_corr["delta_t"] - analysis_utils.linear(common_events_corr["pixel_ts"]*1e-9, *popt),
            # 			output_pdf = output_pdf, bins = 100, range = [0,10000],
            # 			title = "Run 2836 corrected timestamp distances - %s entries" % common_events_corr["delta_t"].shape[0], xlabel = "corrected delta_t", legend = "$t_{px} -t_{RPC} - t_{off}$")

            plot_1d_hist(array_in=common_events_corr["delta_t"], output_pdf=output_pdf, bins=100, range=[-500, 500], log=False,
                         title="Run 2863 - hist $\Delta$ t in spill %s , %s entries" % (spill, common_events_corr.shape[0]),
                         xlabel="$\Delta$ t [ns]", output_png=pdf_path[:-4] + "_delta_t_corr_hist.png")

            # check if pixel events are unique in output_array
            _, counts = np.unique(common_events_corr["pixel_event"], return_counts=True)
            if counts[counts > 1].shape[0] > 0:
                warnings.warn("pixel events used multiple times, decrease delta_t")

        logging.info("saved plots at %s" % pdf_path)
        logging.info("find matched clusters")

    with tb.open_file(out_folder_path + "_matched_spill_%s_raw.h5" % spill, "w") as raw_out_h5:
        matched_events_table = raw_out_h5.create_table(where=raw_out_h5.root, name='MatchedTimestamps', description=common_events_out.dtype,
                                                       title='Matched Pixel and RPC event_numbers and timestamps', filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False)
                                                       )
        matched_events_table.append(common_events_out)
        matched_events_table.flush()
    logging.info("saved matched event_numbers at %s" % out_folder_path + "_matched_spill_%s_raw.h5" % spill)

    cluster_out = choose_matched_events(cluster_table, common_events_corr[common_events_corr["pixel_event"] != -1])
    logging.info("matched %s hits" % cluster_out.shape[0])

    with tb.open_file(matched_clusters_out_path, "w") as out_file_h5:
        merged_cluster_table = out_file_h5.create_table(where=out_file_h5.root, name='MergedClusters', description=cluster_out.dtype,
                                                        title='Merged cluster on event number', filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False)
                                                        )
        merged_cluster_table.append(cluster_out[cluster_out["event_number"] != -1])
        merged_cluster_table.flush()

    logging.info("saved matched pixel events at %s" % matched_clusters_out_path)

    np.savetxt(rpc_output, common_events_corr, delimiter=" ", header="rpc_event rpc_ts pixel_event pixel_ts delta_t", fmt="%i %i %i %i %i %i")
    logging.info("saved rpc event numbers to %s" % rpc_output)


if __name__ == '__main__':

    # cluster_table = load_cluster_table("/media/niko/big_data/charm_testbeam_july18/analysis/output_folder_run_2836/Merged_spills.h5")
    spills = np.loadtxt("/media/niko/big_data/charm_testbeam_july18/rpc_data/run_2836/run_2836/spill_ids.txt", skiprows=1, usecols=0, dtype="str")
    for i, spill in enumerate(spills):
        cluster_table = load_cluster_table(
            "/media/niko/big_data/charm_testbeam_july18/pixel_tracks/run_2836/pix_tracks_spill_%s__hits.h5" % i, table_name="Hits")
        match_rpc(rpc_ts_in="/media/niko/big_data/charm_testbeam_july18/rpc_data/run_2836/run_2836/RPC_RecoTracks_run2836_s%s_tracks.h5" % spill,
                  cluster_table_in=cluster_table,
                  clusters_in="/media/niko/big_data/charm_testbeam_july18/analysis/output_folder_run_2836/Merged_spills.h5",
                  spill=i
                  )
