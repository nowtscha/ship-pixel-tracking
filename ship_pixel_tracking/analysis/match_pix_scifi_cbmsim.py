import numpy as np
import tables as tb
import logging
import os
import warnings


from numba import njit
from numba.typed import List
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_xy, plot_xy_2axis, plot_xy_confidence_band
from beam_telescope_analysis.tools import analysis_utils


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def load_cbmsim_events(in_file):
    '''
    load pytables file of rpc tracks with timestamps and fitted x,y track positions
    '''
    logging.info("load cbmsim events from %s" % in_file)
    with tb.open_file(in_file, "r") as in_file_h5:
        events = in_file_h5.root.Hits[:]
    return events


def load_cluster_table(in_file, spill=None, unique=False, table_name=None):
    #TDOO: chunking
    logging.info("load pixel cluster from %s" % in_file)
    with tb.open_file(in_file, "r") as in_file_h5:
        for table in in_file_h5.walk_nodes('/', classname='Table'):
            if table.name == table_name:
                clusters = table[:]
    if unique:
        _, unique_indices = np.unique(clusters["event_number"], return_index=True)
        return clusters[unique_indices]
    else:
        return clusters


def get_unique_events(clusters):
    _, unique_indices = np.unique(clusters["event_number"], return_index=True)
    return clusters[unique_indices]


def choose_matched_events(cluster_in, common_events):
    cluster_out = []  # List()
    i = 0
    for row in cluster_in:
        while (row["event_number"] > common_events[i]["pixel_event"]) and (i < common_events.size - 1):
            i += 1
        if (row["event_number"] == common_events[i]["pixel_event"]):
            cluster_out.append(row)
        elif (row["event_number"] < common_events[i]["pixel_event"]):
            continue
    return np.array(cluster_out)


@njit()
def find_closest_ts(cluster_table, cbmsim_in, common_in, delta_t=10000, fit_params=np.array([0., 1.])):
    '''
            find the closest match of a pixel timestamp to any rpc timestamp and save the time difference
            allowed maximal distance delta_t in ns
            ALL TIMESTAMPS ARE SAVED IN UNITS OF 1 ns
    '''
    j = 0
    matched_event_index = 0
    for i, timestamp in enumerate(cluster_table["timestamp"]):
        matched_event_index = j
        while (j < cbmsim_in.shape[0]-1) and (np.abs(timestamp*25 - cbmsim_in[j]["scifi_hit_time"]*25) > delta_t):
            j += 1
        # in case pixel event can not be matched, continue with next pixel event and start at last valid rpc index
        if j == (cbmsim_in.shape[0]-1) and (i < cluster_table.shape[0]):
            j = matched_event_index
            continue

        common_in[j]["scifi_event"] = cbmsim_in[j]["event"]
        common_in[j]["scifi_header_time"] = cbmsim_in[j]["event_time"]
        common_in[j]["pixel_event"] = cluster_table[i]["event_number"]
        common_in[j]["pixel_ts"] = cluster_table[i]["timestamp"]*25
        common_in[j]["scifi_detID"] = cbmsim_in[j]["scifi_detID"]
        common_in[j]["delta_t"] = timestamp*25 - cbmsim_in[j]["scifi_hit_time"]*25
        common_in[j]["scifi_hit_time"] = cbmsim_in[j]["scifi_hit_time"]
        common_in[j]["scifi_fine_time"] = cbmsim_in[j]["scifi_fine_time"]

    # remove empty rows and fill new matched event number
    common_out = common_in[common_in["scifi_event"] != -1]
    # for k, row in enumerate(common_out):
    # 	row["matched_event_number"] = k
    return common_out


def match_cbmsim_ts(cbmsim_in_path=None, cluster_table_in=None, clusters_in=None, spill=None):
    '''
            match timestamps of rpc with pixel on per spill basis
            writes output file in same folder as clusters_in
            ----------
            INPUT:
            rpc_ts_in: string
                    path to rpc timestamp file
            clusters_in: string
                    path to pixel merged cluster file
            spill: int
                    number of pixel spill to match
    '''

    # Run 2863 - CHARM0
    # s1f27ef8d -> spill 522710925 -> spill 27

    # rpc_ts = np.loadtxt(rpc_ts_in)

    logging.info("matching spill %s" % spill)

    out_folder_path = os.path.dirname(os.path.realpath(cbmsim_in_path)) + "/"
    # clusters_in[:clusters_in.find("spill")+5] + "_%s_matched_cbmsim.h5" % spill
    matched_clusters_out_path = "/home/niko/cernbox/storage/run2793_pix_tracks_spill_%s_scifi_matched.h5" % spill
    cbmsim_output = cbmsim_in_path[:-4] + "_matched.dat"
    pdf_path = out_folder_path + "_spill_%s_ts_distances.pdf" % spill

    cluster_table = cluster_table_in[cluster_table_in["spill"] == spill]
    cbmsim_in = load_cbmsim_events(cbmsim_in_path)
    with PdfPages(pdf_path) as output_pdf:
        for delta_t in [10000]:
            common_events_in = np.full(fill_value=-1, shape=cbmsim_in.shape[0], dtype=[("scifi_event", np.int64), ("scifi_header_time", np.int64),
                                                                                       ("pixel_event", np.int64), ("pixel_ts",
                                                                                                                   np.int64), ("delta_t", np.int64), ("scifi_detID", np.int64),
                                                                                       ("scifi_hit_time", np.uint32), ("scifi_fine_time", np.uint16)]
                                       )
            logging.info("checking with delta t of %s" % delta_t)
            unique_clusters = get_unique_events(cluster_table)

            common_events_out = find_closest_ts(cluster_table=unique_clusters, cbmsim_in=cbmsim_in, common_in=common_events_in, delta_t=delta_t)

            if common_events_out.shape[0] == 0:
                warnings.warn("no matching events in spill %s - continue with next spill" % spill)
                return

            plot_xy(common_events_out["scifi_event"], common_events_out["pixel_event"], output_pdf=output_pdf,
                    xlim=None, xlabel="cbmsim event", ylim=None, ylabel="pixel event",
                    title="Run 2793: cbmsim pixel event correlation - %s entries" % common_events_out.shape[0],
                    legend="events with $\Delta t$ < %s ns" % delta_t, output_png=pdf_path[:-4] + "matched_events_correlation.png")

            diffs = np.diff(common_events_out["scifi_event"])
            missing_event_mask = diffs > 1
            print(common_events_out[:-1]["scifi_event"][missing_event_mask])

            plot_xy(common_events_out[:-1]["scifi_event"][missing_event_mask], common_events_out[:-1]["pixel_event"][missing_event_mask], output_pdf=output_pdf,
                    title="Missing events in pixel - %s entries" % common_events_out[:-1]["scifi_event"][missing_event_mask].shape[0],
                    output_png=pdf_path[:-4] + "missing_events_correlation.png")

            plot_xy(common_events_out["delta_t"], common_events_out["pixel_ts"]*1e-9, output_pdf=output_pdf,
                    xlim=[-(delta_t*1.05), delta_t*1.05], title="delta_t vs. timestamp",
                    xlabel="$\Delta$ t [ns]", ylabel="pixel timestamp [s]", markersize=1)

            plot_xy(common_events_out["pixel_ts"]*1e-9, common_events_out["delta_t"], output_pdf=output_pdf, xlim=None,
                    ylim=[-50, 50], title="s522366760: time between pixel timestamp and eventbuilder timestamp", ylabel="$\Delta$ t [ns]",
                    xlabel="pixel timestamp [s]",
                    markersize=1, linfit=True, legend="$\Delta t$",
                    fit_data=[common_events_out["pixel_ts"]*1e-9,
                              common_events_out["delta_t"]
                              ],
                    output_png=pdf_path[:-4] + "_delta_t_fit.png")
            plot_xy_2axis(x=common_events_out["pixel_ts"]*1e-9, y=common_events_out["delta_t"], array_in=cluster_table["timestamp"]*25*1e-9, bins=50,
                          output_pdf=output_pdf, xlim=[0, 5], ylim=[-100, 100],
                          title="s522366760: correlation between delta t and occupancy", ylabel="$\Delta$ t [ns]", y2label="nhits",
                          xlabel="pixel timestamp [s]", markersize=1, legend="$\Delta t$",
                          linfit=True,
                          fit_data=[common_events_out["pixel_ts"]*1e-9,
                                    common_events_out["delta_t"]
                                    ],
                          output_png=pdf_path[:-4] + "_delta_t_hist_and_fit.png")

            n_hits_hist, n_hits_edges = np.histogram(cluster_table["timestamp"]*25*1e-9, bins=100)
            y_vals = diffs[missing_event_mask]  # np.ones(shape = common_events_out[:-1]["pixel_ts"][missing_event_mask].shape)
            plot_xy_2axis(x=common_events_out[:-1]["scifi_header_time"][missing_event_mask]*1e-9, y=y_vals,
                          array_in=cluster_table["timestamp"]*25*1e-9, bins=100,
                          output_pdf=output_pdf, xlim=[0, 5], ylim=[-1, 5], y2lim=[0, n_hits_hist.max()*1.05],
                          title="s522366760: missing events - %s entries" % common_events_out[:-1]["scifi_event"][missing_event_mask].shape[0],
                          ylabel="$\Delta$ evnum", y2label="nhits",
                          xlabel="pixel timestamp [s]", markersize=2, legend="$\Delta$ evnum", legend2="nhits per timestamp",
                          linfit=False,
                          output_png=pdf_path[:-4] + "_delta_t_hist_missing_evts.png")

            plot_1d_hist(common_events_out["delta_t"], output_pdf=output_pdf, bins=50,
                         title="s522366760: delta t histogram - %s entries" % common_events_out["delta_t"].shape[0],
                         xlabel="$\Delta$ t [ns]", output_png=pdf_path[:-4] + "_delta_t_hist.png")

            # check if pixel events are unique in output_array
            _, counts = np.unique(common_events_out["pixel_event"], return_counts=True)
            if counts[counts > 1].shape[0] > 0:
                warnings.warn("pixel events used multiple times, decrease delta_t")

        logging.info("saved plots at %s" % pdf_path)
        logging.info("find matched clusters")

    with tb.open_file(out_folder_path + "_matched_spill_%s_raw.h5" % spill, "w") as raw_out_h5:
        matched_events_table = raw_out_h5.create_table(where=raw_out_h5.root, name='MatchedTimestamps', description=common_events_out.dtype,
                                                       title='Matched Pixel and cbmsim event_numbers and timestamps', filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False)
                                                       )
        matched_events_table.append(common_events_out)
        matched_events_table.flush()
    logging.info("saved matched event_numbers at %s" % out_folder_path + "_matched_spill_%s_raw.h5" % spill)

    cluster_out = choose_matched_events(cluster_table, common_events_out[common_events_out["pixel_event"] != -1])
    logging.info("matched %s hits" % cluster_out.shape[0])

    with tb.open_file(matched_clusters_out_path, "w") as out_file_h5:
        merged_cluster_table = out_file_h5.create_table(where=out_file_h5.root, name='MergedClusters', description=cluster_out.dtype,
                                                        title='Merged cluster on event number', filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False)
                                                        )
        merged_cluster_table.append(cluster_out[cluster_out["event_number"] != -1])
        merged_cluster_table.flush()

    logging.info("saved matched pixel events at %s" % matched_clusters_out_path)

    np.savetxt(cbmsim_output, common_events_out, delimiter=" ",
               header="scifi_event scifi_header_time pixel_event pixel_ts scifi_detID delta_t scifi_hit_time scifi_fine_time", fmt="%i %i %i %i %i %i %i %i")
    logging.info("saved cbmsim event numbers to %s" % cbmsim_output)


if __name__ == '__main__':

    cluster_table = load_cluster_table("/home/niko/git/ship_tracking/data/run_2793/pix_tracks_spill_%s_hits.h5" % 10, table_name="Hits")
    match_cbmsim_ts(cbmsim_in_path="/media/niko/big_data/charm_testbeam_july18/combined_data/scifi_data/RUN_0900_2793/SPILLDATA_0900_0522366760_scifi_hits.h5",
                    cluster_table_in=cluster_table,
                    clusters_in="/media/niko/big_data/charm_testbeam_july18/analysis/output_folder_run_2836/Merged_spills.h5",
                    spill=10
                    )
