import numpy as np
import re
import os
import tables as tb
import logging

from tqdm import tqdm
from numba import jit
from scipy import stats

from matplotlib.backends.backend_pdf import PdfPages

from beam_telescope_analysis.tools import analysis_utils
from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_1d_bar, plot_2d_hist, plot_xy, plot_residuals_vs_position, create_polyplot_fig, save_polyplot_fig, plot_2dhist_subplot

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


@jit(nopython=True)
def _build_tracks_file(eff_tracks, eff_tracks_hitx, eff_tracks_hity, eff_tracks_hitused, eff_tracks_cluster_size, tracks, hits, n_tracks, n_hits, z_offset, refmom):
    '''
    Actual working function for matching tracks and hits. Since there are typically more hits than tracks, the main loop increments tracks.
    For every track hits in the same event are looped to look wethere there is one on the investigated plane. 
    One can use the fact that both, hits and tracks are ordered by event_number. 
    So after matching a hit in the event, the index of the hit table is saved and matching the next track is started there.
    At the moment correct results are only guaranteed for single track events.
    If there is more than one hit on the considered plane, the closest hit is taken and assigned to the track. 
    Hits can not be assigned multiple times.
    ----------------
    INPUT:
        eff_tracks : structured np.array
            prepared empty array of lengt = n_tracks, matched hits and tracks will be stored here.
        eff_tracks_hit_x : structured np.array
            Only hit_x column of eff_tracks. Due to limitations of njit it is easier to give this as input compared to indexint it inside the loop.
        eff_tracks_hit_y : structured np.array
            Only hit_y column of eff_tracks. Due to limitations of njit it is easier to give this as input compared to indexint it inside the loop.
        eff_tracks_hitused : structured np.array
            Only hitused column of eff_tracks. Due to limitations of njit it is easier to give this as input compared to indexint it inside the loop.
        tracks : structured np.array
            input tracks file with one track per row
        hits : structured np.array
            input hits file with one hit per row
        n_tracks : int
            length of tracks. Necessary for njit loop length
        n_hits : int
            length of hits. Necessary for njit while loop
        z_offset : float
            z position of investigated plane, for track prediction
        refmom : float
            assumend particle momentum in GeV, for track slope prediction
    '''

    k = 0
    i = 0

    for m in range(n_tracks):
        l = 0
        eff_tracks["event_number"][m] = tracks["event_number"][k]
        eff_tracks["timestamp"][m] = tracks["timestamp"][k]
        eff_tracks["spill"][m] = tracks["spill"][k]
        eff_tracks["x"][m] = tracks["x"][k]# + tracks["x_slope"][k] *z_offset/refmom
        eff_tracks["y"][m] = tracks["y"][k]# + tracks["y_slope"][k] *z_offset/refmom
        eff_tracks["z"][m] = 0. #z_offset
        eff_tracks["x_error"][m] = tracks["x_error"][k]
        eff_tracks["y_error"][m] = tracks["y_error"][k]
        eff_tracks["x_slope"][m] = tracks["x_slope"][k]
        eff_tracks["y_slope"][m] = tracks["y_slope"][k]
        eff_tracks["x_slope_error"][m] = tracks["x_slope_error"][k]
        eff_tracks["y_slope_error"][m] = tracks["y_slope_error"][k]
        eff_tracks["covariance"][m] = tracks["red_chi2"][k]
        eff_tracks["n_hits"][m] = tracks["n_hits"][k]
        eff_tracks["trackID"][m] = tracks["trackID"][k]

        while (hits["event_number"][i] < eff_tracks["event_number"][m]) and (i < (n_hits-2)):
            i+=1
        while hits["event_number"][i] == eff_tracks["event_number"][m] and (i < n_hits-1):
            dx = np.abs(hits["hitx"][i] - (eff_tracks["x"][m] + eff_tracks["x_slope"][m] *z_offset/refmom))
            dy = np.abs(hits["hity"][i] - (eff_tracks["y"][m] + eff_tracks["y_slope"][m] *z_offset/refmom))
            if l==0:
                best_dx = dx
                best_dy = dy
                best_index = i
            elif ((np.square(dx) + np.square(dy)) < (np.square(best_dx) + np.square(best_dy))) and (hits["hitused"][i]==0):
                best_dx = dx
                best_dy = dy
                best_index = i
            i+=1
            l+=1
        if l>0:
            eff_tracks_hitx[m] = hits["hitx"][best_index]
            eff_tracks_hity[m] = hits["hity"][best_index]
            hits["hitused"][best_index] += 1
            eff_tracks_hitused[m] = hits["hitused"][best_index]
            eff_tracks_cluster_size[m] = hits["cluster_size"][best_index]
            # i -=l
        k+=1


def make_efficiency_input_files(track_files, hit_files, planes, reference_momentum=400):
    '''
    Function to create single input file from hits and tracks to be able to calculate efficiency.
    For speedup only preparations are done here, to feed the jitted function _build_tracks_file where the acutal merging takes place.
    ----------------
    INPUT:
        track_files : list of strings
            path to input track files, has to have same length as planes
            For each plane an extra tracking file is created, 
            where the plane to consider for efficiency calculation is not considered in the track fit to enforce unbiased residuals.
        hit_files : list of strings
            path to input hit files, has to have same length as planes.
            As is the tracking produces tracks and hits in seperate files, for efficiency calculation these have to be merged.
        planes : list of int
            planes to be considered
        reference_momentum : float
            assumend particle momentum for track angle prediction, in GeV
    '''

    z_offset = [-0.13, 0.520, 2.412, 3.09, 5.10, 5.79, 7.79, 8.46, 10.462, 11.170, 13.162, 13.85]
    eff_tracks_dtype = [("event_number", np.int64),
                        ("timestamp", np.int64),
                        ("spill", np.int32),
                        ("trackID", np.int16),
                        ("x", np.float32),
                        ("y",np.float32),
                        ("z", np.float32),
                        ("x_error", np.float32),
                        ("y_error", np.float32),
                        ("x_slope", np.float32),
                        ("y_slope", np.float32),
                        ("x_slope_error", np.float32),
                        ("y_slope_error", np.float32),
                        ("n_hits", np.int32),
                        ("covariance", np.float32),

                        ]

    logging.info("writing arrays for efficiency calculation")
    out_file_path = os.path.dirname(os.path.abspath(track_files[0])) + "/efficiency_input.h5"
    with tb.open_file(out_file_path,"w") as out_file_h5:
        for i, plane in tqdm(enumerate(planes)):
            logging.info("write array for plane %i" %plane)
            with tb.open_file(track_files[i], "r") as tracks_in_h5:
                tracks = tracks_in_h5.root.Tracks[:]
            with tb.open_file(hit_files[i], "r") as hits_in_h5:
                hits = hits_in_h5.root.Hits[:]

            eff_tracks_dtype.extend([('hitx_%s'%plane, np.float32), ('hity_%s'%plane, np.float32), ('hitz_%s'%plane, np.float32), ('hit_%s_used'%plane, np.uint32)])
            eff_tracks_dtype.extend([('cluster_charge_%s' %plane, np.float32), ("cluster_size_plane_%s" %plane, np.int32)])
            eff_tracks = np.full(fill_value = np.nan, shape = tracks.shape, dtype = eff_tracks_dtype)
            # plane_fields = set(eff_tracks.dtype.names) - set('h')
            hit_table_out = out_file_h5.create_table(where=out_file_h5.root,
                                                    name='Tracks_plane_%i' % plane,
                                                    description=eff_tracks.dtype, title='Unbiased tracks for plane %i'% plane,
                                                    filters=tb.Filters(complib='blosc',complevel=5,fletcher32=False)
                                                    )

            
            plane_hits  = hits[["event_number","hitx","hity","hitused","cluster_size"]][hits["hitplane"]==plane]

            _build_tracks_file(eff_tracks = eff_tracks, eff_tracks_hitx = eff_tracks["hitx_%s" %plane], eff_tracks_hity = eff_tracks["hity_%s" %plane],
                               eff_tracks_hitused = eff_tracks["hit_%s_used" %plane], eff_tracks_cluster_size = eff_tracks["cluster_size_plane_%s" %plane], tracks = tracks, hits = plane_hits, n_tracks = tracks.shape[0],
                               n_hits = plane_hits.shape[0], z_offset=z_offset[plane], refmom=reference_momentum,)

            hit_table_out.append(eff_tracks)
            hit_table_out.flush()

    logging.info("saved input file at %s" % out_file_path)
    return out_file_path


def getEfficiency(eff_tracks_file, cut_distance = None, max_angle = None, min_angle=None,
                  res_range = [[-400.,400.],[-500.,500]], plot_planes = [1,2,3,5,6,7,9,10,11],
                  efficiency_regions = None, min_occupancy = 2, track_quality = None, additional_plots = True,
                  ):
    '''
    Function to calculate pixel sensor efficiency and residuals. Of main interest may be the residual plots.
    At the moment only single hit events give trustworthy results
    --------------------
    INPUT:
        eff_tracks_file : string
            path to input file with tracks and associated hits
        cut_distance : list of 2 float
            maximum residual in x and y to count hit as efficient
        max_angle : float, optional
            if given only consider tracks below given angle
        min_angle : float, optional
            if given only consider tracks above given angle.
        res_range : list of 2 lists of floats
            plot range for x and y residuals
        plot_planes : list of ints
            list with plane numbers to consider
        efficiency_regions : list of lists of 2 floats, optional
            if given, additioanl to total sensor effiency also calculate efficiency of given regions in x,y
        min_occupancy : int
            for efficiency calculation only pixel with nhits >= min_occupancy are considered
        track_quality : float, optional
            if given, only consider tracks with reduced chi2 below track_quality
        additional_plots : bool
            if True, plot insightful residual plots and maps.

    '''
    

    plane_alignment = np.zeros(shape = (12,), dtype = [("x_align", np.float64),("y_align", np.float64),("z_align", np.float64)])
    plane_alignment["z_align"] = [-0.13, 0.52, 2.142, 3.09, 5.1, 5.79, 7.79, 8.46, 10.462, 11.17, 13.162, 13.85]
    x_offsets =  [15396 - 8400, -2310 + 8400, 6960, 6940, 15285 - 8400, -2430 + 8400, 6620, 6710, 15440 - 8400, -2505 + 8400, 6455, 6320]
    y_offsets = [-15, 20, 7930 - 8400, -8990 + 8400, -370, -610, 7200 - 8400, -9285 + 8400, -700, 690, 7660 - 8400, -8850 + 8400]

    plane_dimensions = {  # acceptance of single planes [[xmin,xmax], [ymin,ymax]]
                        0 : [[-16800.,0],[-20450,20450]],
                        1 : [[0,16800.],[-20450.,20450.]],
                        2 : [[-20450.,20450.],[-16800.,0]],
                        3 : [[-20450.,20450.],[0,16800.]],
                        4 : [[-16800.,0],[-20450.,20450.]],
                        5 : [[0,16800.],[-20450.,20450.]],
                        6 : [[-20450.,20450.],[-16800.,0]],
                        7 : [[-20450.,20450.],[0,16800.]],
                        8 : [[-16800.,0],[-20450.,20450.]],
                        9 : [[0,16800.],[-20450.,20450.]],
                        10 : [[-20450.,20450.],[-16800.,0]],
                        11 : [[-20450.,20450.],[0,16800.]],
                        }

    if os.path.split(eff_tracks_file)[1].find("spill") >0:
        output_folder = os.path.join(os.path.split(eff_tracks_file)[0], '%s_efficiency' % os.path.split(eff_tracks_file)[1][os.path.split(eff_tracks_file)[1].find("spill"):os.path.split(eff_tracks_file)[1].find("spill")+7])
        single_spill = True
    else:
        output_folder = os.path.join(os.path.split(eff_tracks_file)[0], 'efficiency')
        single_spill=False

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    output_efficiency_file = os.path.join(output_folder,"efficiencies.h5")
    out_pdf_polyplot_name = os.path.join(output_folder,"polyplot_residuals.pdf")

    with tb.open_file(eff_tracks_file, "r") as tracks_in_h5:

        # generate shared subplots for comparing residual plots
        dy_x_fig, dy_x_axes = create_polyplot_fig(cols=3, rows=3, super_title = "y residuals vs. track x",)
        dx_y_fig, dx_y_axes = create_polyplot_fig(cols=3, rows=3, super_title = "x residuals vs. track y")
        dy_y_fig, dy_y_axes = create_polyplot_fig(cols=3, rows=3, super_title = "y residuals vs. track y")
        dx_x_fig, dx_x_axes = create_polyplot_fig(cols=3, rows=3, super_title = "x residuals vs. track x")

        dy_fig, dy_axes = create_polyplot_fig(cols=3, rows=3, super_title = "y residuals",)
        dx_fig, dx_axes = create_polyplot_fig(cols=3, rows=3, super_title = "x residuals")
        # plt.subplots returns 2dim numpy array, flatten for easier access
        dx_x_axes = dx_x_axes.flatten()
        dy_y_axes = dy_y_axes.flatten()
        dx_y_axes = dx_y_axes.flatten()
        dy_x_axes = dy_x_axes.flatten()
        dx_axes = dx_axes.flatten()
        dy_axes = dy_axes.flatten()

        # map interesting planes to subplotpositions
        plot_positions= {0:None, 1:0, 2:1, 3:2, 4:None, 5:3, 6:4, 7:5, 8:None, 9:6, 10:7, 11:8}
        with PdfPages(out_pdf_polyplot_name) as out_pdf_polyplot:
            
            first_plane = True
            for plane_node in tracks_in_h5.root:
                
                plane_index =  int(re.findall(r'\d+', plane_node.name)[-1])
                track_array = plane_node[:]
                if plane_index not in plot_planes:
                    continue
                out_pdf_name = os.path.join(output_folder,"efficiencies_plane_%s.pdf" % plane_index)
                with PdfPages(out_pdf_name) as output_pdf:

                    xbins = np.concatenate((np.array([-20450.]),np.arange(-19950.,-200.,250.),np.array([0]),np.arange(450.,20200.,250.),np.array([20450])))
                    ybins = np.array([-16800 + i*50. for i in range(0,336)] + [i*50. for i in range(0,337)])

                    logging.info("================ calculate efficiency for plane %s ================" % plane_index)
                    
                    if track_quality:
                        size_old = track_array.size
                        track_array = track_array[track_array["covariance"] < track_quality]
                        logging.info("choose track chi2 < %s - use %i/%i tracks" % (track_quality,track_array.size, size_old ))
                    if max_angle:
                        size_old = track_array.size
                        logging.info("choose track tx/ty < %s - use %s/%s" % (max_angle, track_array.size, size_old))
                        track_array = track_array[np.logical_and(track_array["x_slope"]<=max_angle[0], track_array["y_slope"]<=max_angle[1])]
                    if min_angle:
                        size_old = track_array.size
                        logging.info("choose track tx/ty > %s - use %s/%s" % (min_angle, track_array.size, size_old))
                        track_array = track_array[np.logical_and(np.abs(track_array["x_slope"])>=min_angle[0], np.abs(track_array["y_slope"])>=min_angle[1])]
                    if plane_index in [0,1,4,5,8,9]:
                        dimension = [[-16800.,16800.],[-20450,20450.]]
                        bins = np.array((ybins, xbins))
                        shape = np.array([672,160])
                        pixel_size = [50.,250.]
                        plot_x_gauss = True
                    elif plane_index in [2,3,6,7,10,11]:
                        dimension = [[-20450,20450.],[-16800.,16800.]]
                        bins = np.array((xbins, ybins))
                        shape = np.array([160,672])
                        pixel_size = [250.,50.]
                        x_cut = cut_distance[0]
                        y_cut = cut_distance[1]
                        cut_distance = [y_cut, x_cut]
                        plot_x_gauss = False

                    dimension = plane_dimensions[plane_index]

                    bins[0] -= x_offsets[plane_index]
                    bins[1] -= y_offsets[plane_index]

                    total_hit_hist = np.zeros(shape = shape, dtype=np.uint32)
                    total_track_density = np.zeros(shape = shape)
                    total_track_density_with_DUT_hit = np.zeros(shape = shape)

                    logging.info("calculate residuals")
                    x_local = (track_array["hitx_%i" %plane_index ] + plane_alignment["x_align"][plane_index])*1e4
                    y_local = (track_array["hity_%i" %plane_index ] + plane_alignment["y_align"][plane_index])*1e4

                    x = (track_array["x"] + plane_alignment["x_align"][plane_index])*1e4
                    x_slope = track_array["x_slope"]
                    x = x + x_slope*plane_alignment["z_align"][plane_index]
                    y = (track_array["y"] + plane_alignment["y_align"][plane_index])*1e4
                    y_slope = track_array["y_slope"]
                    y = y + y_slope*plane_alignment["z_align"][plane_index]

                    x_residuals = x_local - x
                    y_residuals = y_local - y
                    distance_local = np.sqrt(np.square(x_residuals) + np.square(y_residuals))
                    select_finite_distance = np.isfinite(distance_local)

                    select_valid_hit = ~np.isnan(x_local)
                    select_valid_track = ~np.isnan(x)
                # if cut_distance[0] is not None:
                    xlim = [-x_offsets[plane_index] + plane_dimensions[plane_index][0][0] - cut_distance[0],
                            -x_offsets[plane_index] + plane_dimensions[plane_index][0][1] + cut_distance[0]
                            ]
                    ylim = [-y_offsets[plane_index] + plane_dimensions[plane_index][1][0] - cut_distance[1],
                            -y_offsets[plane_index] + plane_dimensions[plane_index][1][1] + cut_distance[1]
                                ]
                    # else:
                    #     xlim = [-x_offsets[plane_index] + plane_dimensions[plane_index][0][0],
                    #         -x_offsets[plane_index] + plane_dimensions[plane_index][0][1]
                    #         ]
                    #     ylim = [-y_offsets[plane_index] + plane_dimensions[plane_index][1][0],
                    #         -y_offsets[plane_index] + plane_dimensions[plane_index][1][1]
                    #         ]
                    # create mask which selects only area of module. Important to ensure proper eff. calculation
                    xmask = np.logical_and(bins[0][:-1] > xlim[0], bins[0][:-1] < xlim[1])
                    ymask = np.logical_and(bins[1][:-1] > ylim[0],bins[1][:-1] < ylim[1])
                    module_area_mask = ~np.logical_and(ymask[np.newaxis, :],xmask[:, np.newaxis])

                    if cut_distance is not None:
                        if cut_distance[0] >= 2.5 * pixel_size[0] and cut_distance[1] >= 2.5 * pixel_size[1]:  # use ellipse
                            select_valid_hit[select_finite_distance] &= ((np.square(x_residuals[select_finite_distance]) / cut_distance[0]**2) + (np.square(y_residuals[select_finite_distance]) / cut_distance[1]**2)) <= 1
                        else:  # use square
                            select_valid_hit[select_finite_distance] &= (np.abs(x_residuals[select_finite_distance]) <= cut_distance[0])
                            select_valid_hit[select_finite_distance] &= (np.abs(y_residuals[select_finite_distance]) <= cut_distance[1])
                    else:
                        select_valid_hit[select_finite_distance] &= np.logical_and((np.square(x_residuals[select_finite_distance]) / np.square(track_array[select_finite_distance]["x_error"]))<=50. ,
                                                                    (np.abs(y_residuals[select_finite_distance])<450.))# / np.square(track_array[select_finite_distance]["y_error"]))<5.)

                    total_track_density += np.histogram2d(x[select_valid_track],y[select_valid_track], bins = bins, range = dimension)[0]
                    total_track_density_with_DUT_hit += np.histogram2d(x[select_valid_hit], y[select_valid_hit], bins = bins, range = dimension)[0]
                    total_hit_hist += (np.histogram2d(x_local, y_local, bins = bins, range = dimension)[0]).astype(np.uint32)

                    total_track_density = np.ma.array(total_track_density, mask = module_area_mask)
                    total_track_density_with_DUT_hit = np.ma.array(total_track_density_with_DUT_hit, mask = module_area_mask)
                    total_hit_hist = np.ma.array(total_hit_hist, mask = module_area_mask)

                    efficiency = np.zeros_like(total_track_density_with_DUT_hit)
                    efficiency[total_track_density != 0] = total_track_density_with_DUT_hit[total_track_density != 0].astype(np.float) / total_track_density[total_track_density != 0].astype(np.float)

                    plane_hits_hist = np.ma.masked_where(total_hit_hist < 1, total_hit_hist)
                    bad_events = track_array[~select_valid_hit & ~np.isnan(x_local)]

                    logging.info("consider only pixel with occupancy >= %s" % min_occupancy)

                    efficiency = np.ma.array(efficiency, mask=np.logical_or(total_track_density < min_occupancy, module_area_mask))
                    
                    bad_hits_hist, bad_x_bins, bad_y_bins = np.histogram2d(x_local[~select_valid_hit], y_local[~select_valid_hit], bins = bins) #[360,360], 
                                                                            # range= [[-28000,17000],[-20000,20000]])
                    bad_hits_hist = np.ma.masked_where(bad_hits_hist<1, bad_hits_hist)
                    if np.argwhere(efficiency>1.).shape[0]>0:
                        logging.warning("=============== there are efficiencies larger 1. ===============")
                        logging.warning("=============== there are efficiencies larger 1. ===============")
                        logging.warning("=============== there are efficiencies larger 1. ===============")
                    if efficiency_regions:
                        # consider only certain region for mean efficiency
                        if efficiency_regions[plane_index]:
                            select_valid_track_region = select_valid_track & np.logical_and(np.logical_and(efficiency_regions[plane_index][0][0]<= x, x <=efficiency_regions[plane_index][0][1]),
                                                                np.logical_and(efficiency_regions[plane_index][1][0]<= y, y <=efficiency_regions[plane_index][1][1]))
                            select_valid_hit_region = select_valid_hit & np.logical_and(np.logical_and(efficiency_regions[plane_index][0][0]<= x, x <=efficiency_regions[plane_index][0][1]),
                                                                np.logical_and(efficiency_regions[plane_index][1][0]<= y, y <=efficiency_regions[plane_index][1][1]))
                            total_track_density_region = np.histogram2d(x[select_valid_track_region],y[select_valid_track_region], bins = bins, range = dimension)[0]
                            total_track_density_with_DUT_hit_region = np.histogram2d(x[select_valid_hit_region],y[select_valid_hit_region], bins = bins, range = dimension)[0]

                    # mask arrays for mean eff calculation
                    count_eff_pix_array = np.ma.masked_where(total_track_density < min_occupancy, total_track_density) # necessary for correct number of pixels considered in efficiency plot
                    total_track_density = np.ma.masked_where(total_track_density < min_occupancy, total_track_density)
                    total_track_density_with_DUT_hit = np.ma.masked_where(total_track_density_with_DUT_hit < min_occupancy, total_track_density_with_DUT_hit)

                    logging.info("use %i/%i tracks" %(total_track_density.sum(), x.shape[0]))
                    # plot residuals
                    resbinsx = np.linspace(res_range[0][0],res_range[0][1],200)
                    resbinsy = np.linspace(res_range[1][0],res_range[1][1],200)
                    clip_resx = np.clip(x_residuals[~np.isnan(x_residuals)], resbinsx[0], resbinsx[-1])
                    clip_resy = np.clip(y_residuals[~np.isnan(y_residuals)], resbinsy[0], resbinsy[-1])
                    resx_hist, resx_hist_bins = np.histogram(clip_resx, bins = resbinsx)
                    resy_hist, resy_hist_bins = np.histogram(clip_resy, bins = resbinsy)
                    logging.info("plot residuals")

                    # plot_1d_bar(hist_in = resx_hist, output_pdf = output_pdf, ax = dx_axes[plot_positions[plane_index]], fig= dx_fig,
                    #                 bar_edges = resx_hist_bins, xlim = res_range[0],
                    #                 legend = "mean = %.1f $\mu m$" % (np.mean(clip_resx)),
                    #                 title ="x residuals of Plane %s - %s entries" %(plane_index, resx_hist.sum()), xlabel = "[$\mu m$]",
                    #                 log = True, vlines = [-cut_distance[0],cut_distance[0]], gauss_fit = True,
                    #                 output_png = os.path.join(output_folder,"xresiduals_plane_%s.png" %plane_index))

                    # plot_1d_bar(hist_in = resx_hist, output_pdf = output_pdf, ax = dx_axes[plot_positions[plane_index]], fig= dx_fig,
                    #                 bar_edges = resx_hist_bins, xlim = res_range[0],
                    #                 legend = "mean = %.1f $\mu m$" % (np.mean(clip_resx)),
                    #                 title ="x residuals of Plane %s - %s entries" %(plane_index, resx_hist.sum()), xlabel = "[$\mathrm{\mu} \mathrm{m}$]",
                    #                 log = True, vlines = None, gauss_fit = True,
                    #                 output_png = None)

                    plot_1d_bar(hist_in = resx_hist, output_pdf = output_pdf,
                                    bar_edges = resx_hist_bins, xlim = res_range[0],
                                    legend = "mean = %.1f $\mu m$" % (np.mean(clip_resx)),
                                    title ="x residuals of Plane %s - %s entries" %(plane_index, resx_hist.sum()), xlabel = r"$\Delta x$ [$\mathrm{\mu m}$]",
                                    log = False, vlines = None, gauss_fit = plot_x_gauss, box_fit = not plot_x_gauss,
                                    output_png = os.path.join(output_folder,"xresiduals_plane_%s.png" %plane_index))

                    plot_1d_bar(hist_in = resx_hist, output_pdf = output_pdf,
                                    bar_edges = resx_hist_bins, xlim = res_range[0],
                                    legend = "mean = %.1f $\mu m$" % (np.mean(clip_resx)),
                                    title ="x residuals of Plane %s - %s entries" %(plane_index, resx_hist.sum()), xlabel = r"$\Delta x$ [$\mathrm{\mu m}$]",
                                    log = True, vlines =None, gauss_fit = plot_x_gauss, box_fit = not plot_x_gauss,
                                    output_png = os.path.join(output_folder,"xresiduals_plane_%s_log.png" %plane_index))

                    plot_1d_bar(hist_in = resx_hist, output_pdf = output_pdf,
                                    bar_edges = resx_hist_bins, xlim = res_range[0],
                                    legend = "mean = %.1f $\mu m$" % (np.mean(clip_resx)),
                                    title ="x residuals of Plane %s - %s entries" %(plane_index, resx_hist.sum()), xlabel = r"$\Delta x$ [$\mathrm{\mu m}$]",
                                    log = False, vlines = None, gauss_fit = False, box_fit = False,
                                    output_png = os.path.join(output_folder,"xresiduals_plane_%s_no_fit.png" %plane_index))

                    plot_1d_bar(hist_in = resx_hist, output_pdf = output_pdf,
                                    bar_edges = resx_hist_bins, xlim = res_range[0],
                                    legend = "mean = %.1f $\mu m$" % (np.mean(clip_resx)),
                                    title ="x residuals of Plane %s - %s entries" %(plane_index, resx_hist.sum()), xlabel = r"$\Delta x$ [$\mathrm{\mu m}$]",
                                    log = True, vlines = None, gauss_fit = False, box_fit = False,
                                    output_png = os.path.join(output_folder,"xresiduals_plane_%s_no_fit_log.png" %plane_index))
                    
                    # plot_1d_bar(hist_in = resy_hist, output_pdf = output_pdf, ax = dy_axes[plot_positions[plane_index]], fig= dy_fig, 
                    #             bar_edges = resy_hist_bins, xlim = res_range[1],
                    #             legend = "mean = %.1f $\mu m$" % (np.mean(clip_resy)),
                    #             title ="y residuals of Plane %s - %s entries" %(plane_index, resy_hist.sum()), xlabel = "[$\mu m$]" ,
                    #             log = True, vlines = [-cut_distance[1],cut_distance[1]], gauss_fit = True,
                    #             output_png = os.path.join(output_folder,"yresiduals_plane_%s.png" %plane_index) )

                    # plot_1d_bar(hist_in = resy_hist, output_pdf = output_pdf, ax = dy_axes[plot_positions[plane_index]], fig= dy_fig, 
                    #             bar_edges = resy_hist_bins, xlim = res_range[1],
                    #             legend = "mean = %.1f $\mu m$" % (np.mean(clip_resy)),
                    #             title ="y residuals of Plane %s - %s entries" %(plane_index, resy_hist.sum()), xlabel = r"$\Delta y$ [$\mathrm{\mu m}$]" ,
                    #             log = True, vlines = None, gauss_fit = True,
                    #             output_png = None)

                    plot_1d_bar(hist_in = resy_hist, output_pdf = output_pdf,
                                bar_edges = resy_hist_bins, xlim = res_range[1],
                                legend = "mean = %.1f $\mu m$" % (np.mean(clip_resy)),
                                title ="y residuals of Plane %s - %s entries" %(plane_index, resy_hist.sum()), xlabel = r"$\Delta y$ [$\mathrm{\mu m}$]" ,
                                log = False, vlines = None, gauss_fit = not plot_x_gauss, box_fit = plot_x_gauss,
                                output_png = os.path.join(output_folder,"yresiduals_plane_%s.png" %plane_index) )

                    plot_1d_bar(hist_in = resy_hist, output_pdf = output_pdf,
                                    bar_edges = resy_hist_bins, xlim = res_range[1],
                                    legend = "mean = %.1f $\mu m$" % (np.mean(clip_resy)),
                                    title ="y residuals of Plane %s - %s entries" %(plane_index, resy_hist.sum()), xlabel = r"$\Delta y$ [$\mathrm{\mu m}$]" ,
                                    log = True, vlines = None, gauss_fit = not plot_x_gauss, box_fit = plot_x_gauss,
                                    output_png = os.path.join(output_folder,"yresiduals_plane_%s_log.png"%plane_index ) )
                    cs_res_x = []
                    cs_res_y = []
                    for cluster_size in range(1,5):
                        cluster_size_mask = np.logical_and(~np.isnan(x_residuals), track_array["cluster_size_plane_%s" %plane_index]==cluster_size)
                        cs_res_x.append(x_residuals[cluster_size_mask])
                        cs_res_y.append(y_residuals[cluster_size_mask])
                        cluster_size_hist_x, cluster_size_bins_x = np.histogram(x_residuals[cluster_size_mask], bins = resbinsx)
                        cluster_size_hist_y, cluster_size_bins_y = np.histogram(y_residuals[cluster_size_mask], bins = resbinsy)
                        
                        plot_1d_bar(hist_in = cluster_size_hist_x, output_pdf = output_pdf,
                                    bar_edges = cluster_size_bins_x, xlim = res_range[0],
                                    legend = "mean = %.1f $\mu m$" % (np.mean(cluster_size_hist_x)),
                                    title ="x residuals of plane %s for cluster size %s - %s entries" %(plane_index, cluster_size, cluster_size_hist_x.sum()), xlabel = r"$\Delta y$ [$\mathrm{\mu m}$]" ,
                                    log = False, vlines = None, gauss_fit = plot_x_gauss, box_fit = not plot_x_gauss,
                                    output_png = os.path.join(output_folder,"xresiduals_plane_%s_cluster_size_%s.png"%(plane_index, cluster_size) ) )

                        plot_1d_bar(hist_in = cluster_size_hist_y, output_pdf = output_pdf,
                                    bar_edges = cluster_size_bins_y, xlim = res_range[1],
                                    legend = "mean = %.1f $\mu m$" % (np.mean(cluster_size_hist_y)),
                                    title ="y residuals of plane %s for cluster size %s - %s entries" %(plane_index, cluster_size, cluster_size_hist_y.sum()), xlabel = r"$\Delta y$ [$\mathrm{\mu m}$]" ,
                                    log = False, vlines = None, gauss_fit = not plot_x_gauss, box_fit = plot_x_gauss,
                                    output_png = os.path.join(output_folder,"yresiduals_plane_%s_cluster_size_%s.png"%(plane_index, cluster_size) ) )

                    # plot residuals vs coordinates
                    resx_trackx_hist, binsx, binsy = np.histogram2d(x[select_finite_distance], x_residuals[select_finite_distance],
                                                                    bins=[50,100], range = [[-10000,10000],[-cut_distance[0]*1.25,cut_distance[0]*1.25]])
                    x_track_counts, _ = np.histogram(x[select_finite_distance], bins = binsx)
                    plot_residuals_vs_position(x= x[select_finite_distance], y=x_residuals[select_finite_distance], hist2d = resx_trackx_hist, 
                                        bins = [binsx, binsy], output_pdf = output_pdf, min_hits = 1, counts = x_track_counts, fig=dx_x_fig, ax = dx_x_axes[plot_positions[plane_index]],
                                        xlabel="track x position [$\mu m$]", ylabel=r"$\Delta$x [$\mu m$]", title="plane %s" %plane_index, legend=True, 
                                        output_png=os.path.join(output_folder, "x_dx_plane_%s.png")%plane_index, markersize = 10, linfit = True, ylimits = [binsy.min(), binsy.max()])

                    # plot_2dhist_subplot(fig=dx_x_fig, ax = dx_x_axes[plot_positions[plane_index]], hist2d=resx_trackx_hist, bins = [binsx,binsy], 
                    #                     title = "plane %s" %plane_index, xlabel="track x position [$\mu m$]", ylabel=r"$\Delta$x [$\mu m$]",
                    #                     zlog = False, invert_x_axis=False, min_hits=1, limits=None)

                    resy_tracky_hist, binsx, binsy = np.histogram2d(y[select_finite_distance], y_residuals[select_finite_distance],
                                                                    bins=[50,100], range = [[-10000,10000],[-cut_distance[1]*1.25,cut_distance[1]*1.25]])
                    y_track_counts, _ = np.histogram(y[select_finite_distance], bins = binsx)
                    plot_residuals_vs_position(x= y[select_finite_distance], y=y_residuals[select_finite_distance], hist2d = resy_tracky_hist, 
                                        bins = [binsx, binsy], output_pdf = output_pdf, min_hits = 1, counts = y_track_counts, fig=dy_y_fig, ax = dy_y_axes[plot_positions[plane_index]],
                                        xlabel="track y position [$\mu m$]", ylabel=r"$\Delta$y [$\mu m$]", title="plane %s" %plane_index, legend=True, 
                                        output_png=os.path.join(output_folder, "y_dy_plane_%s.png")%plane_index, markersize = 10, linfit = True, ylimits = [binsy.min(), binsy.max()])

                    # plot_2dhist_subplot(fig=dy_y_fig, ax = dy_y_axes[plot_positions[plane_index]], hist2d=resy_tracky_hist, bins = [binsx,binsy], 
                    #                     title = "plane %s" %plane_index, xlabel="track y position [$\mu m$]", ylabel=r"$\Delta$y [$\mu m$]",
                    #                     zlog = False, invert_x_axis=False, min_hits=1, limits=None)

                    resx_tracky_hist, binsx, binsy = np.histogram2d(y[select_finite_distance], x_residuals[select_finite_distance],
                                                                    bins=[50,100], range = [[-10000,10000],[-cut_distance[0]*1.25,cut_distance[0]*1.25]])
                    y_track_counts, _ = np.histogram(y[select_finite_distance], bins = binsx)
                    plot_residuals_vs_position(x= y[select_finite_distance], y=x_residuals[select_finite_distance], hist2d = resx_tracky_hist, 
                                        bins = [binsx, binsy], output_pdf = output_pdf, min_hits = 1, counts = y_track_counts, xlimits = None, fig=dx_y_fig, ax = dx_y_axes[plot_positions[plane_index]],
                                        xlabel="track y position [$\mu m$]", ylabel=r"$\Delta$x [$\mu m$]", title="plane %s" %plane_index, legend=True, 
                                        output_png=os.path.join(output_folder, "y_dx_plane_%s.png")%plane_index, markersize = 10, linfit = True, ylimits = [binsy.min(), binsy.max()])

                    # plot_2dhist_subplot(fig=dx_y_fig, ax = dx_y_axes[plot_positions[plane_index]], hist2d=resx_tracky_hist, bins = [binsx,binsy], 
                    #                     title = "plane %s" %plane_index, xlabel="track y position [$\mu m$]", ylabel=r"$\Delta$x [$\mu m$]",
                    #                     zlog = False, invert_x_axis=False, min_hits=1, limits=None
                    #                     )
                    
                    resy_trackx_hist, binsx, binsy = np.histogram2d(x[select_finite_distance], y_residuals[select_finite_distance],
                                                                    bins=[50,100], range = [[-10000,10000],[-cut_distance[1]*1.25,cut_distance[1]*1.25]])
                    x_track_counts, _ = np.histogram(x[select_finite_distance], bins = binsx)
                    plot_residuals_vs_position(x= x[select_finite_distance], y=y_residuals[select_finite_distance], hist2d = resy_trackx_hist, 
                                        bins = [binsx, binsy], output_pdf = output_pdf, min_hits = 1, counts = x_track_counts, fig=dy_x_fig, ax = dy_x_axes[plot_positions[plane_index]],
                                        xlabel="track x position [$\mu m$]", ylabel=r"$\Delta$y [$\mu m$]", title="plane %s" %plane_index, legend=True, 
                                        output_png=os.path.join(output_folder, "x_dy_plane_%s.png")%plane_index, markersize = 10, linfit = True, ylimits = [binsy.min(), binsy.max()])

                    # plot_2dhist_subplot(fig=dy_x_fig, ax = dy_x_axes[plot_positions[plane_index]], hist2d=resy_trackx_hist, bins = [binsx,binsy], 
                    #                     title = "plane %s" %plane_index, xlabel="track x position [$\mu m$]", ylabel=r"$\Delta$y [$\mu m$]",
                    #                     zlog = False, invert_x_axis=False, min_hits=1, limits=None
                    #                     )

                    stat_2d_residuals_hist, _, _, _ = stats.binned_statistic_2d(x=x[select_valid_hit], y=y[select_valid_hit], values=distance_local[select_valid_hit], statistic='mean', bins=bins)
                    # stat_2d_residuals_hist = np.nan_to_num(stat_2d_residuals_hist)
                    stat_2d_residuals_hist_mask = np.ma.array(stat_2d_residuals_hist, mask = np.isnan(stat_2d_residuals_hist))
                    plot_2d_hist(hist2d = stat_2d_residuals_hist_mask, output_pdf = output_pdf, bins = bins, title = "2D Average residuals for plane %s" % plane_index,
                                xlabel = "x [$\mu m$]", ylabel = "y [$\mu m$]", zlog=False, limits = [0.,np.sqrt(np.sum(np.square(cut_distance)))],
                                output_png =os.path.join(output_folder, "2d_average_residuals_plane_%s.png")%plane_index)

                    stat_2d_x_residuals_hist, _, _, _ = stats.binned_statistic_2d(x=x[select_valid_hit], y=y[select_valid_hit], values=x_residuals[select_valid_hit], statistic='mean', bins=bins)
                    # stat_2d_x_residuals_hist = np.nan_to_num(stat_2d_x_residuals_hist)
                    stat_2d_x_residuals_hist_mask = np.ma.array(stat_2d_x_residuals_hist, mask = np.isnan(stat_2d_x_residuals_hist))
                    plot_2d_hist(hist2d = stat_2d_x_residuals_hist_mask, output_pdf = output_pdf, bins = bins, title = "2D x residuals for plane %s" % plane_index,
                                xlabel = "x [$\mu m$]", ylabel = "y [$\mu m$]", zlog=False, limits = [-cut_distance[0]*1.25,cut_distance[0]*1.25],
                                output_png =os.path.join(output_folder, "2d_x_residuals_plane_%s.png")%plane_index)

                    stat_2d_y_residuals_hist, _, _, _ = stats.binned_statistic_2d(x=x[select_valid_hit], y=y[select_valid_hit], values=y_residuals[select_valid_hit], statistic='mean', bins=bins)
                    # stat_2d_y_residuals_hist = np.nan_to_num(stat_2d_y_residuals_hist)
                    stat_2d_y_residuals_hist_mask = np.ma.array(stat_2d_y_residuals_hist, mask = np.isnan(stat_2d_y_residuals_hist))
                    plot_2d_hist(hist2d = stat_2d_y_residuals_hist_mask, output_pdf = output_pdf, bins = bins, title = "2D y residuals for plane %s" % plane_index,
                                xlabel = "x [$\mu m$]", ylabel = "y [$\mu m$]", zlog=False, limits = [-cut_distance[1]*1.25 ,cut_distance[1]*1.25],
                                output_png =os.path.join(output_folder, "2d_y_residuals_plane_%s.png")%plane_index)

                    x_res_hist = stat_2d_residuals_hist_mask.mean(axis = 1)
                    plot_1d_bar(hist_in = x_res_hist, output_pdf = output_pdf,
                                    bar_edges= bins[0], legend = "mean = %.1f $\mu m$" % np.median(x_res_hist),
                                    title="radial distance vs x position - plane %s" %plane_index, xlabel = "[$\mu m$]", log=False)

                    y_res_hist = stat_2d_residuals_hist_mask.mean(axis = 0)
                    plot_1d_bar(hist_in = y_res_hist, output_pdf = output_pdf,
                                    bar_edges=bins[1], legend = "mean = %.1f $\mu m$" % np.median(y_res_hist),
                                    title="radial distance vs y position - plane %s" %plane_index, xlabel = "[$\mu m$]", log=False)
                    # plot all residuals
                    # plot_1d_hist(array_in = x_residuals[select_finite_distance], output_pdf = output_pdf,
                    #                 bins=100, range=None, legend = "mean = %.1f $\mu m$" %(np.mean(x_residuals[select_finite_distance])),
                    #                 title="x residuals of Plane %s" %plane_index, xlabel = "[$\mu m$]", log=True)
                    # plot_1d_hist(array_in = y_residuals[select_finite_distance], output_pdf = output_pdf,
                    #                 bins=100, range=None, legend = "mean = %.1f $\mu m$" %(np.mean(y_residuals[select_finite_distance])),
                    #                 title="y residuals of Plane %s" %plane_index, xlabel = "[$\mu m$]", log=True)

                    # plot efficiency
                    if efficiency_regions:
                        if efficiency_regions[plane_index]:
                            logging.info("plot efficiency region")
                            box = efficiency_regions[plane_index]
                            eff, a,b =  analysis_utils.get_mean_efficiency(array_pass=total_track_density_with_DUT_hit_region, array_total=total_track_density_region, interval=0.68)
                            n_eff_pixel = np.count_nonzero(total_track_density_region)
                            plot_2d_hist(hist2d = efficiency, output_pdf = output_pdf, bins = bins,
                                            xlabel ="x [$\mu m$]", ylabel = "y [$\mu m$]",
                                            title = "Efficiency in plane %s"% (plane_index),
                                            box = box, mean_eff = [eff, n_eff_pixel],
                                            output_png =os.path.join(output_folder,"region_efficiency_plane_%s.png") %plane_index, limits = [0.,1.], force_int_z_ticks=False)
                            logging.info("region efficiency of plane %s : %.2f %% + %s - %s" %(plane_index, eff*100,b,a))

                    logging.info("plot efficiency")
                    n_eff_pixel = count_eff_pix_array.count()
                    eff, a, b =  analysis_utils.get_mean_efficiency(array_pass=total_track_density_with_DUT_hit, array_total=total_track_density, interval=0.68)
                    plot_2d_hist(hist2d = efficiency, output_pdf = output_pdf, bins = bins,
                                    xlabel ="x [$\mu m$]", ylabel = "y [$\mu m$]",
                                    title = "Mean efficiency in plane %s : %1.2f %% - %i pixel" % (plane_index, eff*100, n_eff_pixel),
                                    output_png =os.path.join(output_folder,"efficiency_plane_%s.png" %plane_index), limits = [0.,1.], force_int_z_ticks=False)
                    plot_2d_hist(hist2d = efficiency, output_pdf = output_pdf, bins = bins,
                                    xlabel ="x [$\mu m$]", ylabel = "y [$\mu m$]",
                                    title = "Mean efficiency in plane %s : %1.2f %% - %i pixel" % (plane_index, eff*100, n_eff_pixel),
                                    output_png =os.path.join(output_folder,"efficiency_plane_%s_limits.png" %plane_index), limits = [0.85,efficiency.max()], force_int_z_ticks=False)

                    logging.info(r"mean efficiency of plane %s : %.2f %% + %s - %s" %(plane_index, eff*100,b,a))

                    # #plot efficiency projection
                    # plot_1d_bar(hist_in = efficiency.mean(axis=1), output_pdf = output_pdf,
                    #             bar_edges = bins[0], xlim = None, xlabel = "x [$\mu m$]",
                    #             title ="X effciency of Plane %s" %plane_index )

                    # plot_1d_bar(hist_in = efficiency.mean(axis=0), output_pdf = output_pdf,
                    #             bar_edges = bins[1], xlim = None, xlabel = "y [$\mu m$]", 
                    #             title ="y effciency of Plane %s" %plane_index )

                    # plot residual projection

                    x_res_proj, x_res_proj_bins, _ = stats.binned_statistic(x=x[select_valid_hit], values=x_residuals[select_valid_hit], statistic='mean', bins=bins[0])
                    plot_1d_bar(hist_in = x_res_proj, bar_edges = x_res_proj_bins, output_pdf = output_pdf, ylim = [-200,200.] ,
                                title = "plane %s - x residuals per pixel" % plane_index, ylabel = "$\Delta x [\mu m]$", xlabel = "x [$\mu m$]",
                                output_png =os.path.join(output_folder,"x_res_proj_plane_%s.png" %plane_index))

                    y_res_proj, y_res_proj_bins, _ = stats.binned_statistic(x=y[select_valid_hit], values=y_residuals[select_valid_hit], statistic='mean', bins=bins[1])
                    plot_1d_bar(hist_in = y_res_proj, bar_edges = y_res_proj_bins, output_pdf = output_pdf, ylim = [-200, 200.] ,
                                title = "plane %s - y residuals per pixel" % plane_index, ylabel = "$\Delta y [\mu m]$",  xlabel = "y [$\mu m$]",
                                output_png =os.path.join(output_folder,"y_res_proj_plane_%s.png" %plane_index))

                    # #plot track histogram
                    # logging.info("plot track histogram")
                    plot_2d_hist(hist2d = total_track_density, output_pdf = output_pdf, bins = bins,
                                    xlabel ="x [$\mu m$]", ylabel = "y [$\mu m$]", scale_large_pixels = False,
                                    title = "Tracks in plane %s - %i entries" % (plane_index, total_track_density.sum()),
                                    output_png =os.path.join(output_folder, "tracks_plane_%s.png")%plane_index)

                    # # plot hit histogram: due to association of hits to tracks, not all hits are displayed
                    # plot_2d_hist(hist2d = plane_hits_hist, output_pdf = output_pdf, bins = bins,
                    #                 xlabel ="x [$\mu m$]", ylabel = "y [$\mu m$]", scale_large_pixels = False,
                    #                 title = "Hits in plane %i - %s entries" % (plane_index, plane_hits_hist.sum()),
                    #                 output_png =os.path.join(output_folder, "hits_plane_%s.png")%plane_index)
                    # if additional_plots :
                    #     logging.info("plot bad hits")
                    #     plot_2d_hist(hist2d = bad_hits_hist, output_pdf = output_pdf, bins = [bad_x_bins, bad_y_bins],
                    #                     xlabel ="x [$\mu m$]", ylabel = "y [$\mu m$]", scale_large_pixels = True,
                    #                     title = "Bad Hits in plane %i - %s entries" % (plane_index, bad_hits_hist.sum()),
                    #                     output_png =os.path.join(output_folder, "bad_hits_plane_%s.png")%plane_index)

                    #     plot_1d_bar(hist_in = bad_hits_hist.sum(axis=1).astype(np.uint32), output_pdf = output_pdf,
                    #                 bar_edges = bad_x_bins, xlabel = "x [$\mu m$]", legend = "%i entries" % bad_hits_hist.sum(),
                    #                 title ="X bad hits %s" %plane_index )

                    #     plot_1d_bar(hist_in = bad_hits_hist.sum(axis=0).astype(np.uint32), output_pdf = output_pdf,
                    #                 bar_edges = bad_y_bins, xlabel = "x [$\mu m$]", legend = "%i entries" % bad_hits_hist.sum(),
                    #                 title ="Y bad hits %s" %plane_index )

                    #     logging.info("plot errors")
                    #     #plot chi2 distribution
                    #     plane_tracks = track_array[select_valid_track][np.logical_and(np.logical_and(y> ylim[0], y<ylim[1]), np.logical_and(x>xlim[0], x<xlim[1]))]["covariance"]
                    #     plot_1d_hist(array_in = plane_tracks, output_pdf = output_pdf, bins=200,
                    #                     range=None, legend="%s entries" %plane_tracks.shape[0],
                    #                     title=None, xlabel=r"$\chi^2 / Ndf$", log=False,
                    #                     output_png =os.path.join(output_folder, "chi2_plane%s.png")%plane_index)
                    #     plot_1d_hist(array_in = track_array[select_valid_track]["covariance"], output_pdf = output_pdf, bins=200,
                    #                     range=None, legend="%s entries" %track_array[select_valid_track]["covariance"].shape[0],
                    #                     title=None, xlabel=r"$\chi^2 / Ndf$", log=True,
                    #                     output_png =os.path.join(output_folder, "chi2_plane_%s_log.png")%plane_index)
                    #     # plot distance local
                    #     plot_1d_hist(array_in = distance_local[~np.isnan(distance_local)], output_pdf = output_pdf, bins=200,
                    #                     range=[0,2*res_range[1][1]], legend="%s entries" % distance_local[~np.isnan(distance_local)].shape[0],
                    #                     title="distance local", xlabel="dr [um]", log=False,
                    #                     output_png =os.path.join(output_folder, "distance_plane_%s.png")%plane_index)
                    #     plot_1d_hist(array_in = distance_local[~np.isnan(distance_local)], output_pdf = output_pdf, bins=200,
                    #                     range=[0,2*res_range[1][1]], legend="%s entries" % distance_local[~np.isnan(distance_local)].shape[0],
                    #                     title="distance local", xlabel="dr [um]", log=True,
                    #                     output_png =os.path.join(output_folder, "distance_plane_%s_log.png")%plane_index)
                    #     # plot x and y track errors
                    #     plot_1d_hist(array_in = track_array[select_valid_track]["x_error"], output_pdf = output_pdf, bins=None,
                    #                     range=None, legend="%s entries" %track_array[select_valid_track]["x_error"].shape[0],
                    #                     title="x errors", xlabel=r"$\sigma_x$ [$\mu m$]", log=True)
                    #     plot_1d_hist(array_in = track_array[select_valid_track]["y_error"], output_pdf = output_pdf, bins=None,
                    #                     range=None, legend="%s entries" %track_array[select_valid_track]["x_error"].shape[0],
                    #                     title="y errors", xlabel=r"$\sigma_y$ [$\mu m$]", log=True)

                    #     _, n_tracks_per_event = np.unique(track_array["event_number"], return_counts = True)
                    #     plot_1d_hist(n_tracks_per_event, output_pdf = output_pdf, title = "number of tracks per event", log = True)

                logging.info("create output tables")
                if first_plane:
                    mode = "w"
                else:
                    mode = "a"
                first_plane = False
                with tb.open_file(output_efficiency_file, mode) as out_file_h5:
                    dut_group = out_file_h5.create_group(out_file_h5.root, 'plane_%d' % plane_index)
                    
                    out_efficiency = out_file_h5.create_carray(dut_group, name='Efficiency', title='Efficiency map of plane_%d' % plane_index, atom=tb.Atom.from_dtype(efficiency.dtype), shape=efficiency.T.shape, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    
                    out_pass = out_file_h5.create_carray(dut_group, name='Passing_tracks', title='Passing events of plane_%d' % plane_index, atom=tb.Atom.from_dtype(total_track_density_with_DUT_hit.dtype), shape=total_track_density_with_DUT_hit.T.shape, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    
                    out_total = out_file_h5.create_carray(dut_group, name='Total_tracks', title='Total events of plane_%d' % plane_index, atom=tb.Atom.from_dtype(total_track_density.dtype), shape=total_track_density.T.shape, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    
                    out_hits = out_file_h5.create_carray(dut_group, name='Hits', title='Hits of plane_%d' % plane_index, atom=tb.Atom.from_dtype(plane_hits_hist.dtype), shape=plane_hits_hist.T.shape, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    
                    mean_res = out_file_h5.create_carray(dut_group, name='Mean_residuals', title='Average residuals on plane_%d' % plane_index, atom=tb.Atom.from_dtype(stat_2d_residuals_hist_mask.dtype), shape=stat_2d_residuals_hist_mask.T.shape, filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

                    bad_tracks_out = out_file_h5.create_table(where=dut_group,
                                                name='plane_%s_bad_events' %plane_index,
                                                description=track_array.dtype, title='events with tracks outside cut distance',
                                                filters=tb.Filters(
                                                    complib='blosc',
                                                    complevel=5,
                                                    fletcher32=False))

                    out_residuals_x = out_file_h5.create_carray(dut_group, name='plane_%s_x_residuals' %plane_index, 
                                                                atom=tb.Atom.from_dtype
                                                                (x_residuals.dtype), shape=x_residuals.shape, filters=tb.Filters(complib='blosc', 
                                                                complevel=5, fletcher32=False))

                    out_residuals_y = out_file_h5.create_carray(dut_group, name='plane_%s_y_residuals' %plane_index, 
                                                                atom=tb.Atom.from_dtype
                                                                (y_residuals.dtype), shape=y_residuals.shape, filters=tb.Filters(complib='blosc', 
                                                                complevel=5, fletcher32=False))

                    for cs in range(1,5):
                        x_res_out_cs = out_file_h5.create_carray(dut_group, name='cs_%s_x_residuals' %cs, title='x resdiuals for cluster size %s on plane %s' %(cs, plane_index), 
                                                                atom=tb.Atom.from_dtype
                                                                (cs_res_x[cs-1].dtype), shape=cs_res_x[cs-1].shape, filters=tb.Filters(complib='blosc', 
                                                                complevel=5, fletcher32=False))

                        y_res_out_cs = out_file_h5.create_carray(dut_group, name='cs_%s_y_residuals' %cs, title='y resdiuals for cluster size %s on plane %s' %(cs, plane_index),
                                                                atom=tb.Atom.from_dtype
                                                                (cs_res_y[cs-1].dtype), shape=cs_res_y[cs-1].shape, filters=tb.Filters(complib='blosc', 
                                                                complevel=5, fletcher32=False))
                        
                        x_res_out_cs[:] = cs_res_x[cs-1]
                        y_res_out_cs[:] = cs_res_y[cs-1]

                    bad_tracks_out.append(bad_events)
                    bad_tracks_out.flush()

                    out_residuals_x[:] = x_residuals
                    out_residuals_y[:] = y_residuals

                    out_efficiency[:] = efficiency.T
                    out_pass[:] = total_track_density_with_DUT_hit.T
                    out_total[:] = total_track_density.T
                    out_hits[:] = plane_hits_hist.T
                    mean_res[:] = stat_2d_residuals_hist_mask.T

            # logging.info("save polyplots")
            # save_polyplot_fig(fig = dx_y_fig, output_pdf = out_pdf_polyplot,) #output_png = out_pdf_polyplot_name[:-4] + "_dx_vs_y.png")
            # save_polyplot_fig(fig = dy_x_fig, output_pdf = out_pdf_polyplot,) #output_png = out_pdf_polyplot_name[:-4] + "_dy_vs_x.png")
            # save_polyplot_fig(fig = dx_x_fig, output_pdf = out_pdf_polyplot,) #output_png = out_pdf_polyplot_name[:-4] + "_dx_vs_x.png")
            # save_polyplot_fig(fig = dy_y_fig, output_pdf = out_pdf_polyplot,) #output_png = out_pdf_polyplot_name[:-4] + "_dy_vs_y.png")
            # save_polyplot_fig(fig = dy_fig, output_pdf = out_pdf_polyplot, output_png = out_pdf_polyplot_name[:-4] + "_dy.png")
            # save_polyplot_fig(fig = dx_fig, output_pdf = out_pdf_polyplot, output_png = out_pdf_polyplot_name[:-4] + "_dx.png")

    logging.info("saved histograms in %s/" % output_folder)


if __name__ == '__main__':
    pass

