import numpy as np
import tables as tb
from tqdm import tqdm
import os
import matplotlib.pyplot as plt
from ship_pixel_tracking.tools.analysis_utils import load_table, save_table, gauss, load_array, get_deflection, merged_tracks_dtype, merged_clusters_dtype, red_chisquare
from ship_pixel_tracking.tools.plot_utils import plot_2d_hist, plot_1d_hist, plot_1d_bar, create_polyplot_fig, save_polyplot_fig, plot_xy_2axis
from ship_pixel_tracking.ConvertRoot.pix_tracks_to_h5 import open_pixel_tracks_root, open_pixel_2TrackVertices_root, open_pixel_vertices_root
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.backends.backend_agg import FigureCanvas
from matplotlib import colors, cm
from matplotlib.figure import Figure, figaspect
import matplotlib
from scipy import stats
from scipy.spatial.distance import pdist, squareform
from scipy.optimize import curve_fit
from beam_telescope_analysis.tools.analysis_utils import get_mean_efficiency
from numpy.lib import recfunctions as rfn

from beam_telescope_analysis.tools.analysis_utils import get_mean_efficiency

cmap = matplotlib.cm.get_cmap('viridis')

color_1 = cmap(0.2)
color_5 = cmap(0.375)
color_2 = cmap(0.55)
color_3 = cmap(0.75)
color_4 = cmap(0.9)
map10 = cmap(np.linspace(0.2,0.9,11))

def double_gauss(x,A,A2,center, center2, sigma, sigma2):
    return A * np.exp(-np.square((x-center))/(2. * np.square(sigma))) + A2 * np.exp(-np.square((x-center2))/(2. * np.square(sigma2)))

def plot_displacement(passed, total, residuals):

    z_exit = -1.824
    ecc_width = 2.52 + 29*0.033 # 29 tungsten plates of 0.9 mm instead of 1mm lead.
    ecc_z_start = z_exit- ecc_width

    z_lims = [total["vz"].min(), total["vz"].max() ]

    zeff_bins = np.linspace(z_lims[0],z_lims[1],20)
    total_z_density = np.zeros(zeff_bins.size-1, dtype = float)
    found_z_density = np.zeros(zeff_bins.size-1, dtype = float)

    total_hist_z, total_bins_z = np.histogram(total["vz"], bins =zeff_bins )
    total_z_density +=total_hist_z

    found_hist_z, found_bins_z = np.histogram(passed["vz"], bins =zeff_bins)
    found_z_density += found_hist_z
    eff = found_z_density/total_z_density

    reco_eff_z = get_mean_efficiency(array_pass = found_z_density, array_total = total_z_density, interval = 0.68)

    print("ratio displaced = %.4f %.4f +%.4f" %(reco_eff_z))

    hist_errs = np.empty(shape =(eff.size, 2) )     
    for entry in tqdm(range(zeff_bins.size -1)):
        eff_bin = get_mean_efficiency(array_pass = found_z_density[entry], array_total = total_z_density[entry], interval = 0.68)
        hist_errs[entry] = [np.abs(eff_bin[1]), eff_bin[2]]

    plt.errorbar(zeff_bins[:-1] + z_exit, eff, color = color_2, yerr = hist_errs.T, capsize = 3, linestyle = "None", marker = ".", markersize = 4,linewidth = 2, markeredgewidth=2)
    plt.xlabel("$\mathrm{z_{v,emu}}$ in cm", fontsize = 14)
    plt.grid()
    plt.ylim(0,1.025)
    plt.ylabel("$\\frac{n_\mathrm{displ}}{n_\mathrm{unmatched}}$", fontsize = 16)
    # plt.ylabel("$\\frac{n_\mathrm{trk, ns}<2}{n_\mathrm{trk}\geq 2}$", fontsize = 14)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.tight_layout()
    plt.savefig("/home/niko/cernbox/thesis/figures/vertex_displacement_rate_unmatched.pdf")
    plt.clf()

    for index, dim in zip(range(3), ["x","y","z"]):
        if index ==2:
            plt.hist(np.clip(residuals[index,:],-10,10),color = color_2, bins =25 )
        else:
            plt.hist(np.clip(residuals[index,:],-10,10),color = color_2, bins =25 )
        plt.grid()
        plt.tick_params(axis='both', which='major', labelsize=14)
        plt.xlabel("%s displacement in cm" % dim, fontsize = 14)
        plt.ylabel("counts", fontsize = 14)
        plt.tight_layout()
        plt.savefig("/home/niko/cernbox/thesis/figures/vertex_%s_displacement_unmatched.pdf" %dim)
        plt.clf()

    dist_euc = np.linalg.norm(residuals, axis = 0)
    plt.hist(np.clip(dist_euc, 0,10),color = color_2, bins =25 )
    plt.grid()
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.xlabel("euclidean distance $\\mathbf{x}_\mathrm{v} - \\mathbf{x}_\mathrm{s}$ in cm", fontsize = 14)
    plt.ylabel("counts", fontsize = 14)
    plt.tight_layout()
    plt.savefig("/home/niko/cernbox/thesis/figures/vertex_euclid_displacement_unmatched.pdf")
    plt.clf()


def plot_z_matching_eff(matched_out):

    z_exit = -1.824
    ecc_width = 2.52 + 29*0.033 # 29 tungsten plates are thinner thant lead plates. 
    ecc_z_start = z_exit- ecc_width

    matches = matched_out[matched_out["matched"]>0]
    matches = matches[matches["chi2"]<50]
    print("%2.1f %% matchable vertices" % ((matched_out[matched_out["matchable"]==1].size /matched_out.size)*100))
    print("%i matches = %2.2f %% " %(matches.size , (matches.size / matched_out[matched_out["matchable"]==1].size)*100))

    plt.hist(matches["chi2"], bins = 20, color = color_2, label = "mean = %.2f"%np.mean(matches["chi2"]))
    plt.xlabel("$\mathrm{\chi^2}$", fontsize = 14)
    plt.ylabel("counts", fontsize = 14)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.grid()
    plt.tight_layout()
    # plt.legend()
    plt.savefig("/home/niko/cernbox/thesis/figures/vertex_matching_chi2.pdf")
    plt.clf()

    plt.hist(matches["vz"] - z_exit, bins = 20, color = color_2)
    plt.xlabel("z in cm", fontsize = 14)
    plt.ylabel("counts", fontsize = 14)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.grid()
    plt.tight_layout()
    plt.savefig("/home/niko/cernbox/thesis/figures/vertex_matching_z.pdf")
    plt.clf()

    all = matched_out
    matches_emu_ref = matched_out[matched_out["matched"]>0]

    z_lims = [all["vz"].min(),all["vz"].max() ]

    zeff_bins = np.linspace(z_lims[0],z_lims[1],15)
    total_z_density = np.zeros(zeff_bins.size-1, dtype = float)
    found_z_density = np.zeros(zeff_bins.size-1, dtype = float)

    total_hist_z, total_bins_z = np.histogram(all["vz"], bins =zeff_bins )
    total_z_density +=total_hist_z
    
    found_hist_z, found_bins_z = np.histogram(matches_emu_ref["vz"], bins =zeff_bins)
    found_z_density += found_hist_z

    eff = found_z_density/total_z_density
    reco_eff_z = get_mean_efficiency(array_pass = found_z_density, array_total = total_z_density, interval = 0.68)
    print("matching eff : %.4f %.4f +%.4f" % reco_eff_z)

    hist_errs = np.empty(shape =(eff.size, 2) )     
    for entry in tqdm(range(zeff_bins.size -1)) :
        eff_bin = get_mean_efficiency(array_pass = found_z_density[entry], array_total = total_z_density[entry], interval = 0.68)
        hist_errs[entry] = [np.abs(eff_bin[1]), eff_bin[2]]

    bin_centers = (zeff_bins[1:] + zeff_bins[:-1]) / 2.0
    plt.errorbar(zeff_bins[:-1] + z_exit, eff, color = color_2, yerr = hist_errs.T, capsize = 3, linestyle = "None", marker = ".", markersize = 4,linewidth = 2, markeredgewidth=2)
    # plt.bar(zeff_bins[:-1] + z_exit, eff, width = np.diff(zeff_bins), color = color_2)
    # plt.axvspan(ecc_z_start,z_exit, alpha = 0.225, color = color_1, label = "ECC z extent")
    plt.xlabel("z in cm" , fontsize = 14)
    plt.grid()
    # plt.legend(prop={'size': 14})
    plt.ylabel("vertex matching rate", fontsize = 14)
    # plt.ylabel("$\epsilon_\mathrm{r}$", fontsize = 18)
    plt.ylim(0,1)
    plt.tick_params(axis='both', which='major', labelsize=14)
    plt.tight_layout()
    plt.savefig("/home/niko/cernbox/thesis/figures/vertex_matching_eff_vs_z_no_hist.pdf")
    # plt.show()
    plt.clf()

    res_x_hist, res_x_bins = np.histogram(np.clip(matches["dx"],-0.09,0.09)*1e4, bins = 50)
    res_y_hist, res_y_bins = np.histogram(np.clip(matches["dy"],-0.15,0.15)*1e4, bins = 50)
    z_mask = np.abs(matches["dz"])<100000.
    res_z_hist, res_z_bins = np.histogram(np.clip(matches[z_mask]["dz"],-1,1)*10, bins = 50)

    with PdfPages("/home/niko/cernbox/thesis/figures/vertex_matching_x_res_single_gauss.pdf") as out_pdf_x:
        plot_1d_bar(hist_in = res_x_hist, output_pdf=out_pdf_x, bar_edges =res_x_bins, fontsize = 14,gauss_fit=True, xlabel ="$x_\mathrm{emu} - x_\mathrm{pix}$ in $\mathrm{\mu}$m",edge_color = "None" )
    with PdfPages("/home/niko/cernbox/thesis/figures/vertex_matching_y_res_single_gauss.pdf") as out_pdf_y:
        plot_1d_bar(hist_in = res_y_hist, output_pdf=out_pdf_y, bar_edges =res_y_bins, fontsize = 14,gauss_fit=True, xlabel ="$y_\mathrm{emu} - y_\mathrm{pix}$ in $\mathrm{\mu}$m", edge_color = "None" )
    with PdfPages("/home/niko/cernbox/thesis/figures/vertex_matching_z_res_single_gauss.pdf") as out_pdf_z:
        plot_1d_bar(hist_in = res_z_hist, output_pdf=out_pdf_z, bar_edges =res_z_bins, fontsize = 14,gauss_fit=True,xlabel ="$z_\mathrm{emu} - z_\mathrm{pix}$ in mm", gauss_unit="mm", edge_color = "None")

    for bins, hist, dim, gauss_unit in zip([res_x_bins, res_y_bins, res_z_bins],[res_x_hist, res_y_hist, res_z_hist], ["x", "y", "z"], ["$\mathrm{\mu}$m","$\mathrm{\mu}$m","mm"]):
        if dim == "x":
            popt, pcov = curve_fit(double_gauss, bins[:-1], hist , p0 = [600,50,0,0,60,300])
        else:
            popt, pcov = curve_fit(double_gauss, bins[:-1], hist )
        
        reduced_chi2 = red_chisquare(hist, double_gauss(bins[:-1], *popt), np.sqrt(hist), popt, use_empty_bins = False)
        amp = popt[0] + popt[1]
        sigma_add = np.absolute(popt[4])*popt[0]/amp + np.absolute(popt[5])*popt[1]/amp
        print("sigma %s add = %.4f %s" %(dim, sigma_add, gauss_unit))
        gauss_fit_legend_entry = '$\mathrm{A}=%.0f\pm %.0f$\n$\mathrm{\mu}=%.0f\pm %.0f$ %s \n$\mathrm{\sigma}=%.0f\pm %.0f$ %s ' % (popt[0], np.sqrt(pcov[0][0]), popt[2], np.sqrt(pcov[2][2]), gauss_unit, np.absolute(popt[4]),  np.sqrt(pcov[4][4]), gauss_unit)

        gauss_fit_legend_entry1 = '\n$\mathrm{A}=%.0f\pm %.0f$\n$\mathrm{\mu}=%.0f\pm %.0f$ %s \n$\mathrm{\sigma}=%.0f\pm %.0f$ %s \n\n$\mathrm{\chi^2/ndf} = %.2f$' % (popt[1], np.sqrt(pcov[1][1]), popt[3], np.sqrt(pcov[3][3]), gauss_unit, np.absolute(popt[5]),  np.sqrt(pcov[5][5]), gauss_unit, reduced_chi2)

        x_vals = np.linspace(bins[0], bins[-1], 1000)

        plt.plot(x_vals, gauss(x_vals, popt[0], popt[2], popt[4]), color = color_3, label = gauss_fit_legend_entry)
        plt.plot(x_vals, gauss(x_vals, popt[1], popt[3], popt[5]), color = color_4, label = gauss_fit_legend_entry1)
        plt.plot(x_vals, double_gauss(x_vals, *popt), color = color_1, label = "Double Gauss" , linewidth = 0.007*300)
        plt.bar(bins[:-1], hist, np.diff(bins), color = color_2, edgecolor=color_2, linewidth=0.4, label = "data")
        # plt.hist(np.clip(matches["dx"],-0.075,0.075)*1e4, bins = 25, color = color_2, label="mean = %.2f $\mathrm{\mu}$m" %np.mean(matches["dx"]*1e4))
        plt.xlabel("$%s_\mathrm{v,emu} - %s_\mathrm{v,pix}$ in %s" %(dim, dim, gauss_unit), fontsize = 16)
        plt.ylabel("counts", fontsize = 14)
        plt.grid()
        plt.legend(prop={'size': 12})
        plt.tick_params(axis='both', which='major', labelsize=14)
        plt.tight_layout()
        plt.savefig("/home/niko/cernbox/thesis/figures/vertex_matching_%s_res.pdf" %dim)
        plt.clf()

    

    # plt.hist(np.clip(matches["dy"],-0.1,0.1)*1e4, bins = 25, color = color_2, label="mean = %.2f $\mathrm{\mu}$m" %np.mean(matches["dy"]*1e4))
    # plt.xlabel("$y_\mathrm{emu} - y_\mathrm{pix}$ in $\mathrm{\mu}$m", fontsize = 14)
    # plt.grid()
    # plt.legend(prop={'size': 12})
    # plt.tick_params(axis='both', which='major', labelsize=14)
    # plt.tight_layout()
    # plt.savefig("/home/niko/cernbox/thesis/figures/vertex_matching_y_res.pdf")
    # plt.show()
    # plt.clf()

    


    
    # plt.hist(np.clip(matches[z_mask]["dz"],-1,1)*10, bins = 25, color = color_2,  label="mean = %.2f mm"%np.mean(matches[z_mask]["dz"]*10))
    # plt.xlabel("$z_\mathrm{emu} - z_\mathrm{pix}$ in mm", fontsize = 14)
    # plt.grid()
    # plt.legend(prop={'size': 12})
    # plt.tick_params(axis='both', which='major', labelsize=14)
    # plt.tight_layout()
    # plt.savefig("/home/niko/cernbox/thesis/figures/vertex_matching_z_res.pdf")
    # plt.show()
    # plt.clf()


def algin_emu(emu_ref_vtcs):
    ty_algn = -4.63719e-03
    tx_algn = 2.15320e-03

    new_vx = emu_ref_vtcs["vx"] * np.cos(tx_algn) - emu_ref_vtcs["vz"] * np.sin(tx_algn)

    new_vz = emu_ref_vtcs["vx"] * np.sin(tx_algn) + emu_ref_vtcs["vz"] * np.cos(tx_algn)

    new_vy = emu_ref_vtcs["vy"] * np.cos(ty_algn) - new_vz * np.sin(ty_algn)

    new_vzz = emu_ref_vtcs["vy"] * np.sin(ty_algn) + new_vz * np.cos(ty_algn)
    emu_ref_vtcs["vz"] = new_vzz #- 1.837

    emu_ref_vtcs["vy"] = new_vy
    emu_ref_vtcs["vx"] = new_vx

    return emu_ref_vtcs


def get_emu_reference(matched_emu, emulsion_tracks, matched_ts, pix_ts):

    time_mask= np.isin(matched_ts, pix_ts)

    vtx_id, vtx_time, vx, vy, vz, trackIDs, glob_ids, spills = [],[],[],[],[],[], [], []
    ntracks = []
    glob_vtx_id = 0
    
    for i, timestamp in enumerate(matched_ts[time_mask]):
        matched_tracks = matched_emu[matched_emu["match_time"]==timestamp]
        emu_ids = matched_tracks["emu_trk"]
        spill = matched_tracks["spill"][0]
        quarter = -1
        for j, trackID in enumerate(np.unique(emu_ids)):
            emu_vtx = emulsion_tracks[emulsion_tracks["trackID"]==trackID]
            
            # if  (j>0 and ((emu_vtx["vx"]==vx[-1]) and (emu_vtx["vy"]==vy[-1]) and (emu_vtx["vz"]==vz[-1]))) :
            #     continue
            if emu_vtx["quarter"]==quarter:
                continue
            quarter = emu_vtx["quarter"]
            vtx_id.extend(emu_vtx["vID"])
            vtx_time.append(timestamp)
            vx.extend(emu_vtx["vx"])
            vy.extend(emu_vtx["vy"])
            vz.extend(emu_vtx["vz"])
            spills.append(spill)
            trackIDs.append(trackID)
            glob_ids.append(glob_vtx_id)
            glob_vtx_id +=1
        ntracks.append(j)

    emu_ref_vtcs = np.full(fill_value = -1, shape = (len(vtx_id)),dtype = [("vx", np.float32),("vy", np.float32), ("vz", np.float32), ("emu_vID", np.int32), ("pix_vid", np.int32), ("trackID", np.int32), ("timestamp", np.int64), ("chi2", np.float64), ("matched", np.int32), ("global_emu_vtxID", np.int32), ("matchable", np.int32), ("spill", np.int32) ,("spill_y", np.float32)])
    emu_ref_vtcs["matched"] = 0
    emu_ref_vtcs["matchable"] = 1
    emu_ref_vtcs["vx"] = vx
    emu_ref_vtcs["vy"] = vy
    emu_ref_vtcs["vz"] = vz
    emu_ref_vtcs["spill"] = spills
    emu_ref_vtcs["emu_vID"] = vtx_id
    emu_ref_vtcs["timestamp"] = vtx_time
    emu_ref_vtcs["trackID"] = trackIDs
    emu_ref_vtcs["global_emu_vtxID"] = glob_ids


    for spill in np.unique(emu_ref_vtcs["spill"]):
        mask = emu_ref_vtcs["spill"]==spill
        emu_ref_vtcs["spill_y"][mask] = np.mean(emu_ref_vtcs["vy"][mask])

    save_table(emu_ref_vtcs, "/home/niko/git/ship_tracking/data/run_2793/emu_ref_vertices_matching.h5", node_name = "EmuRefVtcs")


def calc_vtx_residuals(emu_ref_vtcs, pixel_vtcs, matches_IDS, max_chi2=50, max_dist=False):
    
    matched_vtcs_out = np.full(fill_value = -1, shape=(emu_ref_vtcs.size), dtype = [("vx", np.float32),("vy", np.float32), ("vz", np.float32),("emu_vID", np.int32), ("global_emu_vtxID", np.int32), ("pix_vID", np.int32), ("timestamp", np.int64), ("matched", np.int32), ("chi2", np.float64), ("dx", np.float64), ("dy", np.float64), ("dz", np.float64), ("matchable", np.int32)])
    matched_vtcs_out["matched"] = 0
    matched_vtcs_out["matchable"] = emu_ref_vtcs["matchable"]
    matched_vtcs_out["timestamp"] = emu_ref_vtcs["timestamp"]
    matched_vtcs_out["global_emu_vtxID"] = emu_ref_vtcs["global_emu_vtxID"]
    matched_vtcs_out["vz"] = emu_ref_vtcs["vz"]
    matched_vtcs_out["vy"] = emu_ref_vtcs["vy"]
    matched_vtcs_out["vx"] = emu_ref_vtcs["vx"]
    matched_vtcs_out["emu_vID"] = emu_ref_vtcs["emu_vID"]

    unmatchable = 0
    
    vtx_ts, vtx_indices, vtx_counts = np.unique(emu_ref_vtcs["timestamp"], return_index=True, return_counts=True)
    
    for i, timestamp in enumerate(vtx_ts):
        emu_mask = emu_ref_vtcs["timestamp"]==timestamp
        emu_vtcs = emu_ref_vtcs[emu_mask]

        pix_mask = pixel_vtcs["timestamp"]==timestamp
        pix_vtcs = pixel_vtcs[pix_mask]
        for k in range(emu_vtcs.size):
            chi2_list = []
            dx, dy, dz = [],[],[]
            unc3d = np.zeros(shape=(0,2))
            # unc = np.zeros(shape=(0,2))
            emu_pos = rfn.structured_to_unstructured(emu_vtcs[k][["vx", "vy", "vz"]])
            
            for pix_vtx in pix_vtcs:
                pix_pos = np.array([pix_vtx["x"], pix_vtx["y"], pix_vtx["z"]])
                unc3d = rfn.structured_to_unstructured(pix_vtx[["vertex_cov_XX", "vertex_cov_YY", "vertex_cov_ZZ"]])
                # unc = unc3d + np.square([0.015, 0.015,0.015]) #  assume 150 um uncertainty in emulsion vertex position 
                chi2_list.append(np.sum(np.square(emu_pos - pix_pos) / unc3d))
                dx.append(emu_pos[0] - pix_pos[0])
                dy.append(emu_pos[1] - pix_pos[1])
                dz.append(emu_pos[2] - pix_pos[2])
            # ambiguity of emulsion vertex ID reduces matching rate. it is fair to only consider vertices in a 2x2 cm area around pixel
            if  max_dist and np.any(np.array([dx,dy]).T>(10 * np.sqrt(unc3d[:-1]))):
                unmatchable += 1
                row = np.argwhere(matched_vtcs_out["global_emu_vtxID"]==emu_vtcs["global_emu_vtxID"][k])[0]
                emu_ref_vtcs["matchable"][row] = 0
                matched_vtcs_out["matchable"][row] = 0
                continue
            if np.any(np.array(chi2_list)< max_chi2):
                best_match = np.argmin(chi2_list)
                match_row = np.argwhere(pix_mask==1)[best_match][0]
                pixel_vtcs["matched"][match_row] +=1
                pixel_vtcs["match_chi2"][match_row] = chi2_list[best_match]

                # emu_vtcs["matched"][k] +=1
                # emu_vtcs["chi2"][k]  = chi2_list[best_match]
                # emu_ref_vtcs["matched"][emu_mask][k] +=1
                matches_IDS[matches_IDS[:,0]==emu_vtcs["global_emu_vtxID"][k],1] += 1
                
                matched_vtcs_row = np.argwhere(matched_vtcs_out["global_emu_vtxID"]==emu_vtcs[k]["global_emu_vtxID"])[0]
                matched_vtcs_out["matched"][matched_vtcs_row] +=1
                # matched_vtcs_out["vx"][matched_vtcs_row] = emu_vtcs["vx"][k]
                # matched_vtcs_out["vy"][matched_vtcs_row] = emu_vtcs["vy"][k]
                # matched_vtcs_out["vz"][matched_vtcs_row] = emu_vtcs["vz"][k]
                matched_vtcs_out["chi2"][matched_vtcs_row]= chi2_list[best_match]
                # print(matched_vtcs_out["chi2"][matched_vtcs_row])
                # matched_vtcs_out["timestamp"][matched_vtcs_row] = timestamp
                matched_vtcs_out["pix_vID"][matched_vtcs_row] = pixel_vtcs["vertexID"][match_row]
                matched_vtcs_out["dx"][matched_vtcs_row] = dx[best_match]
                matched_vtcs_out["dy"][matched_vtcs_row] = dy[best_match]
                matched_vtcs_out["dz"][matched_vtcs_row] = dz[best_match]


    return matched_vtcs_out, unmatchable


def match_vertices(emu_ref_vtcs, pixel_vtcs, max_chi2=25, max_dist = False):

    matches_IDS = np.zeros(shape = (emu_ref_vtcs.size,2), dtype = int)
    matches_IDS[:,0] = emu_ref_vtcs["global_emu_vtxID"]

    # emu_ref_vtcs["matchable"][np.abs(emu_ref_vtcs["spill_y"] - emu_ref_vtcs["vy"]) >0.4] = 0
    emu_ref_vtcs["matchable"] = 1

    matched_out, unmatchable = calc_vtx_residuals(emu_ref_vtcs, pixel_vtcs, matches_IDS, max_chi2 = max_chi2, max_dist=max_dist)

    # choose ambiguously matched pixel tracks and assign to best fit, redo matching for remaining emulsion tracks
    ids1, counts1 = np.unique(matched_out["pix_vID"], return_counts = True)
    # matched_tracks_out array is initalized with -1 so the first id will be -1, we do not need this so we start from the second unique id
    ids = ids1[1:]
    counts = counts1[1:]

    while any (counts>1) : 
        print("%i pixel vertices matched ambiguously, these vertices will be rematched" %ids[counts>1].size)

        pix_mask = np.ones(shape = pixel_vtcs.shape, dtype = bool)
        # only choose matchable tracks which are NOT the ambigously matched track
        for id in ids:
            pix_mask[pixel_vtcs["vertexID"] == id]=0
        
        # loop over ambiguously matched tracks
        for id in tqdm(ids[counts>1]):
            # choose all reference tracks where the same matchable track was matched
            matches = matched_out[matched_out["pix_vID"]==id]
            mask = np.ones(shape = matches.shape, dtype = bool)
            # choose correct match
            mask[np.argmin(matches["chi2"])]=0
            # reset output array where the ambiguous matchable track was incorrectly matched 
            matches[["dx", "dy", "dz", "emu_vID", "chi2"]][mask] = -1
            matches['matched'][mask]=0
            # matches['matchable'][mask]=1
            # match again with remaining tracks
            second_matches, unmatchable_loop = calc_vtx_residuals(matches[mask], pixel_vtcs[pix_mask], matches_IDS, max_chi2=max_chi2, max_dist=max_dist)
            unmatchable += unmatchable_loop
            # fill output array with new matches
            for match_row in range(second_matches.size):
                select_row = np.argwhere(matched_out["global_emu_vtxID"]==second_matches[match_row]["global_emu_vtxID"])
                for column in second_matches[match_row].dtype.names:
                    matched_out[column][select_row] = second_matches[match_row][column]
                    # matched_out["matched"][select_row] +=1
                select_emu_row = np.argwhere(emu_ref_vtcs["global_emu_vtxID"]==second_matches[match_row]["global_emu_vtxID"])
                emu_ref_vtcs["matched"][select_emu_row] = second_matches[match_row]["matched"]
                emu_ref_vtcs["chi2"][select_emu_row] = second_matches[match_row]["chi2"]
        # look for tracks again matched ambiguously
        ids1, counts1 = np.unique(matched_out["pix_vID"], return_counts = True)
        ids = ids1[1:]
        counts = counts1[1:]
    print("%i not matchable vertices" %unmatchable)

    # mark matches in emu_ref_vtcs. Can not acces view of array in loop due to advanced indexing which only gives copy.
    for k in range(0,matches_IDS.shape[0]):
        emu_ref_vtcs["matched"][emu_ref_vtcs["global_emu_vtxID"]==matches_IDS[k,0]] = matches_IDS[k,1]
        
    return matched_out


def get_displacement(matched_vertices_in, ref_vertices_in, emulsion_segments_in):
    matched_vtcs = load_table(matched_vertices_in)
    emu_ref_vtcs = load_table(ref_vertices_in)

    # estimate multiple scattering displacement of emulsion vertices
    # project track segments sf to vertex position
    # project whole track to vertex position, get difference of intersection at last emu plane

    emu_sf = load_table(emulsion_segments_in)

    z_exit = 0 #-1.824
    ecc_width = 2.52 + 29*0.033 # 29 tungsten plates of 0.9 mm instead of 1mm lead.
    ecc_z_start = z_exit- ecc_width

    pix_sigma_x_emu = 0.00541
    pix_sigma_y_emu = 0.01032
    pix_sigma_z_emu = 0.7749

    # pix_sigma_y_emu = np.sqrt(0.0061319924020956885)
    # pix_sigma_x_emu = np.sqrt(0.0007481261066177297)
    # pix_sigma_z_emu = np.sqrt(0.012482289592579826)

    print("sigma x %.5f" % pix_sigma_x_emu)
    print("sigma y %.5f" % pix_sigma_y_emu)
    print("sigma z %.5f" % pix_sigma_z_emu)

    x_cut = pix_sigma_x_emu * 6
    y_cut = pix_sigma_y_emu * 6
    z_cut = pix_sigma_z_emu * 6

    print("x cut %.4f" %x_cut)
    print("y cut %.4f" %y_cut)
    print("z cut %.4f" %z_cut)

    v_id_mask_sf =emu_sf["nTracks"]>=6
    v_id_mask_sf &= emu_sf["vz"]>(ecc_z_start)
    v_id_mask_sf &= np.logical_and(np.abs(emu_sf["tx"])<0.15,np.abs(emu_sf["ty"])<0.15)

    vertex_sf = emu_sf[v_id_mask_sf]

    unmatched_vtcs = matched_vtcs[matched_vtcs["matched"]==0]
    unmatched_emu_ids = unmatched_vtcs["global_emu_vtxID"]
    emu_vIDs = np.zeros(shape = unmatched_emu_ids.size, dtype = int)
    for i in range(unmatched_emu_ids.size):
        un_vID = unmatched_emu_ids[i]
        emu_vIDs[i] = emu_ref_vtcs[emu_ref_vtcs["global_emu_vtxID"]==un_vID]["emu_vID"]

    un_mask = np.zeros(shape = vertex_sf.size, dtype = bool)
    ids_sorted = np.sort(emu_vIDs)
    for un_vID in ids_sorted:
        un_mask[vertex_sf["vID"]==un_vID] = 1

    emu_sf_unmatched = vertex_sf[un_mask]
    emu_sf_unmatched.sort(order = "vID")
    sf_vtx_x = emu_sf_unmatched["x"] + emu_sf_unmatched["tx"]* (z_exit - emu_sf_unmatched["vz"] )
    sf_vtx_res_x = sf_vtx_x - emu_sf_unmatched["vx"]

    sf_vtx_y = emu_sf_unmatched["y"] + emu_sf_unmatched["ty"]* (z_exit- emu_sf_unmatched["vz"] )
    sf_vtx_res_y = sf_vtx_y - emu_sf_unmatched["vy"]

    sf_vtx_z_x = (emu_sf_unmatched["vx"] - emu_sf_unmatched["x"]) / emu_sf_unmatched["tx"]
    z_x_res = sf_vtx_z_x - emu_sf_unmatched["vz"]

    sf_vtx_z_y = (emu_sf_unmatched["vy"] - emu_sf_unmatched["y"]) / emu_sf_unmatched["ty"]
    z_y_res = sf_vtx_z_y - emu_sf_unmatched["vz"]

    sf_vtx_res_z = z_x_res + z_y_res

    outside_x = sf_vtx_res_x[np.abs(sf_vtx_res_x)>x_cut]
    outside_y = sf_vtx_res_y[np.abs(sf_vtx_res_y)>y_cut]
    outside_z = sf_vtx_res_z[np.abs(sf_vtx_res_z)>z_cut]

    print("outside pix x cut %.4f" %((outside_x.size)/ sf_vtx_res_x.size))
    print("outside pix y cut %.4f" %((outside_y.size)/ sf_vtx_res_y.size))
    print("outside pix z cut %.4f" %((outside_z.size)/ sf_vtx_res_z.size))

    sigma_mask_unmatch = np.abs(sf_vtx_res_x)>x_cut
    sigma_mask_unmatch += np.abs(sf_vtx_res_y)>y_cut
    sigma_mask_unmatch += np.abs(sf_vtx_res_z)>z_cut

    print("rate of displaced tracks on all unmatched tracks %.4f" %(emu_sf_unmatched[sigma_mask_unmatch].size / emu_sf_unmatched.size))

    vertices_displaced, indx_displaced, counts_displaced = np.unique(emu_sf_unmatched[sigma_mask_unmatch]["vID"], return_index= True, return_counts = True)
    vertices_ref, indx_ref, counts_ref = np.unique(emu_sf_unmatched["vID"], return_index= True, return_counts = True)
    not_match  = []

    l =0
    for k, vertex in enumerate(vertices_ref):
        if vertex != vertices_displaced[l]:
            continue
        else:
            # if (counts_displaced[l] / counts_ref[k]) > 0.5 :
            if (counts_ref[k] - counts_displaced[l]) < 2 :
                not_match.append(indx_ref[k])
            l+=1
        if l == vertices_displaced.size-1:
            break

    print("ratio of displaced of all unmatched vertices %.4f" %(len(not_match)/vertices_ref.size))

    return emu_sf_unmatched[not_match], emu_sf_unmatched[indx_ref], np.array([sf_vtx_res_x, sf_vtx_res_y, sf_vtx_res_z])


if __name__ == '__main__':


    emu = load_table("/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf_trackID_vtx_merged_tx_aligned.h5", table_name = "EmuTracks")
    matched_emu = load_table("/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched_z_coordinate.h5", table_name = "MatchedTracks")
    pixel_vtcs_in = load_table("/home/niko/git/ship_tracking/data/run_2793/pix_vertices_emu_system.h5")

    redo_emu_ref = False
    chi2 = 25

    z_exit = 0 #-1.824
    ecc_width = 2.52 + 29*0.033 # 29 tungsten plates of 0.9 mm instead of 1mm lead.
    ecc_z_start = z_exit- ecc_width

    dtypes = []
    for j, name in enumerate(pixel_vtcs_in.dtype.names):
        dtypes.append((pixel_vtcs_in.dtype.names[j], pixel_vtcs_in[name].dtype))
    dtypes.extend([('matched', np.int32), ('match_chi2', np.float64)])
    pixel_vtcs = np.zeros(shape=pixel_vtcs_in.shape, dtype=dtypes)
    for column in pixel_vtcs_in.dtype.names:
        pixel_vtcs[column] = pixel_vtcs_in[column]

    unq_pix_vtcs, pix_vtcs_indices = np.unique(pixel_vtcs["vertexID"], return_index = True)
    pixel_vtcs = pixel_vtcs[pix_vtcs_indices]

    matched_emu.sort(order ="match_time")
    pixel_vtcs.sort(order = "timestamp")
    pix_ts, pix_ts_indices = np.unique(pixel_vtcs["timestamp"], return_index = True)
    matched_ts, matched_ts_indices, matched_ts_counts = np.unique(matched_emu["match_time"], return_index = True, return_counts = True)

    if redo_emu_ref:
        emu_ref_vtcs_in = get_emu_reference(matched_emu, emu, matched_ts, pix_ts, )
    else:
        emu_ref_vtcs_in = load_table("/home/niko/git/ship_tracking/data/run_2793/emu_ref_vertices_matching.h5")
        emu_ref_vtcs = emu_ref_vtcs_in[emu_ref_vtcs_in["vz"]>=ecc_z_start]

    # emu_ref_vtcs = algin_emu(emu_ref_vtcs_in)

    matched_out = match_vertices(emu_ref_vtcs, pixel_vtcs, max_chi2=chi2, max_dist = False)
    matches = matched_out[matched_out["matched"]>0]
    print("%i vertices considered" % matched_out.size)
    print("%i matches = %2.2f %% " %(matches.size , (matches.size / matched_out[matched_out["matchable"]==1].size)*100))

    plot_z_matching_eff(matched_out)

    matched_out_path = "/home/niko/git/ship_tracking/data/run_2793/matched_vertices_emu_system_chi2_%.1f.h5" %(chi2)
    save_table(matched_out, matched_out_path , node_name = "matched_vertices")

    emu_ref_path = "/home/niko/git/ship_tracking/data/run_2793/ref_vertices_emu_system_matched_chi2_%.1f.h5" %(chi2)
    save_table(emu_ref_vtcs, emu_ref_path , node_name = "emu_ref_vertices")

    displaced, ref, residuals = get_displacement(matched_vertices_in = matched_out_path, ref_vertices_in = emu_ref_path, emulsion_segments_in ="/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf_trackID_vtx_merged_tx_aligned.h5" )

    plot_displacement(displaced, ref, residuals)