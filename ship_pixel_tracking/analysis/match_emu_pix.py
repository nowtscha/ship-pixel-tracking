import numpy as np
from ship_pixel_tracking.tools import analysis_utils as tracking_utils, plot_utils
from ship_pixel_tracking.ConvertRoot import emu_trks_to_h5
import matplotlib.pyplot as plt
from tqdm import tqdm
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')


def select_vertices_with_unique_tracks(vtcs):
    '''
    tracks can be assigned to several vertices, this function selects only vertices with tracks that have not been assigned multiple times.
    '''
    vert_sel_mask = np.ones(dtype = np.bool_, shape = vtcs.shape)

    vert_sel = vtcs[vert_sel_mask]
    trk_ids, trk_indices, trk_counts = np.unique(vert_sel["trackID"], return_index = True, return_counts = True)

    # filter vertices with not uniquely assigned tracks
    duplicate_track_ids = trk_ids[trk_counts>1]
    track_ambig_mask = np.ones(dtype = np.bool_, shape = vert_sel.shape)
    for i, trkID in enumerate(tqdm(vert_sel["trackID"])):
        if trkID in duplicate_track_ids:
            track_ambig_mask[i] = 0
    vert_sel = vert_sel[track_ambig_mask]
    print("found %i vertices with unique track assignment" %vert_sel.size)
    return vert_sel


def select_unique_vertexID(vtcs):
    '''
    due to the large combinatorics the emulsion surface was divided into four quarters, 
    and the FEDRA vertex reconstruction algorithm was employed four times independently on each of this quarters.
    Therefore vertexIDs are not unique but up to 4 vertices can have the same ID. 
    This function drops all vertices with an ID that was used multiple times.
    '''
    
    vert_ambg_mask = np.ones(dtype = np.bool_, shape = vtcs.shape)
    vert_ids, vert_indices, vert_counts = np.unique(vtcs["vID"], return_index = True, return_counts = True)
    for v_id in tqdm(vert_ids):
        loc_mask = vtcs["vID"]==v_id
        vertices = vtcs[loc_mask]
        if not np.all(vertices["quarter"]==vertices[0]["quarter"]):
            vert_ambg_mask[loc_mask]=0

    vert_final = vtcs[vert_ambg_mask]
    return vert_final


def get_in_vertex_matching_rate(matched_vtcs, matched_tracks, selected_emulsion):
    '''
    Calculate the fraction of matchable tracks of one vertex that have been matched.
    '''
    matched_tracks.sort(order = ["emu_vid"])
    matchted_vtcs, matched_tracks_in_vtcs = np.unique(matched_tracks["emu_vid"], return_counts = True)

    selected_emulsion.sort(order = ["emu_vid"])
    sel_vtcs, sel_indices, tracks_in_sel_vtcs = np.unique(selected_emulsion["emu_vid"], return_index = True, return_counts = True)

    vtx_matching_eff = []
    # first unique vtx_id is -1, so start from second entry, also for counts of course
    for i, matched_vtx in enumerate(matchted_vtcs[1:]):
        rate = float(matched_tracks_in_vtcs[1:][i]/ tracks_in_sel_vtcs[sel_vtcs==matched_vtx])
        vtx_matching_eff.append(rate)

    return np.array(vtx_matching_eff)


def select_vertices(vtx_file_in_path):
    sel_vtx_file_out_path = vtx_file_in_path[:-3] + "_unique_selected.h5"

    vtcs = tracking_utils.load_table(vtx_file_in_path)
    vtx_sel = select_vertices_with_unique_tracks(vtcs)
    print("found %i vertices with unique track assignment" %vtx_sel.size)

    vtx_unique = select_unique_vertexID(vtx_sel)
    print("found %i vertices with unique vertexID" % vtx_unique.size)
    tracking_utils.save_table(vtx_unique, path = sel_vtx_file_out_path, node_name="EmuVtx")
    return sel_vtx_file_out_path


def select_pix_tracks(pix_tracks, chi_2_range=[0, 5.], x_pos_range=None, y_pos_range=None, x_slope_range=None, y_slope_range=None, n_hits=None, n_tracks = None):
    '''
    Cut on input pixel data. Topical selections based on pixel rest frame coordinate system.
    All selections are equally weighted.
    '''

    if chi_2_range is not None:
        chi2_selection = np.logical_and(pix_tracks["red_chi2"] >= chi_2_range[0], pix_tracks["red_chi2"] <= chi_2_range[1])
    else:
        chi2_selection = ~np.isnan(pix_tracks["red_chi2"])

    if x_pos_range is not None:
        x_selection = np.logical_and(pix_tracks["x"] >= x_pos_range[0], pix_tracks["x"] <= x_pos_range[1])
    else:
        x_selection = ~np.isnan(pix_tracks["x"])

    if y_pos_range is not None:
        y_selection = np.logical_and(pix_tracks["y"] >= y_pos_range[0], pix_tracks["y"] <= y_pos_range[1])
    else:
        y_selection = ~np.isnan(pix_tracks["y"])

    if x_slope_range is not None:
        x_slope_selection = np.logical_and(pix_tracks["x_slope"] >= x_slope_range[0], pix_tracks["x_slope"] <= x_slope_range[1])
    else:
        x_slope_selection = ~np.isnan(pix_tracks["x_slope"])

    if y_slope_range is not None:
        y_slope_selection = np.logical_and(pix_tracks["y_slope"] >= y_slope_range[0], pix_tracks["y_slope"] <= y_slope_range[1])
    else:
        y_slope_selection = ~np.isnan(pix_tracks["y_slope"])

    if n_hits is not None:
        nhits_selection = np.logical_and(pix_tracks["n_hits"] >= n_hits[0], pix_tracks["n_hits"] <= n_hits[1])
    else:
        nhits_selection = ~np.isnan(pix_tracks["n_hits"])
    
    if n_tracks is not None:
        ntracks_selection = np.logical_and(pix_tracks["n_tracks_evt"] >= n_tracks[0], pix_tracks["n_tracks_evt"] <= n_tracks[1])
    else:
        ntracks_selection = ~np.isnan(pix_tracks["n_tracks_evt"])

    selection = chi2_selection & x_selection & y_selection & x_slope_selection & y_slope_selection & nhits_selection & ntracks_selection

    return pix_tracks[selection]


def select_emulsion_tracks(emu_tracks, alignment, spill, chi2_range=[0, 100.], x_pos_range=None, y_pos_range=None, x_slope_range=None, y_slope_range=None, quarter=None, n_tracks_vtx=None, nseg=[2, 28], cut_on_spill_width=False, pixel_z_length = 13.8):
    '''
    Cut on input emulsion data. Topical selections based on pixel rest frame coordinate system.
    All selections are equally weighted.
    '''
    # only consider tracks leaving the emulsion. This is ensured by demanding z==0 , the last emulsion layer is at z=0
    exit_mask = emu_tracks["z"]==0

    if cut_on_spill_width:
        spill_mask, _ = cut_spill_width(emu_tracks, upper_lim=0.6, lower_lim=0.6)
    else:
        spill_mask = ~np.isnan(emu_tracks["y"])

    if chi2_range is not None:
        chi2_selection = np.logical_and(emu_tracks["track_chi2"] >= chi2_range[0], emu_tracks["track_chi2"] <= chi2_range[1])
    else:
        chi2_selection = ~np.isnan(emu_tracks["track_chi2"])

    if x_pos_range is not None:
        x_selection = np.logical_and(emu_tracks["x"] >= x_pos_range[0], emu_tracks["x"] <= x_pos_range[1])
    else:
        x_selection = ~np.isnan(emu_tracks["x"])

    if y_pos_range is not None:
        z_proj = pixel_z_length + alignment[spill]["z_0"]
        y_off = alignment[spill]["y_0"] + 2*spill
        # print("y offset spill %i: %f" %(spill, y_off))
        y_selection = np.logical_and((emu_tracks["y"] + emu_tracks["ty"]*z_proj - y_off) >= y_pos_range[0], (emu_tracks["y"] + emu_tracks["ty"]*z_proj - y_off) <= y_pos_range[1])
        # print("y mean: %f" %(emu_tracks[y_selection]["y"].mean()))
        
        # y_selection = np.logical_and(emu_tracks["y"] >= y_pos_range[0], emu_tracks["y"] <= y_pos_range[1])

    else:
        y_selection = ~np.isnan(emu_tracks["y"])

    if x_slope_range is not None:
        x_slope_selection = np.logical_and(emu_tracks["tx"] >= x_slope_range[0], emu_tracks["tx"] <= x_slope_range[1])
    else:
        x_slope_selection = ~np.isnan(emu_tracks["tx"])

    if y_slope_range is not None:
        y_slope_selection = np.logical_and(emu_tracks["ty"] >= y_slope_range[0], emu_tracks["ty"] <= y_slope_range[1])
    else:
        y_slope_selection = ~np.isnan(emu_tracks["ty"])

    if quarter is not None:
        quarter_selection = np.in1d(emu_tracks["quarter"], quarter)
    else:
        quarter_selection = ~np.isnan(emu_tracks["quarter"])

    if n_tracks_vtx is not None:
        ntracks_selection = np.logical_and(emu_tracks["nTracks"] >= n_tracks_vtx[0], emu_tracks["nTracks"] <= n_tracks_vtx[1])
    else:
        ntracks_selection = ~np.isnan(emu_tracks["nTracks"])

    if nseg is not None:
        nseg_selection = np.logical_and(emu_tracks["nseg"] >= nseg[0], emu_tracks["nseg"] <= nseg[1])
    else:
        nseg_selection = ~np.isnan(emu_tracks["nseg"])

    selection = spill_mask & chi2_selection & x_selection & y_selection & x_slope_selection & y_slope_selection & quarter_selection & ntracks_selection & nseg_selection & exit_mask

    return emu_tracks[selection]


def calculate_residuals(reference_tracks, matchable_tracks, emu_covs = None, max_pos_diff=[0.05, 0.05], max_slope_diff=[0.015, 0.015], max_chi2 = 100., printout= True):
    '''
    Actual work loop for matching. The combination of all tracks is achieved by looping over each reference track and choosing all matchable tracks within the given position and slope distances.
    The best match is determined by calculating chi2 via: r^T * V^(-1) * r where r is a column vector of residuals and V is the sum of the covariance matrices for the corresponding tracks.
    All results are in FairShip units of cm and rad.
    Here the same pixel track can be matched to several reference tracks, this ambiquity is resolved in the calling function.
    ----------------
    INPUT:
        reference_tracks : numpy structured array
            contains tracks for outer loop, with columns match_emu_x, match_emu_y, match_emu_tx, match_emu_ty, emu_trk
        matchable_tracks : numpy structured array
            contains tracks to choose from, with columns match_pix_x, match_pix_y, match_pix_tx, match_pix_ty, match_time, a unique track id and the covariance matrix diagonal (one column per entry)
        emu_covs : numpy structured array
            contains full covariance matrix of emulsion tracks (one column per entry, one row per track), the row number is the emu_track id
        max_pos_diff : iterable of floats, 2 entries
            distance of tracks to consider for matching
        max_slope_diff : iterable of floats, 2 entries
            slope distance of tracks to consider for matching
        max_chi2 : float
            chi2/NDF limit to consider track pair as valid
        printout : bool
            whether or not to print statistics
    OUTPUT:
        matched_tracks_out : numpy structured array
            has the same length as reference_tracks. All entries without matches will be -1
    '''

    npix = 0
    nemu = 0
    err = 0
    det = 0
    inv = 0
    psd = 0
    npsd_tries=0
    ntest =0

    matched_tracks_out = np.full(fill_value = -1, shape = reference_tracks.shape, dtype = tracking_utils.matched_emulsion_pixel_dtype)
    matched_tracks_out["matched"]=0
    matched_tracks_out["emu_trk"] = reference_tracks["emu_trk"]
  
    # hide progressbar in case of ambiguity removal loops
    if printout:
        itval = tqdm(range(int(reference_tracks.size)))
    else:
        itval = range(int(reference_tracks.size))

    for i in itval:
        chi2_list = []
        slope_range = np.logical_and(np.abs(matchable_tracks["match_pix_tx"] - reference_tracks["match_emu_tx"][i]) <= max_slope_diff[0],
                                    np.abs(matchable_tracks["match_pix_ty"] - reference_tracks["match_emu_ty"][i]) <= max_slope_diff[1])
        pos_range = np.logical_and(np.abs(matchable_tracks["match_pix_x"] - reference_tracks["match_emu_x"][i]) <= max_pos_diff[0],
                                np.abs(matchable_tracks["match_pix_y"] - reference_tracks["match_emu_y"][i]) <= max_pos_diff[1])
        # bitwise and of position and slope masks
        pix_range_mask = (slope_range & pos_range ) #& (matchable_tracks["matched"]==0))
        pix_range = matchable_tracks[pix_range_mask]

        if pix_range.size == 0:
            continue

        pix_tracks = np.array([pix_range["match_pix_x"], pix_range["match_pix_y"], pix_range["match_pix_tx"], pix_range["match_pix_ty"]], ndmin=2).T
        pix_tracks_covs = pix_range[["pix_x_error","pix_y_error","pix_tx_error","pix_ty_error", "pix_x_y_error", "pix_x_tx_error", "pix_x_ty_error", "pix_y_tx_error", "pix_y_ty_error", "pix_tx_ty_error"]]
        # emu_trkID = pix_range["trackID"]
        emu_track = np.array([reference_tracks["match_emu_x"][i], reference_tracks["match_emu_y"][i],
                            reference_tracks["match_emu_tx"][i], reference_tracks["match_emu_ty"][i]], ndmin=2)
        track_distance = (emu_track - pix_tracks)
        emu_cov = emu_covs[i]
        emu_covs_array = np.array([[emu_cov["x_x"]*1e-8, emu_cov["x_y"]*1e-8, emu_cov["x_tx"]*1e-4, emu_cov["x_ty"]*1e-4],
                                    [emu_cov["x_y"]*1e-8, emu_cov["y_y"]*1e-8, emu_cov["y_tx"]*1e-4, emu_cov["y_ty"]*1e-4],
                                    [emu_cov["x_tx"]*1e-4, emu_cov["y_tx"]*1e-4, emu_cov["tx_tx"], emu_cov["tx_ty"]],
                                    [emu_cov["x_ty"]*1e-4, emu_cov["y_ty"]*1e-4, emu_cov["tx_ty"] ,emu_cov["ty_ty"]]
                                ], dtype = np.float32)

        # emulsion covariances are provided in µm and rad!! so they need to be converted 
        # emu_covs_array = np.array([[emu_cov["x_x"]*1e-8, 0., 0., 0.],
        #                             [0.,emu_cov["y_y"]*1e-8, 0.,0.],
        #                             [0., 0., emu_cov["tx_tx"], 0.],
        #                             [0.,0.,0. ,emu_cov["ty_ty"]]
        #                         ])
            
        for pix_track in range(pix_tracks.shape[0]):
            # create pixel covariance matrix from individual pixel errors
            pix_track_cov = np.square(np.array([[pix_tracks_covs["pix_x_error"][pix_track], 0., 0., 0.],
                                                [0., pix_tracks_covs["pix_y_error"][pix_track], 0., 0.],
                                                [0., 0., pix_tracks_covs["pix_tx_error"][pix_track], 0.],
                                                [0., 0., 0., pix_tracks_covs["pix_ty_error"][pix_track]],
                                                ]))

            ntest +=1
            V = pix_track_cov + emu_covs_array
            
            if not np.all(np.linalg.eigvals(V) >= 0):
                try:
                    V = tracking_utils._make_matrix_psd_mod(V,atol=1e-7, rtol=1e-10)
                except RuntimeError:
                    npsd_tries +=1
                    continue
                
            if not np.all(np.linalg.eigvals(V) >= 0):
                psd +=1
                continue
            if not (np.linalg.det(V)!=0):
                det +=1
                continue
            if not np.allclose(V.dot(np.linalg.inv(V)),np.eye(4,4)):
                inv +=1
                continue
            nemu += 1
            
            try:
                # chi2 is r^T * V^(-1) * r where r is a column vector of residuals
                chi2 = np.linalg.multi_dot([track_distance[pix_track,:], np.linalg.inv(V), track_distance[pix_track,:].T])
                
                if chi2<0.:
                    print(np.vstack((emu_track, pix_tracks[pix_track,:])))
                    print(V)
                    raise ValueError("computed negative chi2")
                # return chi2/ndf. NDF = 4 dimensions * number of hits - 4 dimensions of resulting match = 4
                chi2_list.append(chi2/4)
            except np.linalg.LinAlgError:  # TODO: some covariance matrices are not invertable, seems to mean that track combination is not valid?
                err += 1
                chi2_list.append(1e19)

        if np.any(np.array(chi2_list)<max_chi2):
            best_match = np.argmin(chi2_list)
            match_result_row = np.argwhere(pix_range_mask==1)[best_match][0]

            matchable_tracks[match_result_row]["matched"] +=1
            matchable_tracks[match_result_row]["match_chi2"] = chi2_list[best_match]

            reference_tracks[i]["matched"] +=1
            reference_tracks[i]["match_chi2"] = chi2_list[best_match]

            matched_tracks_out[i]["matched"] +=1
            matched_tracks_out[i]["emu_vid"] = reference_tracks[i]["emu_vid"]
            matched_tracks_out[i]["emu_trk"] = reference_tracks[i]["emu_trk"]
            matched_tracks_out[i]["match_emu_x"] = emu_track[0][0]
            matched_tracks_out[i]["match_emu_y"] = emu_track[0][1]
            matched_tracks_out[i]["match_emu_tx"] = emu_track[0][2]
            matched_tracks_out[i]["match_emu_ty"] = emu_track[0][3]
            matched_tracks_out[i]["match_pix_x"] = matchable_tracks[match_result_row]["match_pix_x"]
            matched_tracks_out[i]["match_pix_y"] = matchable_tracks[match_result_row]["match_pix_y"]
            matched_tracks_out[i]["match_pix_tx"] = matchable_tracks[match_result_row]["match_pix_tx"]
            matched_tracks_out[i]["match_pix_ty"] = matchable_tracks[match_result_row]["match_pix_ty"]

            matched_tracks_out[i]["spill"] = matchable_tracks[match_result_row]["spill"]
            matched_tracks_out[i]["match_time"] = matchable_tracks[match_result_row]["match_time"]
            matched_tracks_out[i]["global_trackID"] = matchable_tracks[match_result_row]["global_trackID"]

            matched_tracks_out[i]["match_dxr"] = track_distance[best_match][0]
            matched_tracks_out[i]["match_dyr"] = track_distance[best_match][1]
            matched_tracks_out[i]["match_dtxr"] = track_distance[best_match][2]
            matched_tracks_out[i]["match_dtyr"] = track_distance[best_match][3]
            matched_tracks_out[i]["match_chi2"] = chi2_list[best_match]

        npix += 1
    if printout is True:
        print("looped %i reference tracks and %i matchable tracks, inversion failed %i times" % (npix, nemu, err))
        print("no psd matrix found : %i times in %i tries\nmatrix not invertable : %i times, = %.2f %% \nmatrix determinant not !=0 : %i times"% (psd, npsd_tries, inv, (inv/ntest * 100), det))

    return matched_tracks_out


def calculate_residuals_pix(emulsion_tracks_in, pix_tracks_in, emu_covs, max_pos_diff=[0.05, 0.05], max_slope_diff=[0.015, 0.015], max_chi2 = 100.):
    '''
    Actual work loop for matching. This loop is using pixel tracks as reference and loops over emulsion tracks.
    The combination of all tracks is achieved by looping over each reference track and choosing all matchable tracks within the given position and slope distances.
    The best match is determined by calculating chi2 via: r^T * V^(-1) * r where r is a column vector of residuals and V is the sum of the covariance matrices for the corresponding tracks.
    Here the same pixel track can be matched to several reference tracks, this ambiquity is resolved in the calling function.
    ----------------
    INPUT:
        pix_tracks_in : numpy structured array
            contains tracks for inner loop, with columns match_pix_x, match_pix_y, match_pix_tx, match_pix_ty, match_time, a unique track id and the covariance matrix diagonal (one column per entry) 
        emulsion_tracks_in : numpy structured array
            contains tracks to choose from, with columns match_emu_x, match_emu_y, match_emu_tx, match_emu_ty, emu_trk
        emu_covs : numpy structured array
            contains full covariance matrix of emulsion tracks (one column per entry, one row per track), the row number is the emu_track id
        max_pos_diff : iterable of floats, 2 entries
            distance of tracks to consider for matching
        max_slope_diff : iterable of floats, 2 entries
            slope distance of tracks to consider for matching
        max_chi2 : float
            chi2 limit to consider track pair as valid, NOT divided by NDF
        printout : bool
            whether or not to print statistics
    OUTPUT:
        matched_tracks_out : numpy structured array
            has the same length as reference_tracks. All entries without matches will be -1
    '''

    npix = 0
    nemu = 0
    err = 0
    det = 0
    inv = 0
    psd = 0
    npsd_tries=0
    ntest =0
    
    # if emu_covs is None:
    #     # use same values for all tracks
    #     emu_covs = np.array([[0.0015*0.0015 + 0.0003*0.0003, 0, 0, 0],
    #                       [0, 0.00263*0.00263 + 0.0003*0.0003, 0, 0],
    #                       [0,0,0.00026*0.00026 + 0.001*0.001,0],
    #                       [0,0,0,0.00025*0.00025 + 0.001*0.001]])

    for i in tqdm(range(int(pix_tracks_in.size))):
        chi2_list = []
        slope_range = np.logical_and(np.abs(pix_tracks_in["match_pix_tx"][i] - emulsion_tracks_in["tx"]) <= max_slope_diff[0],
                                     np.abs(pix_tracks_in["match_pix_ty"][i] - emulsion_tracks_in["ty"]) <= max_slope_diff[1])
        pos_range = np.logical_and(np.abs(pix_tracks_in["match_pix_x"][i] - emulsion_tracks_in["x"]) <= max_pos_diff[0],
                                   np.abs(pix_tracks_in["match_pix_y"][i] - emulsion_tracks_in["y"]) <= max_pos_diff[1])
        # bitwise and of position and slope masks
        emu_range_mask = (slope_range & pos_range ) #& (emulsion_tracks_in["matched"]==0))

        emu_range = emulsion_tracks_in[emu_range_mask]
        if emu_range.size == 0:
            continue
        emu_tracks = np.array([emu_range["x"], emu_range["y"], emu_range["tx"], emu_range["ty"]], ndmin=2).T
        emu_trkID = emu_range["trackID"]
        pix_track = np.array([pix_tracks_in["match_pix_x"][i], pix_tracks_in["match_pix_y"][i],
                              pix_tracks_in["match_pix_tx"][i], pix_tracks_in["match_pix_ty"][i]], ndmin=2)
        track_distance = (pix_track - emu_tracks)

        pix_track_cov = np.square(np.array([[pix_tracks_in["pix_x_error"][i], 0., 0., 0.],
                                 [0., pix_tracks_in["pix_y_error"][i], 0., 0.],
                                 [0., 0., pix_tracks_in["pix_tx_error"][i], 0.],
                                 [0., 0., 0., pix_tracks_in["pix_ty_error"][i]],
                                ]))
        # pix_track_cov = np.array([[pix_tracks_in["pix_x_error"][i] * pix_tracks_in["pix_x_error"][i], pix_tracks_in["pix_x_y_error"][i], pix_tracks_in["pix_x_tx_error"][i], pix_tracks_in["pix_x_ty_error"][i]],
        #                          [pix_tracks_in["pix_x_y_error"][i], pix_tracks_in["pix_y_error"][i] * pix_tracks_in["pix_y_error"][i] , pix_tracks_in["pix_y_tx_error"][i], pix_tracks_in["pix_y_ty_error"][i]],
        #                          [pix_tracks_in["pix_x_tx_error"][i], pix_tracks_in["pix_y_tx_error"][i], pix_tracks_in["pix_tx_error"][i] * pix_tracks_in["pix_tx_error"][i], pix_tracks_in["pix_tx_ty_error"][i]],
        #                          [pix_tracks_in["pix_x_ty_error"][i], pix_tracks_in["pix_y_ty_error"][i], pix_tracks_in["pix_tx_ty_error"][i], pix_tracks_in["pix_ty_error"][i] * pix_tracks_in["pix_ty_error"][i]]
        #                         ])
            
        for emu_track in range(emu_tracks.shape[0]):
            # create emulsion covariance matrix
            emu_cov = emu_covs[emu_trkID[emu_track]]
            # emu_covs_array = np.array([[emu_cov["x_x"]*1e-8, emu_cov["x_y"]*1e-8, emu_cov["x_tx"]*1e-4, emu_cov["x_ty"]*1e-4],
            #                             [emu_cov["x_y"]*1e-8, emu_cov["y_y"]*1e-8, emu_cov["y_tx"]*1e-4, emu_cov["y_ty"]*1e-4],
            #                             [emu_cov["x_tx"]*1e-4, emu_cov["y_tx"]*1e-4, emu_cov["tx_tx"], emu_cov["tx_ty"]],
            #                             [emu_cov["x_ty"]*1e-4, emu_cov["y_ty"]*1e-4, emu_cov["tx_ty"] ,emu_cov["ty_ty"]]
            #                         ], dtype = np.float32)

            # emulsion covariances are provided in µm and rad!! so they need to be converted 
            emu_covs_array = np.array([[emu_cov["x_x"]*1e-8, 0., 0., 0.],
                                        [0.,emu_cov["y_y"]*1e-8, 0.,0.],
                                        [0., 0., emu_cov["tx_tx"], 0.],
                                        [0.,0.,0. ,emu_cov["ty_ty"]]
                                    ])
            ntest +=1
            V = emu_covs_array + pix_track_cov
            
            if not np.all(np.linalg.eigvals(V) >= 0):
                try:
                    V = tracking_utils._make_matrix_psd_mod(V,atol=1e-7, rtol=1e-10)
                except RuntimeError:
                    npsd_tries +=1
                    continue
                
            if not np.all(np.linalg.eigvals(V) >= 0):
                psd +=1
                continue
            if not (np.linalg.det(V)!=0):
                det +=1
                continue
            if not np.allclose(V.dot(np.linalg.inv(V)),np.eye(4,4)):
                inv +=1
                continue
            nemu += 1
            
            try:
                # chi2 is r^T * V^(-1) * r where r is a column vector of residuals
                chi2 = np.linalg.multi_dot([track_distance[emu_track,:], np.linalg.inv(V), track_distance[emu_track,:].T])
                
                if chi2<0.:
                    print(np.vstack((pix_track, emu_tracks[emu_track,:])))
                    print(V)
                    raise ValueError("computed negative chi2")
                chi2_list.append(chi2)
            except np.linalg.LinAlgError:  # TODO: some covariance matrices are not invertable, seems to mean that track combination is not valid?
                err += 1
                chi2_list.append(1e19)

        if np.any(np.array(chi2_list)<max_chi2):
            best_match = np.argmin(chi2_list)
            matched_emu = emu_range[best_match]
            emulsion_tracks_in["matched"][np.argwhere(emu_range_mask==1)[best_match]] += 1
            
            pix_tracks_in[i]["match_emu_x"] = matched_emu["x"]
            pix_tracks_in[i]["match_emu_y"] = matched_emu["y"]
            pix_tracks_in[i]["match_emu_tx"] = matched_emu["tx"]
            pix_tracks_in[i]["match_emu_ty"] = matched_emu["ty"]
            pix_tracks_in[i]["emu_vid"] = matched_emu["vID"]
            pix_tracks_in[i]["emu_trk"] = matched_emu["trackID"]
            pix_tracks_in[i]["match_dxr"] = track_distance[best_match][0]
            pix_tracks_in[i]["match_dyr"] = track_distance[best_match][1]
            pix_tracks_in[i]["match_dtxr"] = track_distance[best_match][2]
            pix_tracks_in[i]["match_dtyr"] = track_distance[best_match][3]
            pix_tracks_in[i]["match_chi2"] = chi2_list[best_match]
            pix_tracks_in[i]["matched"] += 1

        npix += 1

    print("looped %i pixel tracks and %i emulsion tracks, inversion failed %i times" % (npix, nemu, err))
    print("no psd matrix found : %i times in %i tries\nmatrix not invertable : %i times, = %.2f %% \nmatrix determinant not !=0 : %i times"% (psd, npsd_tries, inv, (inv/ntest * 100), det))


def match_tracks(emulsion_tracks_in, pix_tracks_in, emu_covs, max_pos_diff=[0.05, 0.05], max_slope_diff=[0.015, 0.015], max_chi2 = 100.):
    '''
    Connect emulsion and pixel tracks based on chi2 minimization. Both track sets have to be in a common rest frame.
    For given slope and position ranges all sensible combinations of tracks are calculated and the best one is assigned. To ambiguously matched tracks are resolved by looping again.
    Uses true diagonal covariance matrices of both tracks. If either position or slope are too different the corresponding track combination will not be considered.
    ----------------
    INPUT:
        emulsion_tracks_in : numpy structured array
            contains emulsion data, with columns match_emu_x, match_emu_y, match_emu_tx, match_emu_ty, emu_trk
        pix_tracks_in : numpy structured array
            contains pixel data with columns match_pix_x, match_pix_y, match_pix_tx, match_pix_ty, match_time, a unique track id and the covariance matrix diagonal (one column per entry)
        emu_covs : numpy structured array
            contains full covariance matrix of emulsion tracks (one column per entry, one row per track), the row number is the emu_track id
        max_pos_diff : iterable of floats, 2 entries
            distance of tracks to consider for matching
        max_slope_diff : iterable of floats, 2 entries
            slope distance of tracks to consider for matching
        max_chi2 : float
            chi2/NDF limit to consider track pair as valid
    OUTPUT:
        matched_tracks_out : numpy structured array
            has the same length as emulsion_tracks_in. All entries without matches will be -1
    '''

    
    matched_tracks_out = calculate_residuals(reference_tracks = emulsion_tracks_in, matchable_tracks = pix_tracks_in, emu_covs = emu_covs, max_pos_diff = max_pos_diff, max_slope_diff =max_slope_diff , max_chi2 = max_chi2, printout = True)
    
    # choose ambiguously matched pixel tracks and assign to best fit, redo matching for remaining emulsion tracks
    ids1, counts1 = np.unique(matched_tracks_out["global_trackID"], return_counts = True)
    # matched_tracks_out array is initalized with -1 so the first id will be -1, we do not need this so we start from the second unique id
    ids = ids1[1:]
    counts = counts1[1:]
    
    # loop until all ambiguities are resolved
    while any (counts>1) : 
        print("%i pixel tracks matched ambiguously, these tracks will be rematched" %ids[counts>1].size)

        pix_mask = np.ones(shape = pix_tracks_in.shape, dtype = bool)
        # only choose matchable tracks which are NOT the ambigously matched track
        for id in ids:
            pix_mask[pix_tracks_in["global_trackID"] == id]=0
        
        # loop over ambiguously matched tracks
        for id in tqdm(ids[counts>1]):
            # choose all reference tracks where the same matchable track was matched
            matches = matched_tracks_out[matched_tracks_out["global_trackID"]==id]
            mask = np.ones(shape = matches.shape, dtype = bool)
            # choose correct match
            mask[np.argmin(matches["match_chi2"])]=0
            # reset output array where the ambiguous matchable track was incorrectly matched 
            matches[['pix_trk', 'match_chi2', 'match_time', 'match_pix_x', 'match_pix_y', 'match_pix_tx', 'match_pix_ty', 'match_dx', 'match_dy', 'match_dtx', 'match_dty', 'match_dxr', 'match_dyr', 'match_dtxr', 'match_dtyr', 'x_0', 'y_0', 'z_0', 'tx_0', 'ty_0', 'txy_0', 'v_x', 'v_y', 'spill', 'pix_x_error', 'pix_y_error', 'pix_tx_error', 'pix_ty_error', 'pix_x_y_error', 'pix_x_tx_error', 'pix_x_ty_error', 'pix_y_tx_error', 'pix_y_ty_error', 'pix_tx_ty_error', 'global_trackID']][mask]=-1
            matches['matched'][mask]=0
            # match again with remaining tracks
            second_matches = calculate_residuals(reference_tracks = matches[mask], matchable_tracks = pix_tracks_in[pix_mask], emu_covs = emu_covs[matches[mask]["emu_trk"]], max_pos_diff = max_pos_diff, max_slope_diff =max_slope_diff , max_chi2 = max_chi2, printout = False)
            # fill output array with new matches
            for match in second_matches:
                select_row = np.argwhere(matched_tracks_out["emu_trk"]==match["emu_trk"])
                for column in match.dtype.names:
                    matched_tracks_out[column][select_row] = match[column]
                select_emu_row = np.argwhere(emulsion_tracks_in["emu_trk"]==match["emu_trk"])
                emulsion_tracks_in["matched"][select_emu_row] = match["matched"]
                emulsion_tracks_in["match_chi2"][select_emu_row] = match["match_chi2"]

        # look for tracks again matched ambiguously
        ids1, counts1 = np.unique(matched_tracks_out["global_trackID"], return_counts = True)
        ids = ids1[1:]
        counts = counts1[1:]

    return matched_tracks_out
    

def cut_spill_width(emu_tracks, upper_lim=[0.7]*5, lower_lim=[0.7]*5):
    # for now hardcoded values from Chris' matching file, since spill is not a feature of emulsion track set.
    # TODO: change to automated peak detection.
    
    mask = np.zeros(shape=emu_tracks.shape, dtype=np.bool_)
    masks = {}
    for mean, spill, upper_limit, lower_limit in zip([0.94671804, 2.5423198, 4.5117917, 6.510371, 8.3560705], [8, 9, 10, 11, 12], upper_lim, lower_lim):
        # mask[np.logical_and(emu_tracks["y"]<(mean+upper_lim),emu_tracks["y"]>(mean-lower_lim))] = 1
        masks[spill] = np.logical_and(emu_tracks["y"] < (mean+upper_limit), emu_tracks["y"] > (mean-lower_limit))
        mask[masks[spill]] = 1
    return mask, masks


if __name__ == "__main__":
    # vertex selection
    # vertex_file = "/media/niko/big_data/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6_vertices_merged.h5"
    # sel_vtx_file_path = select_vertices(vertex_file)
    # emu_tracks_sel_path = emu_trks_to_h5.merge_vtx_sf("/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf.h5", sel_vtx_file_path )

    emu_tracks_sel_path = emu_trks_to_h5.merge_vtx_sf("/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf.h5", "/media/niko/big_data/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6_vertices_merged.h5" )

    # pix_tracks_path = "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/pix_tracks_nhits.h5"
    pix_tracks_path = "/home/niko/git/ship_tracking/data/run_2793/pix_tracks.h5"
    emu_tracks_path = "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/linked_tracks_sf_trackID_vtx_merged.h5"
    alignment_file_path = "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched_alignment.yaml"
    emu_cov_path =  "/home/niko/cernbox/SHiP/charm_xsec_july18/match_emu/CH1R6_emu_sf_covs.h5"
    good_emu_vtcs_path = "/media/niko/big_data/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/good_vertices_CH1R6.h5"
    
    tracking_utils.fill_global_trackID(pix_tracks_path)

    pix_tracks_rest_frame = tracking_utils.load_table(pix_tracks_path, table_name="Tracks")
    emulsion_tracks_start = tracking_utils.load_table(emu_tracks_path)
    emu_covs = tracking_utils.load_table(emu_cov_path, table_name="CovMat")
    valid_vtcs = tracking_utils.load_table(good_emu_vtcs_path, table_name="goodtrks")["vtx_vID"]
    alignment = tracking_utils.load_config(alignment_file_path)

    # global emulsion alignment from Chris
    emulsion_tracks_start["tx"] += 2.1532e-3
    emulsion_tracks_start["ty"] += -4.63719e-3

    emu_dtype = tracking_utils.emu_matching_input_dtype + [("match_chi2", "<f4")]
    emulsion_tracks = np.full(fill_value = 0, shape = emulsion_tracks_start.shape, dtype =emu_dtype )

    for dtype in emulsion_tracks_start.dtype.names:
        emulsion_tracks[dtype] = emulsion_tracks_start[dtype]
    
    emulsion_tracks["match_emu_x"] = emulsion_tracks_start["x"]
    emulsion_tracks["match_emu_y"] = emulsion_tracks_start["y"]
    emulsion_tracks["match_emu_tx"] = emulsion_tracks_start["tx"]
    emulsion_tracks["match_emu_ty"] = emulsion_tracks_start["ty"]
    emulsion_tracks["emu_trk"] = emulsion_tracks_start["trackID"]
    emulsion_tracks["emu_vid"] = emulsion_tracks_start["vID"]
    emulsion_tracks["match_chi2"] = -1

    # only select "good vertices": compare vID with vertexID from valid_vtcs
    valid_track_mask = np.isin(emulsion_tracks["emu_vid"], valid_vtcs)
    emulsion_tracks = emulsion_tracks[valid_track_mask]
    _, spill_masks = cut_spill_width(emulsion_tracks) #upper_lim=[0.225, 0.45, 0.45, 0.45, 0.6], lower_lim=[1, 0.45, 0.45 ,0.45, 0.45]
    
    # for chi2 in np.arange(75,125,0.5):
    for chi2 in [25]:
        append = False
        for spill, mask in spill_masks.items():
            emulsion_tracks["spill"][mask] = spill
            print("matching spill %i" % spill)
            selected_pixel = select_pix_tracks(pix_tracks_rest_frame[pix_tracks_rest_frame["spill"] == spill], n_hits=[4, 100], n_tracks = [2,1000])
            selected_emulsion = select_emulsion_tracks(emulsion_tracks[mask], alignment = alignment, spill=spill, nseg=[2, 28], x_slope_range=[-0.15, 0.15], y_pos_range=[-20., 20.], n_tracks_vtx = [6,1000], chi2_range=[0,0.1],y_slope_range=[-0.15, 0.15], pixel_z_length = 455.) # 
            pix_transformed, _ = tracking_utils.transform_to_emulsion(selected_pixel, alignment_file=alignment_file_path)
            matched_emulsion = match_tracks(emulsion_tracks_in = selected_emulsion, pix_tracks_in = pix_transformed, emu_covs=emu_covs, max_chi2 = chi2, max_slope_diff = [0.015,0.015], max_pos_diff=[0.1,0.1])
            tracking_utils.save_table([matched_emulsion, selected_emulsion, selected_pixel, pix_transformed], path = "/home/niko/cernbox/storage/chi2_analysis/python_matched_tracks_ch1r6_emu_cov_%.1f.h5"%chi2, node_name=["MatchedTracks", "SelectedEmulsion", "SelectedPixel", "SelectedTransformedPixel"], append = append)
            append = True