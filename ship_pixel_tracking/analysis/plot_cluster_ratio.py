import numpy as np
import tables as tb
import os.path

from matplotlib.backends.backend_pdf import PdfPages
from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_1d_bar, plot_xy, plot_2d_hist
from ship_pixel_tracking.tools.analysis_utils import load_table


def compare_single_modules(in_files):
    with PdfPages("/Volumes/big_data/charm_testbeam_july18/analysis/output_folder_run_2818/check_module_orientation.pdf") as out_pdf:
        for i, in_file_path in enumerate(in_files):
            min_hits = 1
            limits = None
            
            hits = load_table(in_file_path, table_name="Hits")
            hits_hist, xbins, ybins = np.histogram2d(hits["column"], hits["row"], bins = [np.arange(1,81), np.arange(1,337)])
            if i == 4:
                limits = [1,np.mean(hits_hist)*10]
            plot_2d_hist(hist2d=hits_hist, output_pdf = out_pdf, bins = [xbins,ybins], xlabel="column", ylabel = "row", limits = limits, min_hits = min_hits, title = "FrontEnd %i - %i hits" %(i, hits_hist.sum()))
            print("plotted FE %i" %i)


def cluster_vs_sensor_thickness(in_file):
    output_folder = os.path.split(in_file)[0]
    out_pdf = os.path.join(output_folder,"cluster_ratios.pdf")
    thin_clusters = []
    thick_clusters = []
    ratios = []
    with tb.open_file(in_file) as in_file_h5:
        merged_clusters = in_file_h5.root.MergedClusters[:]
        with PdfPages(out_pdf) as output_pdf:
            for spill in range(0,20):
                merged_clusters_spill = merged_clusters[merged_clusters["spill"]==spill]
                split_spill(merged_clusters_spill, output_pdf, spill = spill)
            split_spill(merged_clusters, output_pdf)
            for i in [2,11]:
                thin_clusters.append(np.float(merged_clusters[merged_clusters["n_hits_dut_%i" %i]>=2].shape[0])/merged_clusters[merged_clusters["n_hits_dut_%i" %i]==1].shape[0])
            for i in [0,1,3,4,5,6,7,8,9,10]:
                thick_clusters.append(np.float(merged_clusters[merged_clusters["n_hits_dut_%i" %i]>=2].shape[0])/merged_clusters[merged_clusters["n_hits_dut_%i" %i]==1].shape[0])
            thickness = [245,245,200,245,245,245,245,245,245,245,245,200]
            ratios = [thick_clusters[0],thick_clusters[1],thin_clusters[0],thick_clusters[2],thick_clusters[3],thick_clusters[4],thick_clusters[5],thick_clusters[6],thick_clusters[7],thick_clusters[8],thick_clusters[9],thin_clusters[1]]

            plot_xy(x=thickness, y= ratios, output_pdf = output_pdf, xlim=None, xlabel="sensor thickness [$\mu$m]", ylim=None, 
                    ylabel="clustersize >= 2 / clustersize 1", title="cluster size ratio vs sensor thickness", 
                    legend=None, output_png=None, markersize = 1, linfit = False, fit_data = None)

            plot_xy(x=np.arange(0,12), y= ratios, output_pdf = output_pdf, xlim=None, xlabel="module ID", ylim=None, 
                    ylabel="clustersize >= 2 / clustersize 1", title="cluster size ratio vs module position", 
                    legend=None, output_png=None, markersize = 1, linfit = False, fit_data = None)
            
            plot_xy(x=ratios, y= thickness, output_pdf = output_pdf, xlim=None, xlabel="clustersize >= 2 / clustersize 1", ylim=None, 
                    ylabel="sensor thickness [$\mu$m]", title="cluster size ratio vs sensor thickness", 
                    legend=None, output_png=None, markersize = 1, linfit = False, fit_data = None)


def split_spill(merged_clusters, output_pdf, spill = None):
    bins = np.linspace(merged_clusters["trigger_time_stamp"].min(),merged_clusters["trigger_time_stamp"].max(), 50)
    large_cluster_hist,_ = np.histogram(merged_clusters["trigger_time_stamp"][merged_clusters["n_hits_dut_1"]>=2],bins = bins)
    single_pixel_cluster_hist,_ = np.histogram(merged_clusters["trigger_time_stamp"][merged_clusters["n_hits_dut_1"]==1],bins = bins)
    ratio_hist = [np.float(large_cluster_hist[i]) / np.float(single_pixel_cluster_hist[i])for i,ts in enumerate(bins[:-1])]

    if spill is not None:
        title = ("Run 2815 module - 1\nRatio of large and single pixel clusters within spill %s" % spill)
    else:
        title = ("Run 2815 module 1 - \nRatio of large and single pixel clusters for all spills")
    plot_1d_bar(hist_in = ratio_hist, output_pdf= output_pdf, bar_edges = bins*25/1e6, xlabel = "timestamp [ms]", ylabel = "nhits>=2/nhits=1", title = title )


def angles_in_spill(track_array, output_pdf, spill = None, out_file = None):
    bin_edges = np.linspace(track_array["timestamp"].min(),track_array["timestamp"].max(), 51)
    stds_x_angle = np.full(shape = (bin_edges.shape[0]-1,2), fill_value = np.nan)
    stds_y_angle = np.full(shape = (bin_edges.shape[0]-1,2), fill_value = np.nan)
    for i in range(0,bin_edges.shape[0]-1):
        if i == bin_edges.shape[0]-2:
            stds_x_angle[i][0] = np.mean(track_array["x_slope"][track_array["timestamp"]>= bin_edges[i]])
            stds_y_angle[i][0] = np.mean(track_array["y_slope"][track_array["timestamp"]>= bin_edges[i]])
            stds_x_angle[i][1] = np.std(track_array["x_slope"][track_array["timestamp"]>= bin_edges[i]])
            stds_y_angle[i][1] = np.std(track_array["y_slope"][track_array["timestamp"]>= bin_edges[i]])
        else:
            stds_x_angle[i][0] = np.mean(track_array["x_slope"][np.logical_and(track_array["timestamp"]>= bin_edges[i], track_array["timestamp"]<bin_edges[i+1])])
            stds_y_angle[i][0] = np.mean(track_array["y_slope"][np.logical_and(track_array["timestamp"]>= bin_edges[i], track_array["timestamp"]<bin_edges[i+1])])
            stds_x_angle[i][1] = np.std(track_array["x_slope"][np.logical_and(track_array["timestamp"]>= bin_edges[i], track_array["timestamp"]<bin_edges[i+1])])
            stds_y_angle[i][1] = np.std(track_array["y_slope"][np.logical_and(track_array["timestamp"]>= bin_edges[i], track_array["timestamp"]<bin_edges[i+1])])
    # x_angle = np.arctan(track_array["x_slope"])
    # x_angle_hist, _ = np.histogram(x_angle, bins = bins)
    # y_angle = np.arctan(track_array["y_slope"])
    # y_angle_hist, _ = np.histogram(y_angle, bins = bins)
    if spill is not None:
        title = "Run 2815 track angles in spill %s" % spill
    else:
        title = "Run 2815 track angles in spill for all spills"
    plot_1d_bar(hist_in = stds_x_angle[:,0], output_pdf = output_pdf, bar_edges = bin_edges[:-1]*25/1e6, 
                xlim=None, ylim = None, xlabel="timestamp [ms]", ylabel = "x angle", 
                title=title, legend = None, log = False, output_png = None, vlines=None, 
                gauss_fit=False, yerr = stds_x_angle[:,1], error_kw = {"elinewidth": 0.7, "capthick": 0.7}, capsize = 2)
    plot_1d_bar(hist_in = stds_y_angle[:,0], output_pdf = output_pdf, bar_edges = bin_edges[:-1]*25/1e6, 
                xlim=None, ylim = None, xlabel="timestamp [ms]", ylabel = "y angle", 
                title=title, legend = None, log = False, output_png = None, vlines=None, 
                gauss_fit=False, yerr = stds_y_angle[:,1], error_kw = {"elinewidth": 0.7, "capthick": 0.7}, capsize = 2)


def plot_tracks(tracks_file_in):
    output_folder = os.path.split(tracks_file_in)[0]
    out_file = os.path.join(output_folder,"track_angle_vs_time")
    with tb.open_file(tracks_file_in) as in_file_h5:
        tracks = in_file_h5.root.Tracks[:]
        with PdfPages(out_file + ".pdf") as output_pdf:
            for spill in range(0,tracks["spill"].max()):
                print( "starting spill %s" % spill)
                angles_in_spill(tracks[tracks["spill"]==spill], output_pdf, spill = spill)
            angles_in_spill(track_array = tracks, spill = None, output_pdf = output_pdf, out_file = out_file)



def get_plane_files(pyBARrun, subpartition_dir, string='s_hi_p.h5'):

    first_plane_modules = ('module_0','module_1','module_2','module_3')
    second_plane_modules = ('module_4','module_5','module_6','module_7')

    partID = subpartition_dir[subpartition_dir.find('0x08') + 5]

    first_plane, second_plane = [], []
    for dirpath,_,filenames in os.walk(subpartition_dir):
        for f in filenames:
            if pyBARrun in f and string in f: #f[-9:] == 's_hi_p.h5':
                if 'module_0' in f or 'module_1'in f or 'module_2'in f or 'module_3'in f :
                    first_plane.append(os.path.abspath(os.path.join(dirpath, f)))
                elif 'module_4' in f or 'module_5'in f or 'module_6'in f or 'module_7'in f :
                    second_plane.append(os.path.abspath(os.path.join(dirpath, f)))

    return sorted(first_plane), sorted(second_plane)

if __name__ == '__main__':

    hit_files = [
    #             "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0800/module_0/396_module_0_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
    #             "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0800/module_1/396_module_1_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
    #             "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0800/module_2/396_module_2_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
    #             "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0800/module_3/396_module_3_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
    #             "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0800/module_5/396_module_5_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
    #             "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0800/module_6/396_module_6_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
    #             "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0800/module_7/396_module_7_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",

                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_0/290_module_0_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_1/290_module_1_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_2/290_module_2_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_3/290_module_3_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_4/290_module_4_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_5/290_module_5_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_6/290_module_6_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0801/module_7/290_module_7_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",

                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_0/224_module_0_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_1/224_module_1_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_2/224_module_2_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_3/224_module_3_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_4/224_module_4_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_5/224_module_5_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_6/224_module_6_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",
                # "/Volumes/big_data/charm_testbeam_july18/pybar_data/part_0x0802/module_7/224_module_7_ext_trigger_scan_s_hi_p_interpreted_formatted_fei4.h5",

                ]

    compare_single_modules(hit_files)
    # plot_tracks("/Users/Niko/Desktop/charm_exp_2018/run_2815/pix_tracks_tracks.h5")
    # cluster_vs_sensor_thickness("/Users/Niko/Desktop/charm_exp_2018/run_2815/Merged_spills.h5")
