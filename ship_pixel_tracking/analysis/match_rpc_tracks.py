import numpy as np
import tables as tb
import logging
import os

import matplotlib.pyplot as plt
from numba import njit
from numba.typed import List
from tqdm import tqdm

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt

from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_xy, plot_1d_bar, plot_2d_hist
from ship_pixel_tracking.tools.analysis_utils import save_table
from beam_telescope_analysis.tools.analysis_utils import get_events_in_both_arrays

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s')

def plot_spill_xy_all(clusters_in_file):
	with tb.open_file(clusters_in_file, "r") as clusters_in:
		tracks = clusters_in.root.Tracks[:]
	plt.plot(tracks["x"], tracks["y"], linestyle = "None", marker = ".")
	plt.title("Pixel tracks in s1f255fcf - %s entries" % tracks["x"].shape[0])
	plt.grid()
	plt.xlabel("x [cm]")
	plt.ylabel("y [cm]")
	plt.show()

def plot_spill_xy_matched(matched_clusters_in):
	with tb.open_file(matched_clusters_in, "r") as clusters_in:
		tracks = clusters_in.root.MergedClusters[:]
	plt.plot(tracks["x2"], tracks["y2"], linestyle = "None", marker = ".")
	plt.title("matched RPC tracks in s1f255fcf - %s entries" % tracks["x2"].shape[0])
	plt.grid()
	plt.ylim(-23,-19)
	plt.xlim(-5,5)
	plt.xlabel("x [cm]")
	plt.ylabel("y [cm]")
	plt.show()

def plot_xy_rpc(rpc_in):
	with tb.open_file(rpc_in, "r") as clusters_in:
		tracks = clusters_in.root.RPCTracks[:]
	plt.plot(tracks["x2"], tracks["y2"], linestyle = "None", marker = ".")
	plt.title("RPC tracks in s1f255fcf - %s entries" % tracks["x2"].shape[0])
	plt.grid()
	plt.xlabel("x [cm]")
	plt.ylabel("y [cm]")
	plt.show()

def plot_nhits_per_event(matched_clusters_in):

	with tb.open_file(matched_clusters_in, "r") as clusters_in:
		clusters = clusters_in.root.MergedClusters[:]
	nhits_per_event = np.full(fill_value = -1, shape = clusters.shape, dtype = [("event_number", np.int64), ("n_hits", np.int32), ("timestamp", np.int64)])
	events = np.unique(clusters["event_number"])
	j=0
	for k, event in enumerate(events):

		while clusters[j]["event_number"] == event:
			for i in range(12):
				nhits_per_event["n_hits"][k]+= clusters[j]["n_hits_dut_%s" % i]
			nhits_per_event["timestamp"][k] = clusters[j]["trigger_time_stamp"]
			nhits_per_event["event_number"][k] = clusters[j]["event_number"]
			if j == clusters.shape[0] - 1:
				break
			j+=1
	nhits_per_event = nhits_per_event[nhits_per_event["event_number"]!=-1]
	plt.hist(clusters["trigger_time_stamp"]*25*1e-9, bins = 50)
	plt.title("nhits per timestamp - %s entries" % clusters["trigger_time_stamp"].shape[0])
	# plt.plot(nhits_per_event["timestamp"]*25*1e-9, nhits_per_event["n_hits"], linestyle = "None", marker = ".")
	# plt.ylim(10,2000)
	# plt.yscale("log")
	plt.grid()
	plt.show()


def plot_ntracks_per_event(merged_file_in):
	with tb.open_file(merged_file_in,"r") as in_file: 
		tracks = in_file.root.MergedClusters[:] 
		_, ntracks = np.unique(tracks["event_number"], return_counts = True)
		print(np.median(ntracks))
		plt.hist(ntracks, log = False, label = "mean = %.2f" % np.mean(ntracks))
		plt.legend()
		plt.grid()
		plt.title("Number of tracks per event - %s entries" % ntracks.shape[0])
		plt.show()


def get_large_slopes(merged_file_in, min_slope_x=0., min_slope_y=0., min_x = 0., min_y = 0., apply_alignment = True):

	output_path = os.path.dirname(os.path.realpath(merged_file_in)) + "/selected_slopes"
	with tb.open_file(merged_file_in,"r") as in_file: 
		tracks = in_file.root.MergedClusters[:]
		if apply_alignment:
			apply_rpc_alignment(tracks, parameters = "pixel")
		tracks = tracks[np.logical_and(~np.isnan(tracks["x1"]),~np.isnan(tracks["y1"]))]
		
		with PdfPages(output_path + ".pdf") as output_pdf:
			for i in range(2):
				if i==0:
					x_hist, xbins = np.histogram(tracks["x1"],bins = 200)
					y_hist, ybins = np.histogram(tracks["y1"],bins = 200)
					plot_1d_bar(x_hist, output_pdf = output_pdf, title = "run 2836 all RPC tracks - %i entries" %x_hist.sum(), xlabel = "x1", bar_edges = xbins, gauss_fit = True, log = True)
					plot_1d_bar(y_hist, output_pdf = output_pdf, title = "run 2836 all RPC tracks - %i entries" %y_hist.sum(), xlabel = "y1", bar_edges = ybins, gauss_fit = True, log = True)
					x_slope_hist, x_slope_bins = np.histogram(tracks["slopexz"], bins = 200)
					y_slope_hist, y_slope_bins = np.histogram(tracks["slopeyz"], bins = 200)
					plot_1d_bar(x_slope_hist,output_pdf = output_pdf, title = "run 2836 all RPC tracks - %i entries" %x_slope_hist.sum(), xlabel = "x_slope", bar_edges = x_slope_bins, gauss_fit = True, log = True)
					plot_1d_bar(y_slope_hist,output_pdf = output_pdf, title = "run 2836 all RPC tracks - %i entries" %y_slope_hist.sum(), xlabel = "y_slope", bar_edges = y_slope_bins, gauss_fit = True, log = True)
					x_slope_res = np.abs(tracks["x_slope"]) - np.abs(tracks["slopexz"])
				if i==1:
					if apply_alignment:
						tracks = tracks[np.logical_and(np.abs(tracks["x1"])>min_x, np.abs(tracks["y1"]) > min_y)]
						tracks = tracks[np.logical_and(np.logical_and(np.abs(tracks["slopexz"])<0.15, np.abs(tracks["slopexz"])>min_slope_x), np.logical_and(np.abs(tracks["slopeyz"])<0.15, np.abs(tracks["slopeyz"])>min_slope_y))]
						x_hist, xbins = np.histogram(tracks["x1"],bins = 200)
						y_hist, ybins = np.histogram(tracks["y1"],bins = 200)
						plot_1d_bar(x_hist, output_pdf = output_pdf, title = "run 2836 secondary candidates RPC - %i entries" %x_hist.sum(), xlabel = "x1", bar_edges = xbins, gauss_fit = True)
						plot_1d_bar(y_hist, output_pdf = output_pdf, title = "run 2836 secondary candidates RPC - %i entries" %y_hist.sum(), xlabel = "y1", bar_edges = ybins, gauss_fit = True)
						pix_x_hist, pix_xbins = np.histogram(tracks["x"],bins = 200)
						pix_y_hist, pix_ybins = np.histogram(tracks["y"],bins = 200)
						plot_1d_bar(pix_x_hist, output_pdf = output_pdf, title = "run 2836 secondary candidates pixel - %i entries" %x_hist.sum(), xlabel = "x", bar_edges = pix_xbins, gauss_fit = True)
						plot_1d_bar(pix_y_hist, output_pdf = output_pdf, title = "run 2836 secondary candidates pixel - %i entries" %y_hist.sum(), xlabel = "y", bar_edges = pix_ybins, gauss_fit = True)
						x_slope_hist, x_slope_bins = np.histogram(tracks["slopexz"], bins = 200)
						y_slope_hist, y_slope_bins = np.histogram(tracks["slopeyz"], bins = 200)
						plot_1d_bar(x_slope_hist,output_pdf = output_pdf, title = "run 2836 secondary candidates RPC - %i entries" %x_slope_hist.sum(), xlabel = "x_slope", bar_edges = x_slope_bins, gauss_fit = True)
						plot_1d_bar(y_slope_hist,output_pdf = output_pdf, title = "run 2836 secondary candidates RPC - %i entries" %y_slope_hist.sum(), xlabel = "y_slope", bar_edges = y_slope_bins, gauss_fit = True)
						pix_x_slope_hist, pix_x_slope_bins = np.histogram(tracks["x_slope"], bins = 200)
						pix_y_slope_hist, pix_y_slope_bins = np.histogram(tracks["y_slope"], bins = 200)
						plot_1d_bar(pix_x_slope_hist,output_pdf = output_pdf, title = "run 2836 secondary candidates pixel - %i entries" %x_slope_hist.sum(), xlabel = "x_slope", bar_edges = pix_x_slope_bins, gauss_fit = True)
						plot_1d_bar(pix_y_slope_hist,output_pdf = output_pdf, title = "run 2836 secondary candidates pixel - %i entries" %y_slope_hist.sum(), xlabel = "y_slope", bar_edges = pix_y_slope_bins, gauss_fit = True)
						x_slope_res = np.abs(tracks["x_slope"]) - np.abs(tracks["slopexz"])
					else:
						tracks = tracks[np.logical_and(np.abs(tracks["x1"] + 3.023)>min_x, np.abs(tracks["y1"] + 21.47)>min_y)]
						tracks = tracks[np.logical_and(np.abs(tracks["x1"])>min_x, np.abs(tracks["y1"])>min_y)]
						tracks = tracks[np.logical_and(np.logical_and(np.abs(tracks["slopexz"])<0.15, np.abs(tracks["slopexz"] - 0.0043)>min_slope_x), np.logical_and(np.abs(tracks["slopeyz"])<0.15, np.abs(tracks["slopeyz"] - 0.0037)>min_slope_y))]
					# slope_hist, xbins, ybins = np.histogram2d(tracks["slopexz"], tracks["x_slope"], bins =[50,50], range = [[-0.15, 0.15],[-0.15,0.15]])
					# plot_2d_hist(hist2d = slope_hist, output_pdf = output_pdf, bins = [xbins, ybins], xlabel="rpc x slope", ylabel="pixel x slope",
					# 			title="Run 2836 spill 5: x correlation of slopes larger %.3f - %i entries" % (min_slope_x,slope_hist.sum()), box = None, mean_eff = None,
					#			output_png=None, zlog=False, min_hits=1, limits=None, scale_large_pixels = False)
					# pos_hist, xbins, ybins = np.histogram2d(tracks["slopeyz"], tracks["y_slope"], bins =[50,50], range = [[-0.15, 0.15],[-0.15,0.15]])
					# plot_2d_hist(hist2d = pos_hist, output_pdf = output_pdf, bins = [xbins, ybins], xlabel="rpc y slope", ylabel="pixel y slope",
					# 			title="Run 2836 spill 5: y correlation of slopes larger %.3f - %i entries" % (min_slope_x,slope_hist.sum()), box = None, mean_eff = None,
					#			output_png=None, zlog=False, min_hits=1, limits=None, scale_large_pixels = False)

	save_table(array_in = tracks, path = output_path + ".h5", node_name = "MergedClusters" )
	logging.info("saved large angle plots at %s" % output_path + ".pdf")
	# for line in [10,11,26]:
		# 	track = tracks[line]
		# 	z_rpc = 1768.23 - 1008.33
		# 	extrapol_x = [track["x"], track["x"] + track["x_slope"]*z_rpc, track["x1"],track["x2"] ,track["x3"], track["x4"], track["x5"] ]
		# 	x_z = [0., z_rpc, z_rpc + track["z1x"], z_rpc + track["z2x"], z_rpc + track["z3x"], z_rpc + track["z4x"], z_rpc + track["z5x"] ]
		# 	extrapol_y = [track["y"],track["y"] + track["y_slope"]*z_rpc, track["y1"], track["y2"], track["y3"], track["y4"], track["y5"]]
		# 	y_z = [0., z_rpc, z_rpc + track["z1y"], z_rpc + track["z2y"], z_rpc + track["z3y"], z_rpc + track["z4y"], z_rpc + track["z5y"] ]
		# 	# extrapol_x = -=
		# 	plt.plot(x_z, extrapol_x)
		# 	plt.plot(y_z, extrapol_y)
		# 	plt.show()
		# 	plt.plot(extrapol_x[1:3], extrapol_y[1:3], linestyle = "None", marker = "o")
		# 	plt.show()
	return output_path + ".h5"


def make_fake_tracks(merged_tracks):
	logging.info("making table with fake tracks")
	fake_tracks = np.full(fill_value = -1, shape = (merged_tracks.shape[0],), dtype = merged_tracks.dtype)
	fake_tracks["event_number"] = merged_tracks["event_number"]
	fake_tracks["timestamp"] = merged_tracks["timestamp"]
	fake_tracks["pix_trackID"] = merged_tracks["pix_trackID"]
	fake_tracks["x"] = merged_tracks["x"] # *(-1.)
	fake_tracks["y"] = merged_tracks["y"]
	fake_tracks["z"] = merged_tracks["z"]
	fake_tracks["x_slope"] = merged_tracks["x_slope"]
	fake_tracks["y_slope"] = merged_tracks["y_slope"]

	fake_tracks = fake_tracks[fake_tracks["event_number"]!=-1]

	for l, _ in enumerate(merged_tracks):
		if l ==0:
			continue
		if l==merged_tracks.shape[0] - 2:
			break
		for plane in range(1,6):
			fake_tracks[l+1]["x%i" %plane] = merged_tracks[l]["x%i" %plane]
			fake_tracks[l+1]["y%i" %plane] = merged_tracks[l]["y%i" %plane]
			fake_tracks[l+1]["z%ix" %plane] = merged_tracks[l]["z%ix" %plane]
			fake_tracks[l+1]["z%iy" %plane] = merged_tracks[l]["z%iy" %plane]
			fake_tracks[l+1]["slopexz"] = merged_tracks[l]["slopexz"]
			fake_tracks[l+1]["slopeyz"] = merged_tracks[l]["slopeyz"]
			fake_tracks[l+1]["rpc_timestamp"] = merged_tracks[l]["timestamp"]
	return fake_tracks


@njit()
def match_track_timestamps(rpc_tracks_in, pixel_tracks_in, merged_tracks, delta_t=10000):
	j = 0
	matched_event_number = 0
	for i, pixel_track in tqdm(enumerate(pixel_tracks_in)):
		while (j < rpc_tracks_in.shape[0]-1) and (np.abs(pixel_track["timestamp"]*25 - rpc_tracks_in[j]["timestamp"]) > delta_t):
			j+=1
		
		merged_tracks[j]["pix_trackID"] = pixel_track["trackID"]
		merged_tracks[j]["x"] = pixel_track["x"]
		merged_tracks[j]["y"] = pixel_track["y"]
		merged_tracks[j]["z"] = pixel_track["z"]
		merged_tracks[j]["x_error"] = pixel_track["x_error"]
		merged_tracks[j]["y_error"] = pixel_track["y_error"]
		merged_tracks[j]["x_slope"] = pixel_track["x_slope"]
		merged_tracks[j]["y_slope"] = pixel_track["y_slope"]
		merged_tracks[j]["covariance"] = pixel_track["covariance"]
		merged_tracks[j]["event_number"] = pixel_track["event_number"]

		merged_tracks[j]["timestamp"] = rpc_tracks[j]["timestamp"]
		merged_tracks[j]["x1"] = rpc_tracks[j]["x1"]
		merged_tracks[j]["y1"] = rpc_tracks[j]["y1"]
		merged_tracks[j]["z1x"] = rpc_tracks[j]["z1x"]
		merged_tracks[j]["z1y"] = rpc_tracks[j]["z1y"]
		merged_tracks[j]["slopexz"] = rpc_tracks[j]["slopexz"]
		merged_tracks[j]["slopeyz"] = rpc_tracks[j]["slopeyz"]
		merged_tracks[j]["rpc_trackID"] = rpc_tracks[j]["trackID"]

		if j == (rpc_tracks_in.shape[0]-1) and (i<pixel_tracks_in.shape[0]):
			j=0
			continue
	final_track_array = merged_tracks[~np.isnan(merged_tracks["timestamp"])]

	return final_track_array


def load_pixel_tracks(pixel_file_path, spill=None):
	with tb.open_file(pixel_file_path, "r") as pixel_tracks_in_h5:
		pixel_tracks = pixel_tracks_in_h5.root.Tracks[:]
		if spill:
			pixel_tracks = pixel_tracks[pixel_tracks["spill"]==spill]
	return pixel_tracks


def load_rpc_tracks(rpc_file_path):
	with tb.open_file(rpc_file_path, "r") as pixel_tracks_in_h5:
		rpc_tracks = pixel_tracks_in_h5.root.RPCTracks[:]
	return rpc_tracks


def make_matched_track_array(rpc_tracks_dtype, pixel_tracks_dtype, shape):
	pixel_tracks_dtype[3] = ('pix_trackID','<i2')
	# remove pixel timestamp
	pixel_tracks_dtype.pop(1)
	#remove pixel spill
	pixel_tracks_dtype.pop(1)
	rpc_tracks_dtype[3] = ('rpc_trackID', '<i2')
	# remove rpc event_number
	rpc_tracks_dtype.pop(0)
	#remove rpc nclusters
	rpc_tracks_dtype.pop(1)
	merged_tracks_dtype = pixel_tracks_dtype + rpc_tracks_dtype
	merged_tracks = np.full(fill_value = -1, shape = shape, dtype = merged_tracks_dtype)
	return merged_tracks


def merge_tracks_by_timestamp(rpc_tracks_in, pixel_tracks_in, delta_t=10000):

	pixel_dtype = pixel_tracks_in.dtype.descr
	rpc_dtype = rpc_tracks_in.dtype.descr

	merged_tracks = make_matched_track_array(rpc_tracks_dtype = rpc_dtype, pixel_tracks_dtype = pixel_dtype, shape = pixel_tracks_in.shape)
	# match_track_timestamps(rpc_tracks_in = rpc_tracks_in, pixel_tracks_in = pixel_tracks_in, merged_tracks = merged_tracks, delta_t=delta_t)
	j = 0
	matched_event_number = 0
	for i, pixel_track in tqdm(enumerate(pixel_tracks_in)):
		while (j < rpc_tracks_in.shape[0]-1) and (np.abs(pixel_track["timestamp"]*25 - rpc_tracks_in[j]["timestamp"]) > delta_t):
			j+=1
		
		merged_tracks[j]["pix_trackID"] = pixel_track["trackID"]
		merged_tracks[j]["x"] = pixel_track["x"]
		merged_tracks[j]["y"] = pixel_track["y"]
		merged_tracks[j]["z"] = pixel_track["z"]
		merged_tracks[j]["x_error"] = pixel_track["x_error"]
		merged_tracks[j]["y_error"] = pixel_track["y_error"]
		merged_tracks[j]["x_slope"] = pixel_track["x_slope"]
		merged_tracks[j]["y_slope"] = pixel_track["y_slope"]
		merged_tracks[j]["covariance"] = pixel_track["covariance"]

		merged_tracks[j]["timestamp"] = rpc_tracks[j]["timestamp"]
		merged_tracks[j]["x1"] = rpc_tracks[j]["x1"]
		merged_tracks[j]["y1"] = rpc_tracks[j]["y1"]
		merged_tracks[j]["z1x"] = rpc_tracks[j]["z1x"]
		merged_tracks[j]["z1y"] = rpc_tracks[j]["z1y"]
		merged_tracks[j]["slopexz"] = rpc_tracks[j]["slopexz"]
		merged_tracks[j]["slopeyz"] = rpc_tracks[j]["slopeyz"]
		merged_tracks[j]["rpc_trackID"] = rpc_tracks[j]["trackID"]

		if j == (rpc_tracks_in.shape[0]-1) and (i<pixel_tracks_in.shape[0]):
			j=0
			continue
	merged_tracks = merged_tracks[merged_tracks["timestamp"]!=-1]

	for k, row in enumerate(merged_tracks):
		row["event_number"] = k

	return merged_tracks


def apply_rpc_alignment(tracks_array, parameters = "pixel", reverse = False):
	logging.info("aligning tracks with %s paramters" %parameters)
	if reverse==False:
		if parameters=="rpc":
			tracks_array["slopexz"] += -0.0043
			tracks_array["slopeyz"] += -0.0037
			tracks_array["x1"] += 1.4649 # 3.023
			tracks_array["y1"] += 21.47
		elif parameters=="pixel":
			tracks_array["slopexz"] += -0.006959
			tracks_array["slopeyz"] += -0.004024
			tracks_array["x1"] += 1.46487
			tracks_array["y1"] += 21.41954
	elif reverse==True:
		if parameters=="rpc":
			tracks_array["slopexz"] -= -0.0043
			tracks_array["slopeyz"] -= -0.0037
			tracks_array["x1"] -= 1.4649 # 3.023
			tracks_array["y1"] -= 21.47
		elif parameters=="pixel":
			tracks_array["slopexz"] -= -0.006959
			tracks_array["slopeyz"] -= -0.004024
			tracks_array["x1"] -= 1.46487
			tracks_array["y1"] -= 21.41954


def get_matched_tracks_rpc(tracks_in, matched_rpc_events):
	tracks_out = [] #List()
	i=0
	for row in tracks_in:
		while (row["event_number"] > matched_rpc_events[i]["rpc_event"]) and (i<matched_rpc_events.size - 1):
			i+=1
		if (row["event_number"] == matched_rpc_events[i]["rpc_event"]):
			tracks_out.append(row)
			tracks_out[-1]["event_number"] = matched_rpc_events[i]["matched_event_number"]
		elif (row["event_number"] < matched_rpc_events[i]["rpc_event"]):
			continue
	return tracks_out


def get_matched_tracks_pixel(tracks_in, matched_rpc_events):
	tracks_out = [] #List()
	i=0
	for row in tracks_in:
		while (row["event_number"] > matched_rpc_events[i]["pixel_event"]) and (i<matched_rpc_events.size - 1):
			i+=1
		if (row["event_number"] == matched_rpc_events[i]["pixel_event"]):
			tracks_out.append(row)
			tracks_out[-1]["event_number"] = matched_rpc_events[i]["matched_event_number"]
		elif (row["event_number"] < matched_rpc_events[i]["pixel_event"]):
			continue
	return tracks_out


def load_matched_rpc_tracks(rpc_tracks_file, matched_rpc_events_file):
	logging.info("loading rpc tracks")
	rpc_events = np.loadtxt(matched_rpc_events_file,dtype={'names': ('rpc_event', 'rpc_ts', 'pixel_event','pixel_ts','delta_t', 'matched_event_number'),'formats': (np.int64, np.int64, np.int64, np.int64, np.int64, np.int64)})
	with tb.open_file(rpc_tracks_file, "r") as rpc_in_h5:
		rpc_tracks = rpc_in_h5.root.RPCTracks[:]
		matched_rpc_tracks = np.array(get_matched_tracks_rpc(rpc_tracks, rpc_events))
	return matched_rpc_tracks


def load_matched_pixel_tracks(pixel_tracks_file, matched_rpc_events_file):
	logging.info("loading pixel tracks")
	rpc_events = np.loadtxt(matched_rpc_events_file, dtype={'names': ('rpc_event', 'rpc_ts', 'pixel_event','pixel_ts','delta_t', 'matched_event_number'),'formats': (np.int64, np.int64, np.int64, np.int64,  np.int64, np.int64)})
	with tb.open_file(pixel_tracks_file, "r") as pixel_tracks_in_h5:
		pixel_tracks = pixel_tracks_in_h5.root.Tracks[:]
		matched_pixel_tracks = np.array(get_matched_tracks_pixel(pixel_tracks, rpc_events))
	return matched_pixel_tracks


def get_track_distance(merged_tracks, corr = None, pos = "rpc", rpc_plane = 1):
	'''
	calculate distance for every
	'''

	# z positions in cm from survey
	pixel_global_z = 1008.33 
	rpc_global_z = 1768.23
	
	if pos == "rpc":
		global_delta_z = rpc_global_z - pixel_global_z
		if corr:
			logging.info("calculating corrected track to track distance at RPC")
			merged_tracks["delta_x"] = merged_tracks["x_slope"] * global_delta_z + merged_tracks["x"] - merged_tracks["x%s" %rpc_plane]# - corr[0]
			merged_tracks["delta_y"] = merged_tracks["y_slope"] * global_delta_z + merged_tracks["y"] - merged_tracks["y%s" %rpc_plane]# - corr[1]
		else:
			logging.info("calculating track to track distance at RPC")
			merged_tracks["delta_x"] = merged_tracks["x_slope"] * global_delta_z + merged_tracks["x"] - merged_tracks["x%s" %rpc_plane]
			merged_tracks["delta_y"] = merged_tracks["y_slope"] * global_delta_z + merged_tracks["y"] - merged_tracks["y%s" %rpc_plane]

	elif pos == "pix" :
		global_delta_z = pixel_global_z - rpc_global_z
		if corr:
			logging.info("calculating corrected track to track distance at pixel")
			merged_tracks["delta_x"] = merged_tracks["slopexz"] * (global_delta_z - merged_tracks["z2x"]) + merged_tracks["x%s" %rpc_plane] - merged_tracks["x"] # - corr[0]
			merged_tracks["delta_y"] = merged_tracks["slopeyz"] * (global_delta_z - merged_tracks["z2y"]) + merged_tracks["y%s" %rpc_plane] - merged_tracks["y"] # - corr[1]
		else:
			logging.info("calculating track to track distance at pixel")
			merged_tracks["delta_x"] = merged_tracks["slopexz"] * (global_delta_z - merged_tracks["z2x"]) + merged_tracks["x%s" %rpc_plane] - merged_tracks["x"]
			merged_tracks["delta_y"] = merged_tracks["slopeyz"] * (global_delta_z - merged_tracks["z2y"]) + merged_tracks["y%s" %rpc_plane] - merged_tracks["y"]

	merged_tracks["delta_r"] = np.sqrt(np.square(merged_tracks["delta_x"]) + np.square(merged_tracks["delta_y"]))
	
	merged_tracks2 = merged_tracks.copy()
	x_distances = np.sort(merged_tracks, order = ["event_number", "rpc_trackID", "delta_x"])
	y_distances = np.sort(merged_tracks2, order = ["event_number", "rpc_trackID", "delta_y"])

	return x_distances, y_distances


@njit()
def _mark_matches(tracks_in, indices, counts):

	track_id = 0
	best_matches = []
	best_match_index = 0
	for i, event in enumerate(indices):
		smallest_distance = 1e6
		track_id = 0 #tracks_in[event]["rpc_trackID"]
		for track_count in range(counts[i]):
			# in case of n_rpc_track > 1 consider all rpc tracks
			if tracks_in[event + track_count]["rpc_trackID"] != track_id:
				track_id = tracks_in[event + track_count]["rpc_trackID"]
				best_matches.append(best_match_index)
				smallest_distance = 1e6
			if tracks_in[event + track_count]["delta_r"] < smallest_distance:
				smallest_distance = tracks_in[event + track_count]["delta_r"]
				best_match_index = event + track_count
		best_matches.append(best_match_index)
			
	return best_matches


def find_best_match(tracks_in):
	logging.info("finding best matches")
	tracks_in.sort(order = ["event_number","rpc_trackID"])
	events, indices, counts = np.unique(tracks_in["event_number"], return_index = True, return_counts = True)
	best_matches = _mark_matches(tracks_in, indices, counts)
	tracks_in["best_match"][best_matches] = 1


def assign_new_event_number(merged_tracks_in, path):
	add_evt = 0
	for i in range(merged_tracks_in.shape[0]):
		if (merged_tracks_in[i]["spill"]!=merged_tracks_in[i-1]["spill"]) and i>0:
			add_evt = merged_tracks_in[i-1]["event_number"]
			if merged_tracks_in[i]["event_number"]==0:
				add_evt +=1
		merged_tracks_in[i]["event_number"] += add_evt
	save_table(array_in = merged_tracks_in, path = path, node_name = "MergedClusters")
	return path


def get_track_projection(merged_tracks, corr = [0.,0.], zpos="rpc", rpc_plane = 1):
	'''
	fit either pixel tracks to rpc position, or other way round
	'''
	logging.info("project tracks to %s" % zpos)
	# z positions in cm from survey	
	pixel_global_z = 1008.33 
	rpc_global_z = 1768.23
	if zpos == "pix":
		global_delta_z = pixel_global_z - rpc_global_z
		merged_tracks["x_project"] = merged_tracks["slopexz"] * (global_delta_z - merged_tracks["z%sx" %rpc_plane]) + merged_tracks["x%s" %rpc_plane]# + corr[0]
		merged_tracks["y_project"] = merged_tracks["slopeyz"] * (global_delta_z - merged_tracks["z%sy" %rpc_plane]) + merged_tracks["y%s" %rpc_plane]# + corr[1]
	elif zpos == "rpc":
		global_delta_z = rpc_global_z - pixel_global_z
		merged_tracks["x_project"] = merged_tracks["x_slope"] * global_delta_z + merged_tracks["x"]# - corr[0]
		merged_tracks["y_project"] = merged_tracks["y_slope"] * global_delta_z + merged_tracks["y"]# - corr[1]
		merged_tracks["x_project_error"] = np.sqrt(np.square(merged_tracks["x_error"]) + np.square(global_delta_z * merged_tracks["x_slope_error"]))
		merged_tracks["y_project_error"] = np.sqrt(np.square(merged_tracks["y_error"]) + np.square(global_delta_z * merged_tracks["y_slope_error"]))


def merge_data_by_event_number(rpc_tracks_in, pixel_tracks_in):
	'''
	build new table with rpc and pixel tracks
	timestamps are saved in 1 ns units
	RPC trackID starts at 0 in output file
	----------------
	INPUT:
		rpc_tracks_in: structured numpy.array
			array with tracks of matched RPC events
		pixel_tracks_in: structured numpy.array
			pixel tracks of matched events.
	OUTPUT:
		merged_tracks: structured numpy.array
			array with pixel and rpc tracks for every matched event
	'''
	common_events, rpc_indices, pixel_indices = np.intersect1d(rpc_tracks_in["event_number"], pixel_tracks_in["event_number"], return_indices = True)

	merged_tracks_dtype = [("event_number",np.int64),
						   ("pix_trackID", np.int16),
						   ("spill", np.int32),
						   ("x",np.float64), 
						   ("y",np.float64), 
						   ("z",np.float64), 
						   ("x_slope", np.float32), 
						   ("y_slope", np.float32),
						   ("x_error", np.float32), 
						   ("y_error", np.float32),
						   ("x_slope_error", np.float32), 
						   ("y_slope_error", np.float32),
						   ("timestamp", np.int64),
						   ("n_pix_tracks", np.int64),
						   ("rpc_timestamp", np.int64), 
						   ("rpc_trackID", np.int16), 
						   ("slopexz", np.float32),
						   ("slopeyz", np.float32),
						   ("x1", np.float64),
						   ("y1", np.float64),
						   ("z1x", np.float64), 
						   ("z1y", np.float64),
						   ("x2", np.float64), 
						   ("y2", np.float64), 
						   ("z2x", np.float64), 
						   ("z2y", np.float64), 
						   ("x3", np.float64),
						   ("y3", np.float64),
						   ("z3x", np.float64), 
						   ("z3y", np.float64),
						   ("x4", np.float64),
						   ("y4", np.float64),
						   ("z4x", np.float64), 
						   ("z4y", np.float64),
						   ("x5", np.float64),
						   ("y5", np.float64),
						   ("z5x", np.float64), 
						   ("z5y", np.float64),
						   ("n_rpc_tracks", np.int64),
						   ("x_project", np.float32),
						   ("y_project", np.float32),
						   ("x_project_error", np.float32),
						   ("y_project_error", np.float32),
						   ("delta_x", np.float32),
						   ("delta_y", np.float32),
						   ("delta_r", np.float32),
						   ("best_match", np.int32)
						   ]

	# find subdetector events in common events
	pix_event_mask = np.isin(pixel_tracks_in["event_number"], common_events)
	rpc_event_mask = np.isin(rpc_tracks_in["event_number"], common_events)

	# use found events to mask input tracks files
	_, _, unique_pix_counts = np.unique(pixel_tracks_in["event_number"][np.nonzero(pix_event_mask)], return_index = True, return_counts = True)
	_, _, unique_rpc_counts = np.unique(rpc_tracks_in["event_number"][np.nonzero(rpc_event_mask)], return_index = True, return_counts = True)

	# allocate new array of size n_pix_tracks * n_rpc_tracks
	merged_tracks = np.full(fill_value = -1, shape = (unique_pix_counts * unique_rpc_counts).sum(), dtype = merged_tracks_dtype )

	j = 0
	for i, event in enumerate(common_events):
		for rpc_track_count in range(unique_rpc_counts[i]):
			for pixel_track_count in range(unique_pix_counts[i]):
				
				merged_tracks[j]["event_number"] = event

				merged_tracks[j]["n_pix_tracks"] = unique_pix_counts[i]
				merged_tracks[j]["timestamp"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["timestamp"]*25
				merged_tracks[j]["x"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["x"]
				merged_tracks[j]["y"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["y"]
				merged_tracks[j]["z"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["z"]
				merged_tracks[j]["x_error"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["x_error"]
				merged_tracks[j]["y_error"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["y_error"]
				merged_tracks[j]["x_slope"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["x_slope"]
				merged_tracks[j]["y_slope"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["y_slope"]
				merged_tracks[j]["x_slope_error"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["x_slope_error"]
				merged_tracks[j]["y_slope_error"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["y_slope_error"]
				merged_tracks[j]["pix_trackID"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["trackID"]
				merged_tracks[j]["spill"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["spill"]

				for plane in range(1,6):
					merged_tracks[j]["x%i" %plane] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["x%i" %plane]
					merged_tracks[j]["y%i" %plane] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["y%i" %plane]
					merged_tracks[j]["z%ix" %plane] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["z%ix" %plane]
					merged_tracks[j]["z%iy" %plane] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["z%iy" %plane]
				merged_tracks[j]["slopexz"] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["slopexz"]
				merged_tracks[j]["slopeyz"] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["slopeyz"]
				merged_tracks[j]["rpc_timestamp"] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["timestamp"]*10
				merged_tracks[j]["rpc_trackID"] = rpc_tracks_in[rpc_indices[i] + rpc_track_count]["trackID"] -1
				merged_tracks[j]["n_rpc_tracks"] = unique_rpc_counts[i]
				
				j+=1
	merged_tracks["best_match"] = 0
	return merged_tracks[merged_tracks["event_number"]>-1]


def match_tracks_in_event(merged_tracks_in, out_pdf_name, apply_alignment = True, max_event_size = None, 
						min_event_size = 1, multiple_spills = True, rpc_plane = 1, clip_range  = [[-10,10],[-50,50]], cut = False):

	merged_tracks_in = merged_tracks_in[np.logical_and(~np.isnan(merged_tracks_in["x%s" % rpc_plane]),~np.isnan(merged_tracks_in["y%s" % rpc_plane]))]

	selected_tracks = merged_tracks_in
	
	if max_event_size is not None:
		logging.info("selecting nTracks/event <= %s" %max_event_size)
		selected_tracks = merged_tracks_in[np.logical_and(merged_tracks_in["n_pix_tracks"] < max_event_size, merged_tracks_in["n_pix_tracks"] >= min_event_size )]
		out_pdf_name = out_pdf_name[:-4] + "_selected.pdf"

	with PdfPages(out_pdf_name) as output_pdf:

		_, unique_event_indices = np.unique(selected_tracks["event_number"], return_index = True)
		n_tracks_array = selected_tracks[unique_event_indices]

		plot_1d_hist(array_in = n_tracks_array["n_pix_tracks"], output_pdf=output_pdf, bins = np.arange(0,160,1), log = True, 
					 title = "Run 2836 n_pix_tracks per event - %s entries" % n_tracks_array.shape[0],
					 output_png = out_pdf_name[:-4] + "_n_pix_tracks.png")
		plot_1d_hist(array_in = n_tracks_array["n_rpc_tracks"], output_pdf=output_pdf, bins = np.arange(0,160,1), log = True,
					 title = "Run 2836 n_rpc_tracks per event - %s entries" % n_tracks_array.shape[0],
					 output_png = out_pdf_name[:-4] + "_n_rpc_tracks.png")

		if apply_alignment:
			tracks_before_alignment = selected_tracks.copy()
			find_best_match(tracks_before_alignment)
			tracks_before_alignment = tracks_before_alignment[tracks_before_alignment["best_match"]==1]

			# plot rpc track pos
			rpc_x_hist, rpc_x_bins = np.histogram(tracks_before_alignment["x%s" %rpc_plane], bins = 100)
			plot_1d_bar(hist_in = rpc_x_hist, output_pdf=output_pdf, bar_edges = rpc_x_bins, gauss_fit = True,
						xlabel = "x%s [cm]"  % rpc_plane, title ="Run 2836 RPC track pos. - %s entries" %rpc_x_hist.sum() )
			rpc_y_hist, rpc_y_bins = np.histogram(tracks_before_alignment["y%s" %rpc_plane], bins = 100)
			plot_1d_bar(hist_in = rpc_y_hist, output_pdf=output_pdf, bar_edges = rpc_y_bins, gauss_fit = True,
						xlabel = "y%s [cm]" % rpc_plane, title ="Run 2836 RPC track pos. - %s entries" %rpc_y_hist.sum() )

			# plot pixel track pos
			pix_x_hist, pix_x_bins = np.histogram(tracks_before_alignment["x"], bins = 100)
			plot_1d_bar(hist_in = pix_x_hist, output_pdf=output_pdf, bar_edges = pix_x_bins, gauss_fit = True,
						xlabel = "x [cm]", title ="Run 2836 pix track pos. - %s entries" %pix_x_hist.sum() )
			pix_y_hist, pix_y_bins = np.histogram(tracks_before_alignment["y"], bins = 100)
			plot_1d_bar(hist_in = pix_y_hist, output_pdf=output_pdf, bar_edges = pix_y_bins, gauss_fit = True,
						xlabel = "y [cm]", title ="Run 2836 pix track pos. - %s entries" %pix_y_hist.sum() )

			# make histograms without alignment
			distances = get_track_distance(tracks_before_alignment, pos = "rpc", rpc_plane = rpc_plane)
			
			# _, x_indices, counts = np.unique(distances[0]["event_number"], return_index = True, return_counts = True)
			delta_x = tracks_before_alignment["delta_x"] 
			delta_x_hist, x_bins = np.histogram(np.clip(delta_x[~np.isnan(delta_x)], clip_range[0][0], clip_range[0][1]), bins = 100)
			x_corr = plot_1d_bar(hist_in=delta_x_hist, output_pdf=output_pdf, bar_edges = x_bins, 
						xlim=clip_range[0], xlabel="$\Delta$x [cm]",
						title="Run 2836 Track distances in x - %s entries" %delta_x[~np.isnan(delta_x)].shape[0] , 
						legend = None, log = False, output_png = out_pdf_name[:-4] + "_delta_x.png",  vlines=None, gauss_fit=True)

			# _, y_indices, counts = np.unique(distances[1]["event_number"], return_index = True, return_counts = True)
			delta_y = tracks_before_alignment["delta_y"]
			delta_y_hist, y_bins = np.histogram(np.clip(delta_y[~np.isnan(delta_y)], clip_range[1][0], clip_range[1][1]), bins = 100)
			y_corr = plot_1d_bar(hist_in=delta_y_hist, output_pdf=output_pdf, bar_edges = y_bins, 
						xlim=clip_range[1],legend=None, 
						title="Run 2836 Track distances in y - %s entries" %delta_y[~np.isnan(delta_y)].shape[0], 
						xlabel="$\Delta$y [cm]", log=False, output_png = out_pdf_name[:-4] + "_delta_y.png", gauss_fit=True)

			# now apply alignment and make histograms again
			apply_rpc_alignment(selected_tracks, parameters = "pixel")

		distances_corr = get_track_distance(selected_tracks, pos = "rpc", rpc_plane = rpc_plane)
		find_best_match(selected_tracks)
		selected_tracks = selected_tracks[selected_tracks["best_match"]==1]
		
		if cut:
			min_slope_x = 0.004, 
			min_slope_y = 0.005, 
			min_x = 0.86, 
			min_y = 1.42

			selected_tracks = selected_tracks[np.logical_and(np.abs(selected_tracks["x1"])>min_x, np.abs(selected_tracks["y1"]) > min_y)]
			selected_tracks = selected_tracks[np.logical_and(np.logical_and(np.abs(selected_tracks["slopexz"])<0.15, np.abs(selected_tracks["slopexz"])>min_slope_x), np.logical_and(np.abs(selected_tracks["slopeyz"])<0.15, np.abs(selected_tracks["slopeyz"])>min_slope_y))]
		
		n_tracks_hist, n_tracks_bins = np.histogram(selected_tracks["spill"], bins = 130)
		plot_1d_bar(hist_in = n_tracks_hist, output_pdf=output_pdf, log = False, bar_edges = n_tracks_bins, title = "matched tracks per spill - %s entries" % n_tracks_hist.sum(),
					 output_png = out_pdf_name[:-4] + "_tracks_per_spill.png")

		# plot rpc track pos
		rpc_x_hist, rpc_x_bins = np.histogram(selected_tracks["x%s" %rpc_plane], bins = 100)
		plot_1d_bar(hist_in = rpc_x_hist, output_pdf=output_pdf, bar_edges = rpc_x_bins, gauss_fit = True,
					xlabel = "x%s [cm]"  % rpc_plane, title ="Run 2836 RPC track pos. aligned - %s entries" %rpc_x_hist.sum(),
					output_png = out_pdf_name[:-4] + "_rpc_x.png")
		rpc_y_hist, rpc_y_bins = np.histogram(selected_tracks["y%s" %rpc_plane], bins = 100)
		plot_1d_bar(hist_in = rpc_y_hist, output_pdf=output_pdf, bar_edges = rpc_y_bins, gauss_fit = True,
					xlabel = "y%s [cm]" % rpc_plane, title ="Run 2836 RPC track pos. aligned - %s entries" %rpc_y_hist.sum(),
					output_png = out_pdf_name[:-4] + "_rpc_y.png")

		# plot pixel track pos
		pix_x_hist, pix_x_bins = np.histogram(selected_tracks["x"], bins = 100)
		plot_1d_bar(hist_in = pix_x_hist, output_pdf=output_pdf, bar_edges = pix_x_bins, gauss_fit = True,
					xlabel = "x [cm]", title ="Run 2836 pix track pos. aligned - %s entries" %pix_x_hist.sum(),
					output_png = out_pdf_name[:-4] + "_pixel_x.png" )
		pix_y_hist, pix_y_bins = np.histogram(selected_tracks["y"], bins = 100)
		plot_1d_bar(hist_in = pix_y_hist, output_pdf=output_pdf, bar_edges = pix_y_bins, gauss_fit = True,
					xlabel = "y [cm]", title ="Run 2836 pix track pos. aligned - %s entries" %pix_y_hist.sum(),
					output_png = out_pdf_name[:-4] + "_pixel_y.png")

		# _, x_indices, counts = np.unique(distances_corr[0]["event_number"], return_index = True, return_counts = True)
		clip_range = [-0.1,0.1]
		# delta_x = selected_tracks[x_indices]["delta_x"]
		delta_x = selected_tracks["delta_x"]
		delta_x_hist, x_bins = np.histogram(np.clip(delta_x[~np.isnan(delta_x)], clip_range[0], clip_range[1]), bins = 100)

		# _, y_indices, counts = np.unique(distances_corr[1]["event_number"], return_index = True, return_counts = True)
		# delta_y = selected_tracks[y_indices]["delta_y"] 
		delta_y = selected_tracks["delta_y"] 
		delta_y_hist, y_bins = np.histogram(np.clip(delta_y[~np.isnan(delta_y)], clip_range[0], clip_range[1]), bins = 100)

		plot_1d_bar(hist_in=delta_x_hist, output_pdf=output_pdf, bar_edges = x_bins, 
					xlim=clip_range, xlabel="$\Delta$x [cm]",
					title="Run 2836 Track distances in x - %s entries" %delta_x[~np.isnan(delta_x)].shape[0] , 
					legend = None, log = False, output_png = out_pdf_name[:-4] + "_delta_x_corr.png",  vlines=None, gauss_fit=False)
		plot_1d_bar(hist_in=delta_y_hist, output_pdf=output_pdf, bar_edges = y_bins, 
					xlim=clip_range,legend=None, 
					title="Run 2836 Track distances in y - %s entries" %delta_y[~np.isnan(delta_y)].shape[0], 
					xlabel="$\Delta$y [cm]", log=False, output_png = out_pdf_name[:-4] + "_delta_y_corr.png", gauss_fit=False)

		plot_1d_hist(array_in = selected_tracks["slopexz"],output_pdf=output_pdf, log = True, bins = 100, title = "RPC slope distribution")
		plot_1d_hist(array_in = selected_tracks["x_slope"], output_pdf = output_pdf, log = True, bins = 100, title  = "pixel slope distribution")
		
		# plot pixel track projection to rpc
		get_track_projection(selected_tracks, corr=None, zpos="rpc", rpc_plane = rpc_plane)
		get_track_projection(merged_tracks_in, corr=None, zpos="rpc", rpc_plane = rpc_plane)
		print("mean x_project_error", np.mean(selected_tracks["x_project_error"]))
		print("mean y_project_error", np.mean(selected_tracks["y_project_error"]))

		pix_proj_x_hist, pix_proj_x_bins = np.histogram(selected_tracks["x_project"], bins = 100)
		plot_1d_bar(hist_in = pix_proj_x_hist, output_pdf=output_pdf, bar_edges = pix_proj_x_bins, gauss_fit = True,
					xlabel = "x", title ="Run 2836 pix track proj. - %s entries" %pix_proj_x_hist.sum(),
					output_png = out_pdf_name[:-4] + "_pix_proj_x.png", log = True)
		pix_proj_y_hist, pix_proj_y_bins = np.histogram(selected_tracks["y_project"], bins = 100)
		plot_1d_bar(hist_in = pix_proj_y_hist, output_pdf=output_pdf, bar_edges = pix_proj_y_bins, gauss_fit = True,
					xlabel = "y", title ="Run 2836 pix track proj. - %s entries" %pix_proj_y_hist.sum(),
					output_png = out_pdf_name[:-4] + "_pix_proj_y.png", log = True)

		x_correlation_hist, xcorr_xedge, xcorr_yedges = np.histogram2d(selected_tracks["x_project"][~np.isnan(selected_tracks["x%s"%rpc_plane])], 
																		selected_tracks["x%s" %rpc_plane][~np.isnan(selected_tracks["x%s" %rpc_plane])],
																		bins = [70,70], range = [[-2,2],[-2.5,2.5]] )
		plot_2d_hist(hist2d = x_correlation_hist, output_pdf = output_pdf, bins = [xcorr_xedge, xcorr_yedges], xlabel="pixel x [cm]",
					 ylabel="rpc x%s [cm]" %rpc_plane, title="Run 2836: x correlation at RPC - %i entries" %x_correlation_hist.sum(), box = None, mean_eff = None,
					 output_png=out_pdf_name[:-4] + "_x_corr.png", zlog=False, min_hits=2, limits=None, scale_large_pixels = False)

		y_correlation_hist, ycorr_xedge, ycorr_yedges = np.histogram2d(selected_tracks["y_project"][~np.isnan(selected_tracks["y%s" %rpc_plane])],
																		selected_tracks["y%s" %rpc_plane][~np.isnan(selected_tracks["y%s" %rpc_plane])],
																		bins = [50,50], range = [[-2,2],[-2.5,2.5]])
		plot_2d_hist(hist2d = y_correlation_hist, output_pdf = output_pdf, bins = [ycorr_xedge, ycorr_yedges], xlabel="pixel y [cm]",
					 ylabel="rpc y%s [cm]" %rpc_plane, title="Run 2836: y correlation at RPC - %i entries" %y_correlation_hist.sum(), box = None, mean_eff = None,
					 output_png=out_pdf_name[:-4] + "_y_corr.png", zlog=False, min_hits=2, limits=None, scale_large_pixels = False)

		# project rpc tracks to pixel
		get_track_projection(selected_tracks, zpos="pix", rpc_plane = rpc_plane)
		x_correlation_hist, xcorr_xedge, xcorr_yedges = np.histogram2d(selected_tracks["x_project"][~np.isnan(selected_tracks["x%s" %rpc_plane])], 
																		selected_tracks["x"][~np.isnan(selected_tracks["x%s" %rpc_plane])] ,
																		bins = [50,50], range = [[-6,6],[-2,2]] ) # [[-15,2.5],[-2,2]]
		plot_2d_hist(hist2d = x_correlation_hist, output_pdf = output_pdf, bins = [xcorr_xedge, xcorr_yedges], xlabel="RPC x [cm]",
					 ylabel="pixel x [cm]", title="Run 2836: x correlation at pixel - %i entries" %x_correlation_hist.sum(), box = None, mean_eff = None,
					 output_png=None, zlog=False, min_hits=2, limits=None, scale_large_pixels = False)

		y_correlation_hist, ycorr_xedge, ycorr_yedges = np.histogram2d(selected_tracks["y_project"][~np.isnan(selected_tracks["y%s" %rpc_plane])],
																		selected_tracks["y"][~np.isnan(selected_tracks["y%s" %rpc_plane])],
																		bins = [50,50], range = [[-6,6],[-2,2]]) # [[-28,-22],[-2,2]]
		plot_2d_hist(hist2d = y_correlation_hist, output_pdf = output_pdf, bins = [ycorr_xedge, ycorr_yedges], xlabel="RPC y [cm]",
					 ylabel="pixel y [cm]", title="Run 2836: y correlation at pixel - %i entries" %y_correlation_hist.sum(), box = None, mean_eff = None,
					 output_png=None, zlog=False, min_hits=2, limits=None, scale_large_pixels = False)

		# correlate slopes
		logging.info("correlate slopes")
		x_correlation_hist, xcorr_xedge, xcorr_yedges = np.histogram2d(selected_tracks["x_slope"], 
																		selected_tracks["slopexz"],
																		bins = [100,100], range = [[-0.0025,0.0025],[0.0025,0.01]])
		plot_2d_hist(hist2d = x_correlation_hist, output_pdf = output_pdf, bins = [xcorr_xedge, xcorr_yedges], xlabel="pixel x slope",
					 ylabel="rpc x slope", title="Run 2836: x slope correlation - %i entries" %x_correlation_hist.sum(), box = None, mean_eff = None,
					 output_png=None, zlog=False, min_hits=1, limits=None, scale_large_pixels = False)

		y_correlation_hist, ycorr_xedge, ycorr_yedges = np.histogram2d(selected_tracks["y_slope"], 
																		selected_tracks["slopeyz"],
																		bins = [100,100], range = [[-0.0025,0.0025],[0.0005,0.008]])
		plot_2d_hist(hist2d = y_correlation_hist, output_pdf = output_pdf, bins = [ycorr_xedge, ycorr_yedges], xlabel="pixel y slope",
					 ylabel="RPC y slope", title="Run 2836: y slope correlation - %i entries" %y_correlation_hist.sum(), box = None, mean_eff = None,
					 output_png=None, zlog=False, min_hits=1, limits=None, scale_large_pixels = False)

		# plot slope residuals and find alignment
		logging.info("plot slope residuals")
		x_slope_hist, x_slope_bins = np.histogram(np.clip(selected_tracks["x_slope"] - selected_tracks["slopexz"],-0.1,0.1), bins = 100)
		x_slope_corr = plot_1d_bar(hist_in=x_slope_hist, output_pdf=output_pdf, bar_edges = x_slope_bins, 
					xlim=[-0.1,0.1],legend=None, 
					title="Run 2836 x slope difference - %s entries" %x_slope_hist.sum(), 
					xlabel="pix slope - rpc slope", log=False, output_png = out_pdf_name[:-4] + "_x_slope.png", gauss_fit=True)

		y_slope_hist, y_slope_bins = np.histogram(np.clip(selected_tracks["y_slope"] - selected_tracks["slopeyz"],-0.1,0.1), bins = 100)
		y_slope_corr = plot_1d_bar(hist_in=y_slope_hist, output_pdf=output_pdf, bar_edges = y_slope_bins, 
					xlim=[-0.1,0.1],legend=None, 
					title="Run 2836 y slope difference - %s entries" %y_slope_hist.sum(), 
					xlabel="pix slope - rpc slope", log=False, output_png = out_pdf_name[:-4] + "_y_slope.png", gauss_fit=True)

		plot_1d_hist(array_in = selected_tracks["slopexz"], title = "rpc x slope - %s entries" % selected_tracks.shape[0], bins = 100, output_pdf=output_pdf, log = True)
		plot_1d_hist(array_in = selected_tracks["slopeyz"], title = "rpc y slope - %s entries" % selected_tracks.shape[0], bins = 100, output_pdf=output_pdf, log = True)
		plot_1d_hist(array_in = selected_tracks["x_slope"], title = "pixel x slope - %s entries" % selected_tracks.shape[0], bins = 100, output_pdf = output_pdf, log = True)
		plot_1d_hist(array_in = selected_tracks["y_slope"], title = "pixel y slope - %s entries" % selected_tracks.shape[0], bins = 100, output_pdf = output_pdf, log = True)
		plot_1d_hist(array_in = selected_tracks["x_project_error"], title = "projection error - %s entries" % selected_tracks.shape[0], bins = 100, xlabel = "x error [cm]",output_pdf = output_pdf, log = False)
		plot_1d_hist(array_in = selected_tracks["y_project_error"], title = "projection error - %s entries" % selected_tracks.shape[0], bins = 100, xlabel = "y error [cm]", output_pdf = output_pdf, log = False)

		# plot track residuals in 2d histo
		distance_hist, xedges, yedges = np.histogram2d(delta_x[~np.isnan(delta_x)], delta_y[~np.isnan(delta_x)], range = [clip_range]*2, bins = 50)
		plot_2d_hist(hist2d = distance_hist, output_pdf = output_pdf, bins = [xedges, yedges], xlabel="$\Delta$ x",
					 ylabel="$\Delta$ y", title="Run 2836: pixel-rpc track distance - %i entries" %distance_hist.sum(), box = None, mean_eff = None,
					 output_png=None, zlog=False, min_hits=1, limits=None, scale_large_pixels = False)
		

	logging.info("saved plots at %s" % out_pdf_name)
	return selected_tracks


if __name__ == '__main__':

	spills =  np.loadtxt("/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/run_2836/run_2836/spill_ids.txt", skiprows = 1, usecols = 0, dtype = "str")
	merged_tracks_in_file_path = "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/match_timestamps/match_input.h5"
	
	for i, spill in enumerate(spills):
		append = True
		# if i in [0,7,27,48,59,62,64,71,75,77,110,123,129]:
		# 	continue
		if i in [64,75]:
			continue
		if i == 0:
			append = False
		logging.info("load spill %s - identifier %s" % (i, spill))
		rpc_tracks = load_matched_rpc_tracks(matched_rpc_events_file = "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/run_2836/run_2836/RPC_RecoTracks_run2836_s%s_track_matched.dat" % spill,
						rpc_tracks_file = "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/run_2836/run_2836/RPC_RecoTracks_run2836_s%s_tracks.h5" % spill)
		pixel_tracks = load_matched_pixel_tracks(matched_rpc_events_file = "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/run_2836/run_2836/RPC_RecoTracks_run2836_s%s_track_matched.dat" % spill,
						pixel_tracks_file = "/media/niko/big_data/charm_testbeam_july18/match_rpc/pixel_tracks/run_2836/pix_tracks_spill_%s_tracks.h5" % i)
		logging.info("get rpc track projections for spill %s" %i)
		merged_tracks = merge_data_by_event_number(rpc_tracks, pixel_tracks)

		save_table(array_in = merged_tracks, path = merged_tracks_in_file_path, append = append, node_name = "MergedClusters")
	logging.info("saved input table at %s" % merged_tracks_in_file_path)

	with tb.open_file(merged_tracks_in_file_path, "r") as matching_start:
		merged_tracks = matching_start.root.MergedClusters[:]
		aligned_input_file_path = assign_new_event_number(merged_tracks, path = merged_tracks_in_file_path[:-3] + "_aligned_events.h5")
	logging.info("saved event aligned input table at %s" % aligned_input_file_path)
	
	with tb.open_file(aligned_input_file_path, "r") as matching_start:
		merged_tracks = matching_start.root.MergedClusters[:]
	matched_tracks = match_tracks_in_event(merged_tracks_in=merged_tracks, apply_alignment = True, rpc_plane = 1,
											out_pdf_name= "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/match_timestamps/tracks_matching.pdf", 
											max_event_size = 10, multiple_spills = True)
	merged_tracks_out_file_path = "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/match_timestamps/matched_tracks.h5"
	save_table(matched_tracks, merged_tracks_out_file_path )
	logging.info("saved matched tracks at %s" % merged_tracks_out_file_path)

	# with tb.open_file(aligned_input_file_path, "r") as matching_start:
	# 	merged_tracks = matching_start.root.MergedClusters[:]
	# fake_tracks = make_fake_tracks(merged_tracks)
	# fake_matches = match_tracks_in_event(merged_tracks_in = fake_tracks, 
	# 									 out_pdf_name = "/media/niko/big_data/charm_testbeam_july18/rpc_data/match_timestamps/tracks_fake.pdf",
	# 									 max_event_size = None, multiple_spills = True)
	# fake_out_file_path = "/media/niko/big_data/charm_testbeam_july18/rpc_data/match_timestamps/tracks_fake.h5"
	# save_table(fake_matches, fake_out_file_path)

	# selected_tracks_path = get_large_slopes(aligned_input_file_path, 
	# 										min_slope_x = 0.004, min_slope_y = 0.005, min_x = 0.86, min_y = 1.42, apply_alignment = True)
	with tb.open_file(aligned_input_file_path, "r") as matching_start:
		merged_tracks = matching_start.root.MergedClusters[:]

	matched_tracks = match_tracks_in_event(merged_tracks_in=merged_tracks, apply_alignment = True, rpc_plane = 1,
											out_pdf_name= "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/match_timestamps/multi_tracks_matching.pdf", 
											max_event_size = 10, min_event_size = 2, multiple_spills = True, cut  = True)
	selected_tracks_out_file_path = "/media/niko/big_data/charm_testbeam_july18/match_rpc/rpc_data/match_timestamps/matched_multi_tracks.h5"
	save_table(array_in = matched_tracks, path = selected_tracks_out_file_path, node_name = "MergedClusters" )
	logging.info("saved matched tracks at %s" % selected_tracks_out_file_path)

	# with tb.open_file(aligned_input_file_path, "r") as matching_start:
	# 	merged_tracks = matching_start.root.MergedClusters[:]
	# fake_tracks = make_fake_tracks(merged_tracks)
	# fake_matches = match_tracks_in_event(merged_tracks_in = fake_tracks, 
	# 									 out_pdf_name = "/media/niko/big_data/charm_testbeam_july18/rpc_data/match_timestamps/selected_tracks_fake.pdf",
	# 									 max_event_size = 50, multiple_spills = True, cut  = True)
	# selected_fake_out_file_path = "/media/niko/big_data/charm_testbeam_july18/rpc_data/match_timestamps/selected_tracks_fake.h5"
	# save_table(fake_matches, selected_fake_out_file_path)