from pathlib import Path

import numpy as np
from numba import njit
from scipy import stats
import tables as tb
import matplotlib.pyplot as plt
from beam_telescope_analysis.tools.analysis_utils import get_mean_efficiency
from ship_pixel_tracking.tools.analysis_utils import load_table, save_table, count_matches, count_matches_emulsion, calculate_matching_eff, _correct_emu_alignment, count_column
from ship_pixel_tracking.tools.plot_utils import plot_1d_bar, plot_2d_hist, plot_xy_2axis
from matplotlib.backends.backend_pdf import PdfPages
import logging

logging.basicConfig(level=logging.INFO, format="%(asctime)s - [%(levelname)s] %(message)s")



'''
The selection was as follows:
N_vtx >=6
1 < N_seg < 28
N_cluster > 7
N_pixel/event > 1

where N_vtx is the numnber of emulsion tracks per vertex, N_seg is the number of emulsion segments per track, N_cluster is the cluster size for hits in the pixel tracks, 
N_pixel/event is the number of pixel tracks per event

For the matching:
chi2/ndf < 25
dx < 5 mm
dy < 5 mm
dtx < 15 mrad
dty < 15 mrad
'''

@njit
def mask_more_n(arr, n):
    mask = np.ones(arr.shape, np.bool_)

    current = arr[0]
    count = 0
    for idx, item in enumerate(arr):
        if item == current:
            count += 1
        else:
            current = item
            count = 1
        mask[idx] = count <= n
    return mask


def combine_spills(folder, spills, run = "CH1R6"):
    all_matched = []
    all_unmatched_emu = []
    all_unmatched_pixel = []
    
    node_names = ["MatchedTracks", "unmatched_emulsion", "unmatched_pixel"]

    for spill in spills:
        spill_file = folder + run + "_Spill_%i.h5" % spill
        matched_tracks = load_table(in_file=spill_file, table_name="MatchedTracks")
        unmatched_emulsion = load_table(in_file=spill_file, table_name="unmatched_emulsion")
        unmatched_pixel = load_table(in_file=spill_file, table_name="unmatched_pixel")
        arrays = []
        for i, table in enumerate([matched_tracks, unmatched_emulsion, unmatched_pixel]):
            dtypes = []
            for j, name in enumerate(table.dtype.names):
                dtypes.append((table.dtype.names[j], table[name].dtype))
            dtypes.extend([('spill', np.int32), ("matched", np.int32)])
            new_array = np.full(fill_value=np.nan, shape=table.shape, dtype=dtypes)
            new_array["spill"] = spill
            for column in table.dtype.names:
                new_array[column] = table[column]
            if i==0:
                _correct_emu_alignment(in_array= new_array)
            arrays.append(new_array)
        save_table(arrays, folder + run + "_Spill_%i_spill_aligned.h5" % spill, node_name = node_names)
        all_matched.append(arrays[0])
        all_unmatched_emu.append(arrays[1])
        all_unmatched_pixel.append(arrays[2])
    all_matched = np.concatenate(all_matched)
    all_unmatched_emu = np.concatenate(all_unmatched_emu)
    all_unmatched_pixel = np.concatenate(all_unmatched_pixel)

    all_matched["matched"] = 1
    all_unmatched_emu["matched"] = 0
    all_unmatched_pixel["matched"] = 0

    save_table(array_in = [all_matched, all_unmatched_emu, all_unmatched_pixel], path = folder + "CH1R6_matched.h5", node_name=node_names)


def combine_matched_unmatched(in_file):

    matched_tracks = load_table(in_file=in_file, table_name="MatchedTracks")
    unmatched_emulsion = load_table(in_file=in_file, table_name="unmatched_emulsion")
    unmatched_pixel = load_table(in_file=in_file, table_name="unmatched_pixel")
    
    all_pixel = np.full(fill_value=np.nan, shape = (matched_tracks.shape[0] + unmatched_pixel.shape[0],), dtype = matched_tracks.dtype)
    all_emulsion = np.full(fill_value=np.nan, shape = (matched_tracks.shape[0] + unmatched_emulsion.shape[0],), dtype = matched_tracks.dtype)

    all_emulsion["matched"] = 0
    all_pixel["matched"] = 0

    for column in matched_tracks.dtype.names:
        all_pixel[column][:matched_tracks.shape[0]] = matched_tracks[column]
        all_emulsion[column][:matched_tracks.shape[0]] = matched_tracks[column]

    all_emulsion["match_emu_nseg"][matched_tracks.shape[0]:] = unmatched_emulsion["unmatch_emu_nseg"]
    all_emulsion["match_emu_x"][matched_tracks.shape[0]:] = unmatched_emulsion["unmatch_emu_x"]
    all_emulsion["match_emu_tx"][matched_tracks.shape[0]:] = unmatched_emulsion["unmatch_emu_tx"]
    all_emulsion["match_emu_y"][matched_tracks.shape[0]:] = unmatched_emulsion["unmatch_emu_y"]
    all_emulsion["match_emu_ty"][matched_tracks.shape[0]:] = unmatched_emulsion["unmatch_emu_ty"]
    all_emulsion["emu_vid"][matched_tracks.shape[0]:] = unmatched_emulsion["unmatch_emu_vid"]
    all_emulsion["emu_trk"][matched_tracks.shape[0]:] = unmatched_emulsion["unmatch_emu_trk"]
    all_emulsion["spill"][matched_tracks.shape[0]:] = unmatched_emulsion["spill"]
    
    # all_pixel["matched"][:matched_tracks.shape[0]] = 1
    # all_pixel["matched"][matched_tracks.shape[0]:] = 0
    all_pixel["match_time"][matched_tracks.shape[0]:] = unmatched_pixel["unmatch_time"]
    all_pixel["match_pix_x"][matched_tracks.shape[0]:] = unmatched_pixel["unmatch_pix_x"]
    all_pixel["match_pix_tx"][matched_tracks.shape[0]:] = unmatched_pixel["unmatch_pix_tx"]
    all_pixel["match_pix_y"][matched_tracks.shape[0]:] = unmatched_pixel["unmatch_pix_y"]
    all_pixel["match_pix_ty"][matched_tracks.shape[0]:] = unmatched_pixel["unmatch_pix_ty"]
    all_pixel["pix_trk"][matched_tracks.shape[0]:] = unmatched_pixel["unmatch_pix_trk"]
    all_pixel["spill"][matched_tracks.shape[0]:] = unmatched_pixel["spill"]
    
    save_table([all_pixel, all_emulsion], in_file, node_name=["all_pixel", "all_emu"], append=True)


def plot_matching(tracks_file, out_file, plot_spills = False, max_n_tracks = None):

    with PdfPages(out_file) as out_pdf:

        ts_counts_bins = np.arange(0, 30)
        vtx_counts_bins = np.arange(0, 30)

        matched_tracks = load_table(in_file=tracks_file, table_name="MatchedTracks")
        unmatched_emulsion = load_table(in_file=tracks_file, table_name="unmatched_emulsion")
        unmatched_pixel = load_table(in_file=tracks_file, table_name="unmatched_pixel")
        all_pixel = load_table(in_file = tracks_file, table_name = "all_pixel")

        if max_n_tracks:
            time, indices, counts = np.unique(all_pixel["match_time"], return_index = True, return_counts = True)
            # only choose timestamps with counts < max_n_tracks
            indices = indices[counts<=max_n_tracks]
            counts = counts[counts<=max_n_tracks]
            mask = np.zeros(all_pixel.shape, np.bool_)
            # unmask complete events with tracks larger max_n_tracks
            for i, index in enumerate(indices[counts<=max_n_tracks]):
                mask[index:(index+counts[i])] = 1
                
            all_pixel_masked = all_pixel[mask]

            matched_mask = all_pixel_masked["matched"] ==1
            matched_tracks = all_pixel_masked[matched_mask]
            unmatched_pixel = all_pixel_masked[~matched_mask]
        
        if plot_spills:
            
            for spill in [8, 9, 10, 11, 12]:
                # load spill file
                spill_matched = matched_tracks[matched_tracks["spill"] == spill]
                spill_unmatched_emu = unmatched_emulsion[unmatched_emulsion["spill"]==spill]
                spill_umatched_pix = unmatched_pixel[unmatched_pixel["spill"]==spill]
                #histogram and plot
                counted_vtx = count_matches(spill_matched)
                counted_pix = count_matches_emulsion(spill_matched)

                vtx, vtx_index, vtx_counts = np.unique(spill_matched["emu_vid"], return_counts=True, return_index=True)
                ts, ts_index, ts_counts = np.unique(spill_matched["match_time"], return_counts=True, return_index=True)
                ts_unmatched, ts_un_index, ts_un_counts = np.unique(spill_umatched_pix["unmatch_time"], return_counts=True, return_index=True)

                vtxhist, vtx_bins = np.histogram(vtx_counts, bins=vtx_counts_bins)
                plot_1d_bar(hist_in=vtxhist, output_pdf=out_pdf, bar_edges=vtx_bins, title = "CH1R6 S%i - %i matched vertices" %(spill, vtxhist.sum()),
                            log = False, ylabel="#", xlabel = "matched emulsion tracks/vtx")

                ts_count_hist, ts_count_bins = np.histogram(ts_counts, bins=ts_counts_bins)
                plot_1d_bar(hist_in=ts_count_hist, output_pdf=out_pdf, bar_edges=ts_count_bins, title = "CH1R6 S%i - %i matched events" %(spill, ts_count_hist.sum()),
                            log = False, ylabel="#", xlabel = "matched pixel tracks/timestamp")

                ts_hist, ts_bins = np.histogram(ts*25e-9, bins=np.linspace(0, 5., 50))
                plot_1d_bar(hist_in=ts_hist, output_pdf=out_pdf, bar_edges=ts_bins, title = "CH1R6 S%i matched tracks vs. timestamp - %i entries" %(spill, ts_hist.sum()),
                            log = False, ylabel="#", xlabel = "time [s]")

                chi2_hist, chi2_bins = np.histogram(matched_tracks["match_chi2"], bins=100)
                plot_1d_bar(hist_in=chi2_hist, output_pdf=out_pdf, bar_edges=chi2_bins, title = "CH1R6 S%i - matching Chi2 for %i tracks" %(spill, chi2_hist.sum()),
                            log = False, ylabel="#", xlabel = "$\chi ^2$")

                ts_vid_hist, ts_bins, vid_bins = np.histogram2d(counted_vtx["pix_counts"], counted_vtx["emu_counts"],
                                                                bins =[np.arange(0,30),np.arange(0,30)])
                plot_2d_hist(hist2d=ts_vid_hist, output_pdf=out_pdf, bins=[ts_bins, vid_bins], zlog=False, min_hits = 1, xlabel="# of pixel tracks",
                            ylabel = "# of emulsion tracks", title = "CH1R6 S%i : matched tracks per event - %i entries" % (spill,ts_vid_hist.sum()))

                vtx_ts_hist, vid_bins, ts_bins = np.histogram2d(counted_pix["emu_counts"], counted_pix["pix_counts"], bins=[
                                                                np.arange(0.5, 26, 1), np.arange(0.5, 26, 1)])
                plot_2d_hist(hist2d=vtx_ts_hist, output_pdf=out_pdf, bins=[vid_bins, ts_bins], zlog=True, min_hits = 1,
                             xlabel="# of emulsion tracks", limits = [1,vtx_ts_hist.max()*0.25],grid = True,
                             ylabel = "# of pixel tracks", title = "CH1R6 S%i : matched tracks per event - %i entries" % (spill,vtx_ts_hist.sum()))

        counted_vtx = count_matches(matched_tracks)
        counted_pix = count_matches_emulsion(matched_tracks)

        vtx_ts_hist, vid_bins, ts_bins = np.histogram2d(np.clip(counted_pix["emu_counts"], 0, 25), np.clip(
            counted_pix["pix_counts"], 0, 25), bins=[np.arange(0.5, 26, 1), np.arange(0.5, 26, 1)])
        plot_2d_hist(hist2d=vtx_ts_hist, output_pdf=out_pdf, bins=[vid_bins, ts_bins], zlog=True, min_hits=1,
                     xlabel="# of emulsion tracks", grid=True,
                     ylabel="# of pixel tracks", title="CH1R6 : matched tracks per event - %i entries" % vtx_ts_hist.sum())

        
        vtx_ts_hist, vid_bins = np.histogram(counted_pix["pix_rate_correct"], bins=np.linspace(0.5, 1, 20))

        plot_1d_bar(hist_in=vtx_ts_hist, output_pdf=out_pdf, bar_edges=vid_bins, log=True,
                    xlabel="# correct ts / # matched tracks",
                    ylabel="# ", title="CH1R6 : ratio of correctly matched pixel tracks per event - %i entries" % vtx_ts_hist.sum())

        plot_1d_bar(hist_in=vtx_ts_hist, output_pdf=out_pdf, bar_edges=vid_bins, log=False,
                    xlabel="# correct ts / # matched tracks",
                    ylabel="# ", title="CH1R6 : ratio of correctly matched pixel tracks per event - %i entries" % vtx_ts_hist.sum())
        
        ts, ts_index, ts_counts = np.unique(matched_tracks["match_time"], return_counts=True, return_index=True)

        ts_unmatched, ts_un_index, ts_un_counts = np.unique(unmatched_pixel["match_time"], return_counts=True, return_index=True)
        ts_unmatched_hist, ts_unmatched_bins = np.histogram(ts_unmatched*25e-9, bins=np.linspace(0, 5., 50))
        
        ts_matched_hist, ts_bins = np.histogram(ts*25e-9, bins=np.linspace(0, 5., 50))

        ts_all_hist = ts_unmatched_hist + ts_matched_hist
        ts_rate_hist = np.zeros(shape=ts_all_hist.shape)
        ts_rate_hist[ts_all_hist != 0] += ts_matched_hist[ts_all_hist != 0] / (ts_unmatched_hist[ts_all_hist != 0] + ts_all_hist[ts_all_hist != 0])

        plot_xy_2axis(x=ts_unmatched_bins[:-1], y=ts_rate_hist, hist_in=ts_all_hist, bar_edges=ts_unmatched_bins, output_pdf=out_pdf, y2label="#", legend="rate of matched ts",
                      legend2="all ts - %i entries" % ts_all_hist.sum(),  markersize=5, xlabel="timestamp [s]",
                      ylabel="# matched ts / all ts ", title="CH1R6 : rate of matched pixel events")

        plot_xy_2axis(x=ts_bins[:-1], y=ts_matched_hist, hist_in=ts_all_hist, bar_edges=ts_unmatched_bins, output_pdf=out_pdf, y2label="#", legend="number of matched ts",
                      legend2="all ts - %i entries" % ts_all_hist.sum(),  markersize=5, xlabel="timestamp [s]",
                      ylabel="# matched ts", title="CH1R6 : matched pixel events vs time")

        ts_count_hist, ts_count_bins = np.histogram(ts_counts, bins=ts_counts_bins)
        plot_1d_bar(hist_in=ts_count_hist, output_pdf=out_pdf, bar_edges=ts_count_bins, title="CH1R6 - %i matched events" % ts_matched_hist.sum(),
                    log=False, ylabel="#", xlabel="matched pixel tracks/timestamp")

        vtx, vtx_index, vtx_counts = np.unique(matched_tracks["emu_vid"], return_counts=True, return_index=True)
        vtxhist, vtx_bins = np.histogram(vtx_counts, bins=vtx_counts_bins)
        plot_1d_bar(hist_in=vtxhist, output_pdf=out_pdf, bar_edges=vtx_bins, title="CH1R6 - %i matched events" % vtxhist.sum(),
                    log=False, ylabel="#", xlabel="matched emulsion tracks/vtx")
        
        chi2_hist, chi2_bins = np.histogram(matched_tracks["match_chi2"], bins=100)
        plot_1d_bar(hist_in=chi2_hist, output_pdf=out_pdf, bar_edges=chi2_bins, title="CH1R6 : matching Chi2 for %i tracks" % chi2_hist.sum(),
                    log=False, ylabel="#", xlabel="$\chi ^2$")

        pix_matched_tracks = np.array([matched_tracks["match_pix_x"], matched_tracks["match_pix_y"]])
        all_pix_tracks = np.append(pix_matched_tracks, np.array([unmatched_pixel["match_pix_x"], unmatched_pixel["match_pix_y"]]), axis=1)
        emu_matched_tracks = np.array([matched_tracks["match_emu_x"], matched_tracks["match_emu_y"]])
        all_emu_tracks = np.append(emu_matched_tracks, np.array([unmatched_emulsion["unmatch_emu_x"], unmatched_emulsion["unmatch_emu_y"]]), axis=1)

        pix_eff, pix_track_density, pix_matched_density, pix_eff_xbins, pix_eff_ybins = calculate_matching_eff(pix_matched_tracks, all_pix_tracks, x_dim=12.25, n_bins=100)
        mean_pix, lower, upper = get_mean_efficiency(pix_matched_density, pix_track_density)

        emu_eff, emu_track_density, emu_matched_density, emu_eff_xbins, emu_eff_ybins = calculate_matching_eff(emu_matched_tracks, all_emu_tracks, x_dim=12.25, n_bins = 100)
        mean_emu, lower, upper = get_mean_efficiency(emu_matched_density, emu_track_density)

        difference_local = np.column_stack((np.array(matched_tracks["match_dxr"])*1e4, np.array(matched_tracks["match_dyr"])*1e4))
        distance_local = np.sqrt(np.einsum('ij,ij->i', difference_local, difference_local))
        # stat_2d_residuals_hist, _, _, _ = stats.binned_statistic_2d(
        #     x=pix_matched_tracks_x, y=pix_matched_tracks_y, values=distance_local, statistic='mean', bins=[pix_eff_xbins, pix_eff_ybins])
        stat_2d_residuals_hist, _, _, _ = stats.binned_statistic_2d(
            x=matched_tracks["match_emu_x"], y=matched_tracks["match_emu_y"], values=distance_local, statistic='mean', bins=[emu_eff_xbins, emu_eff_ybins])
        
        stat_2d_residuals_hist = np.nan_to_num(stat_2d_residuals_hist)
        stat_2d_residuals_hist_masked = np.ma.masked_equal(stat_2d_residuals_hist, 0)

        plot_2d_hist(hist2d=pix_eff, output_pdf=out_pdf, bins=[pix_eff_xbins, pix_eff_ybins], zlog=False, xlabel="x [cm]", force_int_z_ticks=False, #limits=[0, 1],
                     ylabel="y [cm]", title=f'CH1R6 \n pixel track matching efficiency - mean = ${mean_pix*100:.2f}_{{{lower*100:.2f}}}^{{+{upper*100:.2f}}}$ %')

        plot_2d_hist(hist2d=emu_eff, output_pdf=out_pdf, bins=[emu_eff_xbins, emu_eff_ybins], zlog=False, xlabel="x [cm]", force_int_z_ticks=False,
                     ylabel="y [cm]", title=f'CH1R6 \n emulsion track matching efficiency - mean = ${mean_emu*100:.2f}_{{{lower*100:.2f}}}^{{+{upper*100:.2f}}}$ %')

        plot_2d_hist(hist2d=stat_2d_residuals_hist_masked, output_pdf=out_pdf, bins=[emu_eff_xbins, emu_eff_ybins], zlog=False, xlabel="x [cm]",
                     ylabel="y [cm]", limits=[0, stat_2d_residuals_hist_masked.mean()*2], force_int_z_ticks=False,
                     title="CH1R6 \n average residuals - mean : %.2f $\mu$m " % distance_local.mean())

        plot_2d_hist(hist2d=pix_matched_density, output_pdf=out_pdf, bins=[pix_eff_xbins, pix_eff_ybins], zlog=False, xlabel="x [cm]",
                     ylabel="y [cm]", print_bin_values=False, limits=[1, 22], force_int_z_ticks=True,
                     title="CH1R6 \n matched track density - %i entries" % pix_matched_density.sum(), min_hits=1)


if __name__ == "__main__":

    home = str(Path.home())

    combine_spills("/media/niko/big_data/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/", spills = [8, 9, 10, 11, 12])
    # combine_matched_unmatched(home + "/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched2.h5")
    combine_matched_unmatched("/media/niko/big_data/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched.h5")
    
    # plot_matching(tracks_file = home + "/cernbox/SHiP/charm_xsec_july18/match_emu/CH1_R6/CH1R6_matched.h5",
    #               out_file = "/Users/niko/Desktop/matched_emulsion.pdf", max_n_tracks = 100000)

    