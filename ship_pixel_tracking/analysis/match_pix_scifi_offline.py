import numpy as np
import tables as tb
import logging
import os
import warnings

from tqdm import tqdm
from numba import njit

from scipy.optimize import curve_fit
from matplotlib.backends.backend_pdf import PdfPages


from ship_pixel_tracking.tools.plot_utils import plot_1d_hist, plot_xy, plot_xy_2axis, plot_xy_confidence_band, plot_1d_bar, plot_2d_hist, plot_residuals_vs_position, plot_magnetic_deflection, plot_magnet_example
from ship_pixel_tracking.tools.analysis_utils import save_table, load_table, get_matched_tracks_pixel, circle, get_files, merge_spills, rotate_around_z, scifi_tracks_dtype, red_chisquare, line, radius, radius2, load_config, merged_tracks_dtype, merged_clusters_dtype, combined_tracks_dtype, get_deflection


'''
Script to build common events of pixel and scifi data from SHiP charm testbeam of July 2018.
Pixel data is clustered with clusterizer from beam_telescope_analysis (https://github.com/SiLab-Bonn/beam_telescope_analysis)
and tracking is performed with "TestbeamFullPix.c" of this repository.
SciFi data is refined with code from Maria Elena Stramaglia and Ana Barbara Calvacante. No repository available.
SciFi timestamps needed to be recalculated, channel to position mapping is performed and clustering is done.
No SciFi tracks available at the moment.
Pixel tracks are projected to scifi clusters and best match is chosen
'''


logging.basicConfig(level=logging.INFO, format='%(asctime)s - [%(levelname)-8s] %(message)s')


def get_unique_events(clusters, column_name):
    '''
    Get array with first occurence of each unique value in column with column_name
    --------------------
    INPUT:
        clusters: numpy structured array
            array with hits or clusters
        column_name: string
            name of the column in "clusters" to check for unique values
    OUTPUT:
        returns lines of "clusters" where unique values first appear
    '''
    _, unique_indices = np.unique(clusters[column_name], return_index=True)
    return clusters[unique_indices]


def get_unique_deltas(array_in, ts_limit=None):
    '''
    Only return first occurence of timestamp and its delta to the preceeding one. Discard all other hits of the event.
    If ts_limit is given, discard all events with larger timestamp.
    --------------
    INPUT:
        array_in: structured numpy.array()
            contains scifi data, needs to have column "delta_trigger_shift"
        ts_limit: int or float
            limit for timestamp. If given events with larger timestamp will be discarded. If given array needs column "trigger_shift"
    OUTPUT:
        array_out: structured numpy.array()
            array with same dtype as array_in but only one row per event
    '''

    array_unique = array_in[array_in["delta_trigger_shift"] != 0]
    mask = array_unique["delta_trigger_shift"] < 0
    array_out = array_unique[~mask]
    if ts_limit:
        mask = np.logical_or(array_unique["trigger_shift"] > ts_limit, mask)
        array_out = array_unique[~mask]
    if mask[mask == True].shape[0] > 0:
        logging.warning("found %s broken events in SciFi" % mask[mask == True].shape[0])
    return array_out


def get_timestamp_diff(table):
    '''
    Create new column in table with diffrence of event timestamp to preceeding timestamp
    ------------------
    INPUT:
        table: structured numpy.array()
            array must contain a column "trigger_time_stamp"
    OUTPUT:
        new_array: structured numpy.array()
            array with the same dtype and content as input,
            but with additional column "delta_timestamp"
            where the difference of the rows timestamp to the preceeding one is saved.
    '''

    dtypes = []
    for j, name in enumerate(table.dtype.names):
        dtypes.append((table.dtype.names[j], table[name].dtype))
    dtypes.extend([('delta_timestamp', np.int64)])
    new_array = np.zeros(shape=table.shape, dtype=dtypes)
    for column in table.dtype.names:
        new_array[column] = table[column]

    # if input table is empty one can not assign entries...
    if new_array.shape[0] > 0:
        new_array["delta_timestamp"][1:] = np.diff(table["trigger_time_stamp"])
        new_array["delta_timestamp"][0] = new_array["trigger_time_stamp"][0]
    return new_array


def choose_matched_events_pixel(cluster_in, common_events):
    '''
    After matching the timestamps collect the events with all the clusters for the matched timestamps
    --------------------
    INPUT:
        cluster_in: structured numpy.array()
            array with all pixel clusters of the spill

        common_events: structured numpy.array()
            array with matched timestamps and event numbers
    OUTPUT:
        cluster_out: structured numpy.array()
            array with all pixel clusters of matched events
    '''

    cluster_out = []  # List()
    i = 0
    for row in cluster_in:
        while (row["event_number"] > common_events[i]["pixel_event"]) and (i < common_events.size - 1):
            i += 1
        if (row["event_number"] == common_events[i]["pixel_event"]):
            cluster_out.append(row)
        elif (row["event_number"] < common_events[i]["pixel_event"]):
            continue
    return np.array(cluster_out)


def get_matched_events_scifi(scfi_hits_in, matched_events):
    '''
    After matching the timestamps collect the events with all the clusters for the matched timestamps.
    Assigns "matched_event_number" to scifi events.
    --------------------
    INPUT:
        scfi_hits_in: structured numpy.array()
            array with all scifi hits of the spill

        common_events: structured numpy.array()
            array with matched timestamps and event numbers
    OUTPUT:
        hits_out: structured numpy.array()
            array with all scifi hits of matched events
    '''
    hits_out = []
    i = 0
    for row in scfi_hits_in:
        while (row["event_number"] > matched_events[i]["scifi_event"]) and (i < matched_events.size - 1):
            i += 1
        if (row["event_number"] == matched_events[i]["scifi_event"]):
            hits_out.append(row)
            hits_out[-1]["event_number"] = matched_events[i]["matched_event_number"]
        elif (row["event_number"] < matched_events[i]["scifi_event"]):
            continue
    return np.array(hits_out)


@njit()
def find_closest_delta_t(cluster_table, scifi_in, common_in, unmatched_scifi, delta_t=1000):
    '''
    Check if the difference of event timestamp to preceeding event timestamp is the same or close for both detectors.
    Careful, the difference of timestamps is not unique in a spill.
    --------------------
    INPUT:
        cluster_table: numpy structured array
            first entry of every unique event in cluster data.
        scifi_in: numpy array
            all scifi data for the spill
        common_in: numpy structured array
            array to be filled with matched events.
        delta_t: int
            maximum allowed distance between difference of pixel and scifi events
    OUTPUT:
        common_out: numpy structured array
            array with matched events
        unmatched_scifi: structured numpy.array
            all rows which could not be matched
    OUTPUT CONTAINS ALL TIMESTAMPS IN UNITS OF 1 ns
    '''

    j = 0
    matched_event_index = 0
    for i, timestamp in enumerate(cluster_table["delta_timestamp"]):
        matched_event_index = j

        while (j < scifi_in.shape[0]-1) and (np.abs(timestamp*25 - scifi_in[j]["delta_trigger_shift"]) > delta_t):
            j += 1
        # in case pixel event can not be matched, continue with next pixel event and start at last valid scifi index
        if j >= (matched_event_index + 10):
            j = matched_event_index
            continue
        # edge case for last entry
        if j == scifi_in.shape[0]-1:
            if (np.abs(timestamp*25 - scifi_in[j]["delta_trigger_shift"]) > delta_t):
                unmatched_scifi[j] = scifi_in[j]
                break
        # found matching event, fill output array
        else:
            common_in[j]["scifi_event"] = scifi_in[j]["event_number"]
            common_in[j]["pixel_event"] = cluster_table[i]["event_number"]
            common_in[j]["scifi_trigger_shift"] = scifi_in[j]["trigger_shift"]
            common_in[j]["pixel_ts"] = cluster_table[i]["trigger_time_stamp"]*25
            common_in[j]["delta_scifi_trigger_shift"] = scifi_in[j]["delta_trigger_shift"]
            common_in[j]["delta_pixel_ts"] = timestamp*25
            common_in[j]["delta_t"] = timestamp*25 - scifi_in[j]["delta_trigger_shift"]
            # collect not matched events
            if (matched_event_index - j) < 0:
                unmatched_scifi[matched_event_index+1:j] = scifi_in[matched_event_index+1:j]
            # TODO edge case: if first event can not be matched. Correct?
            if matched_event_index == 0 and j != 0:
                unmatched_scifi[0] = scifi_in[0]

    # remove empty rows and fill new matched event number
    common_out = common_in[common_in["scifi_event"] != -1]
    unmatched_scifi = unmatched_scifi[unmatched_scifi["event_number"] != -1]
    for k, row in enumerate(common_out):
        row["matched_event_number"] = k

    return common_out, unmatched_scifi


def find_scifi_tracks(scifi_clusters, event_indices, counts, out_path, track_fit_angular_limit = 0.15, max_sigma = 2.5, max_chi2 = 100, alignment_file = None, dimension = "x", restrict_hit_used = True):
    '''
    Tracking for SciFi x planes. Only build tracks in one dimension, for y coordinates use matched pixel tracks (avoid ghosting).
    Same algorithm as for pixel tracking. To reduce combinatorics only all the possible combinations of first and last plane hits are formed.
    For the planes in between a search cone is calculated, only hits within this radius are considered for tracking.
    The reduced chi2 is used to decide which combination is the best one. Only this one will be written to output array.
    ----------------------------------------
    INPUT:
        scifi_clusters : numpy structured array
            has to contain event_number, x values, plane, also tot and cluster_size
        event_indices: numpy array
            contains indices of start of new events in scifi_clusters
        counts: numpy array
            same size as indices, contains event size for each event
        out_path: string
            location where to write track file
        track_fit_angular_limit : float
            maximum slope to consider for tracking cone
        max_sigma : float
            maximum deviation of track in cone from predicted track
        alignment_file: string
            path to yaml file with alignment values
        dimension : strings
            fit either x or y dimension
        restrict_hit_used: bool
            if true use each hit only once
    OUTPUT:
        none. Track file is written to disk
    '''
    z_vals = {1:0, 2:18.06, 3:39.5, 4:57.55, 5: 160.0, 7: 199.5, 8: 217.55}

    if dimension == "y":
        fit_planes = [3,4,5]
        seed_planes = [3,5]
    else:
        fit_planes = [1,2,7,8]
        seed_planes = [1,8]
    
    if alignment_file:
        alignment = load_config(alignment_file)
        for plane in fit_planes:
            scifi_clusters[dimension][scifi_clusters["plane"]==plane] += alignment[plane]["offset"]

    fit_z = [z_vals[plane] for plane in fit_planes]

    scifi_res = 0.01 # 100 um
    tracks = []
    
    for seed in seed_planes:
        if seed in fit_planes:
            fit_planes.remove(seed)
    
    for event_start, start_index in enumerate(event_indices):
        track_ID = 0
        event = scifi_clusters[start_index:start_index + counts[event_start]]
        
        seed1 = event[event["plane"]==seed_planes[0]]
        seed2 = event[event["plane"]==seed_planes[1]]
        fit_plane_1 = event[event["plane"]==fit_planes[0]]
        fit_plane_2 = event[event["plane"]==fit_planes[1]]
        
        # start on first plane and build all possible combinations with hits on last plane
        for seed_1_index in range(seed1.shape[0]):
            track_indices = None
            seed_1_hit = seed1[seed_1_index]
            if restrict_hit_used and seed_1_hit["hitused"] > 0:
                continue
            for seed_2_index in range(seed2.shape[0]):
                seed_2_hit = seed2[seed_2_index]
                if restrict_hit_used and seed_2_hit["hitused"] > 0:
                    continue
                # calculate simple track to last plane for search cone
                ax = (seed_2_hit["x"] - seed_1_hit["x"]) / (seed_2_hit["z"] - seed_1_hit["z"]) # // track is X=ax*Z+bx
                if np.abs(ax) > track_fit_angular_limit:
                    continue
                bx = seed_1_hit["x"] - ax * seed_1_hit["z"]
                dZ2 = np.square(seed_2_hit["z"] - seed_1_hit["z"])
                covAA_x = (2 * scifi_res) / dZ2
                covBB_x = (scifi_res * np.square(seed_2_hit["z"]) + scifi_res * np.square(seed_1_hit["z"])) / dZ2
                covAB_x = -(scifi_res * seed_2_hit["z"] + scifi_res * seed_1_hit["z"]) / dZ2

                candX_1 = ax * z_vals[fit_planes[0]] + bx
                candX_2 = ax * z_vals[fit_planes[1]] + bx
                best_chi2 = 1e10 
                for hit_1_index in range(fit_plane_1.shape[0]):
                    hit_1 = fit_plane_1[hit_1_index]
                    if restrict_hit_used and hit_1["hitused"] > 0:
                        continue
                    #calculate covariance of track extrapolation to second plane
                    extrapCovX_1 = np.square(hit_1["z"]) * covAA_x + covBB_x + 2. * hit_1["z"] * covAB_x
                    # search radius on plane 1
                    LimX_1 = max_sigma * np.sqrt(extrapCovX_1 + scifi_res)
                    if np.abs(candX_1 - hit_1["x"]) > LimX_1:
                        continue
                    for hit_2_index in range(fit_plane_2.shape[0]):
                        hit_2 = fit_plane_2[hit_2_index]
                        if restrict_hit_used and hit_2["hitused"] > 0:
                            continue
                        # calculate search radius and covriance for plane 2
                        extrapCovX_2 = np.square(hit_2["z"]) * covAA_x + covBB_x + 2. * hit_2["z"] * covAB_x
                        LimX_2 = max_sigma * np.sqrt(extrapCovX_2 + scifi_res)
                        if np.abs(candX_2 - hit_2["x"]) > LimX_2:
                            continue
                        # use found hits for track fit
                        y = [seed_1_hit["x"], hit_1["x"], hit_2["x"], seed_2_hit["x"]]
                        popt, pcov = curve_fit(line, xdata = fit_z, ydata =y , p0 = [ax, bx], sigma = np.array([scifi_res]*4), absolute_sigma = True)
                        chi2 = red_chisquare(np.array(y), np.array([line(z_val, popt[0], popt[1]) for z_val in fit_z]), np.array([scifi_res]*4), popt)
#                         chi2 = chisquare(y, f_exp = [line(z_val, popt[0], popt[1]) for z_val in fit_z], ddof = 2)[0]
                        # check if track fit is better than previous one
                        if chi2 < best_chi2:
                            track_indices = {seed_planes[0]: seed_1_index, 
                                             fit_planes[0]: hit_1_index, 
                                             fit_planes[1]: hit_2_index, 
                                             seed_planes[1]: seed_2_index
                                            }
                            track_x = popt[1]
                            track_x_slope = popt[0]
                            track_x_error = np.sqrt(pcov[1][1])
                            x_slope_error = np.sqrt(pcov[0][0])
                            track_chi2 = chi2
                            best_chi2 = chi2
                        
            if track_indices and track_chi2 <= max_chi2:
                
                # mark used hits
                seed1[track_indices[seed_planes[0]]]["hitused"] += 1
                seed2[track_indices[seed_planes[1]]]["hitused"] += 1
                fit_plane_1[track_indices[fit_planes[0]]]["hitused"] +=1
                fit_plane_2[track_indices[fit_planes[1]]]["hitused"] +=1
                # fill output array
                hits_out = np.full(fill_value = np.nan, shape = (1,), dtype = scifi_tracks_dtype )
                hits_out["x"] = track_x
                hits_out["x_slope"] = track_x_slope
                hits_out["x_error"] = track_x_error
                hits_out["x_slope_error"] = x_slope_error
                hits_out["z"] = 0
                hits_out["chi2"] = track_chi2
                hits_out["track_ID"] = track_ID
                hits_out["event_number"] = seed1[track_indices[seed_planes[0]]]["event_number"]
                hits_out["trigger_shift"] = seed1[track_indices[seed_planes[0]]]["trigger_shift"]
                hits_out["spill"] = seed1[track_indices[seed_planes[0]]]["spill"]
                
                hits_out["x%s" % seed_planes[0]] = seed1[track_indices[seed_planes[0]]]["x"]
                hits_out["x%s" % seed_planes[1]] = seed2[track_indices[seed_planes[1]]]["x"]
                hits_out["x%s" % fit_planes[0]] = fit_plane_1[track_indices[fit_planes[0]]]["x"]
                hits_out["x%s" % fit_planes[1]] = fit_plane_2[track_indices[fit_planes[1]]]["x"]
                
                hits_out["z%sx" % seed_planes[0]] = z_vals[seed_planes[0]]
                hits_out["z%sx" % seed_planes[1]] = z_vals[seed_planes[1]]
                hits_out["z%sx" % fit_planes[0]] = z_vals[fit_planes[0]]
                hits_out["z%sx" % fit_planes[1]] = z_vals[fit_planes[1]]

                hits_out["cluster_size_%s" % seed_planes[0]] = seed1[track_indices[seed_planes[0]]]["cluster_size"]
                hits_out["cluster_size_%s" % seed_planes[1]] = seed2[track_indices[seed_planes[1]]]["cluster_size"]
                hits_out["cluster_size_%s" % fit_planes[0]] = fit_plane_1[track_indices[fit_planes[0]]]["cluster_size"]
                hits_out["cluster_size_%s" % fit_planes[1]] = fit_plane_2[track_indices[fit_planes[1]]]["cluster_size"]

                hits_out["tot_%s" % seed_planes[0]] = seed1[track_indices[seed_planes[0]]]["tot"]
                hits_out["tot_%s" % seed_planes[1]] = seed2[track_indices[seed_planes[1]]]["tot"]
                hits_out["tot_%s" % fit_planes[0]] = fit_plane_1[track_indices[fit_planes[0]]]["tot"]
                hits_out["tot_%s" % fit_planes[1]] = fit_plane_2[track_indices[fit_planes[1]]]["tot"]

                hits_out["hit_%s_used" % seed_planes[0]] = seed1[track_indices[seed_planes[0]]]["hitused"]
                hits_out["hit_%s_used" % seed_planes[1]] = seed2[track_indices[seed_planes[1]]]["hitused"]
                hits_out["hit_%s_used" % fit_planes[0]] = fit_plane_1[track_indices[fit_planes[0]]]["hitused"]
                hits_out["hit_%s_used" % fit_planes[1]] = fit_plane_2[track_indices[fit_planes[1]]]["hitused"]

                track_ID +=1
                tracks.append(hits_out)

    tracks = np.array(tracks)
    if tracks.shape[0]>0:
        save_table(tracks, path =out_path, append = False, node_name = "Tracks")


def calculate_scifi_residuals(scifi_tracks_in, out_path, run, residual_bins = 100, ):
    '''
    calculate and plot distance of track to hit for reach scifi x plane. Also plot chi2/ndf and track position
    -----------------------------
    INPUT:
        scifi_tracks_in: numpy strcut array
            contains scifi tracks and associated hits, one track with all the hits in one row
        out_path : string
            where the output pdf file will be saved
        run : int
            run number
    OUTPUT:
        no return, pdf written to disk.
    '''
    planes = [1,2,7,8]
    with PdfPages(out_path) as output_pdf:
        for plane in planes:
            plane_residuals = scifi_tracks_in["x"] + scifi_tracks_in["x_slope"] * scifi_tracks_in["z%ix" % plane] - scifi_tracks_in["x%i" % plane]
            x_res_hist, x_res_hist_bins = np.histogram(np.clip(plane_residuals*10000,-10000,10000), bins = residual_bins,)
            plot_1d_bar(hist_in = x_res_hist, output_pdf = output_pdf, bar_edges = x_res_hist_bins, xlim=None, ylim = None, xlabel="$\Delta x$ [$\mu m$]", ylabel = None, ax=None, fig=None,
                title="Run %s scifi track residuals on plane %s - %i entries" %(run, plane, x_res_hist.sum()), legend = None, yerr = None, error_kw= None, capsize = None, log = False, linewidth = 0.005,
                output_png = out_path[:-4] + "_scifi_plane_%s_x_residuals.png" %plane, vlines=None, gauss_fit=True, gauss_unit = "$\mu$ m")
            
            used_hist, used_bins = np.histogram(scifi_tracks_in["hit_%i_used" % plane], bins = 25)
            plot_1d_bar(hist_in = used_hist, output_pdf = output_pdf, bar_edges = used_bins, xlim=None, ylim = None, xlabel="# of tracks hit was used for", ylabel = None, ax=None, fig=None,
                    title="Run %s plane %i scifi hitused - %i entries" %(run, plane, used_hist.sum()), legend = None, yerr = None, error_kw= None, capsize = None, log = False, linewidth = 0.005,
                    output_png = None, vlines=None, gauss_fit=False, gauss_unit = "$\mu$ m")
    
        x_hist, x_bins = np.histogram(scifi_tracks_in["x"]*10, bins = 100, )
        plot_1d_bar(hist_in = x_hist, output_pdf = output_pdf, bar_edges = x_bins, xlim=None, ylim = None, xlabel="x pos [mm] ", ylabel = None, ax=None, fig=None,
                    title="Run %s scifi track x position  - %i entries" %(run, x_hist.sum()), legend = None, yerr = None, error_kw= None, capsize = None, log = False, linewidth = 0.005,
                    output_png = out_path[:-4] + "_scifi_track_x.png", vlines=None, gauss_fit=False, gauss_unit = "mm")

        x_hist, x_bins = np.histogram(np.clip(scifi_tracks_in["x"]*10, -50, 50), bins = 100, )
        plot_1d_bar(hist_in = x_hist, output_pdf = output_pdf, bar_edges = x_bins, xlim=None, ylim = None, xlabel="x pos [mm] ", ylabel = None, ax=None, fig=None,
                    title="Run %s scifi track x position  - %i entries" %(run, x_hist.sum()), legend = None, yerr = None, error_kw= None, capsize = None, log = False, linewidth = 0.005,
                    output_png = out_path[:-4] + "_scifi_track_x_clip.png", vlines=None, gauss_fit=True, gauss_unit = "mm")

        chi2_hist, chi2_bins = np.histogram(scifi_tracks_in["chi2"], bins = 100)
        plot_1d_bar(hist_in = chi2_hist, output_pdf = output_pdf, bar_edges = chi2_bins, xlim=None, ylim = None, xlabel="$\chi^2 /ndf$ ", ylabel = None, ax=None, fig=None,
                    title="Run %s scifi track chi2  - %i entries" %(run, chi2_hist.sum()), legend = None, yerr = None, error_kw= None, capsize = None, log = False, linewidth = 0.005,
                    output_png = out_path[:-4] + "_chi2.png", vlines=None, gauss_fit=False, gauss_unit = "$\mu$ m")


def fit_combined_tracks(matched_tracks, out_path, max_chi2 = None, max_delta_x = 1., n_planes_required = [4,3]):
    '''
    Use existing pixel tracks and calculated distance of these tracks on each scifi cluster to fit a track.
    Makes use of the fact that only single track events are investigated!
    Pixel tracks need to be already propagated through magnetic field. The seed for the combined track is at z = end of magnet
    For each plane the closest cluster is taken, the minimum number of planes per dimension can be set.
    The results are strongly influenced by the error on the pixel track position after the magnetic field. Due to the large distance it is dominated (99%) by the error on the slope.
    ----------------
    INPUT:
        matched_tracks : np.array
            structured array with pixel tracks and scifi clusters. One cluster per row. Output of get_track_distance
        out_path : str
            location where result will be saved
        max_chi2 : float, optional
            If given only tracks with chi2 < max_chi2 are saved
        max_delta_x : float
            maximum distance of hit to propagated pixel track to be considered for new fit
        n_planes_required : list of 2 int
            number of planes for x and y dimension which is required to have a hit. If fewer hits are collected, the fit is not performed.
            for y dimension there only were 3 planes
    OUTPUT:
        nothing, file is written to disk
    '''

    scifi_res = 0.01 # 100 um

    # z positions in cm from survey
    pixel_global_z = 1008.33  # cm
    scifi_global_z = 1463.46  # cm
    goliath_global_z_center = 1240.67  # cm from EDMS 1836813 and EDMS 1825777

    field_length = 368.73 + 2.5

    distance_pix_scifi = scifi_global_z - pixel_global_z  # 455.11 cm
    magnetic_field_center = goliath_global_z_center - pixel_global_z

    magnetic_field_start_z = magnetic_field_center - field_length/2
    magnetic_field_end_z = magnetic_field_center + field_length/2
    magnet_to_scifi = distance_pix_scifi - magnetic_field_end_z

    event_numbers = np.unique(matched_tracks["event_number"])
    tracks = []
    n_tracks = 0
    n_tries = 0
    
    i=0
    # TODO: also consider last event
    for event_number in tqdm(event_numbers[:-1]):
        track_indices_x = {}
        track_indices_y = {}

        while matched_tracks[i]["event_number"] == event_number :
            # determine which dimension SciFi hit has
            y_dim = False
            if np.isnan(matched_tracks[i]["delta_x"]):
                y_dim = True
            # get all important track information for fit and output table
            x_slope = matched_tracks[i]["x_slope"]
            x = matched_tracks[i]["x"]
            x_error = matched_tracks[i]["x_error"]
            x_slope_error = matched_tracks[i]["x_slope_error"]

            y_slope = matched_tracks[i]["y_slope"]
            y = matched_tracks[i]["y"]
            y_error = matched_tracks[i]["y_error"]
            y_slope_error = matched_tracks[i]["y_slope_error"]

            x_after_field = matched_tracks[i]["x_new"]
            x_after_field_slope = matched_tracks[i]["x_slope_new"]
            x_after_field_error = matched_tracks[i]["x_new_error"] 
            x_after_field_slope_error = matched_tracks[i]["x_slope_new_error"]

            actual_plane = matched_tracks[i]["plane"]
            spill = matched_tracks[i]["spill"]
            timestamp = matched_tracks[i]["timestamp"]
            best_track_distance = max_delta_x
            best_track_index_x = None
            best_track_index_y = None
            # use already calculated distance between pixel track and scifi cluster to find closest hit. Here max_delta_x is used to ensure maximum track - hit distance
            # TODO: use best_match column instead ?
            while matched_tracks[i]["plane"] == actual_plane :
                if matched_tracks[i]["event_number"] != event_number:
                    break
                if y_dim == True:
                    if np.abs(matched_tracks[i]["delta_y"]) < best_track_distance:
                        best_track_index_y = i
                        best_track_distance = np.abs(matched_tracks[i]["delta_y"])
                else:
                    if np.abs(matched_tracks[i]["delta_x"]) < best_track_distance:
                        best_track_index_x = i
                        best_track_distance = np.abs(matched_tracks[i]["delta_x"])
                i+=1
            # only if hit is close enough it is collected for the fit
            if best_track_index_x is not None:
                track_indices_x[actual_plane] = best_track_index_x
            if best_track_index_y is not None:
                track_indices_y[actual_plane] = best_track_index_y
        
        # end of event, if there are at least 3 points, perform fit
        if (len(track_indices_x) >= n_planes_required[0]) and (len(track_indices_y) >= n_planes_required[1]):
            n_tries +=1
            track = np.full(fill_value = np.nan, shape = (1,), dtype = combined_tracks_dtype )
            n_planes_x = 0
            n_planes_y = 0

            track["x"] = x
            track["x_slope"] = x_slope
            track["x_error"] = x_error
            track["x_slope_error"] = x_slope_error
            track["x_deflect"] = x_after_field
            track["x_slope_deflect"] = x_after_field_slope
            track["x_project"] = x_after_field + x_after_field_slope * magnet_to_scifi
            track["x_project_slope"] = x_after_field_slope
            track["x_project_error"] = np.sqrt(np.square(x_after_field_error + np.square(x_after_field_slope_error * magnet_to_scifi)))
            track["x_project_slope_error"] = x_after_field_slope_error
            track["z"] = 0
            track["z_deflect"] = magnetic_field_end_z

            z_vals_x = [0.]
            x_vals = [x_after_field]
            sigmas_x = [x_after_field_error]
            # collect hits and z positions for x dim fit 
            for plane in track_indices_x.keys():
                track["x%i" %plane] = matched_tracks[track_indices_x[plane]]["x_sc"] 
                track["z%ix" %plane] = matched_tracks[track_indices_x[plane]]["z_sc"]
                z_vals_x.append(track["z%ix" %plane][0] + magnet_to_scifi)
                x_vals.append(track["x%i" %plane][0])
                track["delta_x_%i" %plane] = matched_tracks[track_indices_x[plane]]["delta_x"]
                track["n_hits_%i" %plane] = matched_tracks[track_indices_x[plane]]["n_plane_hits"]
                sigmas_x.append(scifi_res)
                n_planes_x +=1
            # perform x fit
            zdata_x = np.array(z_vals_x)
            xdata = np.array(x_vals)
            popt_x, pcov_x = curve_fit(line, xdata = zdata_x, ydata =xdata, p0 = [x_after_field_slope, x_after_field], sigma = np.array(sigmas_x), absolute_sigma = True)

            # repeat everything for y dimension
            track["y"] = y
            track["y_slope"] = y_slope
            track["y_error"] = y_error
            track["y_slope_error"] = y_slope_error
            y_vals = [y + y_slope * magnetic_field_start_z]
            z_vals_y = [0.]
            sigmas_y = [y_error]
            
            for y_plane in track_indices_y.keys():
                track["y%i" %y_plane] = matched_tracks[track_indices_y[y_plane]]["y_sc"] 
                track["z%iy" %y_plane] = matched_tracks[track_indices_y[y_plane]]["z_sc"]
                z_vals_y.append(track["z%iy" %y_plane][0] + magnet_to_scifi)
                y_vals.append(track["y%i" %y_plane][0])
                track["delta_y_%i" %y_plane] = matched_tracks[track_indices_y[y_plane]]["delta_y"]
                track["n_hits_%i" %y_plane] = matched_tracks[track_indices_y[y_plane]]["n_plane_hits"]
                sigmas_y.append(scifi_res)
                n_planes_y +=1
            
            ydata = np.array(y_vals)
            zdata_y = np.array(z_vals_y)
            popt_y, pcov_y = curve_fit(line, xdata = zdata_y, ydata =ydata, p0 = [y_slope, ydata[0]], sigma = np.array(sigmas_y), absolute_sigma = True)

            # for chi2 calculation x and y fit results are combined.
            popt = np.concatenate((popt_x, popt_y))
            pcov = np.concatenate((pcov_x, pcov_y))

            x_fit_chi2 = np.array([line(z, popt_x[0], popt_x[1]) for z in zdata_x])
            y_fit_chi2 = np.array([line(z, popt_y[0], popt_y[1]) for z in zdata_y])

            chi2 = red_chisquare(np.concatenate((xdata,ydata)), np.concatenate((x_fit_chi2,y_fit_chi2)), np.concatenate((sigmas_x, sigmas_y)), popt, use_empty_bins = True)
            # cut on track quality
            if max_chi2 is not None:
                # only save track if fit quality is high enough 
                if (chi2 > max_chi2):
                    continue

            # write new track
            track["track_y_new"] = popt_y[1]
            track["track_y_slope_new"] = popt_y[0]
            track["n_planes"] = n_planes_x + n_planes_y
            track["n_planes_x"] = n_planes_x
            track["n_planes_y"] =  n_planes_y
            track["event_number"] = event_number
            track["timestamp"] = timestamp
            track["track_x_new"] = popt_x[1]
            track["track_x_slope_new"] = popt_x[0]
            track["track_z_new"] = magnetic_field_end_z
            track["chi2"] = chi2
            track["spill"] = spill
            track["track_x_error_new"] = np.sqrt(pcov_x[1][1])
            track["track_x_slope_error_new"] = np.sqrt(pcov_x[0][0])
            tracks.append(track)
            
            n_tracks +=1

    if len(tracks)>0:
        save_table(np.array(tracks), path = out_path, node_name = "NewTracks")

    logging.info("================================= spill %i =================================" % spill)
    if n_tries > 0:
        logging.info("================ %i fit attempts, %i successfull (%.2f %%) ================" % ( n_tries, n_tracks, n_tracks/n_tries * 100))
    else : 
        logging.info("================ %i fit attempts, %i successfull (%.2f %%) ================" % ( n_tries, n_tracks, n_tries))
    logging.info("=============== %i events without fitted track (%.2f %%) ================" % ((event_numbers.shape[0]- n_tracks), ((event_numbers.shape[0]- n_tracks)/event_numbers.shape[0])*100))
    
    return tracks


def get_momentum(merged_tracks, B, q=1 ,field_length = 206.67):
    ''' 
    use known field and track position as well as scifi tracks to fit the proton momentum.
    Project scifi x track back to end of magnetic field, project pixel track to start of magnetic field, calculate bending radius from both points.
    All coordinates are in cm !!!
    '''

    # z positions in cm from survey
    pixel_global_z = 1008.33  # cm
    scifi_global_z = 1463.46  # cm

    distance_pix_scifi = scifi_global_z - pixel_global_z  # 455.11 cm
    # field_length = 206.67 # cm

    # magnetic_field_start_z = distance_pix_scifi/2 - field_length/2
    # magnetic_field_start_z = (distance_pix_scifi/2 - 24.75 - field_length/2)  # magnet position calculated from residual position
    magnetic_field_start_z = (distance_pix_scifi/2 - field_length/2)
    magnetic_field_end_z = magnetic_field_start_z + field_length
    magnet_to_scifi = (distance_pix_scifi - magnetic_field_end_z )

    print("distance_pix_scifi : : %.4f cm" % distance_pix_scifi)
    print("magnetic_field_start_z : %.4f cm" % magnetic_field_start_z )
    print("magnetic_field_end_z : %.4f cm" % magnetic_field_end_z )
    print("magnet_to_scifi : %.4f cm" % magnet_to_scifi)

    # track projection
    merged_tracks["x_sc_project"] = merged_tracks["x_sc"] - magnet_to_scifi * merged_tracks["x_slope_sc"]
    merged_tracks["x_project"] = merged_tracks["x"] + magnetic_field_start_z * merged_tracks["x_slope"]
    
    merged_tracks["radius_calc"] = radius(magnetic_field_start_z, magnetic_field_end_z, merged_tracks["x_project"], merged_tracks["x_sc_project"], merged_tracks["x_slope"])/100 # input values in cm, result shall be in m
    # merged_tracks["radius_calc"] = - radius2(x_in = merged_tracks["x_project"], x_out = merged_tracks["x_sc_project"], L = field_length)/100
    # radius = momentum * 1e9/2.997e8/B/q * 100  # cm
    merged_tracks["momentum_calc"] = merged_tracks["radius_calc"] * 2.997e8 * B * q / 1e9 #/100 # return momentum in GeV
    # merged_tracks["momentum_calc"] = merged_tracks["radius_calc"] * B * q  # return momentum in GeV

    # results = parallel_fit(merged_tracks["x_project"], merged_tracks["z_sc"], merged_tracks["x_error"]*merged_tracks["z_sc"], radius,)


def get_track_distance_steps(merged_tracks, B=False, momentum=400., q=1):
    '''
    Propagate pixel tracks to SciFi planes. Use measured Goliath Field strengths at x,y = 0 and propagate Track through field in case of run with magnetic field on.
    Different field measurements for x, y values != 0 are not considered since the bin width of measurement entries 
    is 5 cm in all directions and the beam was only 1 cm wide (dy) and center was at x = , y = -1 cm.
    ----------------
    INPUT:
        merged_tracks : numpy struct array
            contains mathed pixel tracks and scifi clusters, one track cluster combination per row
        B : bool
            if true use magnetic field for propagation, if false linear projection
        momentum : float or np. array of float (then needs to be same size as merged_tracks)
            particle momentum in GeV for magnetic field propagation
        q : float
            charge of the particle for magnetic field propagation in elctron charge units
    OUTPUT: 
        results are written in respective fields of merged_tracks array
    
    '''
    # z positions of By measurements in local Goliath coordinate system 0,0 at center
    field_z_default = np.array([3.73,   8.73,  13.73,  18.73,  23.73,  28.73,  33.73,  38.73,  43.73,  48.73,
                                53.73,  58.73,  63.73,  68.73,  73.73,  78.73,  83.73,  88.73,  93.73,  98.73,
                                103.73, 108.73, 113.73, 118.73, 123.73, 128.73, 133.73, 138.73, 143.73, 148.73,
                                153.73, 158.73, 163.73, 168.73, 173.73, 178.73, 183.73, 188.73, 193.73, 198.73,
                                203.73, 208.73, 213.73, 218.73, 223.73, 228.73, 233.73, 238.73, 243.73, 248.73,
                                253.73, 258.73, 263.73, 268.73, 273.73, 278.73, 283.73, 288.73, 293.73, 298.73,
                                303.73, 308.73, 313.73, 318.73, 323.73, 328.73, 333.73, 338.73, 343.73, 348.73,
                                353.73, 358.73, 363.73, 368.73])
    # By field measurements along z in goliath at x,y = 0
    By = np.array([-0.1185646, -0.1472104, -0.1807546, -0.2192168, -0.2633682, -0.3126212,
                   -0.3669946, -0.425686,  -0.4891762, -0.5568198, -0.6279358, -0.7021102,
                   -0.7794526, -0.8581002, -0.9345912, -1.009857,  -1.0804222, -1.1461178,
                   -1.2021938, -1.251542,  -1.2937396, -1.3289748, -1.3579024, -1.3819836,
                   -1.4018306, -1.4179538, -1.4308048, -1.4413368, -1.4498608, -1.4565272,
                   -1.4618054, -1.465877,  -1.469,     -1.4711374, -1.4725578, -1.4732642,
                   -1.4732396, -1.4725664, -1.4711934, -1.4690206, -1.46594,   -1.461857,
                   -1.4565832, -1.4498776, -1.441346,  -1.430814,  -1.4177112, -1.4015442,
                   -1.3816976, -1.3575308, -1.3281134, -1.2925762, -1.2503856, -1.2005098,
                   -1.1426408, -1.077467,  -1.006628,  -0.9311426, -0.8531716, -0.7750776,
                   -0.6987264, -0.624223,  -0.5529024, -0.485227,  -0.4216612, -0.36274,
                   -0.3083242, -0.25918,   -0.215198,  -0.1763882, -0.1425964, -0.1140354,
                   -0.0898272])
    # center measurement points
    field_z_default -= 2.5

    pixel_global_z = 1008.33  # cm
    scifi_global_z = 1463.46  # cm
    goliath_global_z_center = 1240.67  # cm from EDMS 1836813 and EDMS 1825777

    distance_pix_scifi = scifi_global_z - pixel_global_z  # 455.11 cm
    magnetic_field_center = goliath_global_z_center - pixel_global_z

    magnetic_field_start_z = magnetic_field_center - field_z_default.max()/2
    magnetic_field_end_z = magnetic_field_center + field_z_default.max()/2
    magnet_to_scifi = distance_pix_scifi - magnetic_field_end_z

    field_z = field_z_default + magnetic_field_start_z

    if B:
        logging.info("propagate track in Goliath magnetic field")
        merged_tracks["x_new"], merged_tracks["x_new_error"], merged_tracks["x_slope_new"], merged_tracks["x_slope_new_error"], plotx = get_deflection(np.full(fill_value=momentum, shape=merged_tracks.shape),
                                                                                                                                                       merged_tracks["x"] + merged_tracks["x_slope"] *
                                                                                                                                                       magnetic_field_start_z,
                                                                                                                                                       merged_tracks["x_error"],
                                                                                                                                                       merged_tracks["x_slope"],
                                                                                                                                                       # ((merged_tracks["x"] + merged_tracks["x_slope"] * magnetic_field_start_z) - merged_tracks["x"]) / magnetic_field_start_z,
                                                                                                                                                       merged_tracks["x_slope_error"],
                                                                                                                                                       By, field_z, q
                                                                                                                                                       )

        merged_tracks["x_project"] = merged_tracks["x_new"] + merged_tracks["x_slope_new"] * (magnet_to_scifi + merged_tracks["z_sc"])
        merged_tracks["x_project_error"] = np.sqrt(np.square(merged_tracks["x_new_error"]) + np.square(merged_tracks["x_slope_new_error"] * magnet_to_scifi))
        merged_tracks["offset_after"] = merged_tracks["x_slope_new"] * (magnet_to_scifi + merged_tracks["z_sc"])

        plot_magnet_example(plotx, merged_tracks, magnetic_field_start_z, merged_tracks["z_sc"] + distance_pix_scifi, field_z,
                            path="/media/niko/big_data/charm_testbeam_july18/scifi_offline/run_2818/matching/find_res/test_field_map_distance.pdf")

    else:
        logging.info("linear projection of pixel tracks to SciFi detector")
        merged_tracks["x_project"] = merged_tracks["x_slope"] * (distance_pix_scifi + merged_tracks["z_sc"]) + merged_tracks["x"]
        merged_tracks["x_project_error"] = np.sqrt(np.square(merged_tracks["x_error"]) + np.square(merged_tracks["x_slope_error"] * distance_pix_scifi))
        merged_tracks["x_slope_new"] = merged_tracks["x_slope"]
        merged_tracks["x_slope_new_error"] = merged_tracks["x_slope_error"]
        merged_tracks["x_new"] = merged_tracks["x_slope"] * magnetic_field_end_z + merged_tracks["x"]
        merged_tracks["x_new_error"] = np.sqrt(np.square(merged_tracks["x_error"]) + np.square(merged_tracks["x_slope_error"] * magnetic_field_end_z))

    merged_tracks["y_project"] = merged_tracks["y_slope"] * (distance_pix_scifi + merged_tracks["z_sc"]) + merged_tracks["y"]

    merged_tracks["delta_x"] = merged_tracks["x_project"] - merged_tracks["x_sc"]
    merged_tracks["delta_y"] = merged_tracks["y_project"] - merged_tracks["y_sc"]
    merged_tracks["delta_r"] = np.sqrt(np.square(merged_tracks["delta_x"]) + np.square(merged_tracks["delta_y"]))
    merged_tracks["delta_slope"] = merged_tracks["x_slope"] - merged_tracks["x_slope_new"]


def get_track_distance(merged_tracks, B=0., momentum=400., print_debug_info=True, q=1):
    '''
    Calculate distance of every pixel track to every scifi hit. 
    Uses a coordinate system where z = 0 ist the origin of pixel tracks which is aligned to the pixel detector first plane. SciFi positions are calculated relative to this from alignment survey values, 
    see https://edms.cern.ch/ui/file/2010858/1/2018.07.27_H4_CHARM.pdf
    Goliath magnetic field maximum is By = -1.5 T. This means that the deflection for protons is towards positive x (in FairShip coordinate system), see "right-hand rule".
    -------------
    INPUT:
        merged_tracks : structured numpy.array
            contains merged data, single row for every track - hit combination
        B : float
            magnetic field strength in y direction
        q : float
            particle electric charge
        momentum : float
            momentum of particle, in GeV
    OUTPUT:
        None, results are saved in the respective columns in merged_tracks ("x_project", "y_project", "delta_x" , "delta_y")
    '''

    # z positions in cm from survey
    pixel_global_z = 1008.33  # cm
    scifi_global_z = 1463.46  # cm
    goliath_global_z_center = 1240.67 # cm from EDMS 1836813 and EDMS 1825777

    distance_pix_scifi = scifi_global_z - pixel_global_z  # 455.11 cm
    field_length = 366.23 # 206.67

    # magnetic_field_start_z = distance_pix_scifi/2 - field_length/2
    magnetic_field_center = goliath_global_z_center - pixel_global_z
    magnetic_field_start_z = magnetic_field_center - field_length/2
    magnetic_field_end_z = magnetic_field_center + field_length/2
    magnet_to_scifi = distance_pix_scifi - magnetic_field_end_z

    if B == 0.:
        merged_tracks["x_project"] = merged_tracks["x_slope"] * (distance_pix_scifi + merged_tracks["z_sc"]) + merged_tracks["x"]
        merged_tracks["x_project_error"] = np.sqrt(np.square(merged_tracks["x_error"]) + np.square(merged_tracks["x_slope_error"] * distance_pix_scifi))
        merged_tracks["x_slope_new"] = merged_tracks["x_slope"]
        merged_tracks["x_slope_new_error"] = merged_tracks["x_slope_error"]
        merged_tracks["x_new"] = merged_tracks["x_slope"] * magnetic_field_end_z + merged_tracks["x"]
        merged_tracks["x_new_error"] = np.sqrt(np.square(merged_tracks["x_error"]) + np.square(merged_tracks["x_slope_error"] * magnetic_field_end_z))
    else:
        '''
        Goliath magnetic field is By = -1.5 T. This means that the deflection for protons is towards positive x (in FairShip coordinate system), see "right-hand rule"
        '''
        logging.info("field starts at z = %.3f" % magnetic_field_start_z)
        logging.info("field ends at z =%.3f " % magnetic_field_end_z)
        logging.info("scifi plane 1 at z = %.3f" % distance_pix_scifi)
        logging.info("distance magnet scifi = %.3f" % magnet_to_scifi)

        gamma = momentum / 0.938  # momentum divided by proton mass
        beta = np.sqrt(1-(1/gamma)**2)
        # radius = 3.3 * momentum / B * 100 # cm
        # r = gamma * beta * m / q / B  , with gamma * beta * m = 400 GeV/c^2
        radius = momentum * 1e9/2.997e8/B/q * 100  # cm
        radius_error = 0.
        logging.info("deflection radius calculated to %.3f cm" % radius)

        # Calculate cirle center
        slope_angle = np.arctan(merged_tracks["x_slope"]) + np.pi/2
        slope_angle_error = 1/(np.square(merged_tracks["x_slope"])+1) *  merged_tracks["x_slope_error"]
        z_center = magnetic_field_start_z - np.cos(slope_angle) * radius
        x_center = merged_tracks["x"] + (merged_tracks["x_slope"] * magnetic_field_start_z) - (np.sin(slope_angle) * radius)

        # x position after magnetic field
        new_x = circle(magnetic_field_end_z, z_center, x_center, radius)
        error_part_2 = slope_angle_error * (radius * np.sin(slope_angle)/(np.square(radius) - 2*(radius * np.cos(slope_angle) - magnetic_field_start_z + magnetic_field_end_z)) - radius * np.cos(slope_angle))
        new_x_error = np.sqrt(np.square(merged_tracks["x_error"]) + np.square(error_part_2))
        # New track angle after magnetic field
        normal_slope = 1/((x_center - new_x) / (magnetic_field_end_z - z_center))  # * np.sign(B) * -1
        normal_slope_error = (magnetic_field_end_z - z_center)/np.square(x_center - new_x) * new_x_error
        new_angle = np.arctan(normal_slope)
        merged_tracks["x_new"] = new_x
        merged_tracks["x_new_error"] = new_x_error
        merged_tracks["x_project"] = new_x + normal_slope * (magnet_to_scifi + merged_tracks["z_sc"])
        
        # merged_tracks["x_project_error"] = np.sqrt(np.square(merged_tracks["x_error"]) + np.square(normal_slope_error * magnet_to_scifi))
        merged_tracks["x_project_error"] = np.sqrt(np.square(new_x_error) + np.square(slope_angle_error * magnet_to_scifi))


        # merged_tracks["x_project"] = merged_tracks["x_slope"] * (distance_pix_scifi + merged_tracks["z_sc"]) + merged_tracks["x"] + 0.489 + 0.445 * np.arctan(merged_tracks["x_slope"])-np.pi/2 * 1e3
        merged_tracks["offset_after"] = normal_slope * (magnet_to_scifi + merged_tracks["z_sc"])
        merged_tracks["x_slope_new"] = normal_slope
        merged_tracks["x_slope_new_error"] = slope_angle_error
        merged_tracks["delta_slope"] = merged_tracks["x_slope"] - merged_tracks["x_slope_new"]

        if print_debug_info:
            print("distance pix scifi check : %s" % (magnetic_field_end_z + magnet_to_scifi))
            print("track z : %s" % merged_tracks["z"][1])
            print("old x : %s" % merged_tracks["x"][1])
            print("old slope : %s" % merged_tracks["x_slope"][1])
            print("old angle : %s" % slope_angle[1])
            print("x at field start : %s" % (merged_tracks["x"][1] + (merged_tracks["x_slope"][1] * magnetic_field_start_z)))
            print("new circle x = %.3f" % (x_center[1]))
            print("new circle z = %.3f" % (z_center[1] - magnetic_field_start_z))
            print("new x  =%s" % new_x[1])
            print("new x error = ", new_x_error[1])
            print("normal slope = %s" % normal_slope[1])
            print("normal slope error = %s" % normal_slope_error[1])
            print("slope angle error = ", slope_angle_error[1])
            print("new angle = %s " % new_angle[1])
            print("in field offset = %s" % (np.abs(new_x[1]) - np.abs(merged_tracks["x"][1])))
            print("after field offset = %s" % (normal_slope[1] * (magnet_to_scifi + merged_tracks["z_sc"][1])))
            print("z scifi plane z : %s" % merged_tracks["z_sc"][1])
            print("new total project = %s " % merged_tracks["x_project"][1])

            with PdfPages("/media/niko/big_data/charm_testbeam_july18/scifi_offline/run_2815/matching/field_deflection_plots.pdf") as output_pdf:
                plot_magnetic_deflection(x_old=merged_tracks["x"], x_new=new_x, x_proj=merged_tracks["x_project"], z_0=magnetic_field_start_z, z_1=magnetic_field_end_z,
                                         z_2=distance_pix_scifi + merged_tracks["z_sc"], old_slope=merged_tracks["x_slope"], new_slope=merged_tracks["x_slope_new"],
                                         z_center=z_center, x_center=x_center, radius=radius, output_pdf=output_pdf)

    merged_tracks["y_project"] = merged_tracks["y_slope"] * (distance_pix_scifi + merged_tracks["z_sc"]) + merged_tracks["y"]
    merged_tracks["delta_x"] = merged_tracks["x_project"] - merged_tracks["x_sc"]
    merged_tracks["delta_y"] = merged_tracks["y_project"] - merged_tracks["y_sc"]
    merged_tracks["delta_r"] = np.sqrt(np.square(merged_tracks["delta_x"]) + np.square(merged_tracks["delta_y"]))

    # merged_tracks2 = merged_tracks.copy()
    # x_distances = 0  # np.sort(merged_tracks, order=["event_number", "pix_trackID", "delta_x"])
    # y_distances = 0  # np.sort(merged_tracks2, order=["event_number", "pix_trackID", "delta_y"])

    # return x_distances, y_distances


def find_momentum(merged_tracks):

    # z positions in cm from survey
    pixel_global_z = 1008.33  # cm
    scifi_global_z = 1463.46  # cm
    # goliath is 4.5 m long, z position of magnetic field center not know at the moment and "fitted" from residual peak position -> center of magnetic field is 24.75 cm closer to pixel than to scifi

    distance_pix_scifi = scifi_global_z - pixel_global_z  # 455.11 cm
    field_length = 206.67

    # magnetic_field_start_z = distance_pix_scifi/2 - field_length/2
    magnetic_field_start_z = distance_pix_scifi/2 - 24.75 - field_length/2  # magnet position calculated from residual position
    magnetic_field_end_z = magnetic_field_start_z + field_length
    magnet_to_scifi = distance_pix_scifi - magnetic_field_end_z

    radii = []
    for p in np.arange(380,420,0.1):
        radii.append(p*1e9/1.5/2.997e8)
    plane_tracks = merged_tracks[merged_tracks["plane"]==1]
    for track in plane_tracks:
        momenta = []
        for i, radius in enumerate(radii):
            new_x, _, normal_slope, _ = get_deflection(radius, track["x"], track["x_error"], track["x_slope"], track["x_slope_error"])
            momenta.append([np.abs(new_x + normal_slope * (magnet_to_scifi + merged_tracks["z_sc"]) - track["x_sc"]),radius])

        momenta = np.array(momenta)
        # momenta.sort()
        print(momenta.shape)
        print(momenta)



@njit()
def _mark_pos_matches(tracks_in, tot_limit=251):
    '''
    Find closest scifi cluster for every pixel track.
    Loop over all planes and then over all tracks- hit combinations and check for distance. If distance is smaller than preceeding one take this.
    Function relies on array sorted first by plane then by event_number.
    -------------------
    INPUT:
        tracks_in : structured numpy.array
            array contains pixel tracks and scifi hits. Every track - hit combination has one line
    OUTPUT:
        match_indices : list
            list with indices of best matching clusters. Can be used to index numpy array
    '''

    i = 0
    best_match_index = 0
    match_indices = []
    track_id = tracks_in[0]["pix_trackID"]
    event_number = tracks_in[0]["event_number"]
    min_distance = 1e6
    next_track = False
    j = 0

    for plane in range(1, 9):
        while (tracks_in[j]["plane"] == plane) and (j < tracks_in.shape[0]-1):
            while (tracks_in[j]["pix_trackID"] == track_id) and (tracks_in[j]["event_number"] == event_number):
                if np.abs(tracks_in[j]["delta_x"]) < min_distance:
                    best_match_index = j
                    min_distance = np.abs(tracks_in[j]["delta_x"])
                elif np.abs(tracks_in[j]["delta_y"]) < min_distance:
                    best_match_index = j
                    min_distance = np.abs(tracks_in[j]["delta_y"])
                if j == tracks_in.shape[0]-1:
                    break
                j += 1
            match_indices.append(best_match_index)
            event_number = tracks_in[j]["event_number"]
            track_id = tracks_in[j]["pix_trackID"]
            min_distance = 1e6

    return match_indices


def find_best_pos_match(tracks_in, tot_limit):
    '''
    Wrap matching function and do some preparation.
    Sort input array for plane, event_number and pixel_trackID
    and after matching bring it back to normal sorting
    -------
    INPUT:
        tracks_in : structured numpy.array
            contains merged tracks with already calculated delta_x and delta_y
    '''
    logging.info("finding best matches")
    tracks_in.sort(order=["plane", "event_number", "pix_trackID"])
    best_matches = _mark_pos_matches(tracks_in, tot_limit)
    tracks_in["best_match"][best_matches] = 1
    tracks_in.sort(order=["event_number", "plane"])


#@njit()
def _merge_arrays(common_events, merged_tracks, pixel_tracks_in, unique_scifi_counts, unique_pix_counts, scifi_in, pixel_indices, scifi_indices, scifi_tracks = False):
    '''
    Numba function for merging events. Create one row for each track - hit combination.
    In case of tracks (scifi_tracks = True) create on row for each track track combination.
    ATTENTION : scifi TRACKS and CLUSTERS have different dtypes!!!!!!!!!
    '''
    j = 0
    for i, event in enumerate(common_events):
        for scifi_hit_count in range(unique_scifi_counts[i]):
            for pixel_track_count in range(unique_pix_counts[i]):

                merged_tracks[j]["event_number"] = event

                merged_tracks[j]["n_pix_tracks"] = unique_pix_counts[i]
                merged_tracks[j]["timestamp"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["timestamp"]*25
                merged_tracks[j]["x"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["x"]
                merged_tracks[j]["y"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["y"]
                merged_tracks[j]["z"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["z"]
                merged_tracks[j]["x_error"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["x_error"]
                merged_tracks[j]["y_error"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["y_error"]
                merged_tracks[j]["x_slope"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["x_slope"]
                merged_tracks[j]["y_slope"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["y_slope"]
                merged_tracks[j]["x_slope_error"] = pixel_tracks_in[pixel_indices[i] + pixel_track_count]["x_slope_error"]
                merged_tracks[j]["y_slope_error"] = pixel_tracks_in[pixel_indices[i] + pixel_track_count]["y_slope_error"]
                merged_tracks[j]["pix_trackID"] = pixel_tracks_in[pixel_indices[i] + pixel_track_count]["trackID"]
                merged_tracks[j]["pix_chi2"] = pixel_tracks_in[pixel_indices[i] + pixel_track_count]["covariance"]
                merged_tracks[j]["spill"] = pixel_tracks_in[pixel_indices[i]+pixel_track_count]["spill"]

                merged_tracks[j]["x_sc"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x"]
                merged_tracks[j]["y_sc"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y"]
                merged_tracks[j]["z_sc"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z"]
                merged_tracks[j]["scifi_timestamp"] = scifi_in[scifi_indices[i] + scifi_hit_count]["trigger_shift"]

                if scifi_tracks:
                    merged_tracks[j]["sc_trackID"] = scifi_in[scifi_indices[i] + scifi_hit_count]["track_ID"]

                    merged_tracks[j]["x_sc_error"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x_error"]
                    merged_tracks[j]["y_sc_error"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y_error"]
                    merged_tracks[j]["x_slope_sc"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x_slope"]
                    merged_tracks[j]["y_slope_sc"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y_slope"]
                    merged_tracks[j]["x_slope_sc_error"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x_slope_error"]
                    merged_tracks[j]["y_slope_sc_error"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y_slope_error"]

                    merged_tracks[j]["sc_tot_1"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot_1"]
                    merged_tracks[j]["sc_tot_2"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot_2"]
                    merged_tracks[j]["sc_tot_3"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot_3"]
                    merged_tracks[j]["sc_tot_4"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot_4"]
                    merged_tracks[j]["sc_tot_5"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot_5"]
                    merged_tracks[j]["sc_tot_7"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot_7"]
                    merged_tracks[j]["sc_tot_8"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot_8"]

                    merged_tracks[j]["sc_cluster_size_1"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size_1"]
                    merged_tracks[j]["sc_cluster_size_2"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size_2"]
                    merged_tracks[j]["sc_cluster_size_3"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size_3"]
                    merged_tracks[j]["sc_cluster_size_4"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size_4"]
                    merged_tracks[j]["sc_cluster_size_5"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size_5"]
                    merged_tracks[j]["sc_cluster_size_7"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size_7"]
                    merged_tracks[j]["sc_cluster_size_8"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size_8"]

                    merged_tracks[j]["sc_hit_1_used"] = scifi_in[scifi_indices[i] + scifi_hit_count]["hit_1_used"]
                    merged_tracks[j]["sc_hit_2_used"] = scifi_in[scifi_indices[i] + scifi_hit_count]["hit_2_used"]
                    merged_tracks[j]["sc_hit_3_used"] = scifi_in[scifi_indices[i] + scifi_hit_count]["hit_3_used"]
                    merged_tracks[j]["sc_hit_4_used"] = scifi_in[scifi_indices[i] + scifi_hit_count]["hit_4_used"]
                    merged_tracks[j]["sc_hit_5_used"] = scifi_in[scifi_indices[i] + scifi_hit_count]["hit_5_used"]
                    merged_tracks[j]["sc_hit_7_used"] = scifi_in[scifi_indices[i] + scifi_hit_count]["hit_7_used"]
                    merged_tracks[j]["sc_hit_8_used"] = scifi_in[scifi_indices[i] + scifi_hit_count]["hit_8_used"]

                else:
                    merged_tracks[j]["plane"] = scifi_in[scifi_indices[i] + scifi_hit_count]["plane"]
                    merged_tracks[j]["scifi_tot"] = scifi_in[scifi_indices[i] + scifi_hit_count]["tot"]
                    merged_tracks[j]["scifi_cluster_size"] = scifi_in[scifi_indices[i] + scifi_hit_count]["cluster_size"]

                # fill x,y,z coordinates per plane
                merged_tracks[j]["x1"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x1"]
                merged_tracks[j]["y1"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y1"]
                merged_tracks[j]["z1x"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z1x"]
                merged_tracks[j]["z1y"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z1y"]

                merged_tracks[j]["x2"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x2"]
                merged_tracks[j]["y2"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y2"]
                merged_tracks[j]["z2x"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z2x"]
                merged_tracks[j]["z2y"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z2y"]

                merged_tracks[j]["x3"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x3"]
                merged_tracks[j]["y3"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y3"]
                merged_tracks[j]["z3x"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z3x"]
                merged_tracks[j]["z3y"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z3y"]

                merged_tracks[j]["x4"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x4"]
                merged_tracks[j]["y4"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y4"]
                merged_tracks[j]["z4x"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z4x"]
                merged_tracks[j]["z4y"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z4y"]

                merged_tracks[j]["x5"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x5"]
                merged_tracks[j]["y5"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y5"]
                merged_tracks[j]["z5x"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z5x"]
                merged_tracks[j]["z5y"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z5y"]

                merged_tracks[j]["x7"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x7"]
                merged_tracks[j]["y7"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y7"]
                merged_tracks[j]["z7x"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z7x"]
                merged_tracks[j]["z7y"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z7y"]

                merged_tracks[j]["x8"] = scifi_in[scifi_indices[i] + scifi_hit_count]["x8"]
                merged_tracks[j]["y8"] = scifi_in[scifi_indices[i] + scifi_hit_count]["y8"]
                merged_tracks[j]["z8x"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z8x"]
                merged_tracks[j]["z8y"] = scifi_in[scifi_indices[i] + scifi_hit_count]["z8y"]
                merged_tracks[j]["n_plane_hits"] = scifi_in[scifi_indices[i] + scifi_hit_count]["n_plane_hits"]

                j += 1


def merge_data_by_event_number(scifi_clusters_in, pixel_tracks_in, scifi_tracks = False):
    '''
    Build new table with scifi and pixel tracks.
    Timestamps are saved in 1 ns units.
    This function essentially wraps the numba function for speed up
    ----------------
    INPUT:
        scifi_clusters_in: structured numpy.array
            array with tracks of matched scifi events
        pixel_tracks_in: structured numpy.array
            pixel tracks of matched events.
    OUTPUT:
        merged_tracks: structured numpy.array
            array with pixel and scifi tracks for every matched event
    '''
    common_events, scifi_indices, pixel_indices = np.intersect1d(scifi_clusters_in["event_number"], pixel_tracks_in["event_number"], return_indices=True)

    # find subdetector events in common events
    pix_event_mask = np.isin(pixel_tracks_in["event_number"], common_events)
    scifi_event_mask = np.isin(scifi_clusters_in["event_number"], common_events)

    # use found events to mask input tracks files
    _, _, unique_pix_counts = np.unique(pixel_tracks_in["event_number"][np.nonzero(pix_event_mask)], return_index=True, return_counts=True)
    _, _, unique_scifi_counts = np.unique(scifi_clusters_in["event_number"][np.nonzero(scifi_event_mask)], return_index=True, return_counts=True)

    # allocate new array of size n_pix_tracks * n_scifi_tracks
    if scifi_tracks:
        dtype = merged_tracks_dtype
    else : 
        dtype = merged_clusters_dtype
    
    merged_tracks = np.full(fill_value=-1, shape=(unique_pix_counts * unique_scifi_counts).sum(), dtype=dtype)
    # do the merging in numba loop
    _merge_arrays(common_events=common_events, merged_tracks=merged_tracks, pixel_tracks_in=pixel_tracks_in, unique_scifi_counts=unique_scifi_counts,
                  unique_pix_counts=unique_pix_counts, scifi_in=scifi_clusters_in, pixel_indices=pixel_indices, scifi_indices=scifi_indices, scifi_tracks = scifi_tracks)

    # initialize new match column to 0 before matching
    merged_tracks["best_match"] = 0

    return merged_tracks[merged_tracks["event_number"] > -1]


def match_scifi_ts(scifi_in_path, cluster_table_in, spill, cycleID, limits=[1000], plot=True):
    '''
    Match timestamps of scifi with pixel on per spill basis.
    Best result is achieved if looking at the difference of event timestamp to preceeeding event timestamp and comparing these for both detecotrs.

    ----------
    INPUT:
    scifi_in_path: string
            path to scifi timestamp file
    cluster_table_in: string
            path to pixel cluster file
    spill: int
            number of pixel spill to match
    cycleID: int
            SHiP spillcounter to identify spills in scifi data
    limits: iterable of ints
        maximim difference of differences. if more than one comparison is performed for every limit. Outputfile is overwritten
    plot: bool
        if true plots are created
    '''

    # Run 2863 - CHARM0
    # s1f27ef8d -> spill 522710925 -> spill 27

    logging.info("matching spill %s" % spill)

    out_folder_path = os.path.dirname(os.path.dirname(os.path.normpath(scifi_in_path))) + "/matching/"
    if not os.path.exists(out_folder_path):
        os.makedirs(out_folder_path)
    matched_clusters_out_path = out_folder_path + "pix_clusters_spill_%s_scifi_matched.h5" % spill

    cluster_table = cluster_table_in[cluster_table_in["spill"] == spill]

    scifi_in = load_table(in_file=scifi_in_path, spill=cycleID, table_name="Clusters")
    # need unique timestamps and delta timestamps for matching
    unique_scifi = get_unique_deltas(array_in=scifi_in, ts_limit=5.5e9)
    if unique_scifi.shape[0] == 0:
        return -1

    # get unique pixel events
    unique_clusters = get_unique_events(cluster_table, "event_number")
    # calculate difference of event timestamp to preceeding one
    unique_clusters = get_timestamp_diff(unique_clusters)

    for delta_t in limits:
        common_events_in = np.full(fill_value=-1, shape=scifi_in.shape[0],
                                   dtype=[("scifi_event", np.int64),
                                          ("pixel_event", np.int64),
                                          ("scifi_trigger_shift", np.int64),
                                          ("pixel_ts", np.int64),
                                          ("delta_scifi_trigger_shift", np.int64),
                                          ("delta_pixel_ts", np.int64),
                                          ("delta_t", np.int64),
                                          ("matched_event_number", np.int64)
                                          ]
                                   )
        logging.info("checking with delta t of %s" % delta_t)

        # compare scifi differences to pixel differences
        unmatched_scifi = np.full(fill_value=-1, shape=unique_scifi.shape, dtype=unique_scifi.dtype)
        common_events_out, unmatched_scifi = find_closest_delta_t(
            cluster_table=unique_clusters, scifi_in=unique_scifi, common_in=common_events_in, unmatched_scifi=unmatched_scifi, delta_t=delta_t)
        if unmatched_scifi.shape[0] > 0:
            logging.warning("could not match %i SciFi events" % unmatched_scifi.shape[0])

        if common_events_out.shape[0] == 0:
            common_events_out = np.full(fill_value=np.nan, shape=(1,), dtype=common_events_out.dtype)
            warnings.warn("no matching events in spill %s - continue with next spill" % spill)
            save_table(path=out_folder_path + "matched_spill_%s_raw.h5" % spill, array_in=common_events_out, node_name="MatchedEvents")
            return -1
        if plot:
            pdf_path = out_folder_path + "spill_%s_ts_distances.pdf" % spill
            with PdfPages(pdf_path) as output_pdf:

                plot_xy(common_events_out["scifi_event"], common_events_out["pixel_event"], output_pdf=output_pdf,
                        xlim=None, xlabel="scifi event", ylim=None, ylabel="pixel event",
                        title="Run 2793: scifi pixel event correlation - %s entries" % common_events_out.shape[0],
                        legend="events with $\Delta t$ < %s ns" % delta_t, output_png=pdf_path[:-4] + "matched_events_correlation.png")

                diffs = np.diff(common_events_out["scifi_event"])
                missing_event_mask = diffs > 1

                plot_xy(common_events_out[:-1]["scifi_event"][missing_event_mask], common_events_out[:-1]["pixel_event"][missing_event_mask], output_pdf=output_pdf,
                        title="Missing events in pixel - %s entries" % common_events_out[:-1]["scifi_event"][missing_event_mask].shape[0],
                        output_png=pdf_path[:-4] + "missing_events_correlation.png")

                plot_xy(common_events_out["delta_t"], common_events_out["pixel_ts"]*1e-9, output_pdf=output_pdf,
                        xlim=[-(delta_t*1.05), delta_t*1.05], title="delta_t vs. timestamp",
                        xlabel="$\Delta$ t [ns]", ylabel="pixel timestamp [s]", markersize=1)

                plot_xy(common_events_out["pixel_ts"]*1e-9, common_events_out["delta_t"], output_pdf=output_pdf, xlim=None,
                        ylim=[-1000, 1000], title="s%s: time between pixel and scifi timestamp" % cycleID, ylabel="$\Delta$ t [ns]",
                        xlabel="pixel timestamp [s]",
                        markersize=1, linfit=True, legend="$\Delta t$",
                        fit_data=[common_events_out["pixel_ts"]*1e-9,
                                  common_events_out["delta_t"]
                                  ],
                        output_png=pdf_path[:-4] + "_delta_t_fit.png")
                # plot_xy_2axis(x=common_events_out["pixel_ts"]*1e-9, y=common_events_out["delta_t"],
                #                 array_in=cluster_table["trigger_time_stamp"*25*1e-9, bins=50,
                #               output_pdf=output_pdf, xlim=[0, 5], ylim=[-100, 100],
                #               title="s%s: correlation between delta t and occupancy" % cycleID, ylabel="$\Delta$ t [ns]", y2label="nhits",
                #               xlabel="pixel timestamp [s]", markersize=1, legend="$\Delta t$",
                #               linfit=True,
                #               fit_data=[common_events_out["pixel_ts"]*1e-9,
                #                         common_events_out["delta_t"]
                #                         ],
                #               output_png=pdf_path[:-4] + "_delta_t_hist_and_fit.png")

                # n_hits_hist, n_hits_edges = np.histogram(cluster_table["trigger_time_stamp"]*25*1e-9, bins=100)
                # y_vals = diffs[missing_event_mask]  # np.ones(shape = common_events_out[:-1]["pixel_ts"][missing_event_mask].shape)
                # plot_xy_2axis(x=common_events_out[:-1]["scifi_trigger_shift"][missing_event_mask]*1e-9, y=y_vals,
                #               array_in=cluster_table["trigger_time_stamp"]*25*1e-9, bins=100,
                #               output_pdf=output_pdf, xlim=[0, 5], ylim=[-1, 5], y2lim=[0, n_hits_hist.max()*1.05],
                #               title="s%s: missing events - %s entries" % (cycleID, common_events_out[:-1]["scifi_event"][missing_event_mask].shape[0]),
                #               ylabel="$\Delta$ evnum", y2label="nhits",
                #               xlabel="pixel timestamp [s]", markersize=2, legend="$\Delta$ evnum", legend2="nhits per timestamp",
                #               linfit=False,
                #               output_png=pdf_path[:-4] + "_delta_t_hist_missing_evts.png")

                plot_1d_hist(common_events_out["delta_t"], output_pdf=output_pdf, bins=np.arange(-delta_t, delta_t, 25),
                             title="s%s: delta t histogram - %s entries" % (cycleID, common_events_out["delta_t"].shape[0]),
                             xlabel="$\Delta$ t [ns]", output_png=pdf_path[:-4] + "_delta_t_hist.png")

            logging.info("saved plots at %s" % pdf_path)
            # check if pixel events are unique in output_array, if not matching output is useless.
        _, counts = np.unique(common_events_out["pixel_event"], return_counts=True)
        if counts[counts > 1].shape[0] > 0:
            warnings.warn("pixel events used multiple times, decrease delta_t")

        logging.info("find matched clusters")

    save_table(array_in=unique_clusters, path=out_folder_path + "pixel_delta_t_spill_%s.h5" % spill, node_name="MergedClusters")

    save_table(path=out_folder_path + "matched_spill_%s_raw.h5" % spill, array_in=common_events_out, node_name="MatchedEvents")

    save_table(array_in=unmatched_scifi, path=out_folder_path + "not_matched_scifi_spill_%s.h5" % spill, node_name="SciFiNot")
    cluster_out = choose_matched_events_pixel(cluster_table, common_events_out[common_events_out["pixel_event"] != -1])
    n_matched_events = np.unique(cluster_out["event_number"]).shape[0]
    if unique_scifi.shape[0] == 0:
        logging.warning("could not match any scifi events")
    else:
        logging.info("matched %s events (= %.2f %% of pixel , %.2f %% of SciFi)" % (n_matched_events,
                                                                                    (n_matched_events/unique_clusters.shape[0])*100.,
                                                                                    (n_matched_events/unique_scifi.shape[0])*100.))

    save_table(array_in=cluster_out[cluster_out["event_number"] != -1], path=matched_clusters_out_path, node_name="MergedClusters")
    return 1


def match_scifi_tracks(scifi_in_path, pixel_tracks_in, matched_events_in, merged_tracks_out_file_path=None, pix_spill=None, magnetic_field=False, momentum = 400., rotate_scifi=True, alignment_file = None, fairhsip_coordinate_system=False, max_scifi_plane_hits=1, max_scifi_cluster_tot=254, scifi_tracks = False):
    '''
    Get matched events and fit pixel track to scifi hits. Look at each plane individually. 
    Only calculations are done here, for plotting call "plot_matched_tracks"
    ----------------------
    INPUT:
        scifi_in_path : string
            path to file with scifi clusters
        pixel_tracks_in: string
            path to pixel tracks file with whole run. Should be only single track events at the moment
        matched_events_in : string
            path to .h5 file with matched event numbers of pixel and scifi data
        merged_tracks_out_file_path : string, optional
            if given the output file will be saved under this path, otherwise it will be saved in the executing directory
        pix_spill : int
            pixel spill number to use
        magnetic_field : bool
            if true use field map for track propagation, if false straight line
        momentum : float
            momentum of the particles to be propagated in magnetic field in GeV
        alignment_file : str
            path to yaml file with alignment values
        rotate_scifi: bool
            If true also apply rotation around z axis to align scifi planes. 
            Uses missing coordinate predicted by pixel track at the moment.
        fairhsip_coordinate_system : bool
            if True, mirror x values to match fairship and scifi coordinate system
        max_scifi_plane_hits : int, optional
            if given only events with n_hits per plane smaller than max_scifi_plane_hits are considerd
        max_scifi_cluster_tot : int, optional
            if given only cluster with smaler tot are considered
        scifi_tracks : bool
            if true match scifi tracks instead of clusters, means different dtype etc..
    '''
    if not os.path.isfile(matched_events_in):
        return
    if merged_tracks_out_file_path is None:
        merged_tracks_out_file_path = scifi_in_path.split(".")[0] + "_pixel_tracks_aligned.h5"

    # scifi planes were numbered 1-8 where plane 6 did not send data. Use dict to store alignment values and dimension (x,y) of actual plane
    # scifi_alignment = {1: 1.9725 - 0.0189, 2: 1.77 - 0.0372, 3: 0.94 - 0.001, 4: 1.15, 5: 1.38 - 0.0148, 7: 2.06 + 0.013, 8: 1.98 - 0.0017}
    scifi_alignment_rot = {1: 1.9725 + 0.0162, 2: 1.77 + 0.029, 3: 0.94 + 0.0017, 4: 1.15 + 0.002, 5: 1.38 - 0.0129, 7: 2.06 + 0.0374, 8: 1.98 + 0.0455}

    # angular_alignment = {1: -0.17, 2: -0.38, 3: 0.048, 4: 0.045, 5: 0.067, 7: -0.189, 8: -0.2699}  # rotation around z, in Rad
    # angular_planes = {1: "x", 2: "u", 3: "v", 4: "y", 5: "y", 7: "u", 8: "x"}
    dimension = {1: "x", 2: "x", 3: "y", 4: "y", 5: "y", 7: "x", 8: "x"}

    # load scifi clusters and pixel tracks
    
    matched_events = load_table(in_file=matched_events_in, table_name="MatchedEvents")
    pixel_in_tracks = load_table(in_file=pixel_tracks_in, spill=pix_spill, unique=False, table_name="Tracks")
    # SciFi and Pixel use mirrored coordinate systems
    if fairhsip_coordinate_system:
        pixel_in_tracks["x"] *= -1
        pixel_in_tracks["x_slope"] *= -1
    pixel_tracks = get_matched_tracks_pixel(pixel_in_tracks, matched_events)

    save_table(array_in=pixel_tracks, path=matched_events_in.split("raw")[0] + "pixel_tracks.h5", node_name="MatchedTracks")
    if scifi_tracks:
        table_name = "Tracks"
    else:
        table_name = "MatchedClusters"

    scifi_clusters = load_table(in_file=scifi_in_path, spill=None, unique=False, table_name=table_name)
        
    # build common table and calculate residuals
    merged_tracks = merge_data_by_event_number(scifi_clusters, pixel_tracks, scifi_tracks = scifi_tracks)

    # align pixel detector to scifi planes
    if alignment_file:
        scifi_alignment = load_config(alignment_file)
        for plane in scifi_alignment.keys():
            merged_tracks["%s_sc" % dimension[plane]][merged_tracks["plane"] == plane] += scifi_alignment[plane]["offset"]
            if rotate_scifi:
            #     merged_tracks["%s_sc" % dimension[plane]][merged_tracks["plane"] == plane] += scifi_alignment[plane]["offset"]
                # use y coordinate predicted by pixel track to rotate scifi x and vice versa
                if dimension[plane] == "x":
                    x_r, y_r = rotate_around_z(merged_tracks["%s_sc" % dimension[plane]][merged_tracks["plane"] == plane],
                                               merged_tracks["y"][merged_tracks["plane"] == plane], scifi_alignment[plane]["gamma"])
                    merged_tracks["%s_sc" % dimension[plane]][merged_tracks["plane"] == plane] = x_r
                else:
                    x_r, y_r = rotate_around_z(merged_tracks["x"][merged_tracks["plane"] == plane], 
                                                merged_tracks["%s_sc" % dimension[plane]][merged_tracks["plane"] == plane], scifi_alignment[plane]["gamma"])
                    merged_tracks["%s_sc" % dimension[plane]][merged_tracks["plane"] == plane] = y_r 

    if not scifi_tracks:
        get_track_distance_steps(merged_tracks=merged_tracks, B=magnetic_field, momentum=momentum)
        # get_track_distance(merged_tracks=merged_tracks, B=magnetic_field, momentum=400.)
        find_best_pos_match(merged_tracks, tot_limit=max_scifi_cluster_tot)
        if max_scifi_cluster_tot is not None:
            merged_tracks = merged_tracks[merged_tracks["scifi_tot"] < max_scifi_cluster_tot]

    save_table(array_in=merged_tracks, path=merged_tracks_out_file_path, append=False, node_name="MergedTracks")

    return merged_tracks_out_file_path


def algin_scifi_pixel(matched_tracks_in_path, scifi_alignment_path, n_tries = 25):

    def _apply_alignment(tracks, scifi_alignment):
        for plane in scifi_alignment.keys():
            if plane in [1,2,7,8]:
                tracks["x%s" % plane] += scifi_alignment[plane]["offset"]
                x_r, y_r = rotate_around_z(tracks["x%s" % plane],
                                            tracks["y"] + tracks["y_slope"] * (tracks["z_deflect"] + tracks["z%sx" % plane]) , scifi_alignment[plane]["gamma"])
                tracks["x%s" % plane] = x_r
            else:
                tracks["y%s" % plane] += scifi_alignment[plane]["offset"]
                x_r, y_r = rotate_around_z(tracks["x"] + tracks["x_slope"] * (tracks["z_deflect"] + tracks["z%sy" % plane]), 
                                            tracks["y%s" % plane], scifi_alignment[plane]["gamma"])
                tracks["y%s" % plane]= y_r 

    tracks = load_table(in_file = matched_tracks_in_path, table_name = "MergedTracks")
    scifi_alignment = load_config(scifi_alignment_path)

    out_path = "/media/niko/big_data/charm_testbeam_july18/scifi_offline/run_%i/tracking/combined_tracks_2d/spill_%i_tracks.h5" %(run, spill)
    combined_tracks = fit_combined_tracks(tracks, out_path, max_chi2 = None, max_delta_x = 100., n_planes_required = [4,3])
    _apply_alignment(combined_tracks, scifi_alignment)

    for _ in range(n_tries):
        pass


def plot_matched_tracks(tracks_in_file_path, scifi_planes=[1, 2, 3, 4, 5, 7, 8], out_pdf_path=None, pngs = True, n_bins=100, hist_range=[-5, 5], n_residual_bins=1000, residual_range=[-5000, 5000]):
    '''
    Plot results of track matching of multiple spills. Creates a pdf file with all plots and single png files for every plot
    ----------------------
    INPUT:
        tracks_in_file_path : string
            path to the input file of matched tracks (merged output of match_scifi_tracks)
        scifi_planes : list of int
            planes which should be plotted
        n_bins : int
            number of bins for most of the plots
        hist_range : list of 2 floats
            minimum and maximum value to consider for plots
        n_residual_bins : int
            number of bins for residual histogram
        residual_range : list of 2 floats
            plot range for residual plots

    '''

    fit_res = []

    if out_pdf_path is None:
        out_pdf_path = tracks_in_file_path.split(".")[0] + "_pixel_tracks.pdf"
    else:
        if not os.path.exists(os.path.dirname(out_pdf_path)):
            os.makedirs(os.path.dirname(out_pdf_path))

    # scifi planes were numbered 1-7 where plane 6 did not send data. Use dict to store alignment values and dimension (x,y) of actual plane
    angular_planes = {1: "x", 2: "u", 3: "v", 4: "y", 5: "y", 7: "u", 8: "x"}  # ["","x", "u", "v","y", "y", "", "u", "x"]
    dimension = {1: "x", 2: "x", 3: "y", 4: "y", 5: "y", 7: "x", 8: "x"}
    opposite_dimension = {1: "y", 2: "y", 3: "x", 4: "x", 5: "x", 7: "y", 8: "y"}

    # load scifi clusters and pixel tracks
    merged_tracks = load_table(tracks_in_file_path, spill=None, table_name="MergedTracks")
    with PdfPages(out_pdf_path) as output_pdf:
        # plot track distribution before projection. Choose plane == 1 to remove doublets from matching process
        x_zero_hist, x_zero_bins = np.histogram(np.clip(merged_tracks["x"][merged_tracks["plane"] == 1]*10, -6, 6), bins=n_bins)
        plot_1d_bar(hist_in=x_zero_hist, bar_edges=x_zero_bins, output_pdf=output_pdf, xlabel="x [mm]", title="pixel tracks at z0 - %i entries" %
                    (x_zero_hist.sum()), gauss_fit=True, gauss_unit="mm", linewidth=0., output_png=out_pdf_path[:-4] + "_x_tracks_z0.png")
        y_zero_hist, y_zero_bins = np.histogram(np.clip(merged_tracks["y"][merged_tracks["plane"] == 1]*10, -15, 15), bins=n_bins)
        plot_1d_bar(hist_in=y_zero_hist, bar_edges=y_zero_bins, output_pdf=output_pdf, xlabel="y [mm]", title="pixel tracks at z0 - %i entries" %
                    y_zero_hist.sum(), gauss_fit=True, gauss_unit="mm", linewidth=0., output_png=out_pdf_path[:-4] + "_y_tracks_z0.png")
        x_end_magnet_hist, x_end_magnet_bins = np.histogram(np.clip(merged_tracks["x_new"][merged_tracks["plane"] == 1]*10, -8, 16), bins=n_bins)
        # x_end_magnet_hist, x_end_magnet_bins = np.histogram(merged_tracks["x_new"][merged_tracks["plane"] == 1], bins=n_bins)
        plot_1d_bar(hist_in=x_end_magnet_hist, bar_edges=x_end_magnet_bins, output_pdf=output_pdf, xlabel="x [mm]", title="pixel tracks after magnet - %i entries" %
                    (x_end_magnet_hist.sum()), gauss_fit=True, gauss_unit="mm", linewidth=0., output_png=out_pdf_path[:-4] + "_x_tracks_magnet.png")
        for plane in scifi_planes:
            logging.info("plotting plane %s" % plane)
            selected_tracks = merged_tracks[np.logical_and(merged_tracks["plane"] == plane, merged_tracks["best_match"] == 1)]
            # selected_tracks = selected_tracks[np.logical_and(selected_tracks["x_slope"]<0.0003, selected_tracks["x_slope"]>0.)]
            # delta_x_hist, x_bins = np.histogram(np.clip(selected_tracks["delta_%s" % dimension[plane]]*1e4, residual_range[0], residual_range[1]), bins=n_residual_bins)
            delta_x_hist, x_bins = np.histogram(selected_tracks["delta_%s" % dimension[plane]]*1e4, bins=n_residual_bins, range=residual_range)

            fit, error, chi2 = plot_1d_bar(hist_in=delta_x_hist, bar_edges=x_bins, output_pdf=output_pdf, title="plane %s pixel track to SciFi hit distance - %i entries" % ((angular_planes[plane] + str(plane)), delta_x_hist.sum()),
                                     xlabel="%s distance [$\mu$m]" % dimension[plane], output_png=out_pdf_path[:-4] + "_plane_%s_delta_%s.png" % (plane, dimension[plane]), gauss_fit=True, gauss_unit="$\mu$m", linewidth=0.)
            fit_res.append([fit, error])
            x_project_hist, x_project_bins = np.histogram(selected_tracks["%s_project" % dimension[plane]]*10, bins=n_bins, range=[-5, 20])
            plot_1d_bar(hist_in=x_project_hist, bar_edges=x_project_bins, output_pdf=output_pdf, xlabel="%s [mm]" % dimension[plane], title="plane %s pixel projection - %i entries" %
                        ((angular_planes[plane] + str(plane)), x_project_hist.sum()), output_png=out_pdf_path[:-4] + "_plane_%s_%s_project.png" % (plane, dimension[plane]), gauss_fit=True, gauss_unit="mm", linewidth=0.)

            x_lin_project_hist, x_lin_project_bins = np.histogram(selected_tracks["offset_after"]*10, bins=250) #, range=[-5, 20])
            plot_1d_bar(hist_in=x_lin_project_hist, bar_edges=x_lin_project_bins, output_pdf=output_pdf, xlabel="%s offset [mm]" % dimension[plane], title="plane %s pixel projection contribution after field - %i entries" %
                        ((angular_planes[plane] + str(plane)), x_lin_project_hist.sum()), output_png=out_pdf_path[:-4] + "_plane_%s_%s_project_after_field.png" % (plane, dimension[plane]), gauss_fit=False, gauss_unit="mm", linewidth=0.)

            x_scifi_cluster, x_scifi_bins = np.histogram(np.clip(selected_tracks["%s_sc" % dimension[plane]]*10, -30, 30), bins=n_bins)
            plot_1d_bar(hist_in=x_scifi_cluster, bar_edges=x_scifi_bins, output_pdf=output_pdf, xlabel="%s [mm]" % dimension[plane], title="plane %s scifi cluster - %i entries" %
                        ((angular_planes[plane] + str(plane)), x_scifi_cluster.sum()), output_png=out_pdf_path[:-4] + "_plane_%s_%s_scifi.png" % (plane, dimension[plane]), gauss_fit=True, gauss_unit="mm")

            x_test = selected_tracks["%s" % dimension[plane]] + 227.23 * selected_tracks["%s_slope" %
                                                                                         dimension[plane]]  # projection in center of magnetic field

            x_vs_dx, dx_bins, dy_bins = np.histogram2d(x_test*10, selected_tracks["delta_%s" % dimension[plane]]*1e4,
                                                       bins=[n_bins, n_residual_bins], range=[[-20, 20], residual_range],)
            plot_2d_hist(hist2d=x_vs_dx, bins=[dx_bins, dy_bins], output_pdf=output_pdf, title="plane %s residuum vs track projection in magnetic field - %i entries" % ((angular_planes[plane] + str(plane)), x_vs_dx.sum()),
                         min_hits=1, ylabel="$\Delta$ %s [$\mu$m]" % dimension[plane], xlabel="%s [mm]" % dimension[plane],
                         output_png=out_pdf_path[:-4] + "_plane_%s_delta_%s_vs_%s.png" % (plane, dimension[plane], dimension[plane]))

            y_test = selected_tracks["%s" % opposite_dimension[plane]] + 227.23 * selected_tracks["%s_slope" %
                                                                                                  opposite_dimension[plane]]  # projection in center of magnetic field
            y_vs_dx, dy_x_bins, dy_y_bins = np.histogram2d(y_test*10, selected_tracks["delta_%s" % dimension[plane]]*1e4,
                                                           bins=[n_bins, n_residual_bins], range=[[-20, 20], residual_range],)
            plot_2d_hist(hist2d=y_vs_dx, bins=[dy_x_bins, dy_y_bins], output_pdf=output_pdf, title="plane %s residuum vs track projection in magnetic field - %i entries" % ((angular_planes[plane] + str(plane)), x_vs_dx.sum()),
                         min_hits=1, ylabel="$\Delta$ %s [$\mu$m]" % dimension[plane], xlabel="%s [mm]" % opposite_dimension[plane],
                         output_png=out_pdf_path[:-4] + "_plane_%s_delta_%s_vs_%s.png" % (plane, dimension[plane], opposite_dimension[plane]))

            plot_residuals_vs_position(hist2d=x_vs_dx, x=selected_tracks["delta_%s" % dimension[plane]],
                                       y=selected_tracks["%s" % dimension[plane]], output_pdf=output_pdf, bins=[
                                           dx_bins, dy_bins], ylimits=residual_range, xlimits=[-10, 10],
                                       counts=x_scifi_cluster, ylabel="$\Delta$ %s [$\mu$m]" % dimension[plane], xlabel="%s [mm]" % dimension[plane],
                                       title="plane %s residuum vs track projection - %i entries" % ((angular_planes[plane] + str(plane)), x_vs_dx.sum()),
                                       output_png=out_pdf_path[:-4] + "_plane_%s_residuals_vs_pos.png" % plane, zlog=False, min_hits=1, limits=None, invert_x_axis=False,
                                       linfit=True, legend=True, markersize=1)
            tx_vs_dx, tx_x_bins, tx_y_bins = np.histogram2d(np.arctan(selected_tracks["%s_slope" % dimension[plane]])*1e3, selected_tracks["delta_%s" % dimension[plane]]*1e4,
                                                            bins=[n_bins, n_residual_bins], range=[[-2, 2], residual_range],)

            plot_2d_hist(hist2d=tx_vs_dx, bins=[tx_x_bins, tx_y_bins], output_pdf=output_pdf, title="plane %s residuum vs track angle - %i entries" % ((angular_planes[plane] + str(plane)), tx_vs_dx.sum()),
                         min_hits=1, ylabel="$\Delta$ %s [$\mu$m]" % dimension[plane], xlabel="%s_slope [mrad]" % dimension[plane],
                         output_png=out_pdf_path[:-4] + "_plane_%s_delta_%s_vs_t%s.png" % (plane, dimension[plane], dimension[plane]))

            ty_vs_dx, ty_x_bins, ty_y_bins = np.histogram2d(np.arctan(selected_tracks["%s_slope" % opposite_dimension[plane]])*1e3, selected_tracks["delta_%s" % dimension[plane]]*1e4,
                                                            bins=[n_bins, n_residual_bins], range=[[-2, 2], residual_range],)
            plot_2d_hist(hist2d=ty_vs_dx, bins=[ty_x_bins, ty_y_bins], output_pdf=output_pdf, title="plane %s residuum vs track angle - %i entries" % ((angular_planes[plane] + str(plane)), ty_vs_dx.sum()),
                         min_hits=1, ylabel="$\Delta$ %s [$\mu$m]" % dimension[plane], xlabel="%s_slope [mrad]" % opposite_dimension[plane],
                         output_png=out_pdf_path[:-4] + "_plane_%s_delta_%s_vs_t%s.png" % (plane, dimension[plane], opposite_dimension[plane]))

            tx_hist, tx_bins = np.histogram(np.arctan(selected_tracks["%s_slope" % dimension[plane]])*1e3, bins=n_bins, range=[-1, 1])
            plot_1d_bar(hist_in=tx_hist, bar_edges=tx_bins, output_pdf=output_pdf, xlabel="%s slope [mrad]" % dimension[plane], title="plane %s %s slope - %i entries" %
                        ((angular_planes[plane] + str(plane)), dimension[plane], tx_hist.sum()), output_png=out_pdf_path[:-4] + "_plane_%s_%s_slope.png" % (plane, dimension[plane]), gauss_fit=True, gauss_unit="mrad")

            ty_hist, ty_bins = np.histogram(np.arctan(selected_tracks["%s_slope" % opposite_dimension[plane]])*1e3, bins=n_bins, range=[-1, 1])
            plot_1d_bar(hist_in=ty_hist, bar_edges=ty_bins, output_pdf=output_pdf, xlabel="%s slope [mrad]" % opposite_dimension[plane], title="plane %s %s slope - %i entries" %
                        ((angular_planes[plane] + str(plane)), opposite_dimension[plane], ty_hist.sum()), output_png=out_pdf_path[:-4] + "_plane_%s_%s_slope.png" % (plane, opposite_dimension[plane]), gauss_fit=True, gauss_unit="mrad")

            if dimension[plane] == "x":

                tx_new_hist, tx_new_bins = np.histogram(np.arctan(selected_tracks["%s_slope_new" % dimension[plane]])*1e3, bins=n_bins) #, range=[-2, 7.5])
                plot_1d_bar(hist_in=tx_new_hist, bar_edges=tx_new_bins, output_pdf=output_pdf, xlabel="%s slope [mrad]" % dimension[plane], title="plane %s %s slope new - %i entries" %
                            ((angular_planes[plane] + str(plane)), dimension[plane], tx_new_hist.sum()), output_png=out_pdf_path[:-4] + "_plane_%s_%s_slope_new.png" % (plane, dimension[plane]), gauss_fit=False, gauss_unit="mrad")

                delta_tx_hist, delta_tx_bins = np.histogram(np.arctan(selected_tracks["delta_slope"])*1e3, bins=200, range=[-10, 10])
                plot_1d_bar(hist_in=delta_tx_hist, bar_edges=delta_tx_bins, output_pdf=output_pdf, xlabel="%s slope [mrad]" % dimension[plane], title="plane %s %s slope delta - %i entries" %
                            ((angular_planes[plane] + str(plane)), dimension[plane], delta_tx_hist.sum()), output_png=out_pdf_path[:-4] + "_plane_%s_%s_slope_delta.png" % (plane, dimension[plane]), gauss_fit=False, gauss_unit="mrad")

            # tx_vs_x, tx_x_xbins, tx_x_ybins = np.histogram2d(
            #     np.arctan(selected_tracks["%s_slope" % dimension[plane]])*1e3, x_test*10, bins=[n_residual_bins, n_bins], range=[[-2, 2], [-20, 20]])
            # plot_2d_hist(hist2d=tx_vs_x, bins=[tx_x_xbins, tx_x_ybins], output_pdf=output_pdf, title="track projection vs slope at center of magnetic field - %i entries" % tx_vs_x.sum(),
            #              min_hits=1, xlabel="t%s [mRad]" % dimension[plane], ylabel="%s [mm]" % dimension[plane],
            #              output_png=out_pdf_path[:-4] + "_%s_vs_t%s_magnet.png" % (dimension[plane], dimension[plane]))

    logging.info("saved pdf at %s" % out_pdf_path)
    return fit_res


if __name__ == '__main__':

    '''
    First load pixel merged cluster table for all spills in one run.
    Then load scifi spill file and match spill.
    After timestamps are matched, get all hits of matched events of both detctors and save them in separate files.
    Then merge data in single file with one row for every track - hit combination
    This is used to calculate track - hit distances and choose the best match for every track
    '''
    B = -1.5
    # B = 0
    run = 2815
    # run = 2827
    # spills = {0: "522521060", 1: 522521130, 2: 522521205, 3: 522521275, 4: 522521350}
    # spills = {0: "1F2509E4"} #, 1: "1F250A2A", 2: "1F250A75", 3: "1F250ABB", 4: "1F250B06"}
    scifi_cluster_files = get_files(folder="/Users/niko/Desktop/charm_exp_2018/scifi_matching/run_%s/scifi_input_data" % run, string="clusters.h5")
    offset = 0
    matched_tracks_files = []
    # cluster_table = load_table(
    #     "/Users/Niko/Desktop/charm_exp_2018/scifi_matching/run_%s/Merged_spills.h5" % run, table_name="MergedClusters")
    for spill, cluster_file in enumerate(scifi_cluster_files):
        # cycle_ID = cluster_file[cluster_file.find("_clusters.h5")-9:cluster_file.find("_clusters.h5")]
        # match_scifi_ts(scifi_in_path = cluster_file,
        #                # match_scifi_ts(scifi_in_path="/media/niko/big_data/charm_testbeam_july18/scifi_offline/run_%s/%s_spill_%s_hits.h5" % (run, run, hex_spill),
        #                cluster_table_in = cluster_table[cluster_table["spill"] == spill + offset],
        #                spill = spill + offset,
        #                cycleID = int(cycle_ID),
        #                #    cycleID= int(hex_spill, 16),  # ,522710490, #
        #                limits = [600], plot = True
        #                )
        # scifi_hits=load_table(in_file = cluster_file, table_name = "Clusters")

        # matched_events=load_table(in_file = "/Users/Niko/Desktop/charm_exp_2018/scifi_matching/run_%s/matching/matched_spill_%s_raw.h5" % (run, spill), table_name = "MatchedEvents")
        # matched_hits=get_matched_events_scifi(scfi_hits_in = scifi_hits, matched_events = matched_events)
        # save_table(matched_hits, path = "/Users/Niko/Desktop/charm_exp_2018/scifi_matching/run_%s/matching/scifi_clusters_spill_%s_matched.h5" % (run, spill), node_name = "MatchedClusters")

        # tracks_output_file = match_scifi_tracks(scifi_in_path="/Users/niko/Desktop/charm_exp_2018/scifi_matching/run_%s/matching/scifi_clusters_spill_%s_matched.h5" % (run, spill),
        #                                         pixel_tracks_in="/Users/niko/Desktop/charm_exp_2018/scifi_matching/run_%s/pix_tracks.h5" % run,
        #                                         pix_spill=spill,
        #                                         matched_events_in="/Users/niko/Desktop/charm_exp_2018/scifi_matching/run_%s/matching/matched_spill_%s_raw.h5" % (
        #                                             run, spill),
        #                                         fairhsip_coordinate_system=True,
        #                                         max_scifi_cluster_tot=254,
        #                                         algin_scifi=True,
        #                                         rotate_scifi = True,
        #                                         magnetic_field=B
        #                                         )
        # plot_matched_tracks(tracks_in_file_path=tracks_output_file, )
        # matched_tracks_files.append(tracks_output_file)
        matched_tracks_files.append(
            "/Users/Niko/Desktop/charm_exp_2018/scifi_matching/run_%s/matching/scifi_clusters_spill_%s_matched_pixel_tracks_aligned.h5" % (run, spill))

    merged_out_path = "/Users/Niko/Desktop/charm_exp_2018/scifi_matching/run_%s/matching/matched_tracks_merged.h5" % run
    # merge_spills(in_files=matched_tracks_files, out_path=merged_out_path, out_node_name="MergedTracks")

    plot_matched_tracks(tracks_in_file_path=merged_out_path, scifi_planes=[1], out_pdf_path=None,
                        n_bins=100, hist_range=[-5, 5], n_residual_bins=200, residual_range=[-8000, 8000])
